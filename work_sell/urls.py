from django.conf import urls

from .views import (
    WorkSellDetail,
    WorkSellUpdateView,
    WorkSellDeleteView,
    UploadView,
    # work_sell_create,
    WorkSellCreateView,
    BasicCreateView,
    PictureCreateView,
    ContractorWorkSellTrashView,
    WorkSellFilterView,

)

app_name = 'work_sell'

urlpatterns = [
    urls.url(r'^$', WorkSellFilterView.as_view(), name='filter'),
    urls.url(r'^create/$', WorkSellCreateView.as_view(), name='create'),
    urls.url(r'^upload/$', UploadView.as_view(), name='upload'),
    urls.url(r'^(?P<pk>\d+)/edit/$', WorkSellUpdateView.as_view(), name='edit'),
    urls.url(r'^(?P<pk>\d+)/delete/$', WorkSellDeleteView.as_view(), name='delete'),
    urls.url(r'^(?P<pk>\d+)/trash/$', ContractorWorkSellTrashView.as_view(), name='contractor-worksell-trash'),
    # urls.url(r'^create/$', work_sell_create, name='create'),
    urls.url(r'^basic/$', BasicCreateView.as_view(), name='upload-basic'),
    urls.url(r'^new/$', PictureCreateView.as_view(), name='upload-new'),
    urls.url(r'^(?P<pk>\d+)/$', WorkSellDetail.as_view(), name='detail'),
]
