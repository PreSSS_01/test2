from rest_framework.serializers import ModelSerializer, ImageField, PrimaryKeyRelatedField, ReadOnlyField

from common.serializers import LocationSerializer
from projects.serializers import BuildingClassficationSerializer, ConstructionTypeSerializer
from specializations.serializers import SpecializationSerializer
from users.serializers import UserSerializer, TeamSerializer
from .models import WorkSell, WorkSellPhoto
from .fields import SerializerThumbnailField


class WorkSellPhotoSerializer(ModelSerializer):
    img = ImageField()
    thumb = SerializerThumbnailField(geometry='255x255', source='img')
    worksell_id = PrimaryKeyRelatedField(read_only=True, source='worksell')

    class Meta:
        model = WorkSellPhoto

        fields = (
            'id',
            'img',
            'thumb',
            'worksell_id',
        )


class WorkSellSerializer(ModelSerializer):
    building_classification = BuildingClassficationSerializer()
    construction_type = ConstructionTypeSerializer()
    contractor = UserSerializer()
    id = ReadOnlyField()
    location = LocationSerializer()
    photos = WorkSellPhotoSerializer(many=True)
    specialization = SpecializationSerializer()
    team = TeamSerializer()

    class Meta:
        model = WorkSell

        fields = (
            'budget',
            'building_classification',
            'construction_type',
            'contractor',
            'created',
            'currency',
            'description',
            'id',
            'location',
            'name',
            'photos',
            'specialization',
            'team',
            'term',
            'term_type',
        )
