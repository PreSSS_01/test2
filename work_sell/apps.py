from django.apps import AppConfig


class WorksellConfig(AppConfig):
    name = 'worksell'
