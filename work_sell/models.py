from django.db import models
from mptt.models import TreeForeignKey, TreeManyToManyField
from sorl.thumbnail import ImageField

from projects.models import BuildingClassfication, ConstructionType, TERM_TYPES, CURRENCIES, Project
from specializations.models import Specialization
from users.models import User, Team


class WorkSell(models.Model):
    # TODO: Add consistency ("work_sell" vs "worksell"):

    budget = models.DecimalField(max_digits=10, decimal_places=0, default=0, null=True, blank=True)
    building_classification = TreeForeignKey(BuildingClassfication, related_name='work_sells', null=True, blank=True)
    construction_type = models.ForeignKey(ConstructionType, related_name='work_sells', null=True, blank=True)
    contractor = models.ForeignKey(User, related_name='work_sell', null=True,
                                   blank=True)  # TODO: Pluralize related name
    created = models.DateTimeField(auto_now_add=True)
    currency = models.CharField(max_length=20, default='rur', choices=CURRENCIES, null=True, blank=True)
    description = models.TextField(blank=True)
    el_format = models.ManyToManyField('ElFormat', related_name='work_sell', blank=True)
    location = TreeForeignKey('common.Location', related_name='work_sells', null=True, blank=True)
    name = models.CharField(max_length=255)
    # deprecated
    specialization = TreeForeignKey(Specialization, related_name='work_sells', null=True, blank=True)
    specializations = TreeManyToManyField(Specialization, blank=True)
    team = models.ForeignKey(Team, related_name='work_sells', null=True, blank=True)
    term = models.IntegerField(default=0, null=True, blank=True)
    term_type = models.CharField(max_length=20, choices=TERM_TYPES, default='hour', null=True, blank=True)
    work_type = models.IntegerField(default=1, choices=Project.WORK_TYPES, blank=True)

    def __str__(self):
        return self.name

    def is_author_for_work(self):
        pass

    def get_prev(self):
        try:
            return self.get_previous_by_created(contractor=self.contractor)
        except self.DoesNotExist:
            return None

    def get_next(self):
        try:
            return self.get_next_by_created(contractor=self.contractor)
        except self.DoesNotExist:
            return None

    def get_cover(self):
        photo = self.photos.first()
        return photo and photo.img

    class Meta:
        ordering = ['-created']
        verbose_name = 'Готовая работа'
        verbose_name_plural = 'Готовые работы'


class WorkSellPhoto(models.Model):
    img = ImageField(upload_to='worksell/worksell/')
    worksell = models.ForeignKey(WorkSell, related_name='photos')
    description = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Изображение Готовая работа'
        verbose_name_plural = 'Изображения Готовые работы'

    def __str__(self):
        return self.img and self.img.url or str(self.img)


class Picture(models.Model):
    file = models.ImageField(upload_to='worksell/pictures/')
    slug = models.SlugField(max_length=50, blank=True)

    def __str__(self):
        return self.file.name

    def save(self, *args, **kwargs):
        self.slug = self.file.name
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.file.delete(False)
        super().delete(*args, **kwargs)


class ElFormat(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Формат электронной версии'
