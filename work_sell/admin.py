from django.contrib import admin

from .models import WorkSell, WorkSellPhoto, Picture, ElFormat

admin.site.register(WorkSell)
admin.site.register(ElFormat)
admin.site.register(Picture)
admin.site.register(WorkSellPhoto)
