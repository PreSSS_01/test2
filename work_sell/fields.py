from rest_framework.serializers import ImageField
from sorl.thumbnail import get_thumbnail


class SerializerThumbnailField(ImageField):
    geometry = None
    options = None

    def __init__(self, geometry='100x100', options=None,  *args, **kwargs):
        # options = dict()
        self.geometry = geometry
        self.options = options or {'crop': 'center'}
        super(SerializerThumbnailField, self).__init__(*args, **kwargs)

    def to_representation(self, image):
        try:
            thumbnail = get_thumbnail(image, self.geometry, **self.options)
            return thumbnail.url
        except:
            return image.url
