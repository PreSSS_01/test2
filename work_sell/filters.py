import django_filters
from rest_framework_filters import FilterSet, RelatedFilter, AllLookupsFilter

from work_sell.models import WorkSell, WorkSellPhoto


class WorkSellFilter(django_filters.FilterSet):
    budget = django_filters.NumberFilter()
    budget__gt = django_filters.NumberFilter(name='budget', lookup_expr='gt')
    budget__lt = django_filters.NumberFilter(name='budget', lookup_expr='lt')

    class Meta:
        model = WorkSell


class WorkSellPhotoFilterSet(FilterSet):
    id = AllLookupsFilter()
    # img = ???

    work_sell = RelatedFilter('work_sell.filters.WorkSellFilterSet')

    class Meta:
        model = WorkSellPhoto


class WorkSellFilterSet(FilterSet):
    budget = AllLookupsFilter()
    created = AllLookupsFilter()
    currency = AllLookupsFilter()
    description = AllLookupsFilter()
    id = AllLookupsFilter()
    name = AllLookupsFilter()
    term = AllLookupsFilter()
    term_type = AllLookupsFilter()

    building_classification = RelatedFilter('projects.filters.BuildingClassficationFilterSet')
    construction_type = RelatedFilter('projects.filters.ConstructionTypeFilterSet')
    contractor = RelatedFilter('users.filters.UserFilterSet')
    location = RelatedFilter('common.filters.LocationFilterSet')
    photos = RelatedFilter('work_sell.filters.WorkSellPhotoFilterSet')
    specialization = RelatedFilter('specializations.filters.SpecializationFilterSet')
    team = RelatedFilter('users.filters.TeamFilterSet')

    class Meta:
        model = WorkSell
