from django import forms
from mptt.forms import TreeNodeChoiceField

from common.models import LiveImageUpload
from projects.models import BuildingClassfication
from specializations.models import Specialization
from .models import WorkSell, WorkSellPhoto


class ContractorWorkSellTrashForm(forms.Form):
    pk = forms.ModelChoiceField(queryset=WorkSell.objects.none())

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

        self.fields['pk'].queryset = self.request.user.work_sell.all()


class WorkSellForm(forms.ModelForm):

    class Meta:
        model = WorkSell

        fields = (
            'building_classification',
            'construction_type',
            'specialization',
            'specializations',
            'contractor',
            'location',
            'budget',
            'currency',
            'description',
            'el_format',
            'name',
            'term',
            'term_type',
            # 'photos'
        )

    # def clean(self):
    #     print("CLEAN", self.cleaned_data)
    #     data = self.cleaned_data.get('specializations')
    #     print("before data = ", data)
    #     # print('spec = ', data.split(','))
    #     # self.cleaned_data['specialization'] = data.split(',')
    #     super().clean()

    # def clean_specializations(self):
    #     data = self.cleaned_data['specializations']
    #     print("before data = ", data)
    #     # data = data.split(',')
    #     print("after data = ", data)
    #     return data

    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop('request')
        except KeyError:
            pass
        super().__init__(*args, **kwargs)
        self.fields['specializations'].required = True
        self.fields['description'].required = True
        # self.fields['photos'].queryset = self.instance.photos.all()


class WorkSellFilterForm(forms.ModelForm):
    keywords = forms.CharField(required=False, max_length=255)

    class Meta:
        model = WorkSell
        fields = (
            'location',
            'building_classification',
            'construction_type',
            'specialization',
        )

        widgets = {
            'building_classification': forms.Select(attrs={'class': 'selectpicker'}),
            'construction_type': forms.Select(attrs={'class': 'selectpicker'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

        self.fields['specialization'].required = False

        self.fields['specialization'].queryset = Specialization.objects.root_nodes()[0].get_descendants()
