from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from .models import Review


class ReviewSerializer(ModelSerializer):
    target_user = serializers.SerializerMethodField(read_only=True)
    order = serializers.SerializerMethodField(read_only=True)
    count_reviews = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Review

        fields = (
            'id',
            'text',
            'type',
            'created',
            'count_reviews',
            'project',
            'from_customer',
            'from_contractor',
            'from_team',
            'target_customer',
            'target_contractor',
            'target_team',
            'target_user',
            'order',
        )

    def get_order(self, obj):
        return obj.project.order.pk

    def get_count_reviews(self, obj):
        return Review.objects.filter(project=obj.project).count()

    def get_target_user(self, obj):
        if obj.target_customer:
            return obj.target_customer.pk
        elif obj.target_contractor:
            return obj.target_contractor.pk
        elif obj.target_team:
            return obj.target_team.owner.pk
