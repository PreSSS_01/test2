from django import forms

from .models import Review


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review

        fields = (
            'type',
            'text',
            'project',
            'from_customer',
            'target_contractor',
            'target_team',
            'from_contractor',
            'target_customer',
            'from_team',
        )
