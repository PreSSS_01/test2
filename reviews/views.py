from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, DeleteView

from .forms import ReviewForm
from .models import Review


class ReviewsView(LoginRequiredMixin, ListView):
    login_url = '/users/login/'
    model = Review
    template_name = 'reviews_list.html'


class ReviewCreateView(CreateView):
    model = Review
    form_class = ReviewForm
    template_name = 'review_create.html'


class ReviewDeleteView(DeleteView):
    model = Review
