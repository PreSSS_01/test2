from django.conf import urls

from .views import ReviewsView, ReviewCreateView

app_name = 'reviews'

urlpatterns = [
    urls.url(r'^$', ReviewsView.as_view(), name='list'),
    urls.url(r'^create/$', ReviewCreateView.as_view(), name='create'),

]
