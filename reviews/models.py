from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone

TYPE_REVIEWS = (
    ('positive', 'Положительный'),
    ('negative', 'Отрицательный'),
    ('neutral', 'Нейтральный'),
)


class Review(models.Model):
    project = models.ForeignKey('projects.Project', related_name='reviews')
    type = models.CharField(max_length=30, choices=TYPE_REVIEWS, default='neutral')
    text = models.TextField(blank=True)
    created = models.DateTimeField(default=timezone.now)
    target_customer = models.ForeignKey('users.User', related_name='reviews_by_customer', null=True, blank=True)
    target_contractor = models.ForeignKey('users.User', related_name='reviews_by_contractor', null=True, blank=True)
    target_team = models.ForeignKey('users.Team', related_name='reviews_by_team', null=True, blank=True)
    from_customer = models.ForeignKey('users.User', related_name='customer_reviews', null=True, blank=True)
    from_contractor = models.ForeignKey('users.User', related_name='contractor_reviews', null=True, blank=True)
    from_team = models.ForeignKey('users.Team', related_name='team_reviews', null=True, blank=True)

    def __str__(self):
        return str(self.pk)

    def get_absolute_url(self):
        return reverse('reviews:detail', kwargs={'pk': self.pk})

    def get_sender(self):
        if not self.from_team is None:
            return self.from_team.name
        elif not self.from_customer is None:
            return self.from_customer.username
        else:
            return self.from_contractor.username

    def get_recipient(self):
        if not self.target_team is None:
            return self.target_team.name
        elif not self.target_customer is None:
            return self.target_customer.username
        else:
            return self.target_contractor.username

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

        unique_together = (
            ('from_customer', 'target_contractor', 'project'),
            ('from_customer', 'target_team', 'project'),
            ('from_contractor', 'target_customer', 'project'),
            ('from_team', 'target_customer', 'project'),
        )
