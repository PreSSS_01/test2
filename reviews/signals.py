from django.db.models.signals import post_save
from django.dispatch import receiver

from ratings.models import HistoryRating
from .models import Review


@receiver(post_save, sender=Review)
def add_rating_review(sender, instance, created, **kwargs):
    hs_rating = HistoryRating()
    print("hs_rating = ", hs_rating)
    if instance.target_team:
        hs_rating.team = instance.target_team
    elif instance.target_contractor or instance.target_customer:
        hs_rating.user = instance.target_contractor or instance.target_customer

    if instance.type == 'positive':
        hs_rating.rating = 1
    elif instance.type == 'negative':
        hs_rating.rating = -1
    else:
        hs_rating = 0
    hs_rating.description = 'Изменения рейтинга после отзыва'
    hs_rating.save()

    count_reviews = Review.objects.filter(project=instance.project).count()
    print("!!!count_reviews = ", count_reviews)
    if count_reviews == 2:
        order = instance.project.order
        order.status = 'completed'
        # order.project.state = 'deleted'
        project = order.project
        project.state = 'deleted'
        project.save()
        order.save()
