from rest_framework_filters import FilterSet, RelatedFilter, AllLookupsFilter

from .models import Review


class ReviewFilterSet(FilterSet):
    created = AllLookupsFilter()
    id = AllLookupsFilter()
    text = AllLookupsFilter()
    type = AllLookupsFilter()

    from_contractor = RelatedFilter('users.filters.UserFilterSet')
    from_customer = RelatedFilter('users.filters.UserFilterSet')
    target_contractor = RelatedFilter('users.filters.UserFilterSet')
    target_customer = RelatedFilter('users.filters.UserFilterSet')

    class Meta:
        model = Review
