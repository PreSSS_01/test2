from django.apps import AppConfig


class ReviewConfig(AppConfig):
    name = 'reviews'

    def ready(self):
        import reviews.signals
