import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from archilance import util
from common.models import Location
from projects.models import TERM_TYPES, CURRENCIES, BuildingClassfication, ConstructionType
from specializations.models import Specialization
from users.models import User, Team
from work_sell.models import WorkSell


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating work sells...')
        print('---------------------------------------')

        def create_work_sell(i):
            ws = WorkSell(
                budget=util.random_amount(),
                created=util.random_date(),
                currency=_.sample(CURRENCIES)[0],
                description=util.lorem(),
                name=util.lorem(words=_.random(2, 20), sentences=1),
                term=_.random(0, 20),
                term_type=_.sample(TERM_TYPES)[0],
            )

            ws.save()

            if _.sample((True, False)):
                ws.contractor = User.contractor_objects.order_by('?').first()
            else:
                ws.team = Team.objects.order_by('?').first()

            ws.building_classification = BuildingClassfication.objects.order_by('?').first()
            ws.construction_type = ConstructionType.objects.order_by('?').first()
            ws.location = Location.objects.root_nodes()[0].get_descendants().order_by('?').first()
            ws.specialization = Specialization.objects.root_nodes()[0].get_descendants().order_by('?').first()

            pic_names = _.split(
                'a.png b.png c.jpg d.png e.jpg f.png g.png h.jpg i.png j.png k.jpg l.png m.png n.png o.png p.png q.jpg',
                ' ')

            _.times(
                lambda i_: ws.photos.create(img='_sample_files/%s' % util.take_one_random(pic_names)),
                _.random(1, 15),
            )

            ws.save()
            return ws

        _.times(create_work_sell, 1000)
