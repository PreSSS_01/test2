from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand
from django.utils import timezone
import pydash as _; _.map = _.map_; _.filter = _.filter_
import random

from archilance import util
from common.models import Location
from projects.models import Project, Order, CURRENCIES, TERM_TYPES, Specialization, Realty
from users.models import User, Team


class Command(BaseCommand):
    def handle(self, *args, **options):
        # for user in User.objects.all():
        #     user.location = Location.objects.root_nodes()[0].get_descendants().order_by('?').first()
        #     user.cro = _.sample((True, False))
        #     
        #     user.save()
        
        #------------------------------------------
        
        for team in Team.objects.all():
            # team.specializations = Specialization.objects.root_nodes()[0].get_descendants().order_by('?')[:_.random(1, 5)]
            team.created = util.random_date()
            
            team.save()
