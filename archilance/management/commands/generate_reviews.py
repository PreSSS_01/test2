import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from archilance import util
from projects.models import Project
from users.models import User
from reviews.models import Review


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating reviews...')
        print('---------------------------------------')

        def create_review(i):
            review = Review()

            review.project = Project.objects.order_by('?').first()
            review.stars = _.random(1, 5)
            review.text = util.lorem()
            review.is_secured = _.sample((True, False))

            review.save()

            if _.sample((True, False)):
                review.from_contractor = User.contractor_objects.order_by('?').first()
                review.target_customer = User.customer_objects.order_by('?').first()
            else:
                review.from_customer = User.customer_objects.order_by('?').first()
                review.target_contractor = User.contractor_objects.order_by('?').first()

            review.save()
            return review

        _.times(create_review, 300)
