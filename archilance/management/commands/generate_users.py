import pydash as _;
from django.contrib.auth.models import Group
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from common.models import Location
from specializations.models import Specialization
from users.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating users...')
        print('---------------------------------------')

        # # Fields:
        # 
        #  ('contractor_specializations', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        #  ('groups', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        #  ('user_permissions', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),

        #  ('answers', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('contractor_financial_info', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('location', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('logentry', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('orders', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('portfolios', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('projects', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('realties', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('recipent_messages', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('recipent_notes', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('registrationprofile', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('reviews', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('sender_messages', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('sender_notes', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('social_auth', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('team', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('Team_users+', 'Relation? True', 'Null? True', '(relation)', 'Hidden? True'),
        #  ('teams', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        #  ('User_contractor_specializations+', 'Relation? True', 'Null? True', '(relation)', 'Hidden? True'),
        #  ('User_groups+', 'Relation? True', 'Null? True', '(relation)', 'Hidden? True'),
        #  ('User_user_permissions+', 'Relation? True', 'Null? True', '(relation)', 'Hidden? True'),
        #  ('work_sell', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),



        #  ('contractor_status', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        #  ('created', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        #  ('data_joined', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        #  ('email', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        #  ('last_time_visit', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        #  ('password', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        #  ('username', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),

        #  ('avatar', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('cro', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('date_of_birth', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        #  ('first_name', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('gender', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('id', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('is_active', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('is_superuser', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('last_login', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        #  ('last_name', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('patronym', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('phone', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        #  ('skype', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        #  ('website', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),



        def create_user(i):
            username = 'user-%s' % i

            return User.objects.create(
                first_name='Василий',
                last_name='Пупкин',
                patronym='Иванович',
                username=username,
                email='%s@example.com' % username,
                is_active=True,
                contractor_status=_.sample(User.STATUSES)[0],
                cro=_.sample((True, False)),
            )

        users = _.times(create_user, 500)

        contractor_group = Group.objects.get(name='Исполнители')
        customer_group = Group.objects.get(name='Заказчики')

        for user in users:
            user.set_password('123')
            user.groups.add(customer_group if user.pk % 2 == 0 else contractor_group)
            user.contractor_specializations = Specialization.objects.root_nodes()[0].get_descendants().order_by('?')[
                                              :_.random(1, 5)]
            user.location = Location.objects.root_nodes()[0].get_descendants().order_by('?').first()

            user.save()
