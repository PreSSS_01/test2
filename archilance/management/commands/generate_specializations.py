import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from specializations.models import Specialization


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating specializations...')
        print('---------------------------------------')

        _root = Specialization.objects.create(name='_root', order=1)

        stages = ('A', 'B', 'C', 'D')

        for s1 in stages:
            x = Specialization.objects.create(name='Специализация %s' % s1, parent=_root, order=1)
            for s2 in stages:
                y = Specialization.objects.create(name='Специализация %s-%s' % (s1, s2), parent=x, order=1)
                for s3 in stages:
                    z = Specialization.objects.create(name='Специализация %s-%s-%s' % (s1, s2, s3), parent=y, order=1)
                    for s4 in stages:
                        Specialization.objects.create(name='Специализация %s-%s-%s-%s' % (s1, s2, s3, s4), parent=z, order=1)
