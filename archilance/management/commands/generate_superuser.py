import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from users.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating superusers...')
        print('---------------------------------------')

        User.objects.create_superuser('admin@example.com', '123456')
