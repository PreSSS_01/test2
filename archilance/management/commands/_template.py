from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand
from django.utils import timezone
import pydash as _; _.map = _.map_; _.filter = _.filter_
import random

from archilance import util
from common.models import Location
from specializations.models import Specialization
from users.models import User, GENDERS, Team


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating something...')
        print('---------------------------------------')
        
        pass
