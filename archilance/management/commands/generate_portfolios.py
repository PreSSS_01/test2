import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from archilance import util
from common.models import Location
from projects.models import Portfolio, CURRENCIES, TERM_TYPES, BuildingClassfication, ConstructionType
from specializations.models import Specialization
from users.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating portfolios...')
        print('---------------------------------------')

        # ('photos', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('budget', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        # ('building_classification', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('construction_type', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('currency', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        # ('description', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('location', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('name', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('specialization', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('team', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('term', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        # ('term_type', 'Relation? False', 'Null? True', 'Blank? True', 'Hidden? False'),
        # ('user', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('worksell', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),


        def create_portfolio(i):
            portf = Portfolio(
                budget=util.random_amount(),
                created=util.random_date(),
                currency=_.sample(CURRENCIES)[0],
                description=util.lorem(),
                name=util.lorem(words=_.random(2, 20), sentences=1),
                term=_.random(0, 20),
                term_type=_.sample(TERM_TYPES)[0],
                worksell=_.sample((True, False)),
            )

            portf.save()

            portf.user = User.contractor_objects.order_by('?').first()

            portf.building_classification = BuildingClassfication.objects.order_by('?').first()
            portf.construction_type = ConstructionType.objects.order_by('?').first()
            portf.location = Location.objects.root_nodes()[0].get_descendants().order_by('?').first()
            portf.specialization = Specialization.objects.root_nodes()[0].get_descendants().order_by('?').first()

            pic_names = _.split(
                'a.png b.png c.jpg d.png e.jpg f.png g.png h.jpg i.png j.png k.jpg l.png m.png n.png o.png p.png q.jpg',
                ' ')

            _.times(
                lambda i_: portf.photos.create(img='_sample_files/%s' % util.take_one_random(pic_names)),
                _.random(1, 15),
            )

            portf.save()
            return portf

        _.times(create_portfolio, 1000)
