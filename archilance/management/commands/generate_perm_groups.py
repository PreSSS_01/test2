import pydash as _;
from django.contrib.auth.models import Group
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating permission groups...')
        print('---------------------------------------')

        Group.objects.create(name='Исполнители')
        Group.objects.create(name='Заказчики')
