import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from projects.models import ConstructionType


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating construction types...')
        print('---------------------------------------')

        _.times(lambda i: ConstructionType.objects.create(name='Constr. type %s' % i), 100)
