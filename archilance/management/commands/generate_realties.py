import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from common.models import Location
from projects.models import Realty, BuildingClassfication, ConstructionType
from users.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating realties...')
        print('---------------------------------------')

        # # Fields:
        # 
        # 
        # ('building_classification', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        # ('construction_type', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        # ('user', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        # 
        # ('projects', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('location', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # 
        # 
        # 
        # ('name', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),


        def create_realty(i):
            realty = Realty(name='Realty %s' % i)

            realty.building_classification = BuildingClassfication.objects.order_by('?').first()
            realty.construction_type = ConstructionType.objects.order_by('?').first()
            realty.location = Location.objects.root_nodes()[0].get_descendants().order_by('?').first()
            realty.user = User.objects.filter(groups__name='Заказчики', is_active=True, is_superuser=False).order_by(
                '?').first()

            realty.save()
            return realty

        _.times(create_realty, 100)
