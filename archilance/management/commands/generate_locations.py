import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from common.models import Location


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating locations...')
        print('---------------------------------------')

        _root = Location.objects.create(name='_root', type='_root')

        depths = ('A', 'B', 'C', 'D')

        for d1 in depths:
            x = Location.objects.create(name='Страна %s' % d1, type='country', parent=_root)
            for d2 in depths:
                y = Location.objects.create(name='Регион %s-%s' % (d1, d2), type='region', parent=x)
                for d3 in depths:
                    z = Location.objects.create(name='Город %s-%s-%s' % (d1, d2, d3), type='town', parent=y)
