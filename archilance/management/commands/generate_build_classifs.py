import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from projects.models import BuildingClassfication


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating building classifications...')
        print('---------------------------------------')

        _.times(lambda i: BuildingClassfication.objects.create(name='Build. classif. %s' % i), 100)
