import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from archilance import util
from projects.models import Portfolio
from specializations.models import Specialization
from users.models import User, Team


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating teams...')
        print('---------------------------------------')

        # ('name', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),

        # ('portfolios', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('owner', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('users', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        # ('specializations', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),


        contractors = list(User.contractor_objects.order_by('?'))

        owners = util.take(contractors, len(contractors) // 2)
        members = contractors
        i = 0

        for owner in owners:
            team = Team(name='Team %s' % i)
            team.save()

            team.owner = owner
            team.contractors = _.sample(members, _.random(1, 5))

            team.specializations = Specialization.objects.root_nodes()[0].get_descendants().order_by('?')[
                                   :_.random(1, 5)]
            team.portfolios = Portfolio.objects.order_by('?')[:_.random(1, 10)]

            team.save()
            i += 1
