import pydash as _;
from django.core.management import BaseCommand

_.map = _.map_;
_.filter = _.filter_

from archilance import util
from projects.models import Project, Order, CURRENCIES, TERM_TYPES, Specialization, Realty
from users.models import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        print('---------------------------------------')
        print('Generating projects...')
        print('---------------------------------------')

        # Fields:


        # ('customer', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),
        # ('specialization', 'Relation? True', 'Null? False', '(relation)', 'Hidden? False'),

        # ('answers', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('candidates', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('files', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('order', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('realty', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),
        # ('reviews', 'Relation? True', 'Null? True', '(relation)', 'Hidden? False'),



        # ('budget', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('created', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('currency', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('deal_type', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('name', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('state', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('term', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('term_type', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),
        # ('work_type', 'Relation? False', 'Null? False', 'Blank? False', 'Hidden? False'),

        # ('budget_by_agreement', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        # ('cro', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        # ('id', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        # ('price_and_term_required', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),
        # ('text', 'Relation? False', 'Null? False', 'Blank? True', 'Hidden? False'),




        def create_project(i):
            project = Project(
                budget=util.random_amount(),
                budget_by_agreement=_.sample((True, False)),
                created=util.random_date(),
                cro=_.sample((True, False)),
                currency=_.sample(CURRENCIES)[0],
                name='Project %s' % i,
                price_and_term_required=_.sample((True, False)),
                deal_type=_.sample(Project.DEAL_TYPES)[0],
                term=_.random(0, 20),
                term_type=_.sample(TERM_TYPES)[0],
                text=util.lorem(_.random(5, 30)),
                work_type=_.sample(Project.WORK_TYPES)[0],
                state='active',
            )

            project.specialization = Specialization.objects.root_nodes()[0].get_descendants().order_by('?').first()
            project.realty = Realty.objects.order_by('?').first()
            project.customer = User.customer_objects.filter(is_active=True).order_by('?').first()

            project.save()

            Order.objects.create(
                project=project,
                contractor=_.sample((None, User.contractor_objects.order_by('?').first())),
                secure=_.sample((True, False)),
                status=_.sample(Order.STATUSES)[0],
            )

            return project

        _.times(create_project, 1000)
