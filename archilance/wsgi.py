"""
WSGI config for archilance project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from django.template.base import Variable

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "archilance.settings.local")

application = get_wsgi_application()

# Patch template Variable to output empty string for None values:

old_resolve_lookup = Variable._resolve_lookup


def new_resolve_lookup(self, *args, **kwargs):
    val = old_resolve_lookup(self, *args, **kwargs)
    return '' if val == None else val


Variable._resolve_lookup = new_resolve_lookup
