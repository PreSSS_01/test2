bind = '127.0.0.1:8046'
workers = 3
user = "www-data"
reload = True

try:
    from local_gunicorn import *
except ImportError:
    pass
