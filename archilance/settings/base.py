import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_DIR = os.path.dirname(BASE_DIR)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'vb6@b9zj7^f!^+x*e8=e!oundyu1!e*&0i(3gu2xwo4%fx4h&n'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['proekton.com']

# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'registration',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',
]

THIRD_PARTY_APPS = [
    'django_extensions',
    'social.apps.django_app.default',
    'mptt',
    'rest_framework',
    'sorl.thumbnail',
    'compressor',
    'password_reset',
    'mathfilters',  # Basic math operations in templates; https://pypi.python.org/pypi/django-mathfilters
    'generic_relations',  # https://github.com/Ian-Foote/rest-framework-generic-relations
    'hitcount',
    'django_activeurl',
    'import_export',
    'captcha',
    'sass_processor'
]

LOCAL_APPS = [
    'specializations',
    'api',
    'common',
    'work_sell',
    'archilance',
    'projects',
    'users',
    'reviews',
    'chat',
    'wallets',
    'ratings',
    # 'corsheaders',
    'printservice',
]

WAGTAIL = [
    'wagtail.wagtailforms',
    'wagtail.wagtailredirects',
    'wagtail.wagtailembeds',
    'wagtail.wagtailsites',
    'wagtail.wagtailusers',
    'wagtail.wagtailsnippets',
    'wagtail.wagtaildocs',
    'wagtail.wagtailimages',
    'wagtail.wagtailsearch',
    'wagtail.wagtailadmin',
    'wagtail.wagtailcore',

    'modelcluster',
    'taggit',

    'cms_pages',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS + WAGTAIL

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'archilance.middlewares.LocaleMiddleware',
    # 'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'common.middleware.SetLastVisitMiddleware',

    'wagtail.wagtailcore.middleware.SiteMiddleware',
    'wagtail.wagtailredirects.middleware.RedirectMiddleware',
]

ROOT_URLCONF = 'archilance.urls'
#
# CORS_ORIGIN_ALLOW_ALL = True
# CORS_ALLOW_CREDENTIALS = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(ROOT_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.core.context_processors.request',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'context_processors.user_info.user_info'
            ],

            # Load these templatetags by default:

            'builtins': [
                'common.templatetags.common_tags',
                'django.contrib.humanize.templatetags.humanize',
                'django.templatetags.static',
                'mathfilters.templatetags.mathfilters',
            ],

            'debug': DEBUG,
            # 'loaders': [
            #     ('django.template.loaders.cached.Loader', [
            #         'django.template.loaders.filesystem.Loader',
            #         'django.template.loaders.app_directories.Loader',
            #     ]),
            # ],
        },
    },
]

WSGI_APPLICATION = 'archilance.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(ROOT_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'archilance2',
        'USER': 'archilance',
        'PASSWORD': 'q3048hgaoasd333knf',
        'HOST': 'localhost',
        'PORT': '',
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    # {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    # {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    # {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    # {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.vk.VKOAuth2',
    'social.backends.odnoklassniki.OdnoklassnikiOAuth2',
    'social.backends.mailru.MailruOAuth2',
    'users.backend.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# SOCIAL_AUTH_FACEBOOK_KEY = '222531191461451'
# SOCIAL_AUTH_FACEBOOK_SECRET = '95e88f7ef396c5b803375f8476cf2ba4'

SOCIAL_AUTH_FACEBOOK_KEY = '1047362798683484'
SOCIAL_AUTH_FACEBOOK_SECRET = '98fedcccb1cb941c2289692bd4de84da'

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id,email',
}

SOCIAL_AUTH_ODNOKLASSNIKI_OAUTH2_KEY = '1247035904'
SOCIAL_AUTH_ODNOKLASSNIKI_OAUTH2_SECRET = '9AD83DB399405EEFAE7641BD'
SOCIAL_AUTH_ODNOKLASSNIKI_OAUTH2_PUBLIC_NAME = 'CBADEFFLEBABABABA'

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '54397003678-ejfrg1la2vh2jdjq7fb1upc916kd6djo.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'UOLE-UM9bo2UL7i3TXy_WPa5'

# SOCIAL_AUTH_TWITTER_KEY = 'YAe05K4IYYxHhA6J1mTOdDBjq'
# SOCIAL_AUTH_TWITTER_SECRET = 'iRuYqRRaoGkCD4ip74NICb8FeZMxvM6MZ8HLMbm1jX99o7pcaL'

SOCIAL_AUTH_TWITTER_KEY = 'dRdPuFDlx8kMxJmECRNjd7ng9'
SOCIAL_AUTH_TWITTER_SECRET = 'WhXRHP6BzNwFS8x94pcaCBwxCSMkAEVm3Rg82XhzUaIqsf2Ur0'

SOCIAL_AUTH_VK_OAUTH2_KEY = '5542865'
SOCIAL_AUTH_VK_OAUTH2_SECRET = 'BsOSDhmyNiDte7cMJlVq'

SOCIAL_AUTH_MAILRU_OAUTH2_KEY = 'f7bad5797a375a5eeba6217d64de71f4'
SOCIAL_AUTH_MAILRU_OAUTH2_SECRET = '480fd6d67e9e8625fbc6b6b9a8ec71f0'

SOCIAL_AUTH_VK_OAUTH2_SCOPE = [
    'notify',
    'friends',
    'email',
]

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/'
SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'users.pipeline.add_email_for_user',
    'social.pipeline.mail.mail_validation',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'users.pipeline.success_social_register',
)

FIELDS_STORED_IN_SESSION = ['user_type']

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'ru_RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('ru_RU', 'Russian'),
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(ROOT_DIR, 'static')
MEDIA_ROOT = os.path.join(ROOT_DIR, 'media')

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'sass_processor.finders.CssFinder',
]

STATICFILES_DIRS = [
    os.path.join(ROOT_DIR, 'templates'),
    os.path.join(ROOT_DIR, 'assets'),
    os.path.join(ROOT_DIR, 'assets/lib/proekton-components'),
]

SASS_PROCESSOR_INCLUDE_DIRS = [
    os.path.join(ROOT_DIR, 'assets/sass'),
]
# print("SASS = ", SASS_PROCESSOR_INCLUDE_DIRS)
# SASS_PROCESSOR_AUTO_INCLUDE = False
# SASS_PROCESSOR_INCLUDE_FILE_PATTERN = r'^.+\.sass$'
SASS_PROCESSOR_ENABLED = DEBUG
# SASS_PRECISION = 8
# SASS_OUTPUT_STYLE = 'compact'
# SASS_PROCESSOR_ROOT = os.path.join(ROOT_DIR, 'assets')

# SASS_PROCESSOR_INCLUDE_DIRS = [
#     os.path.join(ROOT_DIR, 'assets'),
#     os.path.join(ROOT_DIR, 'assets/sass/modules'),
#     os.path.join(ROOT_DIR, 'templates'),
# ]

# SASS_PROCESSOR_AUTO_INCLUDE = False
# print(SASS_PROCESSOR_INCLUDE_DIRS)

# STATICFILES_FINDERS = (
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#     'sass_processor.finders.CssFinder',
#     # 'compressor.finders.CompressorFinder',
# )



AUTH_USER_MODEL = 'users.User'
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
LOGIN_REDIRECT_URL = '/users/dashboard_redirect/'
LOGIN_URL = '/users/login/'

PAGE_SIZE = 10  # Non-api page size (regular views)
API_PAGE_SIZE = 20000  # Django REST framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],

    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
        # 'rest_framework.permissions.DjangoModelPermissions',
    ],

    'PAGE_SIZE': API_PAGE_SIZE,
    'DEFAULT_FILTER_BACKENDS': ('rest_framework_filters.backends.DjangoFilterBackend',),  # djangorestframework-filters
}

SITE_ID = 1

# if DEBUG:
#     EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
# else:
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# sudo python3 -m smtpd -n -c DebuggingServer localhost:25
EMAIL_HOST = 'localhost'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = ''
EMAIL_PORT = 25
# EMAIL_USE_TLS = False
# EMAIL_USE_SSL = False
# EMAIL_TIMEOUT = 60
EMAIL_DEFAULT = 'noreply@proekton.com'
DEFAULT_FROM_EMAIL = 'noreply@proekton.com'

SHELL_PLUS_POST_IMPORTS = (  # Extra auto imports
    'natsort',
    ('archilance', 'util'),
    ('django.shortcuts', 'get_object_or_404'),
    ('pprint', ('pprint', 'pformat')),
)

YANDEX_MONEY = {'shop_password': 'x1uvmS9Iq8WBE3Oo'}

if DEBUG:
    YANDEX_MONEY.update({
        'url': 'https://demomoney.yandex.ru/eshop.xml',
        'shop_id': 59838,
        'scid': 537400,
    })
else:
    YANDEX_MONEY.update({
        'url': 'https://money.yandex.ru/eshop.xml',
        'shop_id': 0,
        'scid': 0,
    })

WAGTAIL_SITE_NAME = 'PROEKTON'

DEFAULT_FILE_STORAGE = 'archilance.util.ASCIIFileSystemStorage'

RECAPTCHA_PUBLIC_KEY = '6LftaQgUAAAAAG0oKrt1ORtop1jfIEzkTnCL4S3Q'
RECAPTCHA_PRIVATE_KEY = '6LftaQgUAAAAAMaIK-YCL1u1AfakVXjblULQCvfa'
# CAPTCHA_AJAX = True
NOCAPTCHA = True
