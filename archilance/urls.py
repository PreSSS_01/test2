from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtailcore import urls as wagtail_urls

from common.views import CustomRegistrationView
from wallets.views import TmpCheckOrderView, TmpPaymentAvisoView
from .views import HomeTemplateView, TestChatTemplateView, TestView, test_404
from projects.views import CustomerRealtyTrashView, CustomerRealtyDeleteView, CustomerRealtyRestoreView, SortRealtyBy, \
    RealtyUpdateView

urlpatterns = [
                  url(r'^$', HomeTemplateView.as_view()),
                  # url(r'^404$', test_404),
                  url('', include('social.apps.django_app.urls', namespace='social')),
                  url(r'^chattest/$', TestChatTemplateView.as_view()),
                  url(r'^work_sell/', include('work_sell.urls')),
                  url(r'^test/$', TestView.as_view(), name='test'),
                  url(r'^projects/', include('projects.urls')),
                  url(r'^object/(?P<pk>\d+)/trash/$', CustomerRealtyTrashView.as_view(), name='customer-object-trash'),
                  url(r'^object/(?P<pk>\d+)/delete/$', CustomerRealtyDeleteView.as_view(), name='customer-object-delete'),
                  url(r'^object/(?P<pk>\d+)/edit/$', RealtyUpdateView.as_view(), name='customer-object-edit'),
                  url(r'^object/(?P<pk>\d+)/restore/$', CustomerRealtyRestoreView.as_view(), name='customer-object-restore'),
                  url(r'^objects/sort/$', SortRealtyBy.as_view(), name='sort-realty-by'),
                  url(r'^reviews/', include('reviews.urls')),
                  url(r'^wallets/', include('wallets.urls')),
                  url(r'^chat/', include('chat.urls')),
                  url(r'^specializations/', include('specializations.urls')),
                  url(r'^users/', include('users.urls')),
                  url(r'^common/', include('common.urls')),
                  url(r'^password/', include('password_reset.urls')),

                  url(r'^users/register/$', CustomRegistrationView.as_view(), name='registration_register'),
                  url(r'^users/', include('registration.backends.default.urls')),

                  url(r'^admin/', admin.site.urls),
                  url(r'^api/', include('api.urls')),
                  url(r'^cms/', include(wagtailadmin_urls)),
                  url(r'^pages/', include(wagtail_urls)),

                  # TODO: Move URLs into the "wallets" app (Yandex Money settings modification needed):

                  url(r'^yandex/check$', TmpCheckOrderView.as_view(), name='tmp-check-order'),
                  url(r'^yandex/aviso$', TmpPaymentAvisoView.as_view(), name='tmp-payment-aviso'),

                  url(r'^payment/success$', TemplateView.as_view(template_name='message.html'), {
                      'message': 'Оплата прошла успешно',
                  }, name='tmp-payment-success'),

                  url(r'^payment/fail$', TemplateView.as_view(template_name='message.html'), {
                      'message': 'Произошла ошибка при оплате',
                  }, name='tmp-payment-fail'),

                  url(r'^print-service/', include('printservice.urls')),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
