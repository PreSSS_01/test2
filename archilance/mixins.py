from django.conf import settings
from django.contrib.sites.models import Site
from django.views.generic.base import ContextMixin


class BaseMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        pk = self.kwargs.get('pk')  # Current object's ID
        next = self.kwargs.get('next')  # Redirect next path
        back = self.kwargs.get('back')  # Redirect back path

        if pk: c['pk'] = int(pk)
        if next: c['next'] = next
        if back: c['back'] = back

        c['domain'] = Site.objects.get_current().domain

        c['TEMPLATE_DEBUG'] = getattr(settings, 'TEMPLATE_DEBUG', None)
        c['TESTING'] = getattr(settings, 'TESTING', None)

        return c

# import code; code.interact(local=dict(globals(), **locals()))
