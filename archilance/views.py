from django.core.files.base import ContentFile
from django.shortcuts import render
from django.views.generic import View

from .mixins import BaseMixin
from chat.models import Documents
from common.models import MainPage
from users.models import User
from work_sell.models import Picture

import pydash as _;
_.map = _.map_;
_.filter = _.filter_


def test_404(request):
    return render(request, '404.html')


class HomeTemplateView(BaseMixin, View):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        main_settings = MainPage.objects.filter(pk=1).first()

        context['main_settings'] = main_settings

        return render(request, self.template_name, context)


class TestChatTemplateView(View):
    template_name = 'chat_test.html'

    def get(self, request, *args, **kwargs):
        test = [54, 55, 56]
        for pk in test:
            picture = Picture.objects.get(pk=pk)
            temp_file = ContentFile(picture.file.read())
            temp_file.name = picture.file.name
            document = Documents()
            document.team_id = 1
            document.order_id = 2
            document.sender_id = 2
            document.recipent_id = 4
            document.file = temp_file
            document.save()


class TestView(BaseMixin, View):
    template_name = 'test.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        context['foo'] = _.flatten_deep(User.contractor_objects.order_by('?')[:10].values_list('pk'))

        return render(request, self.template_name, context)
        # return redirect('projects:detail', pk=153)

# import code; code.interact(local=dict(globals(), **locals()))
