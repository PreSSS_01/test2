from django.utils import translation


class LocaleMiddleware(object):
    """
    This is a very simple middleware that parses a request
    and decides what translation object to install in the current
    thread context. This allows pages to be dynamically
    translated to the language the user desires (if the language
    is available, of course).
    """

    def process_request(self, request):
        user_language = 'ru_RU'
        translation.activate(user_language)
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
