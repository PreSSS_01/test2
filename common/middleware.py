import datetime

from django.utils.timezone import now

from ratings.models import HistoryRating
from users.models import User


class SetLastVisitMiddleware(object):
    def process_response(self, request, response):
        if hasattr(request, 'user'):
            if request.user.is_authenticated():
                User.objects.filter(pk=request.user.pk).update(last_time_visit=now())
        return response


class SetRatingToUserEveryDay(object):
    def process_response(self, request, response):
        if hasattr(request, 'user'):
            today_date = datetime.datetime.now().date()
            hs_last = HistoryRating.objects.filter(user=request.user, type='visit_site').order_by('-created').first()
            if not hs_last or hs_last.created.date() != today_date:
                hs_new = HistoryRating()
                hs_new.type = 'visit_site'
                hs_new.rating = 1
                hs_new.user = request.user
                hs_new.description = 'Балл за вход на сайт'
                hs_new.save()
        return response
