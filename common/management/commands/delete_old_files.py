import os
import datetime
from django.core.management import BaseCommand
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **options):
        days = 20
        directory = os.path.join(settings.MEDIA_ROOT,'common/printdocuments')
        files_list = []
        date_now = datetime.datetime.now()
        for root, subfolders, files in os.walk(directory):
            for f in files:
                files_list.append(os.path.join(root,f))

        for f in files_list:
            try:
                date_modify = datetime.datetime.fromtimestamp(os.path.getmtime(f))
                days_diff = (date_now-date_modify).days
                if days_diff > days:
                    os.remove(f)
            except OSError as e:
                print(e.strerror + " " + e.filename)




