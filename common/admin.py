import pydash as _;
from django import forms
from django.contrib import admin
from mptt.admin import MPTTModelAdmin

_.map = _.map_;
_.filter = _.filter_

from .models import (
    LiveImageUpload,
    Location,
    MainPage,
    PrintDocuments,
    PrintOrder,
    Settings,
    Tooltip,
)


class LocationAdmin(MPTTModelAdmin):
    readonly_fields = ('pk', 'lft', 'rght', 'tree_id', 'level')


class MainPageAdminForm(forms.ModelForm):
    class Meta:
        fields = '__all__'

    class Media:
        js = (
            'admin.js',
            'lib/ckeditor/ckeditor.js',
            'lib/ckeditor/adapters/jquery.js',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in ('contractor_text', 'customer_text'):
            attrs = self.fields[field_name].widget.attrs
            attrs['class'] = _.join(_.compact((attrs.get('class'), '-ckeditor')), ' ')


class MainPageAdmin(admin.ModelAdmin):
    form = MainPageAdminForm
    readonly_fields = ('pk',)


class TooltipAdminForm(forms.ModelForm):
    class Meta:
        fields = '__all__'

    class Media:
        js = (
            'admin.js',
            'lib/ckeditor/ckeditor.js',
            'lib/ckeditor/adapters/jquery.js',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name in ('text', 'example'):
            attrs = self.fields[field_name].widget.attrs
            attrs['class'] = _.join(_.compact((attrs.get('class'), '-ckeditor')), ' ')


class TooltipAdmin(admin.ModelAdmin):
    readonly_fields = ('pk',)
    list_display = ('name', 'pk', 'text')
    form = TooltipAdminForm


admin.site.register(LiveImageUpload)
admin.site.register(Location, LocationAdmin)
admin.site.register(MainPage, MainPageAdmin)
admin.site.register(PrintDocuments)
admin.site.register(PrintOrder)
admin.site.register(Settings)
admin.site.register(Tooltip, TooltipAdmin)


# import code; code.interact(local=dict(globals(), **locals()))
