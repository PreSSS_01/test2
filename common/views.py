from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.views.generic import View, DetailView
from registration.backends.default.views import RegistrationView
from sorl.thumbnail import get_thumbnail

from archilance import util
from archilance.mixins import BaseMixin
from common.mixins import NoCsrfMixin, AjaxUserAuthMixin
from users.models import ContractorResume
from .forms import PrintOrderForm, CustomRegistrationForm
from .models import PrintDocuments, PrintOrder, Settings, LiveImageUpload


class PrintOrderDetailView(DetailView):
    model = PrintOrder
    template_name = 'printorder_detail.html'


class PrintDocumentCreate(BaseMixin, View):
    form_class = PrintOrderForm
    template_name = 'printdocument_create.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(request=request)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES, request=request)
        if form.is_valid():
            print_order = form.save(commit=False)
            print_order.save()

            for file in request.FILES.getlist('new_files'):
                print_doc = PrintDocuments.objects.create(file=file, printorder=print_order)
                print_doc.save()

            print_documents = print_order.print_documents.all()
            limit_size = 10 * 1024 * 1024
            attachments = []
            link_files = []

            for f in print_documents:
                if f.file.size > limit_size:
                    link_files.append(f.file.path)
                else:
                    attachments.append(f.file.path)

            ctx_dict = {
                'username': print_order.sender,
                'phone': print_order.phone,
                'address': print_order.address,
                'files': link_files,
            }

            settings = Settings.objects.all().first()
            subject, from_email, to = 'Заявка на распечатку', settings.noreply_email, settings.document_send_email
            text_content = render_to_string('document_email.txt', ctx_dict)
            html_content = get_template('document_email.html').render(ctx_dict)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            for attach in attachments:
                msg.attach_file(attach)
            msg.send()

            return redirect('common:print-order-detail', pk=print_order.pk)
            # import code; code.interact(local=dict(globals(), **locals()))
        else:
            context = self.get_context_data(**kwargs)
            context.update({'form': form})
            return render(request, self.template_name, context)


class LiveImageUploadCreateView(NoCsrfMixin, LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        image = request.FILES.get('image')

        if not image:
            return JsonResponse({'files': [{'error': 'No image provided'}]})

        live_img = LiveImageUpload.objects.create(file=image)

        return JsonResponse({
            'id': live_img.pk,
            'name': live_img.file.name,
            'size': live_img.file.size,
            'url': live_img.file.url,
            'thumbnailUrl': get_thumbnail(live_img.file, '208x208', crop='center').url,
            'smallThumbnailUrl': get_thumbnail(live_img.file, '200x200', crop='center').url,
            'deleteUrl': reverse('common:live-image-upload-delete', kwargs={'pk': live_img.pk}),
            'deleteType': 'POST',
        })


class LiveImageUploadDeleteView(NoCsrfMixin, LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        live_img = util.get_or_none(LiveImageUpload, pk=kwargs.get('pk'))

        if live_img:
            live_img.file.delete()
            live_img.delete()
            return JsonResponse({'status': 'success'})
        else:
            return JsonResponse({'status': 'error'})


class CustomRegistrationView(AjaxUserAuthMixin, RegistrationView):
    form_class = CustomRegistrationForm
    template_name = 'registration/registration_form.html'
    success_url = reverse_lazy('registration_complete')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.GET.get('type') in ('customer', 'contractor'):
            context['hide_user_type'] = True

        return context

    def register(self, form):
        user = super().register(form)

        if form.cleaned_data.get('user_type') == 'customer':
            group = Group.objects.get(name='Заказчики')
        elif form.cleaned_data.get('user_type') == 'contractor':
            group = Group.objects.get(name='Исполнители')
        else:
            group = None

        if group:
            user.groups.add(group)

        user.contractor_resume = ContractorResume.objects.create(text='')

        user.save()

        return user

# import code; code.interact(local=dict(globals(), **locals()))
