import pydash as _;
from django import forms
from registration.forms import RegistrationFormTermsOfService

_.map = _.map_;
_.filter = _.filter_

from .models import PrintOrder, PrintDocuments
from captcha.fields import ReCaptchaField


class PrintOrderForm(forms.ModelForm):
    files = forms.ModelMultipleChoiceField(
        queryset=PrintDocuments.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
    )

    class Meta:
        model = PrintOrder

        fields = (
            'sender',
            'phone',
            'address',
            'shipping',
            'files',
            'text',
        )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['files'].queryset = self.instance.files


class CustomRegistrationForm(RegistrationFormTermsOfService):
    USER_TYPES = (
        ('', 'Выберите роль'),
        ('customer', 'Заказчик'),
        ('contractor', 'Исполнитель'),
    )

    user_type = forms.ChoiceField(choices=USER_TYPES)
    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop('request')
            super().__init__(*args, **kwargs)
            if self.request.GET.get('type') == 'customer':
                self.fields['user_type'].initial = 'customer'
            elif self.request.GET.get('type') == 'contractor':
                self.fields['user_type'].initial = 'contractor'
        except KeyError:
            super().__init__(*args, **kwargs)

        attrs = self.fields['user_type'].widget.attrs
        attrs['class'] = _.join(_.compact((attrs.get('class'), 'selectpicker3')), ' ')
