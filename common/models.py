from django.db import models
from django.utils import timezone
from mptt.models import TreeForeignKey, MPTTModel


class Location(MPTTModel):
    TYPES = (
        ('_root', 'Корень'),
        ('country', 'Страна'),
        ('region', 'Регион'),
        ('town', 'Город'),
    )

    name = models.CharField(max_length=50)
    parent = TreeForeignKey('self', blank=True, null=True, related_name='children', db_index=True)
    type = models.CharField(max_length=20, choices=TYPES)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Местоположение'
        verbose_name_plural = 'Местоположения'


class MainPage(models.Model):
    heading = models.CharField(max_length=255, blank=True)
    contractor_text = models.TextField(blank=True)
    customer_text = models.TextField(blank=True)
    video_code = models.TextField(blank=True)

    def __str__(self):
        return 'Main page %s' % self.pk

    class Meta:
        verbose_name = 'Главная страница'


class Settings(models.Model):
    time_notification = models.IntegerField(default=180)
    document_send_email = models.EmailField(max_length=100, default="muhtarzubanchi05@gmail.com")
    accountant_send_email = models.EmailField(max_length=100, default="muhtarzubanchi05@gmail.com")
    document_send_description = models.TextField(blank=True)
    document_send_time_remove = models.IntegerField(default=14)
    recalculation_spec_time = models.TimeField()
    recalculation_rating_time = models.TimeField()
    noreply_email = models.CharField(max_length=50, default='noreply@proekton.com')

    def __str__(self):
        return 'Настройки сайта'

    class Meta:
        verbose_name = 'Настройки сайта'
        verbose_name_plural = 'Настройки сайта'


class PrintOrder(models.Model):
    SHIPPINGS = (
        ('self_delivery', 'Самовывоз'),
        ('courier_delivery', 'Доставка курьером'),
    )
    sender = models.CharField(max_length=150)
    phone = models.CharField(max_length=50)
    address = models.CharField(max_length=255)
    created = models.DateTimeField(default=timezone.now)
    shipping = models.CharField(max_length=30, default='self_delivery', choices=SHIPPINGS)
    text = models.TextField()

    def __str__(self):
        return self.sender

    class Meta:
        verbose_name = 'Заявка на распечатку'
        verbose_name_plural = 'Заявки на распечатку'


class PrintDocuments(models.Model):
    printorder = models.ForeignKey(PrintOrder, related_name='print_documents')
    file = models.FileField(upload_to='common/printdocuments/')

    def __str__(self):
        return self.file.url

    class Meta:
        verbose_name = 'Документы на распечатку'
        verbose_name_plural = 'Документы на распечатку'


class Tooltip(models.Model):
    POSITIONS = (
        ('left', 'Слева'),
        ('right', 'Справа'),
        ('top', 'Сверху'),
        ('bottom', 'Снизу'),
    )

    name = models.CharField(max_length=255, unique=True)
    text = models.TextField(blank=True)
    example = models.TextField(blank=True)
    position = models.CharField(max_length=255, choices=POSITIONS, default='top')

    class Meta:
        verbose_name = 'Подсказка'
        verbose_name_plural = 'Подсказки'

    def __str__(self):
        return self.name


class LiveImageUpload(models.Model):
    file = models.ImageField(upload_to='live-image-upload/')
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = 'Живая загрузка изображений'
        verbose_name_plural = 'Живая загрузка изображений'

    def __str__(self):
        return self.file.url
