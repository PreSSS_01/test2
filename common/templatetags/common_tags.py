from django import template
from django.utils.safestring import mark_safe
from pprint import pprint, pformat
import json
import os
import pydash as _; _.map = _.map_; _.filter = _.filter_

from archilance import util
from common.models import Tooltip


register = template.Library()


@register.simple_tag
def morph_words(number, words):
    words = words.split(',')
    try:
        if type(number) == str:
            number = int(number)
    except Exception:
        return words[2]

    if number > 10:
        number %= 10

    if number == 1:
        return words[0]
    elif 1 < number < 5:
        return words[1]
    else:
        return words[2]


@register.filter
def inspect(obj):
    return pformat(obj.__dict__)


@register.simple_tag
def interact(**kwargs):
    import code; code.interact(local=dict(kwargs, **dict(globals(), **locals())))


@register.simple_tag
def random_ident(*args, **kwargs):
    return util.random_ident(*args, **kwargs)


@register.filter('int')
def to_int(val):
    return int(val)


@register.filter('str')
def to_str(val):
    return str(val)


@register.filter('range')
def to_range(num):
    return range(num)


@register.filter('list')
def to_list(val):
    return list(val)


@register.filter('repr')
def to_repr(val):
    return repr(val)


@register.filter('json')
def to_json(val):
    return mark_safe(repr(json.dumps(val)))


@register.filter
def class_name(val):
    return type(val).__name__


@register.filter
def multiply(string, times):
    return string * times


@register.filter
def basename(val):
    return os.path.basename(val)


@register.filter
def pk(obj):
    return obj.pk


@register.simple_tag
def lorem(*args, **kwargs):
    return util.lorem(*args, **kwargs)


@register.filter
def decap(val):
    if isinstance(val, str):
        return util.decap(val)
    
    return val


@register.simple_tag
def tooltip(**kwargs):
    tooltip = util.get_or_none(Tooltip, **kwargs)
    
    if tooltip:
        # return mark_safe(tooltip.text) # Disable autoescaping
        return tooltip.text


@register.simple_tag
def tooltip_placement(**kwargs):
    tooltip = util.get_or_none(Tooltip, **kwargs)
    
    if tooltip:
        return tooltip.position


@register.simple_tag
def morph(number, words_string):
    words = _.split(words_string, ',')
    return '%s %s' % (number, util.morph(number, words))


@register.simple_tag
def fa_currency_classes(currency):
    CURRENCIES = {'rur': 'rub', 'eur': 'eur', 'usd': 'usd'}
    currency_class = CURRENCIES.get(currency)
    
    if currency_class:
        return 'fa fa-%s' % currency_class


@register.filter
def get(dic, key):
    if isinstance(dic, dict):
        return dic.get(key)


@register.filter
def max_count(value, max_value):
    value = int(value) or 0
    if value > max_value:
        return '99+'
    return str(value)

# import code; code.interact(local=dict(globals(), **locals()))
