from django.contrib.contenttypes.models import ContentType
from rest_framework.serializers import ModelSerializer

from .models import Location
from work_sell.models import ElFormat


class NestedLocationSerializer(ModelSerializer):
    class Meta:
        model = Location

        fields = (
            'children',
            'id',
            'level',
            'lft',
            'name',
            'parent',
            'rght',
            'tree_id',
            'type',
        )


class NestedLocationSerializerFlat(ModelSerializer):
    class Meta:
        model = Location

        fields = (
            'id',
        )


class LocationSerializer(ModelSerializer):
    children = NestedLocationSerializer(many=True)
    parent = NestedLocationSerializer()

    class Meta:
        model = Location

        fields = (
            'children',
            'id',
            'level',
            'lft',
            'name',
            'parent',
            'rght',
            'tree_id',
            'type',
        )


class LocationSerializerFlat(ModelSerializer):
    children = NestedLocationSerializerFlat(many=True)
    parent = NestedLocationSerializerFlat()

    class Meta:
        model = Location

        fields = (
            'id',
            'name',
            'children',
            'parent')


class ElFormatSerializer(ModelSerializer):
    # children = NestedLocationSerializerFlat(many=True)
    # parent = NestedLocationSerializerFlat()

    class Meta:
        model = Location

        fields = (
            'id',
            'name',
        )


class ContentTypeSerializer(ModelSerializer):
    class Meta:
        model = ContentType

        fields = (
            'app_label',
            'id',
            'model',
        )
