import json

from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.template import RequestContext, loader


class NoCsrfMixin(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class CustomerRequiredMixin(LoginRequiredMixin, View):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_customer():
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied


class ContractorRequiredMixin(LoginRequiredMixin, View):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_contractor():
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied


class AjaxUserAuthMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def render_to_json_response(self, context, **response_kwargs):
        user_form = self.form_class()
        captcha_html = loader.render_to_string('partials/inc-captcha.html', {"user_form": user_form})
        context.update({"captcha_html": captcha_html})
        data = json.dumps(context, ensure_ascii=False)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return self.render_to_json_response(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        if self.request.is_ajax():
            new_user = self.register(form)
            data = {
                'pk': new_user.pk,
            }

            return self.render_to_json_response(data)
        else:
            response = super().form_valid(form)
            return response
