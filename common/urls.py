from django.conf import urls

from .views import (
    PrintDocumentCreate,
    PrintOrderDetailView,
    LiveImageUploadCreateView,
    LiveImageUploadDeleteView,
)

app_name = 'common'

urlpatterns = [
    urls.url(r'^printdocument/create/$', PrintDocumentCreate.as_view(), name='create'),
    urls.url(r'^printorder/(?P<pk>\d+)/$', PrintOrderDetailView.as_view(), name='print-order-detail'),

    urls.url(r'^live-image-upload/create/$', LiveImageUploadCreateView.as_view(), name='live-image-upload-create'),
    urls.url(r'^live-image-upload/(?P<pk>\d+)/delete/$', LiveImageUploadDeleteView.as_view(),
             name='live-image-upload-delete'),
]
