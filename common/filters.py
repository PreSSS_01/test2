from django.contrib.contenttypes.models import ContentType
from rest_framework_filters import FilterSet, AllLookupsFilter, RelatedFilter

from .models import Location


class LocationFilterSet(FilterSet):
    id = AllLookupsFilter()
    level = AllLookupsFilter()
    lft = AllLookupsFilter()
    name = AllLookupsFilter()
    rght = AllLookupsFilter()
    tree_id = AllLookupsFilter()
    type = AllLookupsFilter()

    children = RelatedFilter('common.filters.LocationFilterSet')
    parent = RelatedFilter('common.filters.LocationFilterSet')

    class Meta:
        model = Location


class ContentTypeFilterSet(FilterSet):
    app_label = AllLookupsFilter()
    id = AllLookupsFilter()
    model = AllLookupsFilter()

    class Meta:
        model = ContentType
