from django.apps import AppConfig


class CmsPagesConfig(AppConfig):
    name = 'cms_pages'
