from rest_framework import routers
from django.conf.urls import url, include

from .views import (
    AnswerViewSet,
    ContractorResumeFilesViewSet,
    ContractorResumeViewSet,
    DocumentViewSet,
    ElFormatViewSet,
    LocationViewSet,
    LocationViewSetFlat,
    MessageViewSet,
    NoteViewSet,
    OrderViewSet,
    PortfolioPhotoViewSet,
    PortfolioViewSet,
    ProjectViewSet,
    RealtyViewSet,
    UpdateRealty,
    ReviewViewSet,
    BuildingClassificationViewSet,
    SpecializationViewSet,
    SpecializationViewSetFlat,
    ConstructionTypeViewSet,
    StageViewSet,
    StageUpdateViewSet,
    TeamViewSet,
    UserViewSet,
    WorkSellPhotoViewSet,
    WorkSellViewSet,
)

router = routers.DefaultRouter()

router.register(r'answers', AnswerViewSet)
router.register(r'building_classifications', BuildingClassificationViewSet)
router.register(r'construction_type', ConstructionTypeViewSet)
router.register(r'contractorresume', ContractorResumeViewSet)
router.register(r'contractorresumefiles', ContractorResumeFilesViewSet)
router.register(r'documents', DocumentViewSet)
router.register(r'el_format', ElFormatViewSet)
router.register(r'locations', LocationViewSet)
router.register(r'locations_flat', LocationViewSetFlat)
router.register(r'message', MessageViewSet, base_name='Message')
router.register(r'note', NoteViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'portfolio-photos', PortfolioPhotoViewSet)
router.register(r'portfolios', PortfolioViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'realties', RealtyViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'specializations', SpecializationViewSet)
router.register(r'specializations_flat', SpecializationViewSetFlat)
router.register(r'stages', StageViewSet)
router.register(r'teams', TeamViewSet)
router.register(r'users', UserViewSet)
router.register(r'work-sell-photos', WorkSellPhotoViewSet)
router.register(r'work-sells', WorkSellViewSet)

urlpatterns = router.urls

urlpatterns = [
    url(r'^update_stages/$', StageUpdateViewSet.as_view()),
    # url(r'^orders/(?P<pk>\d+)/$', OrderViewSet.as_view({'delete'})),
]

urlpatterns += router.urls