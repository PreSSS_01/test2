from django.conf import settings
from django.db.models import Q, F
from django.http import Http404
from rest_framework import permissions
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status

from chat.filters import MessageFilterSet, NoteFilterSet, DocumentFilterSet
from chat.models import Message, Notes, Documents, NewMessage
from chat.serializers import MessageSerializer, NoteSerializer, DocumentsSerializer
from common.filters import LocationFilterSet
from common.models import Location
from common.serializers import LocationSerializer, LocationSerializerFlat, ElFormatSerializer
from projects.filters import (
    ProjectFilterSet, RealtyFilterSet, StageFilterSet, PortfolioFilterSet, OrderFilterSet,
    PortfolioPhotoFilterSet, AnswerFilterSet, BuildingClassficationFilterSet, ConstructionTypeFilterSet
)
from projects.models import (Project, Realty, Stage, Portfolio, PortfolioPhoto, Answer, Order,
                             BuildingClassfication, ConstructionType)
from projects.serializers import (
    ProjectSerializer, RealtySerializer, StageSerializer, PortfolioSerializer,
    PortfolioPhotoSerializer, AnswerSerializer, OrderSerializer, BuildingClassificationSerializeFlat,
    ConstructionTypeSerializer, StageListSerializer)
from reviews.filters import ReviewFilterSet
from reviews.models import Review
from reviews.serializers import ReviewSerializer
from specializations.filters import SpecializationFilterSet
from specializations.models import Specialization
from specializations.serializers import SpecializationSerializer, SpecializationSerializerFlat
from users.filters import UserFilterSet, TeamFilterSet
from users.models import User, ContractorResumeFiles, ContractorResume, Team
from users.serializers import UserSerializer, ContractorResumeFilesSerializer, ContractorResumeSerializer, \
    TeamSerializer
from work_sell.filters import WorkSellFilterSet, WorkSellPhotoFilterSet
from work_sell.models import WorkSell, WorkSellPhoto, ElFormat
from work_sell.serializers import WorkSellSerializer, WorkSellPhotoSerializer


class ContractorResumeFilesViewSet(ModelViewSet):
    queryset = ContractorResumeFiles.objects.all()
    serializer_class = ContractorResumeFilesSerializer
    # filter_class = ContractorResumeFilesFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ContractorResumeViewSet(ModelViewSet):
    queryset = ContractorResume.objects.all()
    serializer_class = ContractorResumeSerializer
    # filter_class = ContractorResumeFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class StageViewSet(ModelViewSet):
    queryset = Stage.objects.all()
    serializer_class = StageSerializer
    filter_class = StageFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class StageUpdateViewSet(generics.UpdateAPIView):
    # def __init__(self, *args, **kwargs):
    #     many = kwargs.pop('many', True)
    #     super().__init__(many=many, *args, **kwargs)
    model = Stage
    queryset = model.objects.all()
    serializer_class = StageSerializer

    def put(self, request, *args, **kwargs):
        # print("put request = ", request.data)
        print(request.data)
        result = request.data
        print("result = ", result)
        # ids = [obj["id"] for obj in result]
        # queryset = self.model.objects.filter(pk__in=ids)
        # print("count = ", queryset.count())
        serializer = self.serializer_class(data=result, many=True)
        serializer.is_valid()
        serializer.save()
        # print("serializer = ", serializer)
        return Response(serializer.data, status=status.HTTP_200_OK)
        # print("errors = ", serializer.errors)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReviewViewSet(ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    filter_class = ReviewFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class DocumentViewSet(ModelViewSet):
    queryset = Documents.objects.all()
    serializer_class = DocumentsSerializer
    filter_class = DocumentFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        queryset = Documents.objects.all()
        search_param = self.request.query_params.get('operand', None)
        recipent_id = self.request.query_params.get('recipent_id', None)
        sender_id = self.request.query_params.get('sender_id', None)
        if search_param:
            # import code; code.interact(local=dict(globals(), **locals()))
            if search_param == 'in':
                queryset = queryset.filter(Q(sender__in=[sender_id, recipent_id]),
                                           Q(recipent__in=[sender_id, recipent_id])).filter(
                    order__isnull=True).order_by('created')
        return queryset


class ProjectViewSet(ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    filter_class = ProjectFilterSet

    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        qs = super().get_queryset()

        x_no_contractor_answer = self.request.GET.get('x_no_contractor_answer', '')

        if x_no_contractor_answer.isdecimal():
            qs = qs.exclude(answers__object_id=x_no_contractor_answer, answers__content_type__model='user')

        return qs


class NoteViewSet(ModelViewSet):
    queryset = Notes.objects.all()
    serializer_class = NoteSerializer
    filter_class = NoteFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        queryset = Notes.objects.all()
        search_param = self.request.query_params.get('operand', None)
        recipent_id = self.request.query_params.get('recipent_id', None)
        sender_id = self.request.query_params.get('sender_id', None)
        if search_param:
            # import code; code.interact(local=dict(globals(), **locals()))
            if search_param == 'in':
                queryset = queryset.filter(Q(sender__in=[sender_id, recipent_id]),
                                           Q(recipent__in=[sender_id, recipent_id])).order_by('created')
        return queryset


class MessageViewSet(ModelViewSet):
    # queryset = Message.objects.all()
    serializer_class = MessageSerializer
    filter_class = MessageFilterSet

    def get_queryset(self):
        queryset = Message.objects.all()
        search_param = self.request.query_params.get('operand', None)
        recipent_id = self.request.query_params.get('recipent_id', None)
        sender_id = self.request.query_params.get('sender_id', None)
        if search_param:
            # import code; code.interact(local=dict(globals(), **locals()))
            if search_param == 'in':
                queryset = queryset.filter(Q(sender__in=[sender_id, recipent_id]),
                                           Q(recipent__in=[sender_id, recipent_id])). \
                    filter(order__isnull=True). \
                    filter(~Q(sender=F('recipent')))

        queryset = queryset.order_by('created')
        return queryset

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        qs = NewMessage.objects.filter(message__in=queryset, user=self.request.user).delete()
        return queryset

from projects.forms import RealtyFormNew


class UpdateRealty(generics.UpdateAPIView):
    queryset = Realty.objects.all()
    serializer_class = RealtySerializer
    # permission_classes = (permissions.IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        # print("put realty request -->", request.data)
        # realty = self.get_object()
        # print("realty.name =", realty.name)
        # realty_form = RealtyFormNew(request.data, request=request, instance=realty)
        # if realty_form.is_valid():
        #     realty_form.name = 'New name'
        #     realty_form.save()
        realty = self.get_object()
        print("realty.id = ", realty.id)
        serializer = self.serializer_class(realty, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
        else:
            print(serializer.errors)

        return Response({'success': True})

        # instance = self.get_object()
        # instance.name = request.data.get("name")
        # instance.save()
        #
        # serializer = self.get_serializer(instance)
        # serializer.is_valid(raise_exception=True)
        # self.perform_update(serializer)
        #
        # return Response(serializer.data)


class RealtyViewSet(ModelViewSet):
    queryset = Realty.objects.filter(is_virtual=False)
    serializer_class = RealtySerializer
    # filter_class = RealtyFilterSet

    # TODO: довести до ума
    def get_queryset(self):
        # TODO: сделать универсальный фильтр
        queryset = self.queryset
        user_param = self.request.query_params.get('user', None)
        if user_param:
            queryset = queryset.filter(user=self.request.user)
        id_param = self.request.query_params.get('id', None)
        if id_param:
            queryset = queryset.filter(id=id_param)

        return queryset

    @list_route(methods=['get'])
    def current_user(self, request, *args, **kwargs):
        if self.request.user.__dict__:
            queryset = self.queryset.filter(user=self.request.user)
            serialiser = self.serializer_class(queryset, many=True)
            return Response(serialiser.data)
        return Response([])


class BuildingClassificationViewSet(ModelViewSet):
    queryset = BuildingClassfication.objects.all()
    serializer_class = BuildingClassificationSerializeFlat
    filter_class = BuildingClassficationFilterSet


class ConstructionTypeViewSet(ModelViewSet):
    queryset = ConstructionType.objects.all()
    serializer_class = ConstructionTypeSerializer
    filter_class = ConstructionTypeFilterSet


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_class = OrderFilterSet
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def destroy(self, request, *args, **kwargs):
        print("destroy request = ", request, 'args = ', args, 'kwargs = ', kwargs)
        try:
            instance = self.get_object()
            project = instance.project
            project.state = 'deleted'
            project.save()
            # self.perform_destroy(instance)
            print('inst = ', instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class SpecializationViewSet(ModelViewSet):
    try:  # TODO: dirty
        queryset = Specialization.objects.root_nodes()[0].get_descendants()
    except:
        queryset = Specialization.objects.all()
    serializer_class = SpecializationSerializer
    filter_class = SpecializationFilterSet


class SpecializationViewSetFlat(ModelViewSet):
    queryset = Specialization.objects.all()
    serializer_class = SpecializationSerializerFlat
    filter_class = SpecializationFilterSet


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_class = UserFilterSet


class LocationViewSet(ModelViewSet):
    try:  # TODO: dirty
        queryset = Location.objects.root_nodes()[0].get_descendants()
    except:
        queryset = Location.objects.all()
    queryset = queryset.order_by('name')
    serializer_class = LocationSerializer
    filter_class = LocationFilterSet


class LocationViewSetFlat(ModelViewSet):
    #queryset = Location.objects.all().order_by('name')
    queryset = Location.objects.all().prefetch_related('parent', 'children')
    serializer_class = LocationSerializerFlat
    filter_class = LocationFilterSet


class ElFormatViewSet(ModelViewSet):
    queryset = ElFormat.objects.all().order_by('name')
    serializer_class = ElFormatSerializer
    # filter_class = LocationFilterSet


class PortfolioPagination(PageNumberPagination):
    page_size = settings.API_PAGE_SIZE  # Default page size
    page_size_query_param = 'page_size'  # Provide custom page size through a query param
    max_page_size = 1000


class PortfolioViewSet(ModelViewSet):
    queryset = Portfolio.objects.all()
    serializer_class = PortfolioSerializer
    filter_class = PortfolioFilterSet
    pagination_class = PortfolioPagination


class PortfolioPhotoViewSet(ModelViewSet):
    queryset = PortfolioPhoto.objects.all()
    serializer_class = PortfolioPhotoSerializer
    filter_class = PortfolioPhotoFilterSet


class WorkSellPagination(PageNumberPagination):
    page_size = settings.API_PAGE_SIZE  # Default page size
    page_size_query_param = 'page_size'  # Provide custom page size through a query param
    max_page_size = 1000


class WorkSellViewSet(ModelViewSet):
    queryset = WorkSell.objects.all()
    serializer_class = WorkSellSerializer
    filter_class = WorkSellFilterSet
    pagination_class = WorkSellPagination


class WorkSellPhotoViewSet(ModelViewSet):
    queryset = WorkSellPhoto.objects.all()
    serializer_class = WorkSellPhotoSerializer
    filter_class = WorkSellPhotoFilterSet


class AnswerViewSet(ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    filter_class = AnswerFilterSet


class TeamViewSet(ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    filter_class = TeamFilterSet
