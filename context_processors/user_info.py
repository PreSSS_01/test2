# from django.core.context_processors import request
from django.db.models import Sum
from wallets.models import InvoiceHistory
from chat.models import NewMessage
from projects.models import Order


def user_info(request):
    if request.user.is_authenticated():
        current_sum_info = InvoiceHistory.objects.filter(user=request.user, type="score").aggregate(Sum('sum'))
        user_balance = current_sum_info['sum__sum'] or 0

        new_messages_count = NewMessage.objects.filter(user=request.user).count()
        num_orders_in_work = request.user.orders.filter(status='process').count()
        try:
            fist_order_id = request.user.orders.all()[0].id
        except IndexError:
            fist_order_id = ""
        return {
            "user_balance": user_balance,
            "new_messages_count": new_messages_count,
            "num_orders_in_work": num_orders_in_work,
            "fist_order_id": fist_order_id
        }

    return {}
