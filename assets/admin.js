window.jQuery = django.jQuery // Export jQuery for CKEditor

jQuery(function() {
    jQuery('textarea.-ckeditor').ckeditor().wrap('<div style="clear: both">')
})
