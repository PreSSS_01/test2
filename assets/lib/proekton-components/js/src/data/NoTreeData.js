export default class NoTreeData {
    constructor(data) {
        this.data = data;
        this.specChain = []
    }

    getElementById(id) {
        for (let i = 0; i < this.data.length; i++) {
            if (this.data[i].id == id) return this.data[i]
        }
    }

    getSpecChain(id, incl) {
        return this.specChain
    }

    isChild(el_id, parent_id) {
        return false
    }

    hasChildren() {
        return false
    }

    dataToList() {
        return this.data
    }
}