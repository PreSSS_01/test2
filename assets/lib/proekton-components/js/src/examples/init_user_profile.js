import SelectedContainer from './SelectedContainer';
import NoTreeSelect from './NoTreeSelect';
import TreeSelect from './TreeSelect';
import SingleTreeSelect from './SingleTreeSelect'

$(function () {
    function createFilterSpecs(url) {
        // SPECIALIZATIONS FILTER
        let sb_main = new TreeSelect($('#select-box-1'), {url, hasEditableContainer: true});
        sb_main.setHeader("Специализации");
        let select_container = new SelectedContainer($('#selected-spec'), {obj: sb_main});
        sb_main.connectSelectedContainer(select_container);
        let sb_1 = new TreeSelect($('#select-box-2'), {obj: sb_main});
        let sb_2 = new TreeSelect($('#select-box-3'), {obj: sb_main});
        let sb_3 = new TreeSelect($('#select-box-4'), {obj: sb_main});
        let sb_4 = new TreeSelect($('#select-box-5'), {obj: sb_main});

        sb_main.setNearbySelectBox(sb_1);
        sb_1.setNearbySelectBox(sb_2, sb_main);
        sb_2.setNearbySelectBox(sb_3, sb_1);
        sb_3.setNearbySelectBox(sb_4, sb_2);
        sb_4.setNearbySelectBox("", sb_3);

    }

    function createFilterBuildingClass(url) {
        // BUILDING-CLASSIFICATION FILTER
        let sb_build_main = new TreeSelect($('#sb-building-classification'), {url, visible:true});
        sb_build_main.setHeader("Классификация здания");

        let sb_build_1 = new TreeSelect($('#sb-building-sub-classification'), {obj: sb_build_main});

        let select_build_container = new SelectedContainer($('#selected-building-classification'), {obj: sb_build_main});
        sb_build_main.connectSelectedContainer(select_build_container);

        sb_build_main.setNearbySelectBox(sb_build_1);
        sb_build_1.setNearbySelectBox("", sb_build_main);
    }

    function createFilterConstructionType(url) {
        let sb_constr_main = new NoTreeSelect($('#sb-construction-type'), {url, visible:true});
        sb_constr_main.setHeader("Вид строительства");
        let select_constr_type = new SelectedContainer($('#selected-construction-type'), {obj:sb_constr_main, noTree: true});
        sb_constr_main.connectSelectedContainer(select_constr_type);
    }

    function createFilerLocations(url) {
        let sb_loc_main = new SingleTreeSelect($('#sb-location-1'), {url, visible:true});
        sb_loc_main.setHeader("Местоположение");
        let select_loc = new SelectedContainer($('#selected-location'), {obj: sb_loc_main, onlyOne: true});
        sb_loc_main.connectSelectedContainer(select_loc);
        let sb_loc_1 = new SingleTreeSelect($('#sb-location-2'), {obj: sb_loc_main});
        let sb_loc_2 = new SingleTreeSelect($('#sb-location-3'), {obj: sb_loc_main});

        // Убираем кнопки add-options
        sb_loc_main.dataPromise.then(()=>{
            sb_loc_1.$buttonAddOptions.remove();
            sb_loc_main.$buttonAddOptions.remove();
        });


        sb_loc_main.setNearbySelectBox(sb_loc_1);
        sb_loc_1.setNearbySelectBox(sb_loc_2, sb_loc_main);
        sb_loc_2.setNearbySelectBox("", sb_loc_1);
        blockNonEmpty(sb_loc_main, select_loc);
    }

    function blockNonEmpty(select, container) {
        container.on("add", () => {
            select.$searchInput.parent().append("<div class='simple-select fill'>Местоположение выбрано</div>");
            select.$searchInput.hide();
        });
        container.on("remove", () => {
            select.$searchInput.siblings('div.simple-select').remove();
            select.$searchInput.show();
        });
    }

    createFilterSpecs('/api/specializations_flat');
    createFilterBuildingClass('/api/building_classifications');
    createFilterConstructionType('/api/construction_type');
    createFilerLocations('/api/locations_flat');


});