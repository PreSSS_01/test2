import SelectedContainer from './SelectedContainer';
import NoTreeSelect from './NoTreeSelect';
import TreeSelect from './TreeSelect';
import TreeSelectPreload from './TreeSelectPreload';

$(function () {
    const url_root = "http://127.0.0.1:8000";

    let sb_main = new TreeSelect($('#select-box-1'),
        {url: url_root + '/api/specializations_flat/', hasEditableContainer: true});
    sb_main.setHeader("Специализации");
    let select_container = new SelectedContainer($('#selected-spec'), {obj: sb_main});
    //
    sb_main.connectSelectedContainer(select_container);

    let sb_1 = new TreeSelect($('#select-box-2'), {obj: sb_main});
    let sb_2 = new TreeSelect($('#select-box-3'), {obj: sb_main});
    let sb_3 = new TreeSelect($('#select-box-4'), {obj: sb_main});
    let sb_4 = new TreeSelect($('#select-box-5'), {obj: sb_main});
    // let sb_2 = new TreeSelect({$container: $('#select-box-3'), data: data});
    // let sb_3 = new TreeSelect({$container: $('#select-box-4'), data: data});
    // let sb_4 = new TreeSelect({$container: $('#select-box-5'), data: data});

    sb_main.setNearbySelectBox(sb_1);
    sb_1.setNearbySelectBox(sb_2, sb_main);
    sb_2.setNearbySelectBox(sb_3, sb_1);
    sb_3.setNearbySelectBox(sb_4, sb_2);
    sb_4.setNearbySelectBox("", sb_3);

    let building_classification = new NoTreeSelect($('#construction-type'),
        {url: url_root + '/api/construction_type/'});
    building_classification.setHeader('Классификация');
    let select_ct = new SelectedContainer($('#select-construction-type'), {obj: building_classification, noTree: true});
    building_classification.connectSelectedContainer(select_ct);

    function tuneCheckBoxes($boxes) {
        let currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';

        $boxes.find("div").hide();
        $boxes.find("div." + currentState).show();
    }

    //PRELOAD-BOX
    let pr_1 = new TreeSelect($('#preload-1'), {url: url_root + '/api/locations_flat', visible: true});
    let pr_2 = new TreeSelect($('#preload-2'), {url: url_root + '/api/locations_flat'});
    let pr_3 = new TreeSelect($('#preload-3'), {url: url_root + '/api/locations_flat'});

    // CUSTOM CHECK-BOX
    let $boxes = $('.custom-check');
    tuneCheckBoxes($boxes);
    $boxes.on("click", function (e) {
        let inside_checkBox = $(e.target).parent().find("input");
        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
        tuneCheckBoxes($boxes);
        e.preventDefault();
        return false;
    });

});