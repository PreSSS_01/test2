import {
    tmpl_selectBoxOptions,
    tmpl_selectBoxEditCont,
    tmpl_selectBox,
    AbsBaseSelect
} from './base/AbsBaseSelect'
import NoTreeData from './data/NoTreeData'

const htmlTemplate = ({header, selectBox, id = "", classes = "", tmpl_selectBoxOptions = () => "", tmpl_selectBoxResults = () => ""}) =>
    `
<div class="select-box-container ${classes}" id="${id}">
    <div class="select-box-search">
        ${selectBox}
    </div>

    ${tmpl_selectBoxOptions()}
    ${tmpl_selectBoxResults()}
    <span style="clear: both"></span>
</div>
`;

const tmpl_selectBoxResults = () =>
    `
    <div style="width: 100%; max-width: 95%" class="select-box-results">
        <button class="button-add create">СОЗДАТЬ</button>
    </div>
`;

// const tmpl_elementResult = (el, id, header) =>
//     `<li data-id="${id}">
//         ${el}
//     </li>`;

export default class SelectOrCreate extends AbsBaseSelect {
    getTemplate(classes) {
        let selectBox = this.hasEditableContainer ? tmpl_selectBoxEditCont() : tmpl_selectBox();
        classes = classes ? classes.join(" ") : "";
        return htmlTemplate({
            header: "TestHeader", selectBox, id: this.containerId, classes,
            tmpl_selectBoxOptions, tmpl_selectBoxResults
        })
    }

    setLinkBoxes(boxes = []) {
        this.boxes = boxes;
    }
    _buildComponents(data) {
        super._buildComponents(data);
        const self = this;
        //TODO: Изменять свойство visible при show/hide
        if (!this.visible) this.hide();
        if (this.hasEditableContainer) this.$editableContainer.hide();
        this.dataTree = this.dataTree || new NoTreeData(data);
        this.$buttonCreate = this.$resultsBox.find('.create');
        // console.log("$buttonCreate = ", $buttonCreate);
        this.$buttonCreate.on("click", () => console.log('Button Create'));
        this.$buttonCreate.on("click", this._onButtonCreate.bind(self));
        this._fillOptionsData();
        this._bindEvents();
        this.$searchInput.attr("placeholder", 'Создать/Выбрать объект')
    }

    getData(url, data = {}) {
        return Promise.resolve($.ajax({
            url: url,
            dataType: 'json',
            data: data,
        }))
    }

    _onLoadDataError(error) {
        console.log("Error loading data -->", error);
    }

    _fillBoxes() {
        const self = this;
        let id = this.selectedEl.id;
        this.getData(`/api/realties?id=${id}`)
            .then((_data)=> {
                    let data = _data.results[0] || _data[0];
                    // this._addToSelectedContainer(this.selectedEl.id);
                    // console.log("res data = ", data);
                    for (let box of this.boxes) {
                        box.selectedContainer.removeAll();
                        //TODO: Костыль!
                        if (box.type == 'building_classifications' && data['building_classification']) {
                            // console.log("building_classification = ",data['building_classification']);
                            box._addToSelectedContainer(data['building_classification'].id)
                        } else if (box.type == 'locations_flat' && data['location']) {
                            // console.log("location = ",data['location']);
                            box._addToSelectedContainer(data['location'].id)
                        } else if (box.type == 'construction_type' && data['construction_type']) {
                            // console.log("construction_type = ",data['construction_type']);
                            box._addToSelectedContainer(data['construction_type'].id)
                        } else {
                            console.log("Нет совпадений для ", box.type);
                            box.selectedContainer.$input.removeAttr('value');
                        }
                    }
                    //Костыль для обновлени имени модельного окна "Редктирование Объекта"
                    let name = data['name'];
                    let $name = $('#realtyEditModal').find('input[name=name]');
                    if ($name.length > 0) $name.val(name)
                }
            )
            .catch(this._onLoadDataError.bind(self));
    }

    _onButtonAddOptions(e) {
        this._fillBoxes();
        super._onButtonAddOptions(e);
    }


    _onButtonCreate(e) {
        this.selectedContainer.add({text: this.$searchInput.val()});
        if (this.boxes) {
            for (let box of this.boxes) {
                console.log('box clear');
                box.selectedContainer.removeAll();
            }
        }
        e.preventDefault();
        this.$buttonCreate.hide();
        this.$searchInput.val("");
    }

    _onInput_searchInput(e) {
        // this._fillResultsData(self .$searchInput.val());
        this.$buttonAddOptions.hide();
        this.$buttonCreate.show();
        // console.log("on input");
        this.$resultsBox.show();
        this.$optionsBox.hide();
    }

    _onClick_searchInput(e) {
        this.$optionsBox.show();
        this.$resultsBox.hide();
        this.$searchInput.val("");
    }
}