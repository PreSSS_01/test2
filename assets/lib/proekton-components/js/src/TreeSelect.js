import {AbsBaseSelect} from './base/AbsBaseSelect'
import DataTree from './data/DataTree'

export default class TreeSelect extends AbsBaseSelect{
    setNearbySelectBox(next, prev) {
        this.nextSelectBox = next;
        this.prevSelectBox = prev;
    }

    clearAllNext() {
        this.clear();
        if (this.nextSelectBox) {
            this.nextSelectBox.hide();
            this.nextSelectBox.clearAllNext()
        }
    }

    clearAllPrev(){
        this.clear();
        if (this.prevSelectBox) {
            this.clear();
            this.hide();
            this.prevSelectBox.clearAllPrev()
        }
    }

    _buildComponents(data) {
        super._buildComponents(data);
        //TODO: Изменять свойство visible при show/hide
        if (!this.visible) this.hide();
        if (this.hasEditableContainer) this.$editableContainer.hide();
        this.dataTree = this.dataTree || new DataTree(data.results);
        this._fillOptionsData();
        this._bindEvents();
    }

    _onclickOptionsElement(e) {
        this.clearAllNext();
        super._onclickOptionsElement(e);
        if (this.nextSelectBox && this.dataTree.hasChildren(this.selectedEl.id)) {
            this.nextSelectBox.setParent(this.selectedEl.id);
            this.nextSelectBox.setHeader(this.selectedEl.value);
            this.nextSelectBox.show();
        }
        if (this.prevSelectBox) {
            this.prevSelectBox.$buttonAddOptions.hide();
            this.prevSelectBox.$searchInput.removeClass("active");
        }
        this.$searchInput.addClass('active');
    }

    _onButtonAddOptions(e) {
        // this._addToSelectedContainer(this.selectedEl.id);
        // this.clear();
        // e.preventDefault();
        // return false;
        super._onButtonAddOptions(e);
        this.clearAllNext();
        this.clearAllPrev();
    }

    _onButtonAdd(e) {
        super._onButtonAdd(e);
        this.clearAllNext();
        this.clearAllPrev();
    }

    _addToSelectedContainer(id){
        if (this.selectedContainer){
            this.selectedContainer.add(id);
            return
        }

        this.prevSelectBox._addToSelectedContainer(id);
    }

}
