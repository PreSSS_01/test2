export default function onBind(target, name, descriptor) {
    const method = descriptor.value;

    descriptor.value = function (...args) {
        let binds = [];
        args = Array.from(args);
        // console.log("args -->", args.slice());
        for (let arg of args.slice()) {
            // console.log("onBind -->", typeof arg, "arg = ", arg);
            // console.log("arg.func -->", typeof arg.originalEvent);
            // typeof arg === 'object' && !(arg.originalEvent)
            if (arg && arg.bindFunc) {
                binds.push(arg);
                args.splice(args.indexOf(arg), 1);
            }
        }
        method.apply(this, args);
        for (let bind of binds) {
            bind.func.bind(this)(args);
        }
        return this;
    }
}

// export {onBind};