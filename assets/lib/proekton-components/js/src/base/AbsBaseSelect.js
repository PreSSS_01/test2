//TEMPLATES `
const tmpl_selectBoxEditCont = ({preloaderTemplate = ""}={}) =>
    `
            <div class="row">
            <div class="col-lg-3">
                
                <div style="width: 100%; position: relative">
                    ${preloaderTemplate}
                    <input class="select-box-search" type="text" placeholder="Выберите/Поиск">
                    <button style="display: none" class="button-add options">ПРИМЕНИТЬ</button>
                </div>
                </div>
            <div class="col-lg-9">
                <span style="display: none" class="editable-container"></span>
            </div>
            </div>
`;

const tmpl_selectBox = ({preloaderTemplate = ""}={}) =>
    `        
            ${preloaderTemplate}
            <input class="select-box-search" type="text" placeholder="Выберите/Поиск" value="">
            <button style="display: none" class="button-add options">ПРИМЕНИТЬ</button>
`;

const tmpl_elementResult = (el, id, header) =>
    `<li><label>
        <input type="checkbox" data-id="${id}">${el}
        <div class="header">${header}</div>
    </label></li>`;

const tmpl_elementOption = (el) =>
    `<li data-id="${el.id}">${el.name}</li>`;

const tmpl_plug = ({header, selectBox}) => htmlTemplate({header, selectBox});

const tmpl_selectBoxOptions = () =>
    `
    <div class="select-box-options">
        <div class="box-wrapper">
            <ul>
            
            </ul>
        </div>
    </div>
`;

const tmpl_selectBoxResults = () =>
    `
    <div class="select-box-results">
        <div class="box-wrapper">
            <div class="main-part">
                <ul>
                </ul>
            </div>
            <div class="other-part">
                <span class="other-header">Из других категорий</span>
                <ul>
                </ul>
            </div>
        </div>
        <div style="padding-top: 5px">
        <button class="button-add results">ПРИМЕНИТЬ</button>
        </div>
    </div>
`;

const htmlTemplate = ({header, selectBox, required = false, id = "", classes = "", tmpl_selectBoxOptions = () => "", tmpl_selectBoxResults = () => ""}) =>
    `
<div class="select-box-container ${classes}" id="${id}">
    <div class="select-box-header">
        <div class="header">${header}</div>
         <i class="fa fa-question-circle-o" aria-hidden="true" title="bla-bla-bla..."></i>
         ${required ? '<span class="required">Обязательно</span>': ''}
    </div>
    <div class="select-box-search">
        ${selectBox}
    </div>
    
    ${tmpl_selectBoxOptions()}
    ${tmpl_selectBoxResults()}
    <span style="clear: both"></span>
</div>
`;
let tmpl_light = (el) => `<span class="highlight">${el}</span>`;

let tmpl_preloader = () => `<div id="component-preloader"><span class="spinner"></span></div>`;

export default class AbsBaseSelect {
    constructor($container, {url, obj, hasEditableContainer = false, visible = false, required = false}) {
        if (new.target === AbsBaseSelect) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        if (obj && url) {
            throw new URIError("Must be either the date or url");
        }

        const self = this;
        //TODO: проверка наличия id контейнера
        this.containerId = $container.attr("id");
        this.$container = $container;
        this.hasEditableContainer = hasEditableContainer;
        this.visible = visible;
        this.required = required;
        // Быстрая заглушка, до отображения данных
        if (visible) {
            let preloaderTemplate = tmpl_preloader();
            let selectBox = this.hasEditableContainer
                ? tmpl_selectBoxEditCont({preloaderTemplate})
                : tmpl_selectBox({preloaderTemplate});
            let plugTemplate = tmpl_plug({header: "Loading...", selectBox});
            $container.html(plugTemplate);

            this.$preloader = $container.find('#component-preloader');
            this.$spinner = this.$preloader.find('.spinner');
        }
        if (url) {
            this.dataPromise = this.getData(url);
            //TODO: дописать нормальную обработку url
            this.type = url.split("/")[2]
        }
        let _dataPromise;
        if (url) {
            _dataPromise = this.dataPromise;
        } else {
            _dataPromise = obj.dataPromise
        }
        // if (dataTree) this.dataTree = dataTree;
        _dataPromise
            .then(
                self._buildComponents.bind(self)
            )
            .catch(
                self._onLoadDataError.bind(self)
            );

        //  INIT EMPTY PROP
        this.selectedEl = {id: undefined, value: undefined};
        this.parentId = undefined;
    }

    getTemplate(classes) {
        let selectBox = this.hasEditableContainer ? tmpl_selectBoxEditCont() : tmpl_selectBox();
        classes = classes ? classes.join(" ") : "";
        return htmlTemplate({
            header: "", selectBox, required: this.required, id: this.containerId, classes,
            tmpl_selectBoxOptions, tmpl_selectBoxResults
        })
    }

    getData(url, data = {}) {
        const self = this;
        return Promise.resolve($.ajax({
            url: url,
            dataType: 'json',
            data: data,
            // success: self._buildComponents.bind(self),
            // error: self._onLoadDataError.bind(self),
        }))
    }

    hidePreloader() {
        // console.log("hide preloader ", this.$preloader);
        this.$spinner.fadeOut();
        return Promise.resolve(this.$preloader.delay(500).fadeOut(2000));
    }


    static getHeader(catChain, {separator = " / ", maxLen = 60}) {
        function toShortString(string, maxLen) {
            return string.slice(0, maxLen) + (string.length > maxLen ? "..." : "");
        }

        let strChain = "";

        catChain.forEach(function (el) {
            strChain = (maxLen ? toShortString(el.name, maxLen) : el.name) + (strChain ? separator : "") + strChain;
        });

        return strChain;
    }

    static highlight(string, sub_string, lastIndex = false) {
        let index = lastIndex
            ? string.toLowerCase().lastIndexOf(sub_string.toLowerCase())
            : string.toLowerCase().indexOf(sub_string.toLowerCase());
        if (index === -1) return string;
        let before, select, after;
        if (lastIndex) {
            [before, select, after] = [string.slice(0, index),
                string.slice(index, string.length),
                ""];
        } else {
            [before, select, after] = [string.slice(0, index),
                string.slice(index, index + sub_string.length),
                string.slice(index + sub_string.length)];
        }

        return `${before}${tmpl_light(select)}${after}`
    }

    clear() {
        this.$searchInput.val("");
        this.$optionsBox.hide();
        this.$resultsBox.hide();
        this.$buttonAdd.hide();
        this.$buttonAddOptions.hide();
        this.selectedEl = {id: undefined, value: undefined};
        if (this.hasEditableContainer) {
            this.$editableContainer.html("");
            this.$editableContainer.hide()
        }
        this.$searchInput.removeClass("active");
    }

    hide() {
        this.$selectBox.hide();
    }

    show() {
        this.$selectBox.show();
    }

    setHeader(header) {

        if (this.$header) {
            this.$header.find('.header').html(header);
            this.$header.show();
        } else {
            this.header = header
        }
        // default hide
        // this.show();
    }

    setParent(parentId) {
        this.parentId = parentId;
        this._fillOptionsData();
    }

    connectSelectedContainer(selectedContainer) {
        this.selectedContainer = selectedContainer;
    }

    getIdsSelectedElements() {
        let allChecked = this.$resultsBox.find(":checked");
        return allChecked.map(function () {
            return $(this).data("id");
        })
    }

    updateEditableContainer(elId) {
        // Если нет контейнера для отображения ...
        if (this.$editableContainer.length) {
            const separator = ' / ';
            let chainHeader = AbsBaseSelect.getHeader(this.dataTree.getSpecChain(elId, true), {separator: separator});
            chainHeader = AbsBaseSelect.highlight(chainHeader, separator, true);
            let elTemplate = `<span class="">${chainHeader}</span>`;
            this.$editableContainer.html(elTemplate);
            this.$editableContainer.show();
            return;
        }
        //..., передаем отображение предыдущему selectBox
        if (this.prevSelectBox) this.prevSelectBox.updateEditableContainer(elId);
    }


    _buildComponents(data) {
        // AFTER PRELOAD
        // this.hidePreloader().then(() => console.log("END -)"));
        let classes = this.$container.attr('class');
        if (classes) classes = classes.split(/\s+/);
        let template = this.getTemplate(classes);
        this.$container.replaceWith(template);

        this.$selectBox = $(`#${this.containerId}`);
        this.$header = this.$selectBox.find('.select-box-header');
        this.$header.find('.header').html(this.header);
        if (!this.header) this.$header.hide();
        this.$resultsBox = this.$selectBox.find('.select-box-results');
        this.$optionsBox = this.$selectBox.find('.select-box-options');
        this.$searchInput = this.$selectBox.find('input.select-box-search');
        this.$buttonAdd = this.$selectBox.find('.button-add.results');
        this.$buttonAddOptions = this.$selectBox.find('.button-add.options');
        this.$editableContainer = this.$selectBox.find('.editable-container');
        this.$resultsBox.hide();
        this.$optionsBox.hide();
        this.$buttonAddOptions.hide();
        //TODO: реализовать нормальное show/hide + visible
        if (!this.visible) this.hide();
        //    TODO: сделать проверку на наличие всех нужных элементов и их корректый jq select

    }

    _fillOptionsData() {
        let self = this;
        let dataList = this.dataTree.dataToList(this.parentId);
        let $container = this.$optionsBox.find('ul');
        $container.html("");
        for (let el of dataList) {
            $container.append($(tmpl_elementOption(el)))
        }

        this.$selectBox.find('li').on("click", this._onclickOptionsElement.bind(self));

    }

    _search({searchText, parentCategoryId = null, attached = true, excludeCategoryId = null}) {
        // :FORMAT spec_list [{name, id}, ...]
        let specList = this.dataTree.dataToList(parentCategoryId, attached, excludeCategoryId);
        return specList.filter(function (el) {
            return el.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
        });
    }

    _fillContainer($container, template, {searchText = "", parentCategoryId = null, attached = true, excludeCategoryId = null}) {
        $container.html("");
        $('.other-part').show();
        let searchRes = this._search({searchText, parentCategoryId, attached, excludeCategoryId});
        if (!searchRes.length) {
            if ($container.closest('div').hasClass('main-part')) {
                $container.append('<li>Ничего не найдено</li>');
                this.$resultsBox.find('.button-add.results').hide();
            } else {
                $('.other-part').hide();
            }
            return;
        }
        this.$resultsBox.find('.button-add.results').show();
        for (let el of searchRes) {
            let header = AbsBaseSelect.getHeader(this.dataTree.getSpecChain(el.id), {});
            $container.append(template(AbsBaseSelect.highlight(el.name, searchText), el.id, header));
        }
    }

    _fillResultsData(searchText) {
        let self = this;
        // FILL RESULTS
        //    MAIN PART
        let $container = this.$resultsBox.find('.main-part ul');
        this._fillContainer($container, tmpl_elementResult, {searchText: searchText, parentCategoryId: self.parentId});

        //    OTHER PART
        // Если нет parentId, не нужно искать в других категориях
        if (!this.parentId) {
            $('.other-part').hide();
        } else {
            $container = this.$resultsBox.find('.other-part ul');
            this._fillContainer($container, tmpl_elementResult, {searchText: searchText, excludeCategoryId: self.parentId});
        }
        this.$resultsBox.find('div.header').hide();
        this.$resultsBox.find('li').on("mouseover", function (e) {
            $(e.target).children('.header').show(300);
            e.preventDefault();
        });


        this.$resultsBox.find('li').on("mouseout", function (event) {
            let e = event.toElement || event.relatedTarget;
            if (e.parentNode == this || e == this) {
                return;
            }
            $(this).find('.header').hide();
        })
    }

    setElementById(id) {
        let value = this.dataTree.getElementById(id).name;
        this.selectedEl.id = id;
        this.selectedEl.value = value;
        this.$searchInput.val(value);
        this.updateEditableContainer(id);
    }

    _onclickOptionsElement(e) {
        let id = $(e.target).data("id");
        this.setElementById(id);
        this.$buttonAddOptions.show();
        this.$optionsBox.hide();
    }

    _onButtonAddOptions(e) {
        this._addToSelectedContainer(this.selectedEl.id);
        this.clear();
        e.preventDefault();
        return false;
    }

    _onButtonAdd(e) {
        let self = this;

        this.getIdsSelectedElements().each(function () {
            self._addToSelectedContainer(this);
        });
        this.clear();
        e.preventDefault();
        return false;
    }

    _onLoadDataError(error) {
        console.log("Error loading data -->", error);
    }

    _addToSelectedContainer(id) {
        this.selectedContainer.add(id)
    }

    _onInput_searchInput(e) {
        this._fillResultsData(this.$searchInput.val());
        this.$resultsBox.show();
        this.$optionsBox.hide();
    }

    _onClick_searchInput(e) {
        this.$optionsBox.show();
        this.$resultsBox.hide();
        this.$searchInput.val("");
    }

    _bindEvents() {
        let self = this;
        $(document).click(function (event) {
            if ($(event.target).closest(`#${self.containerId}`).length) {
                return;
            }
            self._looseFocus();
        });
        // RESULTS BOX
        this.$searchInput.on("input", this._onInput_searchInput.bind(self));
        // OPTIONS BOX
        this.$searchInput.on("click", this._onClick_searchInput.bind(self));

        this.$buttonAdd.on("click", this._onButtonAdd.bind(self));

        this.$buttonAddOptions.on("click", this._onButtonAddOptions.bind(self))
    }

    _looseFocus() {
        this.$resultsBox.hide();
        this.$optionsBox.hide();
        if (!this.selectedEl.id) {
            this.$searchInput.val("");
        } else {
            this.$searchInput.val(this.selectedEl.value);
        }
    }
}

export {
    htmlTemplate,
    tmpl_plug, tmpl_elementOption, tmpl_preloader, tmpl_light, tmpl_elementResult,
    tmpl_selectBox, tmpl_selectBoxEditCont, tmpl_selectBoxResults, tmpl_selectBoxOptions,
    AbsBaseSelect
}