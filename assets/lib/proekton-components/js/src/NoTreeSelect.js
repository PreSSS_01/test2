import {AbsBaseSelect} from './base/AbsBaseSelect'
import NoTreeData from './data/NoTreeData'

export default class NoTreeSelect extends AbsBaseSelect{
    constructor($container, {url, obj, visible=true, required = false}){
        //TODO: сделать автоматическую передачу всех параметров родителю
        super($container, {url, obj, visible, required});
    }

    _buildComponents(data) {
        super._buildComponents(data);
        this.dataTree = this.dataTree || new NoTreeData(data.results || data);
        this.$buttonAddOptions.hide();
        this._fillOptionsData();
        this._bindEvents();
    }

    _onclickOptionsElement(e) {
        super._onclickOptionsElement(e);
        this.clear();
        let id = $(e.target).data("id");
        this.selectedContainer.add(id);
        e.preventDefault();
        return false;
    }

    _onButtonAddOptions(e) {
    //    pass
    }

}
