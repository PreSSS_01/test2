import {htmlTemplate, tmpl_selectBoxOptions, tmpl_selectBoxEditCont, tmpl_selectBox} from './base/AbsBaseSelect'
import TreeSelect from './TreeSelect'

const tmpl_selectBoxResults = () =>
    `
    <div class="select-box-results">
        <div class="box-wrapper">
            <div class="main-part">
                <ul>
                </ul>
            </div>
        </div>
    </div>
`;

const tmpl_elementResult = (el, id, header) =>
    `<li data-id="${id}">
        ${el}
    </li>`;
/**
 * Поиск только в своей категории, отсутствует множественный выбор
 */
export default class SingleTreeSelect extends TreeSelect {
    getTemplate(classes) {
        let selectBox = this.hasEditableContainer ? tmpl_selectBoxEditCont() : tmpl_selectBox();
        classes = classes ? classes.join(" ") : "";
        return htmlTemplate({
            header: "TestHeader", selectBox, id: this.containerId, classes,
            tmpl_selectBoxOptions, tmpl_selectBoxResults
        })
    }

    _fillResultsData(searchText) {
        let self = this;
        // FILL RESULTS
        //    MAIN PART
        let $container = this.$resultsBox.find('.main-part ul');
        this._fillContainer($container, tmpl_elementResult, {searchText: searchText, parentCategoryId: self.parentId, attached: false});

        this.$resultsBox.find('li').on("click", function (e) {
            const id = $(e.target).data("id");
            self.selectedEl.id = id;
            self.selectedEl.value = self.dataTree.getElementById(id).name;
            self.$searchInput.val(self.selectedEl.value);
            self.updateEditableContainer(id);
            if (self.$buttonAddOptions) self.$buttonAddOptions.show();
            self.$resultsBox.hide();

            //TODO: duplicate code
            if (self.nextSelectBox && self.dataTree.hasChildren(self.selectedEl.id)) {
                self.nextSelectBox.setParent(self.selectedEl.id);
                self.nextSelectBox.setHeader(self.selectedEl.value);
                self.nextSelectBox.show();
            }
            if (self.prevSelectBox) {
                self.prevSelectBox.$buttonAddOptions.hide();
                self.prevSelectBox.$searchInput.removeClass("active");
            }
            self.$searchInput.addClass('active');
        })
    }
}