import SelectedContainer from './SelectedContainer'
import onBind from './decorators'

/**
 * Контэйнер - позволяющий принимать/отправлять новый объект для создания
 */
//TODO: попробовать реализовать как Mixin
export default class SelectedContainerCreate extends SelectedContainer {
    @onBind
    add(_id, max_len) {
        console.log("_id = ", _id);
        const self = this;
        if (_id.text) {
            this._removeById(this.elements_id[0]);
            this.elements_id = [0];
            this.$input_id = this.$self.find('input[type="hidden"].-id');
            this.$input_id.val("");
            this.$input_name = this.$self.find('input[type="hidden"].-name');
            this.$input_name.val(_id.text);
            this.$self.append(SelectedContainer.getTemplate("", _id.text, 0));
            this.btn_remove = this.$self.find('.icon-remove');
            this.btn_remove.on("click", this.remove.bind(self));
            if (this.elements_id.length) this.$self.show();
            return
        }
        super.add(_id, max_len)
    }
}