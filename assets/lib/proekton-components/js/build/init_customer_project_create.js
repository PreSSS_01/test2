(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _AbsBaseSelect2 = require('./base/AbsBaseSelect');

var _NoTreeData = require('./data/NoTreeData');

var _NoTreeData2 = _interopRequireDefault(_NoTreeData);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NoTreeSelect = function (_AbsBaseSelect) {
    _inherits(NoTreeSelect, _AbsBaseSelect);

    function NoTreeSelect($container, _ref) {
        var url = _ref.url,
            obj = _ref.obj,
            _ref$visible = _ref.visible,
            visible = _ref$visible === undefined ? true : _ref$visible;

        _classCallCheck(this, NoTreeSelect);

        //TODO: сделать автоматическую передачу всех параметров родителю
        return _possibleConstructorReturn(this, (NoTreeSelect.__proto__ || Object.getPrototypeOf(NoTreeSelect)).call(this, $container, { url: url, obj: obj, visible: visible }));
    }

    _createClass(NoTreeSelect, [{
        key: '_buildComponents',
        value: function _buildComponents(data) {
            _get(NoTreeSelect.prototype.__proto__ || Object.getPrototypeOf(NoTreeSelect.prototype), '_buildComponents', this).call(this, data);
            this.dataTree = this.dataTree || new _NoTreeData2.default(data.results);
            this.$buttonAddOptions.hide();
            this._fillOptionsData();
            this._bindEvents();
        }
    }, {
        key: '_onclickOptionsElement',
        value: function _onclickOptionsElement(e) {
            this.selectedEl.id = $(e.target).data("id");
            this.selectedEl.value = $(e.target).html();
            this.$searchInput.val($(e.target).html());

            this.$buttonAddOptions.show();
            this.$optionsBox.hide();

            this.selectedContainer.add(this.selectedEl.id);
            this.clear();
            e.preventDefault();
            return false;
        }
    }, {
        key: '_onButtonAddOptions',
        value: function _onButtonAddOptions(e) {
            //    pass
        }
    }]);

    return NoTreeSelect;
}(_AbsBaseSelect2.AbsBaseSelect);

exports.default = NoTreeSelect;

},{"./base/AbsBaseSelect":5,"./data/NoTreeData":7}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class; // `


var _DataTree = require('./data/DataTree');

var _DataTree2 = _interopRequireDefault(_DataTree);

var _NoTreeData = require('./data/NoTreeData');

var _NoTreeData2 = _interopRequireDefault(_NoTreeData);

var _decorators = require('./decorators');

var _decorators2 = _interopRequireDefault(_decorators);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}

var tmpl_selectedElement = function tmpl_selectedElement(header, name, id) {
    return '\n            <div class="selected-element">\n                <div class="header">\n                    ' + header + '\n                </div>\n                <div class="name">\n                    ' + name + '\n                </div>\n                <span data-id="' + id + '" class="icon-remove"></span>\n            </div>\n';
};

var SelectedContainer = (_class = function () {
    function SelectedContainer($container, _ref) {
        var _this = this;

        var obj = _ref.obj,
            _ref$noTree = _ref.noTree,
            noTree = _ref$noTree === undefined ? false : _ref$noTree,
            _ref$onlyOne = _ref.onlyOne,
            onlyOne = _ref$onlyOne === undefined ? false : _ref$onlyOne;

        _classCallCheck(this, SelectedContainer);

        // TODO: rename variables to camelCase
        this.$self = $container;
        this.elements_id = []; // [spec_id, spec_id, ...]
        this.onlyOne = onlyOne;
        var self = this;
        this.$self.hide();

        obj.dataPromise.then(function (data) {
            _this.dataTree = noTree ? new _NoTreeData2.default(data.results) : new _DataTree2.default(data.results);
            _this.$input = _this.$self.find('input[type="hidden"]');
            _this.restoreElements();
        }).catch(self._onLoadDataError.bind(self));
    }

    _createClass(SelectedContainer, [{
        key: 'restoreElements',
        value: function restoreElements() {
            var self = this;
            if (this.$input && this.$input.val()) {

                var clearString = this.$input.val().replace(/[\[\]\'\'\"\"]/g, '');
                var data = clearString.split(',').filter(function (el) {
                    return el;
                });
                this.elements_id = [];
                if (this.$input) this.$input.val(this.elements_id.join(','));
                data.forEach(function (el) {
                    return self.add(el);
                });
            }
        }
    }, {
        key: 'on',
        value: function on(methodName, func) {
            this[methodName] = this[methodName].bind(this, { func: func, bindFunc: true });
        }
    }, {
        key: '_removeById',
        value: function _removeById(id) {
            var index = this.elements_id.indexOf(id);
            if (index >= 0) {
                this.elements_id.splice(index, 1);
            }
            this.$self.find('span[data-id=\'' + id + '\']').parents('.selected-element').remove();
        }
    }, {
        key: '_onLoadDataError',
        value: function _onLoadDataError(error) {
            console.log("Error loading data -->", error);
        }
    }, {
        key: 'remove',
        value: function remove(e) {
            var spec_id = $(e.target).data("id");
            this._removeById(spec_id);
            if (this.$input) this.$input.val(this.elements_id.join(','));
            if (!this.elements_id.length) this.$self.hide();
            e.preventDefault();
        }
    }, {
        key: 'replace',
        value: function replace(_id, max_len) {
            var id = Number(_id);
            if (this.elements_id.length > 1) throw new RangeError("Replace error: more than one element");
            // Remove old
            this._removeById(this.elements_id[0]);
            //Add new
            this._addElementToHtml(id, max_len);
            this.elements_id = [id];
        }
    }, {
        key: '_addElementToHtml',
        value: function _addElementToHtml(id, max_len) {
            var self = this;
            var header = SelectedContainer.getHeader(this.dataTree.getSpecChain(id), "", max_len);
            var name = this.dataTree.getElementById(id).name;
            this.elements_id.push(id);
            if (this.$input) this.$input.val(this.elements_id.join(','));
            this.$self.append(SelectedContainer.getTemplate(header || "&nbsp;", name, id));
            this.btn_remove = this.$self.find('.icon-remove');
            this.btn_remove.on("click", this.remove.bind(self));
            if (this.elements_id.length) this.$self.show();
        }
    }, {
        key: 'add',
        value: function add(_id, max_len) {
            var id = Number(_id);
            var self = this;
            if (this.onlyOne) {
                this.replace(_id, max_len);
                return;
            }

            var has_already = this.elements_id.filter(function (el) {
                return self.dataTree.isChild(el, id);
            });

            if (has_already.length || this.elements_id.indexOf(Number(id)) != -1) {
                //TODO: do popup messages
                return;
            }

            var not_valid = this.elements_id.filter(function (el) {
                return self.dataTree.isChild(id, el);
            });

            not_valid.forEach(function (el) {
                self._removeById(el);
            });
            this._addElementToHtml(id, max_len);
        }
    }], [{
        key: 'getTemplate',
        value: function getTemplate(header, name, id) {
            return tmpl_selectedElement(header, name, id);
        }
    }, {
        key: 'getHeader',
        value: function getHeader(spec_chain, separator, max_len) {
            function toShortString(string, max_len) {
                return string.slice(0, max_len) + (string.length > max_len ? "..." : "");
            }

            separator = separator || ' / ';
            var str_chain = "";

            spec_chain.forEach(function (el) {
                str_chain = (max_len ? toShortString(el.name, max_len) : el.name) + (str_chain ? separator : "") + str_chain;
            });

            return str_chain;
        }
    }]);

    return SelectedContainer;
}(), (_applyDecoratedDescriptor(_class.prototype, 'remove', [_decorators2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'remove'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'add', [_decorators2.default], Object.getOwnPropertyDescriptor(_class.prototype, 'add'), _class.prototype)), _class);
exports.default = SelectedContainer;

},{"./data/DataTree":6,"./data/NoTreeData":7,"./decorators":8}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AbsBaseSelect = require('./base/AbsBaseSelect');

var _TreeSelect2 = require('./TreeSelect');

var _TreeSelect3 = _interopRequireDefault(_TreeSelect2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var tmpl_selectBoxResults = function tmpl_selectBoxResults() {
    return '\n    <div class="select-box-results">\n        <div class="box-wrapper">\n            <div class="main-part">\n                <ul>\n                </ul>\n            </div>\n        </div>\n    </div>\n';
};

var tmpl_elementResult = function tmpl_elementResult(el, id, header) {
    return '<li data-id="' + id + '">\n        ' + el + '\n    </li>';
};

var SingleTreeSelect = function (_TreeSelect) {
    _inherits(SingleTreeSelect, _TreeSelect);

    function SingleTreeSelect() {
        _classCallCheck(this, SingleTreeSelect);

        return _possibleConstructorReturn(this, (SingleTreeSelect.__proto__ || Object.getPrototypeOf(SingleTreeSelect)).apply(this, arguments));
    }

    _createClass(SingleTreeSelect, [{
        key: 'getTemplate',
        value: function getTemplate(classes) {
            var selectBox = this.hasEditableContainer ? (0, _AbsBaseSelect.tmpl_selectBoxEditCont)() : (0, _AbsBaseSelect.tmpl_selectBox)();
            classes = classes ? classes.join(" ") : "";
            return (0, _AbsBaseSelect.htmlTemplate)({
                header: "TestHeader", selectBox: selectBox, id: this.containerId, classes: classes,
                tmpl_selectBoxOptions: _AbsBaseSelect.tmpl_selectBoxOptions, tmpl_selectBoxResults: tmpl_selectBoxResults
            });
        }
    }, {
        key: '_fillResultsData',
        value: function _fillResultsData(searchText) {
            var self = this;
            // FILL RESULTS
            //    MAIN PART
            var $container = this.$resultsBox.find('.main-part ul');
            this._fillContainer($container, tmpl_elementResult, { searchText: searchText, parentCategoryId: self.parentId, attached: false });

            this.$resultsBox.find('li').on("click", function (e) {
                var id = $(e.target).data("id");
                self.selectedEl.id = id;
                self.selectedEl.value = self.dataTree.getElementById(id).name;
                self.$searchInput.val(self.selectedEl.value);
                self.updateEditableContainer(id);
                if (self.$buttonAddOptions) self.$buttonAddOptions.show();
                self.$resultsBox.hide();

                //TODO: duplicate code
                if (self.nextSelectBox && self.dataTree.hasChildren(self.selectedEl.id)) {
                    self.nextSelectBox.setParent(self.selectedEl.id);
                    self.nextSelectBox.setHeader(self.selectedEl.value);
                    self.nextSelectBox.show();
                }
                if (self.prevSelectBox) {
                    self.prevSelectBox.$buttonAddOptions.hide();
                    self.prevSelectBox.$searchInput.removeClass("active");
                }
                self.$searchInput.addClass('active');
            });
        }
    }]);

    return SingleTreeSelect;
}(_TreeSelect3.default);

exports.default = SingleTreeSelect;

},{"./TreeSelect":4,"./base/AbsBaseSelect":5}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _AbsBaseSelect2 = require('./base/AbsBaseSelect');

var _DataTree = require('./data/DataTree');

var _DataTree2 = _interopRequireDefault(_DataTree);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TreeSelect = function (_AbsBaseSelect) {
    _inherits(TreeSelect, _AbsBaseSelect);

    function TreeSelect($container, _ref) {
        var url = _ref.url,
            obj = _ref.obj,
            _ref$hasEditableConta = _ref.hasEditableContainer,
            hasEditableContainer = _ref$hasEditableConta === undefined ? false : _ref$hasEditableConta,
            _ref$visible = _ref.visible,
            visible = _ref$visible === undefined ? hasEditableContainer : _ref$visible;

        _classCallCheck(this, TreeSelect);

        //TODO: сделать автоматическую передачу всех параметров родителю
        return _possibleConstructorReturn(this, (TreeSelect.__proto__ || Object.getPrototypeOf(TreeSelect)).call(this, $container, { url: url, obj: obj, hasEditableContainer: hasEditableContainer, visible: visible }));
    }

    _createClass(TreeSelect, [{
        key: 'setNearbySelectBox',
        value: function setNearbySelectBox(next, prev) {
            this.nextSelectBox = next;
            this.prevSelectBox = prev;
        }
    }, {
        key: 'clearAllNext',
        value: function clearAllNext() {
            this.clear();
            if (this.nextSelectBox) {
                this.nextSelectBox.hide();
                this.nextSelectBox.clearAllNext();
            }
        }
    }, {
        key: 'clearAllPrev',
        value: function clearAllPrev() {
            this.clear();
            if (this.prevSelectBox) {
                this.clear();
                this.hide();
                this.prevSelectBox.clearAllPrev();
            }
        }
    }, {
        key: '_buildComponents',
        value: function _buildComponents(data) {
            _get(TreeSelect.prototype.__proto__ || Object.getPrototypeOf(TreeSelect.prototype), '_buildComponents', this).call(this, data);
            //TODO: Изменять свойство visible при show/hide
            if (!this.visible) this.hide();
            if (this.hasEditableContainer) this.$editableContainer.hide();
            this.dataTree = this.dataTree || new _DataTree2.default(data.results);
            this._fillOptionsData();
            this._bindEvents();
        }
    }, {
        key: '_onclickOptionsElement',
        value: function _onclickOptionsElement(e) {
            this.clearAllNext();
            _get(TreeSelect.prototype.__proto__ || Object.getPrototypeOf(TreeSelect.prototype), '_onclickOptionsElement', this).call(this, e);
            if (this.nextSelectBox && this.dataTree.hasChildren(this.selectedEl.id)) {
                this.nextSelectBox.setParent(this.selectedEl.id);
                this.nextSelectBox.setHeader(this.selectedEl.value);
                this.nextSelectBox.show();
            }
            if (this.prevSelectBox) {
                this.prevSelectBox.$buttonAddOptions.hide();
                this.prevSelectBox.$searchInput.removeClass("active");
            }
            this.$searchInput.addClass('active');
        }
    }, {
        key: '_onButtonAddOptions',
        value: function _onButtonAddOptions(e) {
            // this._addToSelectedContainer(this.selectedEl.id);
            // this.clear();
            // e.preventDefault();
            // return false;
            _get(TreeSelect.prototype.__proto__ || Object.getPrototypeOf(TreeSelect.prototype), '_onButtonAddOptions', this).call(this, e);
            this.clearAllNext();
            this.clearAllPrev();
        }
    }, {
        key: '_onButtonAdd',
        value: function _onButtonAdd(e) {
            _get(TreeSelect.prototype.__proto__ || Object.getPrototypeOf(TreeSelect.prototype), '_onButtonAdd', this).call(this, e);
            this.clearAllNext();
            this.clearAllPrev();
        }
    }, {
        key: '_addToSelectedContainer',
        value: function _addToSelectedContainer(id) {
            if (this.selectedContainer) {
                this.selectedContainer.add(id);
                return;
            }

            this.prevSelectBox._addToSelectedContainer(id);
        }
    }]);

    return TreeSelect;
}(_AbsBaseSelect2.AbsBaseSelect);

exports.default = TreeSelect;

},{"./base/AbsBaseSelect":5,"./data/DataTree":6}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//TEMPLATES `
var tmpl_selectBoxEditCont = function tmpl_selectBoxEditCont() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$preloaderTemplat = _ref.preloaderTemplate,
        preloaderTemplate = _ref$preloaderTemplat === undefined ? "" : _ref$preloaderTemplat;

    return "\n            <div class=\"row\">\n            <div class=\"col-lg-3\">\n                \n                <div style=\"width: 100%; position: relative\">\n                    " + preloaderTemplate + "\n                    <input class=\"select-box-search\" type=\"text\" placeholder=\"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435/\u041F\u043E\u0438\u0441\u043A\">\n                    <button style=\"display: none\" class=\"button-add options\">\u041F\u0420\u0418\u041C\u0415\u041D\u0418\u0422\u042C</button>\n                </div>\n                </div>\n            <div class=\"col-lg-9\">\n                <span style=\"display: none\" class=\"editable-container\"></span>\n            </div>\n            </div>\n";
};

var tmpl_selectBox = function tmpl_selectBox() {
    var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref2$preloaderTempla = _ref2.preloaderTemplate,
        preloaderTemplate = _ref2$preloaderTempla === undefined ? "" : _ref2$preloaderTempla;

    return "        \n            " + preloaderTemplate + "\n            <input class=\"select-box-search\" type=\"text\" placeholder=\"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435/\u041F\u043E\u0438\u0441\u043A\">\n            <button style=\"display: none\" class=\"button-add options\">\u041F\u0420\u0418\u041C\u0415\u041D\u0418\u0422\u042C</button>\n";
};

var tmpl_elementResult = function tmpl_elementResult(el, id, header) {
    return "<li><label>\n        <input type=\"checkbox\" data-id=\"" + id + "\">" + el + "\n        <div class=\"header\">" + header + "</div>\n    </label></li>";
};

var tmpl_elementOption = function tmpl_elementOption(el) {
    return "<li data-id=\"" + el.id + "\">" + el.name + "</li>";
};

var tmpl_plug = function tmpl_plug(_ref3) {
    var header = _ref3.header,
        selectBox = _ref3.selectBox;
    return htmlTemplate({ header: header, selectBox: selectBox });
};

var tmpl_selectBoxOptions = function tmpl_selectBoxOptions() {
    return "\n    <div class=\"select-box-options\">\n        <div class=\"box-wrapper\">\n            <ul>\n            \n            </ul>\n        </div>\n    </div>\n";
};

var tmpl_selectBoxResults = function tmpl_selectBoxResults() {
    return "\n    <div class=\"select-box-results\">\n        <div class=\"box-wrapper\">\n            <div class=\"main-part\">\n                <ul>\n                </ul>\n            </div>\n            <div class=\"other-part\">\n                <span class=\"other-header\">\u0418\u0437 \u0434\u0440\u0443\u0433\u0438\u0445 \u043A\u0430\u0442\u0435\u0433\u043E\u0440\u0438\u0439</span>\n                <ul>\n                </ul>\n            </div>\n        </div>\n        <div style=\"padding-top: 5px\">\n        <button class=\"button-add results\">\u041F\u0420\u0418\u041C\u0415\u041D\u0418\u0422\u042C</button>\n        </div>\n    </div>\n";
};

var htmlTemplate = function htmlTemplate(_ref4) {
    var header = _ref4.header,
        selectBox = _ref4.selectBox,
        _ref4$id = _ref4.id,
        id = _ref4$id === undefined ? "" : _ref4$id,
        _ref4$classes = _ref4.classes,
        classes = _ref4$classes === undefined ? "" : _ref4$classes,
        _ref4$tmpl_selectBoxO = _ref4.tmpl_selectBoxOptions,
        tmpl_selectBoxOptions = _ref4$tmpl_selectBoxO === undefined ? function () {
        return "";
    } : _ref4$tmpl_selectBoxO,
        _ref4$tmpl_selectBoxR = _ref4.tmpl_selectBoxResults,
        tmpl_selectBoxResults = _ref4$tmpl_selectBoxR === undefined ? function () {
        return "";
    } : _ref4$tmpl_selectBoxR;
    return "\n<div class=\"select-box-container " + classes + "\" id=\"" + id + "\">\n    <div class=\"select-box-header\">\n        <div class=\"header\">" + header + "</div>\n         <i class=\"fa fa-question-circle-o\" aria-hidden=\"true\" title=\"bla-bla-bla...\"></i>\n    </div>\n    <div class=\"select-box-search\">\n        " + selectBox + "\n    </div>\n    \n    " + tmpl_selectBoxOptions() + "\n    " + tmpl_selectBoxResults() + "\n    <span style=\"clear: both\"></span>\n</div>\n";
};
var tmpl_light = function tmpl_light(el) {
    return "<span class=\"highlight\">" + el + "</span>";
};

var tmpl_preloader = function tmpl_preloader() {
    return "<div id=\"component-preloader\"><span class=\"spinner\"></span></div>";
};

var AbsBaseSelect = function () {
    function AbsBaseSelect($container, _ref5) {
        var url = _ref5.url,
            obj = _ref5.obj,
            _ref5$hasEditableCont = _ref5.hasEditableContainer,
            hasEditableContainer = _ref5$hasEditableCont === undefined ? false : _ref5$hasEditableCont,
            _ref5$visible = _ref5.visible,
            visible = _ref5$visible === undefined ? false : _ref5$visible;

        _classCallCheck(this, AbsBaseSelect);

        if (new.target === AbsBaseSelect) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        if (obj && url) {
            throw new URIError("Must be either the date or url");
        }

        var self = this;
        //TODO: проверка наличия id контейнера
        this.containerId = $container.attr("id");
        this.$container = $container;
        this.hasEditableContainer = hasEditableContainer;
        this.visible = visible;
        // Быстрая заглушка, до отображения данных
        if (visible) {
            var preloaderTemplate = tmpl_preloader();
            var selectBox = this.hasEditableContainer ? tmpl_selectBoxEditCont({ preloaderTemplate: preloaderTemplate }) : tmpl_selectBox({ preloaderTemplate: preloaderTemplate });
            var plugTemplate = tmpl_plug({ header: "Loading...", selectBox: selectBox });
            $container.html(plugTemplate);

            this.$preloader = $container.find('#component-preloader');
            this.$spinner = this.$preloader.find('.spinner');
        }
        if (url) this.dataPromise = this.getData(url);
        var _dataPromise = void 0;
        if (url) {
            _dataPromise = this.dataPromise;
        } else {
            _dataPromise = obj.dataPromise;
        }
        // if (dataTree) this.dataTree = dataTree;
        _dataPromise.then(self._buildComponents.bind(self)).catch(self._onLoadDataError.bind(self));

        //  INIT EMPTY PROP
        this.selectedEl = { id: undefined, value: undefined };
        this.parentId = undefined;
    }

    _createClass(AbsBaseSelect, [{
        key: "getTemplate",
        value: function getTemplate(classes) {
            var selectBox = this.hasEditableContainer ? tmpl_selectBoxEditCont() : tmpl_selectBox();
            classes = classes ? classes.join(" ") : "";
            return htmlTemplate({
                header: "TestHeader", selectBox: selectBox, id: this.containerId, classes: classes,
                tmpl_selectBoxOptions: tmpl_selectBoxOptions, tmpl_selectBoxResults: tmpl_selectBoxResults
            });
        }
    }, {
        key: "getData",
        value: function getData(url) {
            var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            var self = this;
            return Promise.resolve($.ajax({
                url: url,
                dataType: 'json',
                data: data
            }));
        }
    }, {
        key: "hidePreloader",
        value: function hidePreloader() {
            // console.log("hide preloader ", this.$preloader);
            this.$spinner.fadeOut();
            return Promise.resolve(this.$preloader.delay(500).fadeOut(2000));
        }
    }, {
        key: "clear",
        value: function clear() {
            this.$searchInput.val("");
            this.$optionsBox.hide();
            this.$resultsBox.hide();
            this.$buttonAdd.hide();
            this.$buttonAddOptions.hide();
            this.selectedEl = { id: undefined, value: undefined };
            if (this.hasEditableContainer) {
                this.$editableContainer.html("");
                this.$editableContainer.hide();
            }
            this.$searchInput.removeClass("active");
        }
    }, {
        key: "hide",
        value: function hide() {
            this.$selectBox.hide();
        }
    }, {
        key: "show",
        value: function show() {
            this.$selectBox.show();
        }
    }, {
        key: "setHeader",
        value: function setHeader(header) {

            if (this.$header) {
                this.$header.html(header);
            } else {
                this.header = header;
            }
            // default hide
            // this.show();
        }
    }, {
        key: "setParent",
        value: function setParent(parentId) {
            this.parentId = parentId;
            this._fillOptionsData();
        }
    }, {
        key: "connectSelectedContainer",
        value: function connectSelectedContainer(selectedContainer) {
            this.selectedContainer = selectedContainer;
        }
    }, {
        key: "getIdsSelectedElements",
        value: function getIdsSelectedElements() {
            var allChecked = this.$resultsBox.find(":checked");
            return allChecked.map(function () {
                return $(this).data("id");
            });
        }
    }, {
        key: "updateEditableContainer",
        value: function updateEditableContainer(elId) {
            // Если нет контейнера для отображения ...
            if (this.$editableContainer.length) {
                var separator = ' / ';
                var chainHeader = AbsBaseSelect.getHeader(this.dataTree.getSpecChain(elId, true), { separator: separator });
                chainHeader = AbsBaseSelect.highlight(chainHeader, separator, true);
                var elTemplate = "<span class=\"\">" + chainHeader + "</span>";
                this.$editableContainer.html(elTemplate);
                this.$editableContainer.show();
                return;
            }
            //..., передаем отображение предыдущему selectBox
            if (this.prevSelectBox) this.prevSelectBox.updateEditableContainer(elId);
        }
    }, {
        key: "_buildComponents",
        value: function _buildComponents(data) {
            // AFTER PRELOAD
            // this.hidePreloader().then(() => console.log("END -)"));
            var classes = this.$container.attr('class');
            if (classes) classes = classes.split(/\s+/);
            var template = this.getTemplate(classes);
            this.$container.replaceWith(template);

            this.$selectBox = $("#" + this.containerId);
            this.$header = this.$selectBox.find('.select-box-header .header');
            this.$header.html(this.header);
            this.$resultsBox = this.$selectBox.find('.select-box-results');
            this.$optionsBox = this.$selectBox.find('.select-box-options');
            this.$searchInput = this.$selectBox.find('input.select-box-search');
            this.$buttonAdd = this.$selectBox.find('.button-add.results');
            this.$buttonAddOptions = this.$selectBox.find('.button-add.options');
            this.$editableContainer = this.$selectBox.find('.editable-container');
            this.$resultsBox.hide();
            this.$optionsBox.hide();
            this.$buttonAddOptions.hide();
            //    TODO: сделать проверку на наличие всех нужных элементов и их корректый jq select

            // REDEFINE IN CHILD
            // this.dataTree = data;
            // this._bindEvents();
        }
    }, {
        key: "_fillOptionsData",
        value: function _fillOptionsData() {
            var self = this;
            var dataList = this.dataTree.dataToList(this.parentId);
            var $container = this.$optionsBox.find('ul');
            $container.html("");
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = dataList[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var el = _step.value;

                    $container.append($(tmpl_elementOption(el)));
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            this.$selectBox.find('li').on("click", this._onclickOptionsElement.bind(self));
        }
    }, {
        key: "_search",
        value: function _search(_ref6) {
            var searchText = _ref6.searchText,
                _ref6$parentCategoryI = _ref6.parentCategoryId,
                parentCategoryId = _ref6$parentCategoryI === undefined ? null : _ref6$parentCategoryI,
                _ref6$attached = _ref6.attached,
                attached = _ref6$attached === undefined ? true : _ref6$attached,
                _ref6$excludeCategory = _ref6.excludeCategoryId,
                excludeCategoryId = _ref6$excludeCategory === undefined ? null : _ref6$excludeCategory;

            // :FORMAT spec_list [{name, id}, ...]
            var specList = this.dataTree.dataToList(parentCategoryId, attached, excludeCategoryId);
            return specList.filter(function (el) {
                return el.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
            });
        }
    }, {
        key: "_fillContainer",
        value: function _fillContainer($container, template, _ref7) {
            var _ref7$searchText = _ref7.searchText,
                searchText = _ref7$searchText === undefined ? "" : _ref7$searchText,
                _ref7$parentCategoryI = _ref7.parentCategoryId,
                parentCategoryId = _ref7$parentCategoryI === undefined ? null : _ref7$parentCategoryI,
                _ref7$attached = _ref7.attached,
                attached = _ref7$attached === undefined ? true : _ref7$attached,
                _ref7$excludeCategory = _ref7.excludeCategoryId,
                excludeCategoryId = _ref7$excludeCategory === undefined ? null : _ref7$excludeCategory;

            $container.html("");
            $('.other-part').show();
            var searchRes = this._search({ searchText: searchText, parentCategoryId: parentCategoryId, attached: attached, excludeCategoryId: excludeCategoryId });
            if (!searchRes.length) {
                if ($container.closest('div').hasClass('main-part')) {
                    $container.append('<li>Ничего не найдено</li>');
                    this.$resultsBox.find('.button-add.results').hide();
                } else {
                    $('.other-part').hide();
                }
                return;
            }
            this.$resultsBox.find('.button-add.results').show();
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = searchRes[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var el = _step2.value;

                    var header = AbsBaseSelect.getHeader(this.dataTree.getSpecChain(el.id), {});
                    $container.append(template(AbsBaseSelect.highlight(el.name, searchText), el.id, header));
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        }
    }, {
        key: "_fillResultsData",
        value: function _fillResultsData(searchText) {
            var self = this;
            // FILL RESULTS
            //    MAIN PART
            var $container = this.$resultsBox.find('.main-part ul');
            this._fillContainer($container, tmpl_elementResult, { searchText: searchText, parentCategoryId: self.parentId });

            //    OTHER PART
            // Если нет parentId, не нужно искать в других категориях
            if (!this.parentId) {
                $('.other-part').hide();
            } else {
                $container = this.$resultsBox.find('.other-part ul');
                this._fillContainer($container, tmpl_elementResult, { searchText: searchText, excludeCategoryId: self.parentId });
            }
            this.$resultsBox.find('div.header').hide();
            this.$resultsBox.find('li').on("mouseover", function (e) {
                $(e.target).children('.header').show(300);
                e.preventDefault();
            });

            this.$resultsBox.find('li').on("mouseout", function (event) {
                var e = event.toElement || event.relatedTarget;
                if (e.parentNode == this || e == this) {
                    return;
                }
                $(this).find('.header').hide();
            });
        }
    }, {
        key: "_onclickOptionsElement",
        value: function _onclickOptionsElement(e) {
            this.selectedEl.id = $(e.target).data("id");
            this.selectedEl.value = $(e.target).html();
            this.$searchInput.val($(e.target).html());
            this.updateEditableContainer($(e.target).data("id"));
            this.$buttonAddOptions.show();
            this.$optionsBox.hide();
        }
    }, {
        key: "_onButtonAddOptions",
        value: function _onButtonAddOptions(e) {
            this._addToSelectedContainer(this.selectedEl.id);
            this.clear();
            e.preventDefault();
            return false;
        }
    }, {
        key: "_onButtonAdd",
        value: function _onButtonAdd(e) {
            var self = this;

            this.getIdsSelectedElements().each(function () {
                self._addToSelectedContainer(this);
            });
            this.clear();
            e.preventDefault();
            return false;
        }
    }, {
        key: "_onLoadDataError",
        value: function _onLoadDataError(error) {
            console.log("Error loading data -->", error);
        }
    }, {
        key: "_addToSelectedContainer",
        value: function _addToSelectedContainer(id) {
            this.selectedContainer.add(id);
        }
    }, {
        key: "_bindEvents",
        value: function _bindEvents() {
            var self = this;
            $(document).click(function (event) {
                if ($(event.target).closest("#" + self.containerId).length) {
                    return;
                }
                self._looseFocus();
            });
            // RESULTS BOX
            this.$searchInput.on("input", function (e) {
                self._fillResultsData(self.$searchInput.val());
                self.$resultsBox.show();
                self.$optionsBox.hide();
            });
            // OPTIONS BOX
            this.$searchInput.on("click", function (e) {
                self.$optionsBox.show();
                self.$resultsBox.hide();
                self.$searchInput.val("");
            });

            this.$buttonAdd.on("click", function (e) {
                self._onButtonAdd(e);
            });

            this.$buttonAddOptions.on("click", this._onButtonAddOptions.bind(self));
        }
    }, {
        key: "_looseFocus",
        value: function _looseFocus() {
            this.$resultsBox.hide();
            this.$optionsBox.hide();
            if (!this.selectedEl.id) {
                this.$searchInput.val("");
            } else {
                this.$searchInput.val(this.selectedEl.value);
            }
        }
    }], [{
        key: "getHeader",
        value: function getHeader(catChain, _ref8) {
            var _ref8$separator = _ref8.separator,
                separator = _ref8$separator === undefined ? " / " : _ref8$separator,
                _ref8$maxLen = _ref8.maxLen,
                maxLen = _ref8$maxLen === undefined ? 60 : _ref8$maxLen;

            function toShortString(string, maxLen) {
                return string.slice(0, maxLen) + (string.length > maxLen ? "..." : "");
            }

            var strChain = "";

            catChain.forEach(function (el) {
                strChain = (maxLen ? toShortString(el.name, maxLen) : el.name) + (strChain ? separator : "") + strChain;
            });

            return strChain;
        }
    }, {
        key: "highlight",
        value: function highlight(string, sub_string) {
            var lastIndex = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            var index = lastIndex ? string.toLowerCase().lastIndexOf(sub_string.toLowerCase()) : string.toLowerCase().indexOf(sub_string.toLowerCase());
            if (index === -1) return string;
            var before = void 0,
                select = void 0,
                after = void 0;
            if (lastIndex) {
                var _ref9 = [string.slice(0, index), string.slice(index, string.length), ""];
                before = _ref9[0];
                select = _ref9[1];
                after = _ref9[2];
            } else {
                var _ref10 = [string.slice(0, index), string.slice(index, index + sub_string.length), string.slice(index + sub_string.length)];
                before = _ref10[0];
                select = _ref10[1];
                after = _ref10[2];
            }

            return "" + before + tmpl_light(select) + after;
        }
    }]);

    return AbsBaseSelect;
}();

exports.default = AbsBaseSelect;
exports.htmlTemplate = htmlTemplate;
exports.tmpl_plug = tmpl_plug;
exports.tmpl_elementOption = tmpl_elementOption;
exports.tmpl_preloader = tmpl_preloader;
exports.tmpl_light = tmpl_light;
exports.tmpl_elementResult = tmpl_elementResult;
exports.tmpl_selectBox = tmpl_selectBox;
exports.tmpl_selectBoxEditCont = tmpl_selectBoxEditCont;
exports.tmpl_selectBoxResults = tmpl_selectBoxResults;
exports.tmpl_selectBoxOptions = tmpl_selectBoxOptions;
exports.AbsBaseSelect = AbsBaseSelect;

},{}],6:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Node = function Node(data, tree) {
    _classCallCheck(this, Node);

    this.name = data.name;
    this.id = data.id;
    if (data.parent === null) {
        this.parent = "root";
        data.parent = { id: "root" };
        this.name = "";
    }
    if (data.parent.id && data.parent.id !== 'root') {
        var el = tree._getElementById(data.parent.id);
        this.parent = el.node || new Node(el, tree);
    }
    data.node = this;
    this.children = data.children.map(function (el_obj) {
        var el = tree._getElementById(el_obj.id);
        if (el.node) return el.node;
        el.node = new Node(el, tree);
        return el.node;
    });

    this.children = this.children || [];
};

var DataTree = function () {
    function DataTree(data) {
        _classCallCheck(this, DataTree);

        this.baseData = data;
        this._root = new Node(data[0], this);
    }

    /**
     * получить element в базовой структуре
     */


    _createClass(DataTree, [{
        key: "_getElementById",
        value: function _getElementById(id) {
            for (var i = 0; i < this.baseData.length; i++) {
                if (this.baseData[i].id == id) return this.baseData[i];
            }
        }

        /**
         * получить element в дереве
         */

    }, {
        key: "getElementById",
        value: function getElementById(id) {
            function searchInChildren(children) {
                for (var i = 0; i < children.length; i++) {
                    if (children[i].id == id) return children[i];
                    var res = searchInChildren(children[i].children);
                    if (res) return res;
                }
            }

            return searchInChildren(this._root.children);
        }

        /**
         * Является ли узел c el_id дочерним для parent_id
         * @param el_id
         * @param parent_id
         */

    }, {
        key: "isChild",
        value: function isChild(elId, parent_id) {
            function checkParent(el, parent) {
                if (el.parent == parent) return true;
                if (el.parent && el.parent != 'root') return checkParent(el.parent, parent);
                return false;
            }
            return checkParent(this.getElementById(elId), this.getElementById(parent_id));
        }
    }, {
        key: "hasChildren",
        value: function hasChildren(elId) {
            return this.getElementById(elId).children.length ? true : false;
        }

        /**
         * @param start_parent_id(number) - начиная с
         * @param attached(bool) - включая вложенные/дочерние
         * @param exclude_id - исключая узел c exclude_id и всеми его вложенными узлами
         * @returns [{name, id}, ...]
         */

    }, {
        key: "dataToList",
        value: function dataToList(start_parent_id, attached, exclude_id) {
            var data_list = [];

            function goInChildren(children) {
                for (var i = 0; i < children.length; i++) {
                    if (children[i].id == exclude_id) continue;
                    data_list.push({ name: children[i].name, id: children[i].id });
                    if (attached) goInChildren(children[i].children);
                }
            }
            var start = start_parent_id ? this.getElementById(start_parent_id) : this._root;
            goInChildren(start.children);
            return data_list;
        }

        /**
         *
         * @param id
         * @param incl(bool) - исключая сам элемент
         * @returns {Array} всех узлов/элементов от элемента с id до корня
         */

    }, {
        key: "getSpecChain",
        value: function getSpecChain(id) {
            var incl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var chain = [];
            var el = this.getElementById(id);
            function getParent(el) {
                if (el.parent && el.parent != "root") {
                    chain.push(el.parent);
                    getParent(el.parent);
                }
            }
            getParent(el);
            if (incl) chain.unshift(el);
            return chain;
        }
    }]);

    return DataTree;
}();

exports.default = DataTree;

},{}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var NoTreeData = function () {
    function NoTreeData(data) {
        _classCallCheck(this, NoTreeData);

        this.data = data;
        this.specChain = [];
    }

    _createClass(NoTreeData, [{
        key: "getElementById",
        value: function getElementById(id) {
            for (var i = 0; i < this.data.length; i++) {
                if (this.data[i].id == id) return this.data[i];
            }
        }
    }, {
        key: "getSpecChain",
        value: function getSpecChain(id, incl) {
            return this.specChain;
        }
    }, {
        key: "isChild",
        value: function isChild(el_id, parent_id) {
            return false;
        }
    }, {
        key: "hasChildren",
        value: function hasChildren() {
            return false;
        }
    }, {
        key: "dataToList",
        value: function dataToList() {
            return this.data;
        }
    }]);

    return NoTreeData;
}();

exports.default = NoTreeData;

},{}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = onBind;
function onBind(target, name, descriptor) {
    var method = descriptor.value;

    descriptor.value = function () {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        var binds = [];
        args = Array.from(args);
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = args.slice()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var arg = _step.value;

                // console.log("onBind -->", typeof arg, "arg = ", arg);
                // console.log("arg.func -->", typeof arg.originalEvent);
                // typeof arg === 'object' && !(arg.originalEvent)
                if (arg.bindFunc) {
                    binds.push(arg);
                    args.splice(args.indexOf(arg), 1);
                }
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        method.apply(this, args);
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = binds[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var bind = _step2.value;

                bind.func.bind(this)();
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }

        return this;
    };
}

// export {onBind};

},{}],9:[function(require,module,exports){
'use strict';

var _SelectedContainer = require('./SelectedContainer');

var _SelectedContainer2 = _interopRequireDefault(_SelectedContainer);

var _NoTreeSelect = require('./NoTreeSelect');

var _NoTreeSelect2 = _interopRequireDefault(_NoTreeSelect);

var _TreeSelect = require('./TreeSelect');

var _TreeSelect2 = _interopRequireDefault(_TreeSelect);

var _SingleTreeSelect = require('./SingleTreeSelect');

var _SingleTreeSelect2 = _interopRequireDefault(_SingleTreeSelect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

$(function () {
    function createFilterSpecs(url) {
        // SPECIALIZATIONS FILTER
        var sb_main = new _TreeSelect2.default($('#select-box-1'), { url: url, visible: true });
        sb_main.setHeader("Специальность");
        var select_container = new _SelectedContainer2.default($('#selected-spec'), { obj: sb_main });
        sb_main.connectSelectedContainer(select_container);
        var sb_1 = new _TreeSelect2.default($('#select-box-2'), { obj: sb_main });
        var sb_2 = new _TreeSelect2.default($('#select-box-3'), { obj: sb_main });
        var sb_3 = new _TreeSelect2.default($('#select-box-4'), { obj: sb_main });
        var sb_4 = new _TreeSelect2.default($('#select-box-5'), { obj: sb_main });

        sb_main.setNearbySelectBox(sb_1);
        sb_1.setNearbySelectBox(sb_2, sb_main);
        sb_2.setNearbySelectBox(sb_3, sb_1);
        sb_3.setNearbySelectBox(sb_4, sb_2);
        sb_4.setNearbySelectBox("", sb_3);
    }

    function createFilterBuildingClass(url) {
        // BUILDING-CLASSIFICATION FILTER
        var sb_build_main = new _TreeSelect2.default($('#sb-building-classification'), { url: url, visible: true });
        sb_build_main.setHeader("Классификация здания");

        var sb_build_1 = new _TreeSelect2.default($('#sb-building-sub-classification'), { obj: sb_build_main });

        var select_build_container = new _SelectedContainer2.default($('#selected-building-classification'), { obj: sb_build_main });
        sb_build_main.connectSelectedContainer(select_build_container);

        sb_build_main.setNearbySelectBox(sb_build_1);
        sb_build_1.setNearbySelectBox("", sb_build_main);
    }

    function createFilterConstructionType(url) {
        var sb_constr_main = new _NoTreeSelect2.default($('#sb-construction-type'), { url: url, visible: true });
        sb_constr_main.setHeader("Вид строительства");
        var select_constr_type = new _SelectedContainer2.default($('#selected-construction-type'), { obj: sb_constr_main, noTree: true });
        sb_constr_main.connectSelectedContainer(select_constr_type);
    }

    function createFilerLocations(url) {
        var sb_loc_main = new _SingleTreeSelect2.default($('#sb-location-1'), { url: url, visible: true });
        sb_loc_main.setHeader("Местоположение");
        var select_loc = new _SelectedContainer2.default($('#selected-location'), { obj: sb_loc_main, onlyOne: true });
        sb_loc_main.connectSelectedContainer(select_loc);
        var sb_loc_1 = new _SingleTreeSelect2.default($('#sb-location-2'), { obj: sb_loc_main });
        var sb_loc_2 = new _SingleTreeSelect2.default($('#sb-location-3'), { obj: sb_loc_main });

        sb_loc_main.setNearbySelectBox(sb_loc_1);
        sb_loc_1.setNearbySelectBox(sb_loc_2, sb_loc_main);
        sb_loc_2.setNearbySelectBox("", sb_loc_1);
    }

    createFilterSpecs('/api/specializations_flat');
    createFilterBuildingClass('/api/building_classifications');
    createFilterConstructionType('/api/construction_type');
    createFilerLocations('/api/locations_flat');
});

},{"./NoTreeSelect":1,"./SelectedContainer":2,"./SingleTreeSelect":3,"./TreeSelect":4}]},{},[9]);
