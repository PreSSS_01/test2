$(function () {
    let $buttonF1 = $('.resButton');
    if ($('.slide').hasClass("active")) $buttonF1.css('transform', 'rotate(0deg)');

    $buttonF1.on("click", function (e) {
        e.preventDefault();
        let $slide = $('.slide');
        if ($slide.hasClass("active")) {
            $buttonF1.css('transform', 'rotate(180deg)');
            $slide.slideUp(300);
        } else {
            $buttonF1.css('transform', 'rotate(0deg)');
            $slide.slideDown(300);
        }
        $slide.toggleClass("active");
    });

    //CUSTOM-CHECKBOX

    function tuneCheckBoxes($boxes) {
        let currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';
        $boxes.find("div").hide();
        $boxes.find("div." + currentState).show();
    }

    let $boxes = $('.custom-check');
    tuneCheckBoxes($boxes);
    $boxes.on("click", function (e) {
        let inside_checkBox = $(e.target).parent().find("input");
        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
        tuneCheckBoxes($boxes);
        e.preventDefault();
        return false;
    });

    // CUSTOM-SELECT
    let $select_container = $('.custom-select');
    let $sc_headers = $select_container.find('.simple-select');
    let $sc_options = $select_container.find('.select-box-options');

    $sc_options.hide();

    $sc_headers.on("click", function (e) {
        $(e.target).siblings('.select-box-options').show();
    });

    let $options = $sc_options.find('li');
    $options.on("click", function (e) {
        const target = $(e.target);
        let header = target.closest('.select-box-options').siblings('.simple-select');
        let data = target.closest('.select-box-options').siblings('input[type=hidden]');
        header.val(target.html());
        data.val(target.data("id"));
        // $sc_data.val($(e.target).data("id"));
        $sc_options.hide();
        e.preventDefault()
    });

    $(document).click(function (event) {
        //FIXME: запомнить на ком был клик, и не закрывать именно его
        if ($(event.target).closest($select_container).length) {
            return;
        }
        $sc_options.hide();
        //...
    });

    //* Edn CUSTOM SELECT


    // $("#myBtn").click(function () {
    //     $('<div class="alert alert-success alert-dismissable">' +
    //         '<button type="button" class="close" ' +
    //         'data-dismiss="alert" aria-hidden="true">' +
    //         '&times;' +
    //         '</button>' +
    //         'modal info...' +
    //         '</div>').appendTo("#alerts");
    // });
});

function getFormData($form, pageNum) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    indexed_array["page"] = pageNum || "";
    return indexed_array;
}

function modUrl($form, postfix) {
    let curLoc = '?' + $form.serialize() + (postfix || "");
    try {
        history.replaceState($form.serialize(), null, curLoc);
    } catch (e) {
        console.log("Error!");
    }
}

function sendData(url, $form, pageNum) {
    function updateResults(html) {
        $container.html(html);
    }

    const $container = $('#resultsBlock');
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'html',
        data: getFormData($form, pageNum),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
        },
        success: updateResults,
        error: (data) => console.log("Error", data)
    });
}

function sendFilterData(url) {
    const $form = $('#filter-form');
    sendData('/projects/', $form);
    modUrl($form);
    return false;
}