/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _file_upload = __webpack_require__(35);

	var _extended_field = __webpack_require__(32);

	var _custom_check = __webpack_require__(31);

	var _read_more = __webpack_require__(36);

	var _only_one_checkbox = __webpack_require__(37);

	var _popups = __webpack_require__(38);

	var _scroll_on_required = __webpack_require__(39);

	var _ajax_registration = __webpack_require__(40);

	var _test_seeds = __webpack_require__(41);

	function showHideRealry() {
	    var check = $('#checkbox-sb-realty');
	    check.on("click", function (e) {
	        var $realty = $('#sb-realty');
	        if ($(e.target).prop("checked")) {
	            $realty.show();
	        } else {
	            $realty.hide();
	        }
	    });
	}

	function changeButtonText() {
	    //Если перешли со страницы профиля по кнопке "Добавить заказ"
	    var id = window.location.hash.replace("#", "");
	    if (id) $('.js-change-text').val("ДОБАВИТЬ ЗАКАЗ");
	}

	$(function () {
	    (0, _file_upload.fileUploadInit)();
	    (0, _extended_field.extendedFieldInit)();
	    (0, _custom_check.customCheckInit)();
	    showHideRealry();
	    (0, _read_more.readMoreInit)();
	    (0, _only_one_checkbox.onlyOneCheckboxInit)('#safe', '#by_agreement');
	    (0, _scroll_on_required.scrollOnRequiredInit)();
	    (0, _popups.showPopupsInit)();
	    (0, _ajax_registration.ajaxRegistrationInit)('customer');
	    changeButtonText();
	    window.addMessage = _popups.addMessage;
	});

/***/ },

/***/ 3:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            if (cookie.substring(0, name.length + 1) == name + '=') {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}

	function humanFileSize(bytes, si) {
	    var thresh = si ? 1000 : 1024;

	    if (Math.abs(bytes) < thresh) return bytes + ' B';

	    var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];

	    var u = -1;

	    do {
	        bytes /= thresh;
	        ++u;
	    } while (Math.abs(bytes) >= thresh && u < units.length - 1);

	    return bytes.toFixed(1) + ' ' + units[u];
	}

	exports.humanFileSize = humanFileSize;
	exports.getCookie = getCookie;

/***/ },

/***/ 31:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function customCheckInit() {
	    function tuneCheckBoxes($boxes) {
	        var currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';
	        $boxes.find("div").hide();
	        $boxes.find("div." + currentState).show();
	    }

	    var $boxes = $('.custom-check');
	    tuneCheckBoxes($boxes);
	    $boxes.on("click", function (e) {
	        var inside_checkBox = $(e.target).parent().find("input");
	        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
	        tuneCheckBoxes($boxes);
	        e.preventDefault();
	        return false;
	    });
	}

	exports.customCheckInit = customCheckInit;

/***/ },

/***/ 32:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function extendedFieldInit() {

	    var $buttonF1 = $('.resButton');
	    if ($('.slide').hasClass("active")) $buttonF1.css('transform', 'rotate(0deg)');

	    $buttonF1.on("click", function (e) {
	        e.preventDefault();
	        var $slide = $('.slide');
	        if ($slide.hasClass("active")) {
	            $buttonF1.css('transform', 'rotate(180deg)');
	            $slide.slideUp(300);
	        } else {
	            $buttonF1.css('transform', 'rotate(0deg)');
	            $slide.slideDown(300);
	        }
	        $slide.toggleClass("active");
	    });
	}

	exports.extendedFieldInit = extendedFieldInit;

/***/ },

/***/ 35:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.fileUploadInit = undefined;

	var _utils = __webpack_require__(3);

	function fileUploadInit() {
	    var $fileUploadContainer = $('#fileUploadContainer');

	    $('#fileUploadAddBtn').on('click', function ($evt) {
	        $fileUploadContainer.find('.file-upload-widget').last().find('.file-upload-input').click();
	    });

	    $fileUploadContainer.on('change', '.file-upload-input', function ($evt) {
	        var $fileInput = $(this);
	        var $fileUploadWidget = $fileInput.closest('.file-upload-widget');
	        var filePath = $fileInput.val().replace(/\\/g, '/');
	        var fileName = path.basename(filePath);
	        //var fileExt = path.extname(filePath)
	        var fileSize = $fileInput.get(0).files && (0, _utils.humanFileSize)($fileInput.get(0).files[0].size);

	        if (fileName) {
	            $fileUploadWidget.find('.file-upload-label').text(fileName + ' ' + fileSize);

	            var $newFileUploadWidget = $fileUploadWidget.clone();
	            $newFileUploadWidget.find('.file-upload-label').text('');

	            $fileUploadContainer.find('ul').first().append($newFileUploadWidget);

	            $fileUploadWidget.css('display', 'block');
	        }
	    });

	    $fileUploadContainer.on('click', '.file-upload-remove-btn', function ($evt) {
	        var $btn = $(this);
	        $btn.closest('.file-upload-widget').remove();
	    });
	}

	exports.fileUploadInit = fileUploadInit;

/***/ },

/***/ 36:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function readMoreInit() {

	    $('.description .more').on("click", function (e) {
	        var $target = $(e.target);
	        $target.siblings(".complete").toggle();
	        $target.toggleClass("less");
	    });
	}

	exports.readMoreInit = readMoreInit;

/***/ },

/***/ 37:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function onlyOneCheckboxInit(selector_first, selector_second) {
	    //TODO: need refactoring...
	    var $first = $(selector_first);
	    var $second = $(selector_second);
	    $first.change(function () {
	        if ($first.prop("checked")) {
	            $second.prop("checked", false);
	        } else {
	            $second.attr("checked", true);
	        }
	    });
	    $second.change(function () {
	        if ($second.prop("checked")) {
	            $first.attr("checked", false);
	        } else {
	            $first.attr("checked", true);
	        }
	    });
	}

	exports.onlyOneCheckboxInit = onlyOneCheckboxInit;

/***/ },

/***/ 38:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	//Message TYPES: see in 'popups.sass'

	function showMessage(container, template, message, type) {
	    var $popup = $(template);
	    $popup.hide();
	    $popup.find('.message').html(message);
	    $popup.addClass(type);
	    container.append($popup);
	    $popup.fadeIn(1000, function () {
	        setTimeout(function (el) {
	            $(el).fadeOut(1000);
	        }, 2000, this);
	    });
	}

	function addMessage(message, type) {
	    var $popups_storage = $('#popups-storage');
	    var li = '<li class="' + type + '">' + message + '</li>';
	    $popups_storage.append(li);
	}

	function showPopupsInit() {
	    var $popups_storage = $('#popups-storage');
	    var $popups_container = $('#popups-container');
	    var $popup = $popups_container.find(".popup");
	    var popup_html = $popup[0].outerHTML;
	    $popup.remove();
	    if (!$popups_storage.length) return;
	    $.each($popups_storage.find("li"), function (key, value) {
	        var message = $(value).html();
	        var type = $(value).attr('class');
	        showMessage($popups_container, popup_html, message, type);
	        $(value).remove();
	    });
	    $popups_storage.bind("DOMNodeInserted", function () {
	        var $li = $(this).find('li');
	        var message = $li.html();
	        var type = $li.attr('class');
	        $li.remove();
	        showMessage($popups_container, popup_html, message, type);
	    });
	}

	exports.showPopupsInit = showPopupsInit;
	exports.addMessage = addMessage;

/***/ },

/***/ 39:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function scrollOnRequiredInit() {
	    var $required = $('.required.error');
	    // console.log($required);
	    if (!$required.length) return;
	    $('html, body').animate({
	        scrollTop: $required.offset().top - 25
	    }, 1000);
	}

	exports.scrollOnRequiredInit = scrollOnRequiredInit;

/***/ },

/***/ 40:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var $form = $('#form-registration');

	function clearErrors() {
	    var $error_tags = $form.find('.errorlist');
	    if ($error_tags.length > 0) $error_tags.remove();
	}

	function sendData(form_data) {
	    // console.log("form_data = ", form_data);
	    $.ajax({
	        url: '/users/register/',
	        type: 'post',
	        data: form_data,
	        success: function success(data) {
	            // console.log("success data -->", data);
	            $('#registrationFormModal').modal('toggle');
	            $('input[name=not_auth_user_id]').val(data.pk);
	            $('#not_customer').hide();
	            $('#is_customer').show();
	        },
	        error: function error(xhr, ajaxOptions, thrownError) {
	            var status = xhr.status;
	            if (status == 400) {
	                var data = JSON.parse(xhr.responseText);
	                $.each(data, function (key, value) {
	                    var ul = $("<ul class='errorlist'></ul>");
	                    var _iteratorNormalCompletion = true;
	                    var _didIteratorError = false;
	                    var _iteratorError = undefined;

	                    try {
	                        for (var _iterator = value[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                            var error = _step.value;

	                            ul.append('<li>' + error + '</li>');
	                        }
	                    } catch (err) {
	                        _didIteratorError = true;
	                        _iteratorError = err;
	                    } finally {
	                        try {
	                            if (!_iteratorNormalCompletion && _iterator.return) {
	                                _iterator.return();
	                            }
	                        } finally {
	                            if (_didIteratorError) {
	                                throw _iteratorError;
	                            }
	                        }
	                    }

	                    var $field = void 0;
	                    if (key == 'captcha_html') {
	                        // console.log("captcha html = ", key, '/', value);
	                        $form.find('.captcha').html(value);
	                        return;
	                    } else if (key == 'captcha') {
	                        $field = $form.find('.g-recaptcha');
	                    } else if (key == 'tos') {
	                        $field = $form.find('input[name=' + key + ']').parent();
	                    } else {
	                        $field = $form.find('input[name=' + key + ']');
	                        if ($field.length == 0) console.log('!!!field ' + key + ' not found');
	                    }
	                    if ($field.length > 0) $field.parent().append(ul);
	                });
	                // console.log('captcha error = ', data.captcha);
	                // console.log('data type = ', typeof data);
	            }
	            // console.log("data = ", xhr.responseText);
	            // console.log(ajaxOptions);
	            // console.log(thrownError);
	        }
	    });
	}

	function ajaxRegistrationInit(user_type) {
	    // const $form = $('#form-registration');
	    $form.on("submit", function (e) {
	        e.preventDefault();
	        // get form data and add user_type
	        clearErrors();
	        var form_data = $form.serialize() + "&user_type=" + encodeURIComponent(user_type);
	        sendData(form_data);
	    });
	}

	exports.ajaxRegistrationInit = ajaxRegistrationInit;

/***/ },

/***/ 41:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function print(text) {
	    console.log(text);
	}

	exports.print = print;

/***/ }

/******/ });