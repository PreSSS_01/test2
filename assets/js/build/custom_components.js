/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _custom_select = __webpack_require__(44);

	var _custom_check = __webpack_require__(31);

	$(function () {
	    (0, _custom_select.customSelectInit)();
	    (0, _custom_check.customCheckInit)();
	});

/***/ },

/***/ 31:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function customCheckInit() {
	    function tuneCheckBoxes($boxes) {
	        var currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';
	        $boxes.find("div").hide();
	        $boxes.find("div." + currentState).show();
	    }

	    var $boxes = $('.custom-check');
	    tuneCheckBoxes($boxes);
	    $boxes.on("click", function (e) {
	        var inside_checkBox = $(e.target).parent().find("input");
	        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
	        tuneCheckBoxes($boxes);
	        e.preventDefault();
	        return false;
	    });
	}

	exports.customCheckInit = customCheckInit;

/***/ },

/***/ 44:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function customSelectInit() {
	    // CUSTOM-SELECT
	    var $select_container = $('.custom-select');
	    var $sc_headers = $select_container.find('.simple-select');
	    var $sc_options = $select_container.find('.select-box-options');

	    $sc_options.hide();

	    $sc_headers.on("click", function (e) {
	        $(e.target).siblings('.select-box-options').show();
	    });

	    var $options = $sc_options.find('li');
	    $options.on("click", function (e) {
	        var target = $(e.target);
	        var header = target.closest('.select-box-options').siblings('.simple-select');
	        var data = target.closest('.select-box-options').siblings('input[type=hidden]');
	        header.val(target.html());
	        data.val(target.data("id"));
	        // $sc_data.val($(e.target).data("id"));
	        $sc_options.hide();
	        e.preventDefault();
	    });
	    $(document).click(function (event) {
	        //FIXME: запомнить на ком был клик, и не закрывать именно его
	        if ($(event.target).closest($select_container).length) {
	            return;
	        }
	        $sc_options.hide();
	    });
	}

	exports.customSelectInit = customSelectInit;

/***/ }

/******/ });