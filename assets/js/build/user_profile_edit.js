/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _avatar_upload = __webpack_require__(60);

	var _bootstrap_tabs = __webpack_require__(46);

	var _user_check_statuses = __webpack_require__(61);

	var _custom_select = __webpack_require__(44);

	var _custom_check = __webpack_require__(31);

	$(function () {
	    (0, _avatar_upload.avatarUploadInit)();
	    (0, _bootstrap_tabs.tabsHashInit)();
	    (0, _user_check_statuses.checkBoxBindingInit)();
	    (0, _custom_select.customSelectInit)();
	    (0, _custom_check.customCheckInit)();
	});

/***/ },
/***/ 31:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function customCheckInit() {
	    function tuneCheckBoxes($boxes) {
	        var currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';
	        $boxes.find("div").hide();
	        $boxes.find("div." + currentState).show();
	    }

	    var $boxes = $('.custom-check');
	    tuneCheckBoxes($boxes);
	    $boxes.on("click", function (e) {
	        var inside_checkBox = $(e.target).parent().find("input");
	        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
	        tuneCheckBoxes($boxes);
	        e.preventDefault();
	        return false;
	    });
	}

	exports.customCheckInit = customCheckInit;

/***/ },

/***/ 44:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function customSelectInit() {
	    // CUSTOM-SELECT
	    var $select_container = $('.custom-select');
	    var $sc_headers = $select_container.find('.simple-select');
	    var $sc_options = $select_container.find('.select-box-options');

	    $sc_options.hide();

	    $sc_headers.on("click", function (e) {
	        $(e.target).siblings('.select-box-options').show();
	    });

	    var $options = $sc_options.find('li');
	    $options.on("click", function (e) {
	        var target = $(e.target);
	        var header = target.closest('.select-box-options').siblings('.simple-select');
	        var data = target.closest('.select-box-options').siblings('input[type=hidden]');
	        header.val(target.html());
	        data.val(target.data("id"));
	        // $sc_data.val($(e.target).data("id"));
	        $sc_options.hide();
	        e.preventDefault();
	    });
	    $(document).click(function (event) {
	        //FIXME: запомнить на ком был клик, и не закрывать именно его
	        if ($(event.target).closest($select_container).length) {
	            return;
	        }
	        $sc_options.hide();
	    });
	}

	exports.customSelectInit = customSelectInit;

/***/ },

/***/ 46:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function tabsHashInit() {
	    // store the currently selected tab in the hash value
	    $("a[data-toggle=tab]").on("shown.bs.tab", function (e) {
	        var target = $(e.target);
	        if (target.hasClass("tab-inserted")) {
	            target.siblings(".active").removeClass("active");
	            target.addClass("active");
	            return;
	        }
	        var id = target.attr("href").substr(1);
	        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
	        window.location.hash = id;
	        $('html,body').scrollTop(scrollmem);
	    });
	}

	function restoreTab() {
	    // on load of the page: switch to the currently selected tab
	    var hash = window.location.hash;
	    var a = $("a[data-toggle=\"tab\"][href=\"" + hash + "\"]");
	    hash && a.tab('show');
	}

	exports.tabsHashInit = tabsHashInit;
	exports.restoreTab = restoreTab;

/***/ },

/***/ 60:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	// Live image avatar (single) upload ---------------------------------
	var LIVE_IMAGE_UPLOAD_URL = '/common/live-image-upload/create/';

	function avatarUploadInit() {
	    var $container = $('.-live-image-avatar-upload-container').first();

	    var $avatarImage = $container.find('.-avatar-image').first();
	    var $liveImageUpload = $container.find('.-live-image-upload').first();
	    var $liveImageId = $container.find('.-live-image-id').first();
	    var $liveImageDelete = $container.find('.-live-image-delete').first();

	    $avatarImage.attr('orig-src', $avatarImage.attr('src'));

	    $liveImageUpload.fileupload({
	        url: LIVE_IMAGE_UPLOAD_URL,
	        dataType: 'json',

	        done: function done($evt, data) {
	            var image = data.result;

	            $avatarImage.attr('src', image.thumbnailUrl);
	            $liveImageId.val(image.id);

	            $liveImageDelete.data('url', image.deleteUrl);
	            $liveImageDelete.css('display', 'block');
	        }
	    });

	    $liveImageDelete.on('click', function ($evt) {
	        var $that = $(this);

	        $.post($that.data('url')).then(function (res) {
	            if (res.status == 'success') {
	                $avatarImage.attr('src', $avatarImage.attr('orig-src') || STUB_IMAGE_URL);
	                $liveImageId.val('');
	                $that.css('display', 'none');
	            }
	        });
	    });
	}

	exports.avatarUploadInit = avatarUploadInit;

/***/ },

/***/ 61:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function checkBoxBindingInit() {
	    /*
	     Скрываем/Показываем формы/части форм, взависимости от выбранных checkBox'ов
	     */
	    var $residency_checkBoxes = $('input[name=fin_info-residency]');
	    var $legal_status_checkBoxes = $('input[name=fin_info-legal_status]');
	    var $legal_status_boxes = $legal_status_checkBoxes.closest('div');
	    var $fin_infos = $('.js-fin-info');
	    var shows = {
	        'russian_resident': ['individual', 'entity', 'employed'],
	        'non_russian_resident': ['individual', 'entity'],
	        'russian_stay_permit': ['individual']
	    };
	    $fin_infos.hide();
	    $('.-russian_stay_permit-individual').hide();

	    $residency_checkBoxes.on("click", function (e) {
	        $legal_status_checkBoxes.first().trigger("click");
	    });
	    $residency_checkBoxes.on("change", function (e) {
	        $legal_status_boxes.hide();
	        var value = $(e.target).val();
	        var _iteratorNormalCompletion = true;
	        var _didIteratorError = false;
	        var _iteratorError = undefined;

	        try {
	            for (var _iterator = shows[value][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                var legal_status = _step.value;

	                $('input[value=' + legal_status + ']').closest('div').show();
	            }
	        } catch (err) {
	            _didIteratorError = true;
	            _iteratorError = err;
	        } finally {
	            try {
	                if (!_iteratorNormalCompletion && _iterator.return) {
	                    _iterator.return();
	                }
	            } finally {
	                if (_didIteratorError) {
	                    throw _iteratorError;
	                }
	            }
	        }

	        var fin_info_id = '';
	        fin_info_id += '-' + $residency_checkBoxes.filter(':checked').val();
	        fin_info_id += '-' + $legal_status_checkBoxes.filter(':checked').val();
	        if (fin_info_id == '-non_russian_resident-individual') {
	            $('.-non_russian_resident-individual').hide();
	            $('.-non_russian_resident-individual').children('input').val('');
	        } else {
	            $('.-non_russian_resident-individual').show();
	        }
	        if (fin_info_id == '-russian_stay_permit-individual') {
	            $('.-russian_stay_permit-individual').show();
	        } else {
	            $('.-russian_stay_permit-individual').hide();
	            $('.-russian_stay_permit-individual').children('input').val('');
	        }
	    });

	    $legal_status_checkBoxes.on("click", function (e) {
	        $fin_infos.each(function (ind, el) {
	            $(el).find('input').val('');
	        });
	    });
	    $legal_status_checkBoxes.on("change", function (e) {
	        $fin_infos.hide();

	        var fin_info_id = '';
	        fin_info_id += '-' + $residency_checkBoxes.filter(':checked').val();
	        fin_info_id += '-' + $legal_status_checkBoxes.filter(':checked').val();

	        if (fin_info_id != '-non_russian_resident-entity') {
	            fin_info_id = '-russian_resident';
	            fin_info_id += '-' + $legal_status_checkBoxes.filter(':checked').val();
	        }
	        $('#' + fin_info_id).show();
	    });
	    $residency_checkBoxes.first().trigger("change");
	    $legal_status_checkBoxes.first().trigger("change");
	}

	exports.checkBoxBindingInit = checkBoxBindingInit;

/***/ }

/******/ });