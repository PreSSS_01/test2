/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _ChatCustomerPageController = __webpack_require__(29);

	var _BINDS = __webpack_require__(22);

	var _chats = __webpack_require__(27);

	var _documents = __webpack_require__(25);

	var _wsChatConnect = __webpack_require__(28);

	var _notes = __webpack_require__(24);

	var _archiveProjects = __webpack_require__(26);

	window.connect = _wsChatConnect.connect;
	window.socket = undefined;

	$(function () {
	    $('body').on('focus', ".term-picker", function () {
	        $(this).datepicker({
	            minDate: 0
	        });
	    });

	    (0, _BINDS.bindArbitrationSend)();
	    window.onhashchange = function (e) {
	        // console.log("Change Hash!!! ", 'a[data-toggle="tab"][href="#' + location.hash.slice(1) + '"]');
	        $('a[data-toggle="tab"][href="#' + location.hash.slice(1) + '"]').trigger("click");
	    };

	    window.chatController = new _ChatCustomerPageController.ChatPageController();
	    (0, _BINDS.bindOrders)();
	    (0, _BINDS.bindOrderInfo)();
	    (0, _BINDS.bindOnTabs)();
	    (0, _BINDS.restoreTabFromHash)();
	    (0, _BINDS.bindUserContacts)();
	    (0, _BINDS.bindGetUserMessages)();
	    (0, _archiveProjects.bindArchiveProjects)();
	    (0, _BINDS.bindDeleteContact)();
	    (0, _BINDS.bindCtrlEnterSendMessage)();

	    //Chats
	    (0, _chats.chatContactsInit)();
	    (0, _chats.chatOrdersInit)();

	    //Documents
	    (0, _documents.uploadDocumentsContactInit)();
	    (0, _documents.uploadDocumentsOrderInit)();

	    //Notes
	    (0, _notes.bindContractorNotes)();
	    (0, _notes.bindOrderNotes)();
	});

/***/ },
/* 1 */,
/* 2 */,
/* 3 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            if (cookie.substring(0, name.length + 1) == name + '=') {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}

	function humanFileSize(bytes, si) {
	    var thresh = si ? 1000 : 1024;

	    if (Math.abs(bytes) < thresh) return bytes + ' B';

	    var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];

	    var u = -1;

	    do {
	        bytes /= thresh;
	        ++u;
	    } while (Math.abs(bytes) >= thresh && u < units.length - 1);

	    return bytes.toFixed(1) + ' ' + units[u];
	}

	exports.humanFileSize = humanFileSize;
	exports.getCookie = getCookie;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.loadTemplate = undefined;

	var _stage_tmpl = __webpack_require__(5);

	var _stage_tmpl2 = _interopRequireDefault(_stage_tmpl);

	var _stage_approved_tmpl = __webpack_require__(6);

	var _stage_approved_tmpl2 = _interopRequireDefault(_stage_approved_tmpl);

	var _reserved_tmpl = __webpack_require__(7);

	var _reserved_tmpl2 = _interopRequireDefault(_reserved_tmpl);

	var _message_tmpl = __webpack_require__(8);

	var _message_tmpl2 = _interopRequireDefault(_message_tmpl);

	var _work_in_process_tmpl = __webpack_require__(9);

	var _work_in_process_tmpl2 = _interopRequireDefault(_work_in_process_tmpl);

	var _bntCompleteStage_tmpl = __webpack_require__(10);

	var _bntCompleteStage_tmpl2 = _interopRequireDefault(_bntCompleteStage_tmpl);

	var _btnSendReview_tmpl = __webpack_require__(11);

	var _btnSendReview_tmpl2 = _interopRequireDefault(_btnSendReview_tmpl);

	var _document_attach_file_tmpl = __webpack_require__(12);

	var _document_attach_file_tmpl2 = _interopRequireDefault(_document_attach_file_tmpl);

	var _document_link_tmpl = __webpack_require__(13);

	var _document_link_tmpl2 = _interopRequireDefault(_document_link_tmpl);

	var _note_tmpl = __webpack_require__(14);

	var _note_tmpl2 = _interopRequireDefault(_note_tmpl);

	var _order_info_tmpl = __webpack_require__(15);

	var _order_info_tmpl2 = _interopRequireDefault(_order_info_tmpl);

	var _stage_contractor_approve_tmpl = __webpack_require__(16);

	var _stage_contractor_approve_tmpl2 = _interopRequireDefault(_stage_contractor_approve_tmpl);

	var _document_before_upload_tmpl = __webpack_require__(17);

	var _document_before_upload_tmpl2 = _interopRequireDefault(_document_before_upload_tmpl);

	var _switch_to_protected_tmpl = __webpack_require__(18);

	var _switch_to_protected_tmpl2 = _interopRequireDefault(_switch_to_protected_tmpl);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function loadTemplate(template_name) {
	    var templates = {
	        stage_tmpl: _stage_tmpl2.default,
	        stage_approved_tmpl: _stage_approved_tmpl2.default,
	        reserved_tmpl: _reserved_tmpl2.default,
	        message_tmpl: _message_tmpl2.default,
	        work_in_process_tmpl: _work_in_process_tmpl2.default,
	        bntCompleteStage_tmpl: _bntCompleteStage_tmpl2.default,
	        btnSendReview_tmpl: _btnSendReview_tmpl2.default,
	        document_attach_file_tmpl: _document_attach_file_tmpl2.default,
	        document_link_tmpl: _document_link_tmpl2.default,
	        note_tmpl: _note_tmpl2.default,
	        order_info_tmpl: _order_info_tmpl2.default,
	        stage_contractor_approve_tmpl: _stage_contractor_approve_tmpl2.default,
	        document_before_upload_tmpl: _document_before_upload_tmpl2.default,
	        switch_to_protected_tmpl: _switch_to_protected_tmpl2.default
	    };

	    if (!templates[template_name]) throw new Error('Template ' + template_name + ' does not exist');
	    return templates[template_name];
	}

	exports.loadTemplate = loadTemplate;

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	       value: true
	});

	exports.default = function (ctx) {
	       return _template.call(ctx, ctx);
	};

	function _template() {
	       return '<!-- this {stage_num: \'...\', form_name: \'...\', orderId: \'...\', stage: {...} -->\n<div class="numberStepp box-sizing">\n    <!-- <p>\u042D\u0422\u0410\u041F ' + this.stage_num + ' </p> ' + this.stage_status + ' -->\n    <div class="review-type" style="width: 100%">\n        <div class="alignleft">\n            <span>\u042D\u0422\u0410\u041F ' + this.stage_num + '</span>\n        </div>\n        <div class="alignright">\n            <span class="stage-status"> ' + (this.stage_status ? this.stage_status : '') + '</span>\n        </div>\n        <div style="clear: both;"></div>\n    </div>\n    <form class="' + this.form_name + ' js-stage-form" data-stage-id="' + (this.stage && this.stage.id ? this.stage.id : '') + '"\n          id="stage-form-' + (this.stage && this.stage.id ? this.stage.id : '') + '">\n        <label>\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435</label>\n        <input class="form-control" type="text" name="name"\n               value="' + (this.stage && this.stage.name ? this.stage.name : '') + '">\n        <p class="error error-name"></p>\n        <label>\u0426\u0435\u043D\u0430</label>\n        <input class="form-control" type="text" name="cost"\n               value="' + (this.stage && this.stage.cost ? this.stage.cost : '') + '">\n        <p class="error error-cost"></p>\n        <input class="form-control" name="pos" type="hidden"\n               value="' + (this.stage && this.stage.pos ? this.stage.pos : this.stage_num) + '">\n        <p class="error error-pos"></p>\n        <input class="form-control orderStagesInput" type="hidden" name="order"\n               value="' + this.orderId + '">\n        <input class="form-control" type="hidden" name="status" value="send_approve">\n        <label>\u0421\u0440\u043E\u043A</label>\n        <input class="term-picker form-control datepicker" type="text" name="term"\n               value="' + (this.stage && this.stage.term ? this.stage.term : '') + '">\n        <p class="error error-term"></p>\n        <label>\u0420\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442 \u044D\u0442\u0430\u043F\u0430 ' + this.stage_num + '</label>\n        <input class="form-control" type="text" name="result"\n               value="' + (this.stage && this.stage.result ? this.stage.result : '') + '">\n        <p class="error error-result"></p>\n    </form>\n</div>';
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return '<!-- this {stage_num: \'...\',  stage_status: \'...\', stage: {...} -->\n<div class="numberStepp box-sizing"\n     style="border-right: 1px solid gainsboro; border-bottom: 1px solid gainsboro;margin-bottom: 3px">\n    <div class="review-type" style="width: 100%">\n        <div class="alignleft">\n            <span>\u042D\u0422\u0410\u041F ' + this.stage_num + '</span>\n        </div>\n        <div class="alignright">\n            <span class="stage-status"> ' + (this.stage_status ? this.stage_status : '') + '</span>\n        </div>\n        <div style="clear: both;"></div>\n    </div>\n\n    <div class="stage-data">\n        ' + this.stage.name + '\n    </div>\n    \u0420\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442 \u044D\u0442\u0430\u043F\u0430\n    <div class="stage-data">\n        ' + this.stage.result + '\n    </div>\n    \u0426\u0435\u043D\u0430\n    <div class="stage-data">\n        ' + this.stage.cost + ' \u20BD\n    </div>\n    \u0421\u0440\u043E\u043A\n    <div class="stage-data">\n        \u0434\u043E ' + this.stage.term + '\n    </div>\n    <div class="annotate">\n        \u0421\u0440\u043E\u043A \u044D\u0442\u0430\u043F\u0430 \u0440\u0430\u0441\u0447\u0438\u0442\u044B\u0432\u0430\u0435\u0442\u0441\u044F \u0441 \u043C\u043E\u043C\u0435\u043D\u0442\u0430 \u0440\u0435\u0437\u0435\u0440\u0432\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u044F \u0441\u0440\u0435\u0434\u0441\u0442\u0432\n    </div>\n\n</div>';
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!--<li class=\"reserved\"><span class=\"text\">\u0421\u0443\u043C\u043C\u0430 \u0437\u0430 \u044D\u0442\u0430\u043F 1.<br/> \u0417\u0430\u0440\u0435\u0437\u0435\u0440\u0432\u0438\u0440\u043E\u0432\u0430\u043D\u0430</span></li> -->\n<!--<li class=\"unreserved\"><span class=\"text\">\u0421\u0443\u043C\u043C\u0430 \u0437\u0430 \u044D\u0442\u0430\u043F 2.<br/> \u041D\u0435 \u0437\u0430\u0440\u0435\u0437\u0435\u0440\u0432\u0438\u0440\u043E\u0432\u0430\u043D\u0430</span></li> -->\n<li class=\"" + this.reserved_cls + "\"><span class=\"text\">\u0421\u0443\u043C\u043C\u0430 \u0437\u0430 \u044D\u0442\u0430\u043F " + this.stage.pos + "\n    <br/>" + this.reserved_name + "</span>\n</li>";
	};

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!--className   senderName  message={...}-->\n<div class=\"col-lg-12 insetCommChat " + this.className + "\">\n    <div class=\"message-header\">\n        <div class=\"sender_name\">" + this.senderName + "</div><div class=\"created\">" + this.message.created + "</div>\n    </div>\n    <div class=\"message-body\">\n        " + this.message.text + "\n    </div>\n</div>";
	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!-- this {stage: {...}, note_text-->\n<div style=\"margin: 10px 0\">\n    \u0412 \u0440\u0430\u0431\u043E\u0442\u0435: " + this.stage.name + " <br>\n    \u0420\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442 \u044D\u0442\u0430\u043F\u0430: " + this.stage.result + " <br>\n    \u0421\u0440\u043E\u043A \u0441\u0434\u0430\u0447\u0438: <span class=\"select js-select\">" + this.stage.term + "</span><br>\n    <span class=\"select js-select\">" + this.stage.cost + "</span> \u0440\n    <div class=\"note\">\n        " + this.note_text + "\n    </div>\n</div>";
	};

/***/ },
/* 10 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	   value: true
	});

	exports.default = function (ctx) {
	   return _template.call(ctx, ctx);
	};

	function _template() {
	   return "<!-- stage={} , text-->\n<div style=\"margin-left: -27px; margin-right: -27px; text-align: center\">\n    <a href=\"#\"\n       class=\"chat-button icon-complete_stage js-btnComplete\"\n       style=\"padding-left: 60px;\"\n       data-stage-id=\"" + this.stage.id + "\"> " + this.text + " " + this.stage.pos + "\n    </a>\n</div>";
	};

/***/ },
/* 11 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	   value: true
	});

	exports.default = function (ctx) {
	   return _template.call(ctx, ctx);
	};

	function _template() {
	   return "<div id=\"send-review\" style=\"margin-left: -27px; margin-right: -27px; text-align: center\">\n    <a href=\"#\"\n       class=\"chat-button icon-close_chat-positive js-btnComplete\"\n       style=\"padding: 25px 10px;\"\n       data-review-type=\"positive\"> \u0417\u0410\u041A\u0420\u042B\u0422\u042C \u041F\u0420\u041E\u0415\u041A\u0422 \u0418 \u041E\u0421\u0422\u0410\u0412\u0418\u0422\u042C \u041E\u0422\u0417\u042B\u0412\n    </a>\n    <a href=\"#\"\n       class=\"chat-button icon-close_chat-negative js-btnComplete\"\n       style=\"padding: 25px 10px;\"\n       data-review-type=\"negative\"> \u0417\u0410\u041A\u0420\u042B\u0422\u042C \u041F\u0420\u041E\u0415\u041A\u0422 \u0418 \u041E\u0421\u0422\u0410\u0412\u0418\u0422\u042C \u041E\u0422\u0417\u042B\u0412\n    </a>\n</div>\n";
	};

/***/ },
/* 12 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!--href, document_id, text  -->\n<li style=\"word-break: break-all;\">\n    <a class=\"file-link\" href=\"" + this.href + " \">" + this.text + "</a>\n    <div class=\"remove-document\" data-id=\"" + this.document_id + "\" style=\"right:-10px;\"></div>\n</li>";
	};

/***/ },
/* 13 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!-- href, text -->\n<div>\n    \u041F\u0440\u0438\u043B\u043E\u0436\u0435\u043D\u043D\u044B\u0439 \u0444\u0430\u0439\u043B. \u0441\u043A\u0430\u0447\u0430\u0442\u044C: <br>\n    <a href=\"" + this.href + "\">\n        " + this.text + "\n    </a>\n</div>";
	};

/***/ },
/* 14 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!-- this {text: '...', node_id: '...' -->\n<div class=\"note-wrapper\">\n    <li>\n        " + this.text + "\n    </li>\n    <div class=\"remove-note\" data-id=\"" + this.note_id + "\" style=\"right:-10px;\"></div>\n</div>\n";
	};

/***/ },
/* 15 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return '<!-- this {order: {...} -->\n<tr>\n    <td>\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435</td>\n    <td>' + this.order.project.name + '</td>\n</tr>\n<tr>\n    <td>\u0411\u0435\u0437\u043E\u043F\u0430\u0441\u043D\u0430\u044F \u0441\u0434\u0435\u043B\u043A\u0430</td>\n    <td>' + (this.order.secure ? 'Выбрана' : 'Не выбрана') + '</td>\n</tr>\n<tr>\n    <td>\u0422\u0438\u043F \u0437\u0434\u0430\u043D\u0438\u044F</td>\n    <td>' + (this.order.project.realty.building_classification ? this.order.project.realty.building_classification.name : 'не задан') + '</td>\n<tr>\n    <td>\u041A\u043B\u0430\u0441\u0441\u0438\u0444\u0438\u043A\u0430\u0446\u0438\u044F \u0437\u0434\u0430\u043D\u0438\u044F</td>\n    <td>' + (this.order.project.realty.construction_type ? this.order.project.realty.construction_type.name : 'не задана') + '</td>\n</tr>';
	};

/***/ },
/* 16 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!-- this {stage_num: '...',  stage_status: '...', stage: {...} -->\n<div class=\"numberStepp box-sizing\"\n     style=\"border-right: 1px solid gainsboro; border-bottom: 1px solid gainsboro;margin-bottom: 3px\">\n    <div class=\"review-type\" style=\"width: 100%\">\n        <div class=\"alignleft\">\n            <span>\u042D\u0422\u0410\u041F " + this.stage_num + "</span>\n        </div>\n        <div class=\"alignright\">\n            <span class=\"stage-status\"> " + this.stage_status + "</span>\n        </div>\n        <div style=\"clear: both;\"></div>\n    </div>\n\n    <div class=\"select\">\n        " + this.stage.name + "\n    </div>\n    \u0420\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442 \u044D\u0442\u0430\u043F\u0430\n    <div class=\"select\">\n        " + this.stage.result + "\n    </div>\n    \u0426\u0435\u043D\u0430\n    <div class=\"select\">\n        " + this.stage.cost + " \u20BD\n    </div>\n    \u0421\u0440\u043E\u043A\n    <div class=\"select\">\n        \u0434\u043E " + this.stage.term + "\n    </div>\n    <div class=\"annotate\">\n        \u0421\u0440\u043E\u043A \u044D\u0442\u0430\u043F\u0430 \u0440\u0430\u0441\u0447\u0438\u0442\u044B\u0432\u0430\u0435\u0442\u0441\u044F \u0441 \u043C\u043E\u043C\u0435\u043D\u0442\u0430 \u0440\u0435\u0437\u0435\u0440\u0432\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u044F \u0441\u0440\u0435\u0434\u0441\u0442\u0432\n    </div>\n\n</div>";
	};

/***/ },
/* 17 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!-- this {text: '...', file: {...} -->\n<div style=\"float: left\">\n    <a href=\"/chat/download/" + this.file.name + "\" class=\"send-doc\" data-id=\"" + this.file.id + "\"> " + this.file.name + " </a>\n    <div class=\"remove-document\" data-id=\"" + this.file.id + "\"\n         style=\"right:-10px;float:left;position: static;\"></div>\n</div>\n<div style=\"clear: both\"></div>;";
	};

/***/ },
/* 18 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	exports.default = function (ctx) {
	    return _template.call(ctx, ctx);
	};

	function _template() {
	    return "<!-- no context -->\n<div class=\"switch\" style=\"font-size: 12px\">\n    <span class=\"select\">\u0411\u0435\u0437\u043E\u043F\u0430\u0441\u043D\u0430\u044F \u0441\u0434\u0435\u043B\u043A\u0430 \u043D\u0435 \u0430\u043A\u0442\u0438\u0432\u043D\u0430</span>\n    <div class=\"row mod-align-center\">\n        <div class=\"col-lg-1\">\n            <input name=\"price_and_term_required\" id=\"switch-to-protected\" class=\"custom-checkbox\" type=\"checkbox\">\n        </div>\n        <div class=\"col-lg-11\">\n            <label for=\"safe\" style=\"font-weight: normal\" class=\"\">\n                \u041F\u0435\u0440\u0435\u0439\u0442\u0438 \u0432 \u0440\u0435\u0436\u0438\u043C \u0431\u0435\u0437\u043E\u043F\u0430\u0441\u043D\u043E\u0439 \u0441\u0434\u0435\u043B\u043A\u0438\n            </label>\n        </div>\n    </div>\n</div>";
	};

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.StageInWork = exports.StageReserved = exports.StageForm = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _loaders = __webpack_require__(4);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	// new-stages-form
	// update-stages-form
	// remove-stages-form

	var StageForm = function () {
	    function StageForm($container, _ref)
	    // kwargs - auto generate from name_attributes
	    {
	        var orderId = _ref.orderId,
	            stage_num = _ref.stage_num,
	            stage_status = _ref.stage_status,
	            _ref$type = _ref.type,
	            type = _ref$type === undefined ? 'new' : _ref$type,
	            _ref$formNamePostfix = _ref.formNamePostfix,
	            formNamePostfix = _ref$formNamePostfix === undefined ? '-stages-form' : _ref$formNamePostfix,
	            template_name = _ref.template_name,
	            _ref$data = _ref.data,
	            data = _ref$data === undefined ? {} : _ref$data;
	        var kwargs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
	            stage_num: stage_num,
	            stage_status: stage_status,
	            form_name: type + formNamePostfix,
	            orderId: orderId,
	            stage: data
	        };

	        _classCallCheck(this, StageForm);

	        // console.log('Stage form template_name = ', template_name);
	        this.orderId = orderId;
	        this._type = type;
	        this.$container = $container;
	        this.self_tmpl = (0, _loaders.loadTemplate)(template_name);
	        this.data = data;
	        this.$form = undefined;
	        this.stageId = type != 'new' ? data.id : undefined;
	        this.create(kwargs);
	    }

	    _createClass(StageForm, [{
	        key: 'create',
	        value: function create(kwargs) {
	            /**
	             * Добавление шаблона-формы Этапа на страницу
	             */
	            var el = $(this.self_tmpl(kwargs));
	            this.$container.append(el);
	            this.$form = el.find('form');
	            // console.log("form --> ", this.$form);
	            if (this.$form.length) this.$form.find('input[name=cost]').mask('000000000');
	        }
	    }, {
	        key: 'remove',
	        value: function remove() {
	            /**
	             * Удаление, при уменьшении кол-ва этапов
	             * return true - удаляем из [] stages
	             */
	            if (this.type == 'new') {
	                this.$form.parent().remove();
	                return true;
	            }
	            this.type = 'remove';
	            // this.$form.removeClass('update-stages-form').addClass('remove-stages-form');
	            return false;
	        }
	    }, {
	        key: 'restore',
	        value: function restore() {
	            /**
	             * Восстановление, при увеличении кол-ва этапов
	             */
	            if (this.type == 'new') throw new Error("Попытка восстановить элемент с type='new'");
	            this.type = 'update';
	            // this.$form.removeClass('remove-stages-form').addClass('update-stages-form');
	        }
	    }, {
	        key: 'disable',
	        value: function disable() {
	            this.$form.find('input').attr('readonly', true);
	        }
	    }, {
	        key: 'enable',
	        value: function enable() {
	            this.$form.find('input').attr('readonly', false);
	        }
	    }, {
	        key: 'hide',
	        value: function hide() {
	            this.$form.parent().hide();
	        }
	    }, {
	        key: 'show',
	        value: function show() {
	            this.$form.parent().show();
	        }
	    }, {
	        key: 'is_valid',
	        value: function is_valid() {
	            var self = this;
	            var mesage = 'Это поле обязательно';
	            var valid = true;
	            //Очищаем старые ошибки
	            this.$form.find('.error').html("");
	            // Отображаем новые
	            this.$form.find(":input:not([type=hidden])").each(function (i, v) {
	                if (!$(v).val()) {
	                    self.$form.find('.error-' + $(v).attr("name")).html(mesage).css('color', 'red');
	                    valid = false;
	                }
	            });
	            return valid;
	        }
	    }, {
	        key: 'sendAjax_approve',
	        value: function sendAjax_approve() {
	            /**
	             * Отправка Этапа "на согласование"
	             */
	            var self = this;
	            // console.log("Send AJAX Approve");
	            if (this.type == 'new') {
	                // console.log('new stages approve');
	                return Promise.resolve($.ajax({
	                    // async: false,
	                    url: '/api/stages/',
	                    type: 'POST',
	                    beforeSend: function beforeSend(xhr) {
	                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                    },
	                    data: this.$form.serialize(),
	                    dataType: 'json'
	                }).done(function (json) {
	                    self.type = 'update';
	                    self.disable();
	                    self.$form.find('.error').html("");
	                    // console.log("json -->", json);
	                    self.stageId = json.id;
	                    // console.log(json);
	                }).fail(function (xhr, errorMsg, error) {
	                    console.log("ERROR, xhr", xhr);
	                    $.each(xhr.responseJSON, function (i, v) {
	                        self.$form.find('.error-' + i).html(v).css('color', 'red');
	                        // console.log(self.$form);
	                        // console.log(v);
	                        // console.log(i);
	                    });
	                }));
	            } else if (this.type == 'update') {
	                this.$form.find('input[name=status]').val('send_approve');
	                return Promise.resolve($.ajax({
	                    url: '/api/stages/' + this.stageId + '/',
	                    type: 'PUT',
	                    beforeSend: function beforeSend(xhr) {
	                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                    },
	                    data: this.$form.serialize(),
	                    dataType: 'json'
	                }).done(function (json) {
	                    self.$form.find('.error').html("");
	                    self.disable();
	                }).fail(function (xhr, errorMsg, error) {
	                    $.each(xhr.responseJSON, function (i, v) {
	                        self.$form.find('.error-' + i).html(v).css('color', 'red');
	                        console.log(v);
	                        console.log(i);
	                    });
	                }));
	            } else if (this.type == 'remove') {
	                return Promise.resolve($.ajax({
	                    url: '/api/stages/' + this.stageId + '/',
	                    type: 'DELETE',
	                    beforeSend: function beforeSend(xhr) {
	                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                    },
	                    dataType: 'json'
	                }).done(function (json) {}).fail(function (xhr, errorMsg, error) {
	                    console.log("delete fail, json -->", xhr);
	                }));
	            }
	        }
	    }, {
	        key: 'sendAjax_accept',
	        value: function sendAjax_accept(secureOrder) {
	            /**
	             * "Согласовать" Этапы (Исполнителем)
	             */
	            // console.log("secureOrder = ", secureOrder);
	            // console.log("set new status =", secureOrder ? 'agreed': 'in_process');
	            return Promise.resolve($.ajax({
	                url: '/api/stages/' + this.stageId + '/',
	                type: 'PATCH',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                },
	                data: {
	                    status: secureOrder ? 'agreed' : 'in_process'
	                },
	                dataType: 'json'
	            }));
	        }
	    }, {
	        key: 'sendAjax_change',
	        value: function sendAjax_change() {
	            /**
	             * Отправка Этапа "Внести изменения"
	             */
	            var self = this;
	            // this.$form.find('input[name=status]').val('not_agreed');
	            // console.log("ajax Change form -->", this.$form);
	            return Promise.resolve($.ajax({
	                url: '/api/stages/' + this.stageId + '/',
	                type: 'PATCH',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                },
	                data: { status: 'not_agreed' },
	                dataType: 'json'
	            }).done(function (json) {
	                // enableStageFields(json.id);
	                // $form.find('.error').html("");
	                self.enable();
	            }).fail(function (xhr) {
	                console.log("Ошибка, которой не должно быть json -->", xhr.responseJSON);
	            }));
	        }
	    }, {
	        key: 'type',
	        set: function set(newType) {
	            this.$form.removeClass(this._type + '-stages-form').addClass(newType + '-stages-form');
	            if (newType == 'remove') this.hide();
	            if (newType == 'update') this.show();
	            this._type = newType;
	        },
	        get: function get() {
	            return this._type;
	        }
	    }]);

	    return StageForm;
	}();

	var StageReserved = function () {
	    function StageReserved($container, _ref2) {
	        var _ref2$template_name = _ref2.template_name,
	            template_name = _ref2$template_name === undefined ? 'reserved_tmpl' : _ref2$template_name,
	            data = _ref2.data;
	        var kwargs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
	            reserved_cls: '',
	            reserved_name: '',
	            stage: data
	        };

	        _classCallCheck(this, StageReserved);

	        // Вывод текста резервирования в зависимости от статуса этапа
	        var reserved_names = {
	            agreed: 'Не зарезервирована',
	            in_process: 'Зарезервирована',
	            completed: 'Зарезервирована',
	            closed: 'Переведена исполнителю'
	        };
	        // Вывод текста резервирования в зависимости от статуса этапа
	        var reserved_classes = {
	            agreed: 'unreserved',
	            in_process: 'reserved',
	            completed: 'reserved',
	            closed: 'closed'
	        };
	        kwargs.reserved_cls = reserved_classes[data.status];
	        kwargs.reserved_name = reserved_names[data.status];
	        this.data = data;
	        this.self_tmpl = (0, _loaders.loadTemplate)(template_name);
	        this.$container = $container;
	        this.create(kwargs);
	    }

	    _createClass(StageReserved, [{
	        key: 'create',
	        value: function create(kwargs) {
	            /**
	             * Добавление шаблона "Резервирование" Этапа на страницу
	             */
	            this.$self = $(this.self_tmpl(kwargs));
	            this.$container.append(this.$self);
	            // console.log("form --> ", this.$form);
	            // this.$form.find('input[name=cost]').mask('000000000');
	        }
	    }]);

	    return StageReserved;
	}();

	var StageInWork = function () {
	    function StageInWork($container, _ref3) {
	        var _ref3$template_name = _ref3.template_name,
	            template_name = _ref3$template_name === undefined ? 'work_in_process_tmpl' : _ref3$template_name,
	            _ref3$note_text = _ref3.note_text,
	            note_text = _ref3$note_text === undefined ? '' : _ref3$note_text,
	            data = _ref3.data;
	        var kwargs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : { stage: data, note_text: note_text };

	        _classCallCheck(this, StageInWork);

	        this.stageId = data.id;
	        this.data = data;
	        this.self_tmpl = (0, _loaders.loadTemplate)(template_name);
	        this.$container = $container;
	        this.create(kwargs);
	    }

	    _createClass(StageInWork, [{
	        key: 'create',
	        value: function create(kwargs) {
	            /**
	             * Добавление шаблона "Выполнение работы" Этапа на страницу
	             */
	            this.$self = $(this.self_tmpl(kwargs));
	            this.$container.append(this.$self);
	        }
	    }, {
	        key: 'hide',
	        value: function hide() {
	            this.$self.hide();
	        }
	    }, {
	        key: 'sendAjax_complete',
	        value: function sendAjax_complete() {
	            /**
	             * Отправка Этапа "Закрыть этап"
	             */
	            var self = this;
	            return Promise.resolve($.ajax({
	                url: '/api/stages/' + this.stageId + '/',
	                type: 'PATCH',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                },
	                data: { status: 'completed' },
	                dataType: 'json'
	            }).done(function (json) {
	                // enableStageFields(json.id);
	                // $form.find('.error').html("");
	                // self.enable();
	            }).fail(function (xhr) {
	                console.log("Ошибка, которой не должно быть json -->", xhr.responseJSON);
	            }));
	        }
	    }, {
	        key: 'sendAjax_close',
	        value: function sendAjax_close() {
	            /**
	             * Отправка Этапа "Закрыть этап"
	             */
	            var self = this;
	            return Promise.resolve($.ajax({
	                url: '/api/stages/' + this.stageId + '/',
	                type: 'PATCH',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	                },
	                data: { status: 'closed' },
	                dataType: 'json'
	            }).done(function (json) {
	                // enableStageFields(json.id);
	                // $form.find('.error').html("");
	                // self.enable();
	            }).fail(function (xhr) {
	                console.log("Ошибка, которой не должно быть json -->", xhr.responseJSON);
	            }));
	        }
	    }]);

	    return StageInWork;
	}();

	exports.StageForm = StageForm;
	exports.StageReserved = StageReserved;
	exports.StageInWork = StageInWork;

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.MessagesController = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _utils = __webpack_require__(3);

	var _loaders = __webpack_require__(4);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var MessagesController = function () {
	    function MessagesController(orderId) {
	        _classCallCheck(this, MessagesController);

	        console.log('Create MessagesController');
	        var self = this;
	        this.orderId = orderId;
	        this.$inbox = $('#message-chat-order-space');
	        this.$inbox.html("");
	        this.messageTemplate = (0, _loaders.loadTemplate)('message_tmpl');
	        this.dataPromise = this.getMessagesData();
	        this.dataPromise.then(self._onLoadData.bind(self));
	    }

	    _createClass(MessagesController, [{
	        key: 'getMessagesData',
	        value: function getMessagesData() {
	            var self = this;
	            return Promise.resolve($.ajax({
	                url: '/api/message',
	                type: 'GET',
	                data: { 'order': self.orderId, 'team__isnull': 'true' },
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                dataType: 'json',
	                success: function success(json) {
	                    console.log('Success Messages');
	                }
	            }));
	        }
	    }, {
	        key: '_onLoadData',
	        value: function _onLoadData(json) {
	            var self = this;
	            self.$inbox.html("");
	            $.each(json.results, function (i, v) {
	                var senderName = 'Вы';
	                var className = 'youChat';

	                if (v.sender.id !== userId) {
	                    senderName = v.sender.username;
	                    className = '';
	                }
	                if (v.is_system) {
	                    senderName = senderName == 'Вы' ? 'Системное от Вас' : '\u0421\u0438\u0441\u0442\u0435\u043C\u043D\u043E\u0435 \u043E\u0442 ' + senderName;
	                    className = 'systemChat';
	                }
	                var message = $(self.messageTemplate({ className: className, senderName: senderName, message: v }));
	                self.$inbox.append(message);
	            });
	            // console.log("messages render complete");
	            self.$inbox.scrollTop(self.$inbox.prop("scrollHeight"));
	        }
	    }]);

	    return MessagesController;
	}();

	exports.MessagesController = MessagesController;

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.DocumentsController = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _utils = __webpack_require__(3);

	var _loaders = __webpack_require__(4);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var DocumentsController = function () {
	    function DocumentsController(orderId) {
	        _classCallCheck(this, DocumentsController);

	        console.log('Create MessagesController');
	        var self = this;
	        this.orderId = orderId;
	        this.$container = $('#documentOrderSpace');
	        this.$container.html("");

	        this.messageTemplate = (0, _loaders.loadTemplate)('document_attach_file_tmpl');
	        this.dataPromise = this.getMessagesData();
	        this.dataPromise.then(self._onLoadData.bind(self));
	    }

	    _createClass(DocumentsController, [{
	        key: 'getMessagesData',
	        value: function getMessagesData() {
	            var self = this;
	            return Promise.resolve($.ajax({
	                url: '/api/documents',
	                type: 'GET',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                data: {
	                    'order': self.orderId,
	                    'is_delete': false,
	                    'is_send': true
	                },
	                dataType: 'json',
	                // success: function (json) {
	                //
	                // },
	                error: function error(e) {
	                    console.log(e);
	                }
	            }));
	        }
	    }, {
	        key: 'addDocument',
	        value: function addDocument(json) {}
	    }, {
	        key: '_onLoadData',
	        value: function _onLoadData(json) {
	            var self = this;
	            // console.log('mesages json = ', json);
	            // console.log('$inbox = ', this.$inbox);
	            // console.log("messages render start");
	            this.$container.html("");
	            $.each(json.results, function (i, v) {
	                var document = $(self.messageTemplate({
	                    href: '/chat/download/\' + ' + v.file,
	                    text: v.file,
	                    document_id: v.id
	                }));
	                self.$container.append(document);
	            });
	            // console.log("messages render complete");
	            // self.$inbox.scrollTop(self.$inbox.prop("scrollHeight"));
	        }
	    }]);

	    return DocumentsController;
	}();

	exports.DocumentsController = DocumentsController;

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.restoreTabFromHash = exports.bindCtrlEnterSendMessage = exports.bindDeleteContact = exports.bindTeams = exports.bindGetUserMessages = exports.bindUserContacts = exports.bindOnTabs = exports.bindArbitrationSend = exports.bindOrderInfo = exports.bindOrders = undefined;

	var _utils = __webpack_require__(3);

	var _messageCounters = __webpack_require__(23);

	var _loaders = __webpack_require__(4);

	var _notes = __webpack_require__(24);

	function dialog(message, yesCallback, notCallback) {
	    $("#dialog_delete .modal-title").html(message);
	    $("#dialog_delete").modal('show');
	    $("#btnYes").click(function (e) {
	        e.preventDefault();
	        yesCallback();
	        $("#dialog_delete").modal('hide');
	    });
	    $("#btnNot").click(function (e) {
	        e.preventDefault();
	        notCallback();
	        $("#dialog_delete").modal('hide');
	    });
	}

	function isNullOrders() {
	    var buttons = {
	        btnApprove: $('#btnApprove'), // "Согласовать"
	        btnChange: $('#btnChange'), // "Отправить на внесение изменений"
	        btnsToArchive: $('.js-btnToArchive'), // "Отказаться от заказа"
	        btnToArchive: $('#btnToArchive'), // "Отказаться и отправить в Архив"
	        btnReserve: $('#btnReserve'), // "Зарезервировать"
	        btnsArbitration: $('.js-btnArbitration'), // "Обратиться в арбитраж"
	        btnSendReview: $('#order-review-add') // "Оставить отзыв"
	    };
	    var stages_elements = {
	        $approve: $('#conditions-approve'), //1. Согласование условия
	        $reserve: $('#reserveSpace'), //2. Резервирование (Отобразить)
	        $works: $('#completeWork') //3. Выполненная работа
	    };

	    var $orderStagesContainer = $('#order-stages');

	    // hide all buttons
	    for (var key in buttons) {
	        if (buttons[key].length) buttons[key].hide();
	    }

	    for (var _key in stages_elements) {
	        stages_elements[_key].show();
	    }
	    if ($orderStagesContainer.length) $orderStagesContainer.parent().hide();
	}

	function restoreTabFromHash() {
	    var currentHash = URI(location.href).hash();
	    if (currentHash.indexOf("#order") == 0) {
	        $("a[href='#tab2']").trigger('click');
	        // console.log("click on ", "#orderBlock" + currentHash.replace("#order", ""));
	        var obj_id = currentHash.replace("#order", "");
	        // console.log("obj_id =  ", obj_id);
	        if (obj_id) {
	            $("#orderBlock" + currentHash.replace("#order", "")).trigger('click');
	        } else {
	            var $order_block = $(".order-block");
	            if ($order_block.length) {
	                $order_block.first().trigger('click');
	            } else {
	                window.location.hash = '#order';
	                isNullOrders();
	            }
	        }
	    } else if (currentHash.indexOf("#user") == 0) {
	        $("a[href='#tab1']").trigger('click');
	    } else if (currentHash.indexOf("#teamorder") == 0 || currentHash.indexOf("#myteam") == 0) {
	        $("a[href='#tab3']").trigger('click');
	    } else {
	        $("a[href='#tab1']").trigger('click');
	    }
	}

	function bindOrders() {
	    $('.order-block').on('click', function (event) {
	        event.preventDefault();
	        var $this = $(this);
	        $("#chat-order-add").show();
	        (0, _messageCounters.onClickCardWithCount)($this);
	        $('.order-block').each(function (i, v) {
	            $(v).removeClass('orAct');
	        });
	        $this.addClass('orAct');
	        var orderId = $this.data('id');
	        var projectId = $this.data('project-id');
	        var recipentId = $this.data('recipent-id');
	        var orderName = $this.data('order-name');
	        var secureOrder = $(this).data('secure-deal');
	        secureOrder = Boolean(secureOrder);
	        window.location.hash = 'order' + orderId;

	        $("#chat-order-add #orderId").val(orderId);
	        $("#add-form-order-note #orderNote").val(orderId);
	        $("#orderArbitrationId").val(orderId);
	        $("#projectReviewId").val(projectId);
	        $("#reserve-button").attr('data-order-id', orderId);

	        $("#chat-order-add #recipentId").val(recipentId);
	        window.chatController.create(orderId, projectId, recipentId, orderName, secureOrder);

	        $.ajax({
	            url: '/api/note/',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: {
	                'order': orderId
	            },
	            dataType: 'json',
	            success: function success(json) {
	                var noteHtmlInbox = '';
	                var note_tmpl = (0, _loaders.loadTemplate)('note_tmpl');
	                // console.log("note json -->", json);
	                $.each(json.results, function (i, v) {
	                    noteHtmlInbox += note_tmpl({ text: v.text, note_id: v.id });
	                });
	                $(".order-notes-block").html(noteHtmlInbox);
	                (0, _notes.bindRemoveNotes)();
	            }
	        });
	    });
	    $('.order-block .dimovChat').on('click', function (event) {
	        event.preventDefault();
	        event.stopPropagation();
	        // .toggle();
	        var $arrow = $(event.target);
	        var $info = $arrow.siblings('.hideOBB');
	        if ($info.hasClass("open")) {
	            $arrow.css('transform', 'rotate(0deg)');
	            $info.hide();
	        } else {
	            $arrow.css('transform', 'rotate(90deg)');
	            $info.show();
	        }
	        $info.toggleClass("open");
	        // console.log('click on tr');
	    });
	}

	function bindOrderInfo() {
	    $(".messageBlock").on('click', '.full-order-info', function (e) {
	        e.preventDefault();
	        e.stopPropagation();
	        var orderId = $(this).closest('.orderBlock').data('id');
	        if (!orderId) {
	            orderId = $(this).closest('.trashedOrderBlock').data('id');
	        }
	        $.ajax({
	            url: '/api/orders/' + orderId + '/',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            dataType: 'json',
	            success: function success(data) {
	                var outTable_tmpl = (0, _loaders.loadTemplate)('order_info_tmpl');
	                var outTable = outTable_tmpl({ order: data });

	                $("#order-info table").html(outTable);
	                $("#order-info").modal('show');
	            },
	            error: function error(e, jqxhr) {
	                console.log(e);
	            }
	        });
	    });
	}

	function bindTeams() {
	    var message_tmpl = (0, _loaders.loadTemplate)('message_tmpl');
	    $('.team-block').on('click', function () {
	        (0, _messageCounters.onClickCardWithCount)($(this));
	        $('.team-order-block, .team-block').each(function () {
	            $(this).removeClass('orAct');
	        });
	        $(this).addClass('orAct');

	        var teamIds = '';
	        $.each($(this).find('.team-chat-user'), function (i, v) {
	            teamIds += $(this).attr('data-id') + ";";
	        });
	        $("#team-chat-form #teamIds").val(teamIds);

	        var inbox = document.getElementById('message-chat-team-space');
	        inbox.innerHTML = '';

	        var docList = document.getElementById('documentTeamSpace');
	        docList.innerHTML = '';

	        var teamId = $(this).attr('data-team-id');
	        location.hash = '#myteam' + teamId;

	        // var newCount = parseInt($("#count-tab-team").text());
	        // var currNewCount = parseInt($(".team-count-" + teamId).text());
	        // var resCount = newCount - currNewCount;
	        // $("#count-tab-team").text(resCount);
	        $(".team-count-" + teamId).text(0);

	        $("#team-chat-form #teamId").val(teamId);
	        $("#add-form-team-note #teamNote").val(teamId);
	        $("#team-chat-form #recipentTeamId").val("");
	        $("#team-chat-form #orderTeamId").val("");
	        $("#add-form-team-note #orderNote").val("");

	        $.ajax({
	            url: '/api/message',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: { 'team': teamId, 'order__isnull': 'true' },
	            dataType: 'json',
	            success: function success(json) {
	                $.each(json.results, function (i, v) {
	                    var senderName = 'Вы';
	                    var className = 'youChat';
	                    if (v.sender.id !== userId) {
	                        senderName = v.sender.username;
	                        className = '';
	                    }
	                    inbox.innerHTML += message_tmpl({ className: className, senderName: senderName, message: v });
	                    // inbox.innerHTML += '<div class="col-lg-12 insetCommChat ' + className + '"><div class="topCommChat">' +
	                    //     '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
	                    //     '<p class="textCommChat">' + v.text + '</p></div>';
	                });
	                var height = inbox.scrollHeight;
	                inbox.scrollTop = height;
	            }
	        });

	        $.ajax({
	            url: '/api/documents',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: {
	                'team': teamId,
	                'is_delete': false,
	                'is_send': true
	            },
	            dataType: 'json',
	            success: function success(json) {
	                $.each(json.results, function (i, v) {
	                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
	                });
	            },
	            error: function error(e) {
	                console.log(e);
	            }
	        });

	        $.ajax({
	            url: '/api/note/',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: { 'team': teamId },
	            dataType: 'json',
	            success: function success(json) {
	                // console.log(json.results);
	                var noteHtmlInbox = '';
	                var note_tmpl = (0, _loaders.loadTemplate)('note_tmpl');
	                $.each(json.results, function (i, v) {
	                    noteHtmlInbox += note_tmpl({ text: v.text, note_id: v.id });
	                });
	                $(".team-notes-block").html(noteHtmlInbox);
	                (0, _notes.bindRemoveNotes)();
	            }
	        });
	    });
	}

	function bindArbitrationSend() {
	    // TODO: Test it
	    $('#order-arbitration-add').on('click', function (e) {
	        e.preventDefault();
	        e.stopPropagation();
	        var formData = $("#arbitration-add-form").serialize();
	        $.ajax({
	            url: '/projects/arbitration/create/',
	            type: 'POST',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: formData,
	            dataType: 'json',
	            success: function success(json) {
	                // console.log(json);
	                $("#arbitration-add").modal('hide');
	                $.jGrowl("Обращение в арбитраж добавлено", {
	                    life: 4000
	                });
	            },
	            error: function error(e) {
	                console.log('error');
	                console.log(e);
	            }
	        });
	    });
	}

	function bindOnTabs() {
	    /**
	     * Биндит обработчики на Закладки
	     */
	    $('a[data-toggle="tab"]').unbind().on('show.bs.tab', function (e) {
	        // console.log("TAB!");
	        var activeTab = $(this).attr('href').substring(1);
	        var liveHash = URI(location.href).hash();

	        switch (activeTab) {
	            case 'tab1':
	                setTimeout(function () {
	                    if (liveHash.indexOf("#user") != -1) {
	                        var userHashId = liveHash.replace("#user", "");
	                        $("#userBlock" + userHashId).trigger('click');
	                    } else {
	                        $(".user-block").first().trigger('click');
	                    }
	                }, 100);
	                break;

	            case 'tab2':
	                // console.log("tab2");
	                setTimeout(function () {
	                    if (liveHash.indexOf("#order") != -1) {
	                        var ordHashId = liveHash.replace("#order", "");
	                        $("#orderBlock" + ordHashId).trigger('click');
	                    } else {
	                        var $order_block = $(".order-block");
	                        if ($order_block.length) {
	                            $order_block.first().trigger('click');
	                        } else {
	                            window.location.hash = '#order';
	                            isNullOrders();
	                        }
	                    }
	                }, 100);
	                break;

	            case 'tab3':
	                setTimeout(function () {
	                    // console.log("on active TAB team");
	                    if (liveHash.indexOf("#teamorder") != -1) {
	                        var teamHashId = liveHash.replace("#teamorder", "");
	                        $("#teamOrderBlock" + teamHashId).trigger('click');
	                    } else if (liveHash.indexOf("#myteam") != -1) {
	                        var teamHashId = liveHash.replace("#myteam", "");
	                        $("#teamMyBlock" + teamHashId).trigger('click');
	                    } else {
	                        var firstTeamBlock = $(".team-block").first();
	                        var firstTeamOrder = $(".team-order-block").first();
	                        if (firstTeamOrder.length == 1) {
	                            firstTeamOrder.trigger('click');
	                        } else if (firstTeamBlock.length == 1) {
	                            firstTeamBlock.trigger('click');
	                        }
	                    }
	                }, 100);

	        }
	    });
	}

	function bindUserContacts() {
	    $(".conMess").click('on', function (e) {
	        e.preventDefault();
	        e.stopPropagation();

	        var userId = $(this).attr('data-id');
	        $.ajax({
	            url: '/api/users/' + userId + '/',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            dataType: 'json',
	            success: function success(data) {
	                var outTable = '';
	                if (data.username) {
	                    outTable += '<tr><td>Ник</td><td>' + data.username + '</td>';
	                }

	                if (data.fio) {
	                    outTable += '<tr><td>Ф.И.О</td><td>' + data.fio + '</td>';
	                }
	                if (data.skype) {
	                    outTable += '<tr><td>Skype</td><td>' + data.skype + '</td>';
	                }

	                if (data.website) {
	                    outTable += '<tr><td>Сайт</td><td>' + data.website + '</td>';
	                }

	                if (data.phone) {
	                    outTable += '<tr><td>Телефон</td><td>' + data.phone + '</td>';
	                }

	                $("#contact-info table").html(outTable);
	                $("#contact-info").modal('show');
	                // console.log(data);
	            },
	            error: function error(e, jqxhr) {
	                console.log(e);
	            }
	        });
	    });
	}

	function bindGetUserMessages() {
	    var message_tmpl = (0, _loaders.loadTemplate)('message_tmpl');
	    $('.user-block').on('click', function () {
	        (0, _messageCounters.onClickCardWithCount)($(this));
	        // var newCount = parseInt($("#count-tab-contact").text());
	        var contactId = $(this).attr('data-id');
	        location.hash = '#user' + contactId;
	        $("#contact-chat-form #recipentContactId").val(contactId);
	        $("#add-form-contractor-note #recipentNoteContractor").val(contactId);

	        $('.user-block').each(function () {
	            $(this).removeClass('mesAct');
	        });

	        $(this).addClass('mesAct');
	        var inbox = document.getElementById('message-chat-space');
	        var sumSenderRecipent = parseInt(userId) + parseInt(contactId);

	        $("#message-chat-space").removeClass().addClass("contact-space" + sumSenderRecipent);
	        $(".contact-count-" + sumSenderRecipent).text(0);
	        var docList = document.getElementById('documentSpace');
	        inbox.innerHTML = '';
	        docList.innerHTML = '';

	        $.ajax({
	            url: '/api/message',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: {
	                'operand': 'in',
	                'sender_id': userId,
	                'recipent_id': contactId
	            },
	            dataType: 'json',
	            success: function success(json) {
	                $.each(json.results, function (i, v) {
	                    var senderName = 'Вы';
	                    var className = 'youChat';
	                    if (v.sender.id == contactId) {
	                        senderName = v.sender.username;
	                        className = '';
	                    }
	                    inbox.innerHTML += message_tmpl({ className: className, senderName: senderName, message: v });
	                    // '<div class="col-lg-12 insetCommChat  ' + className + '"><div class="topCommChat">' +
	                    //     '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
	                    //     '<p class="textCommChat">' + v.text + '</p></div>'
	                });
	                var height = inbox.scrollHeight;
	                inbox.scrollTop = height;
	            }
	        });

	        $.ajax({
	            url: '/api/documents',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: {
	                'operand': 'in',
	                'sender_id': userId,
	                'recipent_id': contactId,
	                'is_delete': false,
	                'is_send': true
	            },
	            dataType: 'json',

	            success: function success(json) {
	                // console.log(json);

	                $.each(json.results, function (i, v) {
	                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
	                });
	            },
	            error: function error(e) {
	                console.log(e);
	            }
	        });

	        $.ajax({
	            url: '/api/note/',
	            type: 'GET',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: {
	                'operand': 'in',
	                'sender_id': userId,
	                'recipent_id': contactId
	            },
	            dataType: 'json',
	            success: function success(json) {
	                // console.log(json.results);
	                var noteHtmlInbox = '';
	                var note_tmpl = (0, _loaders.loadTemplate)('note_tmpl');
	                $.each(json.results, function (i, v) {
	                    noteHtmlInbox += note_tmpl({ text: v.text, note_id: v.id });
	                });
	                $(".contractor-notes-block").html(noteHtmlInbox);
	                (0, _notes.bindRemoveNotes)();
	            }
	        });
	    });
	}

	function bindDeleteContact() {
	    $('.deleteMess').on('click', function (e) {
	        e.preventDefault();
	        e.stopPropagation();

	        var senderId = userId;
	        var recipentId = $(this).attr('data-recipent-id');
	        var _this = $(this);

	        dialog("Вы действительно хотите удалить сообщения этого пользователя?", function () {
	            $.ajax({
	                url: '/chat/messages_delete/',
	                type: 'POST',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                data: { 'sender_id': senderId, 'recipent_id': recipentId },
	                dataType: 'json',
	                success: function success(json) {

	                    if (json.status == 'ok') {
	                        _this.parent().remove();
	                        $("#message-chat-space").html("");
	                    }
	                },
	                error: function error(e) {
	                    console.log('error');
	                    console.log(e);
	                }
	            });
	        }.bind(null, senderId, recipentId, _this), function () {});
	    });
	}

	function bindCtrlEnterSendMessage() {
	    $('textarea.js-chat').keydown(function (e) {
	        var $target = $(e.target);
	        if (e.ctrlKey && e.keyCode == 13) {
	            // console.log("Send button -->", $target.parent().find('.btn-send'))
	            var $btn_send = $target.parent().find('.btn-send');
	            $btn_send.trigger('click');
	        }
	    });
	}

	exports.bindOrders = bindOrders;
	exports.bindOrderInfo = bindOrderInfo;
	exports.bindArbitrationSend = bindArbitrationSend;
	exports.bindOnTabs = bindOnTabs;
	exports.bindUserContacts = bindUserContacts;
	exports.bindGetUserMessages = bindGetUserMessages;
	exports.bindTeams = bindTeams;
	exports.bindDeleteContact = bindDeleteContact;
	exports.bindCtrlEnterSendMessage = bindCtrlEnterSendMessage;
	exports.restoreTabFromHash = restoreTabFromHash;

/***/ },
/* 23 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function recalculateTabsCounter() {
	    // let tabs = [$('#count-tab-contact'), $('#count-tab-order'), $('#count-tab-team')]
	    var tabs = [$('#tab1'), $('#tab2'), $('#tab3')];
	    var total_messages_count = 0;
	    var _iteratorNormalCompletion = true;
	    var _didIteratorError = false;
	    var _iteratorError = undefined;

	    try {
	        for (var _iterator = tabs[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var tab = _step.value;

	            var count_sum = Array.from(tab.find('.js-count').map(function (i, el) {
	                return parseInt($(el).html());
	            })).reduce(function (a, b) {
	                return a + b;
	            }, 0);
	            var $tab_counter = $('a[href="#' + tab.attr('id') + '"]').find('.count-tab');
	            $tab_counter.html(count_sum);
	            total_messages_count += count_sum;
	            // console.log($tab_counter, 'new value -->', count_sum);
	        }
	    } catch (err) {
	        _didIteratorError = true;
	        _iteratorError = err;
	    } finally {
	        try {
	            if (!_iteratorNormalCompletion && _iterator.return) {
	                _iterator.return();
	            }
	        } finally {
	            if (_didIteratorError) {
	                throw _iteratorError;
	            }
	        }
	    }

	    var $header_counter = $('.js-all-messages');
	    $header_counter.html(total_messages_count);
	}

	function countPlus(message, place) {
	    /**
	     * Увеличиваем счетчик соответствующий сообщению(message)
	     */
	    // console.log("MESSAGE = ", message);
	    var $container = void 0;
	    if (message.answer_type == "add_message_contact") {
	        $container = $('.contact-count-' + message.sender_id);
	    } else if (message.answer_type == "add_message_order" || message.answer_type == "approve_stages") {
	        $container = $('#count-order-' + message.order_id);
	    } else if (message.answer_type == "add_message_team") {
	        $container = $('#count-team-' + message.team_id);
	    }
	    // console.log("container = ", $container);
	    $container.html(parseInt($container.html()) + 1);
	    recalculateTabsCounter();
	}

	function onClickCardWithCount($card) {
	    /**
	     * При нажатии на карточку со счетчиком новых сообщений
	     */
	    // console.log('Обнулем счетчик ', $card);
	    $card.find('.js-count').html(0);
	    recalculateTabsCounter();
	}

	exports.countPlus = countPlus;
	exports.onClickCardWithCount = onClickCardWithCount;

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.bindRemoveNotes = exports.bindTeamNotes = exports.bindOrderNotes = exports.bindContractorNotes = undefined;

	var _loaders = __webpack_require__(4);

	var note_tmpl = (0, _loaders.loadTemplate)('note_tmpl');

	function bindContractorNotes() {
	    $('#add-note-contractor').on('click', function (e) {
	        e.preventDefault();
	        $.ajax({
	            url: '/api/note/',
	            type: 'POST',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	            },
	            data: $("#add-form-contractor-note").serialize(),
	            dataType: 'json',
	            success: function success(json) {
	                // console.log(json);
	                $("#add-form-contractor-note #chat2").val("");
	                var li = note_tmpl({ text: json.text, note_id: json.id });
	                $(li).appendTo(".contractor-notes-block");
	                bindRemoveNotes();
	            },
	            error: function error(e) {
	                console.log('error');
	                console.log(e);
	            }
	        });
	    });
	}

	function bindOrderNotes() {
	    /**
	     * Create new Note
	     */
	    $('#add-note-button').on('click', function (e) {
	        e.preventDefault();
	        $.ajax({
	            url: '/api/note/',
	            type: 'POST',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	            },
	            data: $("#add-form-order-note").serialize(),
	            dataType: 'json',
	            success: function success(json) {
	                // $("<li>" + json.text + "</li>").appendTo(".order-notes-block");
	                // console.log('note json = ', json);
	                var li = note_tmpl({ text: json.text, note_id: json.id });
	                $(li).appendTo(".order-notes-block");
	                $("#add-form-order-note #chat2").val("");
	                bindRemoveNotes();
	            },
	            error: function error(e) {
	                console.log('error');
	                console.log(e);
	            }
	        });
	    });
	}

	function bindTeamNotes() {
	    $('#add-team-note-button').on('click', function (e) {
	        e.preventDefault();
	        $.ajax({
	            url: '/api/note/',
	            type: 'POST',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	            },
	            data: $("#add-form-team-note").serialize(),
	            dataType: 'json',
	            success: function success(json) {
	                var li = note_tmpl({ text: json.text, note_id: json.id });
	                $(li).appendTo(".team-notes-block");
	                $("#add-form-team-note #chat2").val("");
	                bindRemoveNotes();
	            },
	            error: function error(e) {
	                console.log('error');
	                console.log(e);
	            }
	        });
	    });
	}

	function bindRemoveNotes() {
	    // console.log('num notes = ', $('.remove-note').length);
	    $('.remove-note').unbind().on('click', function (e) {
	        var $note = $(e.target).parent();
	        var noteId = $(e.target).data('id');
	        // console.log('click remove note');
	        $.ajax({
	            url: '/api/note/' + noteId,
	            type: 'DELETE',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	            },
	            dataType: 'json',
	            success: function success(json) {
	                $note.remove();
	                // console.log('Note deleted!');

	                // let li = note_tmpl({text: json.text, note_id: json.id});
	                // $(li).appendTo(".team-notes-block");
	                // $("#add-form-team-note #chat2").val("");
	            },
	            error: function error(e) {
	                console.log('error');
	                console.log(e);
	            }
	        });
	    });
	}

	exports.bindContractorNotes = bindContractorNotes;
	exports.bindOrderNotes = bindOrderNotes;
	exports.bindTeamNotes = bindTeamNotes;
	exports.bindRemoveNotes = bindRemoveNotes;

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.bindRemoveDocuments = exports.uploadDocumentsTeamInit = exports.uploadDocumentsOrderInit = exports.uploadDocumentsContactInit = undefined;

	var _utils = __webpack_require__(3);

	var _loaders = __webpack_require__(4);

	var document_before_upload_tmpl = (0, _loaders.loadTemplate)('document_before_upload_tmpl');

	function uploadDocumentsContactInit() {
	    $("#upload-document-contact").bind('fileuploadsubmit', function (e, data) {
	        data.formData = {
	            sender: $("#contact-chat-form #senderContactId").val(),
	            recipent: $("#contact-chat-form #recipentContactId").val()
	        };
	    });

	    $('#upload-document-contact').fileupload({
	        url: '/chat/create/',
	        crossDomain: false,
	        beforeSend: function beforeSend(xhr, settings) {
	            // console.log("Upload form data -->", this.formData);
	            $('#progress .progress-bar').css('width', '0%');
	            xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	        },
	        dataType: 'json',
	        done: function done(e, data) {
	            $.each(data.result.files, function (index, file) {
	                var htmlImg = document_before_upload_tmpl({ file: file });
	                $(htmlImg).appendTo("#document-send-contact");
	            });
	        },
	        fail: function fail(e) {
	            console.log(e);
	        },
	        progressall: function progressall(e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress .progress-bar').css('width', progress + '%');
	        }
	    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	}

	function uploadDocumentsOrderInit() {
	    $("#upload-document-order").bind('fileuploadsubmit', function (e, data) {
	        data.formData = {
	            sender: $("#chat-order-add #senderId").val(),
	            recipent: $("#chat-order-add #recipentId").val(),
	            order: $("#chat-order-add #orderId").val()
	        };
	    });

	    $('#upload-document-order').fileupload({
	        url: '/chat/create/',
	        crossDomain: false,
	        beforeSend: function beforeSend(xhr, settings) {
	            $('#progress .progress-bar').css('width', '0%');
	            xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	        },
	        dataType: 'json',
	        done: function done(e, data) {
	            $.each(data.result.files, function (index, file) {
	                var htmlImg = document_before_upload_tmpl({ file: file });
	                $(htmlImg).appendTo("#document-send-order");
	            });
	        },
	        fail: function fail(e) {
	            console.log(e);
	        },
	        progressall: function progressall(e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress .progress-bar').css('width', progress + '%');
	        }
	    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	}

	function uploadDocumentsTeamInit() {
	    $("#upload-document-team").bind('fileuploadsubmit', function (e, data) {
	        data.formData = {
	            sender: $("#team-chat-form #senderTeamId").val(),
	            recipent: $("#team-chat-form #recipentTeamId").val(),
	            order: $("#team-chat-form #orderTeamId").val(),
	            team: $("#team-chat-form #teamId").val()
	        };
	        // console.log(data.formData);
	    });

	    $('#upload-document-team').fileupload({
	        url: '/chat/create/',
	        crossDomain: false,
	        beforeSend: function beforeSend(xhr, settings) {
	            $('#progress .progress-bar').css('width', '0%');
	            xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	        },
	        dataType: 'json',
	        done: function done(e, data) {
	            $.each(data.result.files, function (index, file) {
	                var currentValue = $("#documentSendIds").val();
	                currentValue += file.id + ';';
	                $("#documentSendIds").val(currentValue);
	                var htmlImg = document_before_upload_tmpl({ file: file });
	                $(htmlImg).appendTo("#document-send");
	            });
	        },
	        fail: function fail(e) {
	            console.log(e);
	        },
	        progressall: function progressall(e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress .progress-bar').css('width', progress + '%');
	        }
	    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	}

	function bindRemoveDocuments() {
	    $('.tab-content').on('click', '.remove-document', function (e) {
	        e.preventDefault();
	        var dataId = $(this).attr('data-id');
	        var _this = $(this);
	        $.ajax({
	            url: '/api/documents/' + dataId + '/',
	            type: 'PATCH',
	            beforeSend: function beforeSend(xhr) {
	                xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	            },
	            data: { is_delete: true },
	            dataType: 'json',
	            success: function success(json) {
	                _this.parent().remove();
	                // console.log(json);
	            },
	            error: function error(e, jqxhr) {
	                console.log(jqxhr);
	            }
	        });
	    });
	}

	exports.uploadDocumentsContactInit = uploadDocumentsContactInit;
	exports.uploadDocumentsOrderInit = uploadDocumentsOrderInit;
	exports.uploadDocumentsTeamInit = uploadDocumentsTeamInit;
	exports.bindRemoveDocuments = bindRemoveDocuments;

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.bindArchiveProjects = undefined;

	var _utils = __webpack_require__(3);

	function bindArchiveProjects() {
	    // Нажимаем на кнопку архивные сообщения
	    $("#trashed-button").on('click', function (e) {
	        e.preventDefault();
	        var state = $(this).attr('data-show');
	        var trashedOrderHtml = "";

	        if (state == 'true') {
	            $(this).attr('data-show', 'false');
	            $(this).text("Скрыть архивные заказы");

	            $("#archive-space").show();
	            $("#show-archive-label").show();
	        } else {
	            $(this).attr('data-show', 'true');
	            $(this).text("Показать архивные заказы");
	            $("#archive-space").hide();
	            $("#show-archive-label").hide();
	        }
	    });

	    // Нажимаем на заказ в архивных заказах
	    $(".messageBlock").on('click', '.trashedOrderBlock', function () {
	        var $this = $(this);
	        $("#chat-order-add").hide();
	        $('.order-block, .trashedOrderBlock').each(function () {
	            $(this).removeClass('orAct');
	        });
	        $this.addClass('orAct');
	        var orderId = $this.data('id');
	        location.hash = '#order' + orderId;
	        window.chatController.create(orderId);
	    });
	}

	exports.bindArchiveProjects = bindArchiveProjects;

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.chatTeamsInit = exports.chatOrdersInit = exports.chatContactsInit = undefined;

	var _loaders = __webpack_require__(4);

	function chatContactsInit() {
	    /**
	     * Bind на кнопку "Отправить" в Закладке "Личные контакты"
	     */
	    $('#contact-chat-add-message').on('click', function (e) {
	        e.preventDefault();
	        var chatMessage = $("#chat").val();
	        var recipentId = $("#recipentContactId").val();
	        var senderId = $("#senderContactId").val();
	        var sendLinks = $("#document-send-contact a");

	        if (chatMessage || sendLinks.length > 0) {
	            var sendLinkIds;
	            var documentLinks;
	            var documentAttachFiles;

	            (function () {
	                $("#contact-chat-form .errorEmptyMessage").hide();

	                sendLinkIds = "";
	                documentLinks = "";
	                documentAttachFiles = "";

	                var document_attach_file_tmpl = (0, _loaders.loadTemplate)('document_attach_file_tmpl');
	                var document_link_tmpl = (0, _loaders.loadTemplate)('document_link_tmpl');
	                $.each(sendLinks, function (i, v) {
	                    sendLinkIds += $(this).attr('data-id') + ';';
	                    documentLinks += document_link_tmpl({ href: $(this).attr('href'), text: $(this).text() });
	                    documentAttachFiles += document_attach_file_tmpl({ href: $(this).attr('href'), document_id: $(this).attr('data-id'), text: $(this).text() });
	                });
	                socket.send_message({
	                    "format_type": "add_message_contact",
	                    "data": {
	                        "sender_id": senderId,
	                        "recipent_id": recipentId,
	                        "chat_message": chatMessage,
	                        "document_send_links": sendLinkIds,
	                        "document_data": {
	                            "document_links": documentLinks,
	                            "document_attach_files": documentAttachFiles
	                        }
	                    }
	                });
	                $("#chat").val("");
	                $("#document-send-contact").html("");
	            })();
	        } else {
	            $("#contact-chat-form .errorEmptyMessage").show();
	        }
	    });
	}

	function chatOrdersInit() {
	    /**
	     * Bind на кнопку "Отправить" в Закладке "Исполнители/Заказчики"
	     */
	    $('#order-chat-add-message').on('click', function (e) {
	        e.preventDefault();
	        var chatMessage = $("#chat-order-add #chat").val();
	        var recipentId = $("#chat-order-add #recipentId").val();
	        var senderId = $("#chat-order-add #senderId").val();
	        var orderId = $("#chat-order-add #orderId").val();
	        var sendLinks = $("#document-send-order a");
	        if (chatMessage || sendLinks.length > 0) {
	            var sendLinkIds;
	            var documentLinks;
	            var documentAttachFiles;

	            (function () {
	                sendLinkIds = "";
	                documentLinks = "";
	                documentAttachFiles = "";

	                var document_attach_file_tmpl = (0, _loaders.loadTemplate)('document_attach_file_tmpl');
	                var document_link_tmpl = (0, _loaders.loadTemplate)('document_link_tmpl');
	                $.each(sendLinks, function (i, v) {
	                    sendLinkIds += $(this).attr('data-id') + ';';
	                    documentLinks += document_link_tmpl({ href: $(this).attr('href'), text: $(this).text() });
	                    documentAttachFiles += document_attach_file_tmpl({ href: $(this).attr('href'), document_id: $(this).attr('data-id'), text: $(this).text() });
	                });
	                socket.send_message({
	                    "format_type": "add_message_order",
	                    "data": {
	                        "sender_id": senderId,
	                        "recipent_id": recipentId,
	                        "chat_message": chatMessage,
	                        "order_id": orderId,
	                        "document_send_links": sendLinkIds,
	                        "document_data": {
	                            "document_links": documentLinks,
	                            "document_attach_files": documentAttachFiles
	                        }
	                    }

	                });
	                $("#chat-order-add #chat").val("");
	                $("#document-send-order").html("");
	            })();
	        } else {
	            $("#chat-order-add .errorEmptyMessage").show();
	        }
	    });
	}

	function chatTeamsInit() {
	    $("#add-team-chat-message").on('click', function (e) {
	        e.preventDefault();
	        var chatMessage = $("#team-chat-form #chatText").val();
	        // var recipentId = $("#team-chat-form #recipentTeamId").val();
	        var senderId = $("#team-chat-form #senderTeamId").val();
	        var teamId = $("#team-chat-form #teamId").val();
	        // var orderId = $("#team-chat-form #orderTeamId").val();
	        var documentSendIds = $("#documentSendIds").val();
	        var teamIds = $("#team-chat-form #teamIds").val();
	        var sendLinks = $("#document-send a");
	        var document_attach_file_tmpl = (0, _loaders.loadTemplate)('document_attach_file_tmpl');
	        if (chatMessage || sendLinks.length > 0) {
	            var sendLinkIds;
	            var documentLinks;
	            var documentAttachFiles;

	            (function () {
	                sendLinkIds = "";
	                documentLinks = "";
	                documentAttachFiles = "";

	                var document_attach_file_tmpl = (0, _loaders.loadTemplate)('document_attach_file_tmpl');
	                var document_link_tmpl = (0, _loaders.loadTemplate)('document_link_tmpl');
	                $.each(sendLinks, function (i, v) {
	                    sendLinkIds += $(this).attr('data-id') + ';';
	                    documentLinks += document_link_tmpl({ href: $(this).attr('href'), text: $(this).text() });
	                    documentAttachFiles += document_attach_file_tmpl({ href: $(this).attr('href'), document_id: $(this).attr('data-id'), text: $(this).text() });
	                });
	                socket.send_message({
	                    "format_type": "add_message_team",
	                    "data": {
	                        "sender_id": senderId,
	                        // "recipent_id": recipentId,
	                        "chat_message": chatMessage,
	                        "team_id": teamId,
	                        "team_ids": teamIds,
	                        // "order_id": orderId,
	                        "document_send_links": sendLinkIds,
	                        "document_data": {
	                            "document_links": documentLinks,
	                            "document_attach_files": documentAttachFiles
	                        }
	                    }
	                });

	                $("#team-chat-form #chatText").val("");
	                $("#document-send").html("");
	                $("#documentSendIds").val("");
	            })();
	        } else {
	            $("#team-chat-form .errorEmptyMessage").show();
	        }
	    });
	}

	exports.chatContactsInit = chatContactsInit;
	exports.chatOrdersInit = chatOrdersInit;
	exports.chatTeamsInit = chatTeamsInit;

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.connect = undefined;

	var _loaders = __webpack_require__(4);

	var _messageCounters = __webpack_require__(23);

	function getUserPlace() {
	    /**
	     * Определяем в какой закладке Чата пользователь
	     */
	    var hash = location.hash;
	    var tab = void 0,
	        id = void 0;

	    var _arr = ["user", "order", "myteam"];
	    for (var _i = 0; _i < _arr.length; _i++) {
	        var str = _arr[_i];
	        if (hash.indexOf('#' + str) != -1) {
	            tab = str;
	            id = hash.replace('#' + str, '');
	        }
	    }
	    return { tab: tab, id: id };
	}

	function checkMessageInPlace(message, place) {
	    /**
	     * Проверяем, направлено ли входящее сообщение на текущую вкладку пользователя
	     */
	    // message.answer_type=place.tab
	    var eq = ['add_message_contact=user', 'add_message_order=order', 'add_message_team=myteam', 'approve_stages=order'];
	    // console.log([message.answer_type, place.tab].join('='));
	    // console.log(message.order_id, '==', place.id, message.order_id == place.id);
	    if (eq.indexOf([message.answer_type, place.tab].join('=')) != -1 && (message.order_id == place.id || message.recipent_id == place.id || message.sender_id == place.id || message.team_id == place.id)) {

	        return true;
	    }
	    return false;
	}

	function connect() {
	    wsConnect.then(function (_socket) {
	        socket = _socket;
	        // Onmessage in Chat page
	        socket.addEventListener("message", function (event) {
	            var data = JSON.parse(event.data);
	            print.ws_print("new message on Chat page");
	            console.log(", message =", data);

	            var user_place = getUserPlace();
	            // console.log("User place ", place.tab, place.id);

	            if (checkMessageInPlace(data, user_place)) {
	                console.log("Сообщение принято открытым чатом");
	                var chat_container_selectors = {
	                    "user": "#message-chat-space",
	                    "order": "#message-chat-order-space",
	                    "myteam": "#message-chat-team-space"
	                };
	                var documents_container_seletors = {
	                    "user": "#documentSpace",
	                    "order": "#documentOrderSpace",
	                    "myteam": "#documentTeamSpace"
	                };
	                var $chat_container = $(chat_container_selectors[user_place.tab]);
	                var $documents_container = $(documents_container_seletors[user_place.tab]);
	                var classMessage = 'youChat';
	                var senderName = 'Вы';
	                if (data.sender_id != userId) {
	                    senderName = data.sender_name;
	                    classMessage = '';
	                }
	                if (data.is_system) {
	                    senderName = senderName == 'Вы' ? 'Системное от Вас' : '\u0421\u0438\u0441\u0442\u0435\u043C\u043D\u043E\u0435 \u043E\u0442 ' + senderName;
	                    classMessage = 'systemChat';
	                }

	                var chat_message = (0, _loaders.loadTemplate)('message_tmpl')({
	                    className: classMessage,
	                    senderName: senderName,
	                    message: { created: data.msg_time, text: data.msg }
	                });
	                $chat_container.append(chat_message);
	                $chat_container.scrollTop($chat_container.prop("scrollHeight"));

	                $documents_container.append(data.docs_attach);

	                if (data.answer_type == 'approve_stages' && data.sender_id != userId) {
	                    if (data.msg.indexOf('перевел заказ') != -1) {
	                        var secure = false;
	                        if (data.msg.indexOf('безопасную сделку') != -1) secure = true;
	                        window.chatController.statesController._changeOrderProtect(secure);
	                    }
	                    window.chatController.statesController.redraw();
	                }
	            } else {
	                console.log("Сообщение учтено счетчиком");
	                (0, _messageCounters.countPlus)(data, user_place);
	            }
	        });

	        socket.addEventListener("close", function () {
	            console.error("Connection Lost");
	            connect();
	        });

	        socket.send_message = function (messageData) {
	            console.log('send message -->', messageData);
	            socket.send(JSON.stringify(messageData));
	        };

	        socket.send_stages_approve = function (messageData) {
	            socket.send(JSON.stringify(messageData));
	        };
	    });
	    wsConnect.catch(function (reason) {
	        console.error("Server is not available", reason);
	    });
	}

	exports.connect = connect;

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.ChatPageController = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _StagesCustomerController = __webpack_require__(30);

	var _MessagesControllers = __webpack_require__(20);

	var _DocumentsControllers = __webpack_require__(21);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var ChatPageController = function () {
	  function ChatPageController() {
	    _classCallCheck(this, ChatPageController);

	    var self = this;
	    // console.log("NEW Chat CUSTOMER PageController");
	    this.statesController = undefined;
	    this.messagesController = undefined;
	    this.documentsController = undefined;
	  }

	  _createClass(ChatPageController, [{
	    key: 'create',
	    value: function create(orderId, projectId, recipentId, orderName, secureOrder) {
	      var kwargs = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : { archive: false };

	      this.statesController = new _StagesCustomerController.StagesController(orderId, projectId, recipentId, orderName, secureOrder, kwargs);
	      this.messagesController = new _MessagesControllers.MessagesController(orderId);
	      this.documentsController = new _DocumentsControllers.DocumentsController(orderId);
	    }
	  }]);

	  return ChatPageController;
	}();

	exports.ChatPageController = ChatPageController;

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.StagesController = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _utils = __webpack_require__(3);

	var _loaders = __webpack_require__(4);

	var _Stages = __webpack_require__(19);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var message_format = {
	    "format_type": "approve_stages",
	    "data": {
	        "sender_id": "",
	        "recipent_id": "",
	        "order_id": "",
	        "msg": "",
	        "is_system": true
	    }
	};

	var STATUSES = {
	    'not_agreed': 'не согласован',
	    'send_approve': '...на согласовании',
	    'agreed': '...согласовано',
	    'cancel_approve': '...исполнитель отказался',
	    'in_process': '...в процессе',
	    'completed': '...завершен',
	    'closed': '...закрыт'

	};

	//Customer
	//TODO: Вынесли общую логику в родительский класс

	var StagesController = function () {
	    function StagesController(orderId, projectId, recipentId, orderName, secureOrder) {
	        var kwargs = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};

	        _classCallCheck(this, StagesController);

	        var self = this;
	        this.orderId = orderId;
	        this.orderName = orderName;
	        this.projectId = projectId;
	        this.recipentId = recipentId;
	        this.secureOrder = secureOrder;
	        // this.is_archive_project
	        this.data = {}; //JSON
	        this.stages = [];
	        this.stages_reserved = [];
	        this.stages_work = [];
	        this.STAGE_STATUSES = {
	            'not_agreed': this.buildNotAgreedStage.bind(self),
	            'send_approve': this.buildSendApproveStage.bind(self),
	            'agreed': this.buildAgreedStage.bind(self),
	            'in_process': this.buildProcessStage.bind(self),
	            'completed': this.buildProcessStage.bind(self),
	            'closed': this.buildProcessStage.bind(self)
	        };
	        this.btnCompleteTmpl = (0, _loaders.loadTemplate)('bntCompleteStage_tmpl');
	        this.btnSendReviewTmpl = (0, _loaders.loadTemplate)('btnSendReview_tmpl');
	        this.switch_to_protected_tmpl = (0, _loaders.loadTemplate)('switch_to_protected_tmpl');

	        this.$orderStagesContainer = $('#order-stages');
	        this.$orderStagesContainer.html('');
	        this.$stagesCount = $('#countStage');
	        this.$stagesCount.unbind().on("change", this._changeNumStages.bind(self));
	        this.$stagesCount.parent().show();
	        this.buttons = {
	            btnApprove: $('#btnApprove'),
	            btnChange: $('#btnChange'),
	            btnToArchive: $('#btnToArchive'),
	            btnReserve: $('#btnReserve'),
	            btnsArbitration: $('.js-btnArbitration'),
	            btnSendReview: $('#order-review-add')
	        };
	        this.stages_elements = {
	            $approve: $('#conditions-approve'), //1. Согласование условия
	            $reserve: $('#reserveSpace'), //2. Резервирование
	            $works: $('#completeWork') //3. Выполненная работа
	        };
	        this.temp = {
	            approve_stage_header_text: this.stages_elements.$approve.find('.js-stage-header').html()
	        };
	        this.init();
	    }

	    _createClass(StagesController, [{
	        key: 'init',
	        value: function init() {
	            var self = this;

	            this.dataPromise = this.getOrderData({ orderId: this.orderId });
	            this.dataPromise.then(self._onLoadData.bind(self)).catch(self._onLoadDataError.bind(self));
	        }
	    }, {
	        key: 'getOrderData',
	        value: function getOrderData(_ref) {
	            var orderId = _ref.orderId;

	            var self = this;
	            return Promise.resolve($.ajax({
	                url: '/api/orders/' + orderId + '/',
	                dataType: 'json',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                }
	            }));
	        }
	    }, {
	        key: 'redraw',
	        value: function redraw() {
	            /**
	             * Полностью перерисовываем страницу Заказа
	             */
	            console.log("Redraw customer stages");
	            // $("#orderBlock" + this.orderId).trigger('click');
	            this.init();
	        }
	    }, {
	        key: 'reload',
	        value: function reload() {
	            /**
	             * Перезагружаем страницу Чата(при удалении заказа)
	             */
	            window.location = '/chat/#order';
	            location.reload();
	        }
	    }, {
	        key: '_buildPage',
	        value: function _buildPage() {
	            console.log("Build Page!");
	            // Restore html to default
	            this.stages_elements.$approve.find('.js-stage-header').html(this.temp.approve_stage_header_text);
	            console.log("restore this -->", this.stages_elements.$approve.find('.js-select'));
	            this.stages_elements.$approve.find('.js-select').addClass("select");
	            this.stages_elements.$reserve.find('.js-select').addClass("select");
	            this.stages_elements.$works.find('.js-select').addClass("select");
	            var $swith_to_protected = this.$orderStagesContainer.siblings('.switch');
	            if ($swith_to_protected.length) $swith_to_protected.remove();
	            this.stages_elements.$reserve.css({ 'border-top': 'none' });
	            this.stages_elements.$works.css({ 'border-top': 'none' });
	            $('#stagesWork').find('.js-select').addClass('select');
	            this.stages_elements.$reserve.find('.js-select').addClass('select');
	            if ($('#send-review').length) $('#send-review').remove();

	            this.stages_elements.$reserve.hide();
	            this.stages_elements.$works.hide();
	            if (this.data.stages.length == 0) {
	                this.buildStartStage();
	            } else {
	                var stageStatus = this.data.stages[0].status;
	                // console.log('stageStatus = ', stageStatus);
	                this.STAGE_STATUSES[stageStatus]();
	            }
	            this._bindEvents();
	        }
	    }, {
	        key: 'buildStartStage',
	        value: function buildStartStage() {
	            /**
	             * Стадия: "Проект Предложен"(нет этапов)
	             */
	            // Выделить цифру 1. красным
	            // $('#conditions-approve').find('.select')
	            this.$orderStagesContainer.show();
	            this.buttons.btnApprove.show();
	            this.buttons.btnChange.hide();
	            this.buttons.btnToArchive.hide();
	            this.$stagesCount.removeAttr('disabled');
	            this.$stagesCount.val(1);
	            this.$stagesCount.trigger('change');
	            this.stages_elements.$approve.find('.js-select').addClass('select');
	            // this.stages_elements.$approve.find('.js-help-text').show();
	            // this.stages_elements.$reserve.find('.js-help-text').show();
	            // this.stages_elements.$reserve.find('.stages-paid').hide();
	            // this.stages_elements.$works.find('.js-help-text').show();
	            // this.stages_elements.$works.find('#stagesWork').show();
	            if (!this.secureOrder) {
	                // console.log("add switch to protect");
	                this.$orderStagesContainer.after(this.switch_to_protected_tmpl());
	                $('#switch-to-protected').unbind().on('change', this._onChangeToProtect.bind(this));
	                this.buttons.btnsArbitration.hide();
	            }
	        } // Нет Этапов

	    }, {
	        key: 'buildNotAgreedStage',
	        value: function buildNotAgreedStage() {
	            console.log("Stage: not_agreed");
	            this._renderStage('stage_tmpl');
	            this.buttons.btnApprove.show();
	            this.buttons.btnChange.hide();
	            this.buttons.btnToArchive.hide();
	            if (!this.secureOrder) {
	                // console.log("add switch to protect");
	                this.$orderStagesContainer.after(this.switch_to_protected_tmpl());
	                $('#switch-to-protected').unbind().on('change', this._onChangeToProtect.bind(this));
	                this.buttons.btnsArbitration.hide();
	            }
	        } // Статус "Не согласован"

	    }, {
	        key: 'buildSendApproveStage',
	        value: function buildSendApproveStage() {
	            console.log("Stage: send_approve");
	            this._renderStage('stage_tmpl', true);
	            this.$stagesCount.attr('disabled', true);
	            this.stages_elements.$reserve.hide();
	            this.stages_elements.$works.hide();
	            this.buttons.btnApprove.hide();
	            this.buttons.btnChange.show();
	            this.buttons.btnToArchive.show();
	        } // Статус "На согласовании"

	    }, {
	        key: 'buildAgreedStage',
	        value: function buildAgreedStage() {
	            console.log('Stage: agreed');
	            this.buttons.btnApprove.hide();
	            this.buttons.btnChange.hide();
	            this.buttons.btnToArchive.hide();
	            this.$stagesCount.parent().hide();
	            this._renderStage('stage_approved_tmpl', true);
	            this.stages_elements.$approve.find('.js-help-text').hide();
	            this.stages_elements.$approve.find('.js-select').removeClass('select');
	            this.stages_elements.$reserve.find('.js-help-text').show();
	            this.stages_elements.$reserve.show();
	            this.buttons.btnReserve.show();
	            this.buttons.btnsArbitration.show();
	            this._renderStageReserved('reserved_tmpl');
	        } // Статус "Согласован"

	    }, {
	        key: 'buildProcessStage',
	        value: function buildProcessStage() {
	            console.log('Stage: in_process');
	            this.buildAgreedStage();
	            // Block-Stage-1
	            this.stages_elements.$approve.find('.js-select').removeClass('select');
	            // Block-Stage-2
	            this.stages_elements.$reserve.find('.js-select').removeClass('select');
	            this.stages_elements.$reserve.find('.js-btnArbitration').hide();
	            this.stages_elements.$reserve.css({ 'border-top': '1px solid black' });
	            // Block-Stage-3
	            this.stages_elements.$works.show();
	            this.stages_elements.$works.css({ 'border-top': '1px solid black' });
	            this._renderStageInWork('work_in_process_tmpl');
	            if (this.secureOrder) {} else {
	                this.stages_elements.$reserve.hide();
	                this.buttons.btnsArbitration.hide();
	            }
	        } // Статус "В процессе"/"Завершен"/"Закрыт"


	    }, {
	        key: '_renderStage',
	        value: function _renderStage(template_name) {
	            var disable = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	            var i = 0;
	            this.$orderStagesContainer.html("");
	            var _iteratorNormalCompletion = true;
	            var _didIteratorError = false;
	            var _iteratorError = undefined;

	            try {
	                for (var _iterator = this.data.stages[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	                    var stage_data = _step.value;

	                    i++;
	                    var stage = new _Stages.StageForm(this.$orderStagesContainer, {
	                        orderId: this.orderId, stage_num: i, stage_status: STATUSES[stage_data.status],
	                        type: 'update', template_name: template_name, data: stage_data
	                    });
	                    if (disable) stage.disable();
	                    this.stages.push(stage);
	                }
	            } catch (err) {
	                _didIteratorError = true;
	                _iteratorError = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion && _iterator.return) {
	                        _iterator.return();
	                    }
	                } finally {
	                    if (_didIteratorError) {
	                        throw _iteratorError;
	                    }
	                }
	            }

	            this.$stagesCount.val(i);
	        }
	    }, {
	        key: '_renderStageReserved',
	        value: function _renderStageReserved(template_name) {
	            var $container = this.stages_elements.$reserve.find('.stages-paid');
	            $container.html("");
	            this.stages_reserved = [];
	            // Нет незарезервированных Этапов
	            var has_not_reserved_stages = false;
	            var _iteratorNormalCompletion2 = true;
	            var _didIteratorError2 = false;
	            var _iteratorError2 = undefined;

	            try {
	                for (var _iterator2 = this.data.stages[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	                    var stage_data = _step2.value;

	                    if (stage_data.status == 'agreed') has_not_reserved_stages = true;
	                    var stage = new _Stages.StageReserved($container, {
	                        template_name: template_name, data: stage_data
	                    });
	                    this.stages_reserved.push(stage);
	                }
	            } catch (err) {
	                _didIteratorError2 = true;
	                _iteratorError2 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
	                        _iterator2.return();
	                    }
	                } finally {
	                    if (_didIteratorError2) {
	                        throw _iteratorError2;
	                    }
	                }
	            }

	            if (!has_not_reserved_stages) {
	                this.buttons.btnReserve.hide();
	                this.stages_elements.$reserve.find('.js-help-text').hide();
	            }
	        }
	    }, {
	        key: '_renderStageInWork',
	        value: function _renderStageInWork(template_name) {
	            /**
	             * Отрисовываем блок "Выволнение работы", включая "Выполненныа работа" и "Оставить отзыв"
	             */
	            var $container = this.stages_elements.$works.find('#stagesWork');
	            $container.html("");
	            var all_closed = this.data.stages.every(function (el) {
	                return el.status == 'closed';
	            });
	            if (all_closed) {
	                this.stages_elements.$works.find('.titleStepss').html('3. Выполненная работа');
	                this.stages_elements.$works.find('.js-btnArbitration').hide();
	                this.stages_elements.$works.find('.js-help-text').hide();
	                var _iteratorNormalCompletion3 = true;
	                var _didIteratorError3 = false;
	                var _iteratorError3 = undefined;

	                try {
	                    for (var _iterator3 = this.data.stages[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
	                        var stage_data = _step3.value;

	                        var stage = new _Stages.StageInWork($container, {
	                            template_name: template_name,
	                            data: stage_data
	                        });
	                        this.stages_work.push(stage);
	                    }
	                } catch (err) {
	                    _didIteratorError3 = true;
	                    _iteratorError3 = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
	                            _iterator3.return();
	                        }
	                    } finally {
	                        if (_didIteratorError3) {
	                            throw _iteratorError3;
	                        }
	                    }
	                }

	                if (!this.data.has_user_review) {
	                    var btnReviewOpenModel = $(this.btnSendReviewTmpl());
	                    btnReviewOpenModel.unbind().on('click', this._onBtnReviewOpenModal.bind(this));
	                    // Если кнопка "Оставить отзыв" еще не добавлена - добавляем
	                    if (!this.stages_elements.$works.find('#send-review').length) this.stages_elements.$works.append(btnReviewOpenModel);
	                } else {
	                    if (this.stages_elements.$works.find('#send-review').length) this.stages_elements.$works.find('#send-review').remove();
	                }
	                $('#stagesWork').find('.js-select').removeClass('select');
	                this.stages_elements.$reserve.find('.js-select').removeClass('select');
	            } else {
	                var _iteratorNormalCompletion4 = true;
	                var _didIteratorError4 = false;
	                var _iteratorError4 = undefined;

	                try {
	                    for (var _iterator4 = this.data.stages[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
	                        var _stage_data = _step4.value;

	                        if (_stage_data.status == 'closed') continue;
	                        var _stage = new _Stages.StageInWork($container, {
	                            template_name: template_name, data: _stage_data, note_text: 'Закройте этап или подробно опишите замечания в чате'
	                        });
	                        if (_stage_data.status == 'completed') {
	                            var $btn = $(this.btnCompleteTmpl({ stage: _stage_data, text: 'Закрыть этап' }));
	                            $container.html();
	                            $container.append($btn);
	                            $btn.on('click', this._onBtnClose.bind(this, _stage));
	                        }
	                        this.stages_work.push(_stage);
	                    }
	                } catch (err) {
	                    _didIteratorError4 = true;
	                    _iteratorError4 = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
	                            _iterator4.return();
	                        }
	                    } finally {
	                        if (_didIteratorError4) {
	                            throw _iteratorError4;
	                        }
	                    }
	                }
	            }
	        }
	    }, {
	        key: '_onLoadData',
	        value: function _onLoadData(json) {
	            this.data = json;
	            this._buildPage();
	            if (this.data.project.state == 'deleted') {
	                for (var key in this.buttons) {
	                    this.buttons[key].hide();
	                }
	            }
	        }
	    }, {
	        key: '_changeNumStages',
	        value: function _changeNumStages(event) {
	            /**
	             * Изменяет кол-во этапов(stages)
	             */
	            var newNumStages = parseInt($(event.target).val());
	            if (newNumStages < 1 || isNaN(newNumStages)) {
	                newNumStages = 1;
	                this.$stagesCount.val(newNumStages);
	            }
	            var currentNumStages = $('.js-stage-form:not(.remove-stages-form)').length;
	            if (currentNumStages == newNumStages) return;
	            if (currentNumStages > newNumStages) {
	                var _iteratorNormalCompletion5 = true;
	                var _didIteratorError5 = false;
	                var _iteratorError5 = undefined;

	                try {
	                    for (var _iterator5 = this.stages.slice().reverse()[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
	                        var stage = _step5.value;

	                        if (stage.remove()) {
	                            var index = this.stages.indexOf(stage);
	                            if (index >= 0) {
	                                this.stages.splice(index, 1);
	                            }
	                        }
	                        currentNumStages--;
	                        if (currentNumStages == newNumStages) break;
	                    }
	                } catch (err) {
	                    _didIteratorError5 = true;
	                    _iteratorError5 = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
	                            _iterator5.return();
	                        }
	                    } finally {
	                        if (_didIteratorError5) {
	                            throw _iteratorError5;
	                        }
	                    }
	                }
	            } else {
	                var _iteratorNormalCompletion6 = true;
	                var _didIteratorError6 = false;
	                var _iteratorError6 = undefined;

	                try {
	                    for (var _iterator6 = this.stages.slice()[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
	                        var _stage3 = _step6.value;

	                        if (_stage3.type == 'remove') {
	                            _stage3.restore();
	                        } else continue;
	                        currentNumStages++;
	                        if (currentNumStages == newNumStages) break;
	                    }
	                } catch (err) {
	                    _didIteratorError6 = true;
	                    _iteratorError6 = err;
	                } finally {
	                    try {
	                        if (!_iteratorNormalCompletion6 && _iterator6.return) {
	                            _iterator6.return();
	                        }
	                    } finally {
	                        if (_didIteratorError6) {
	                            throw _iteratorError6;
	                        }
	                    }
	                }

	                while (currentNumStages < newNumStages) {
	                    currentNumStages++;
	                    var _stage2 = new _Stages.StageForm(this.$orderStagesContainer, { orderId: this.orderId, stage_num: currentNumStages, type: 'new', template_name: 'stage_tmpl' });
	                    this.stages.push(_stage2);
	                }
	            }
	        } //При изменении кол-ва этапов

	    }, {
	        key: '_bindEvents',
	        value: function _bindEvents() {
	            var self = this;
	            this.buttons.btnApprove.unbind().on("click", this._onBtnApprove.bind(self));
	            this.buttons.btnChange.unbind().on("click", this._onBtnChange.bind(self));
	            this.buttons.btnToArchive.unbind().on("click", this._onBtnAToArchive.bind(self));
	            this.buttons.btnReserve.unbind().on("click", this._onBtnReserve.bind(self));
	            this.buttons.btnsArbitration.unbind().on("click", this._onBtnArbitration.bind(self));
	            this.buttons.btnSendReview.unbind().on("click", this._onBtnSendReview.bind(self));
	            $('#paymentfromSite').unbind().on('click', this._onBtnPaymentFromSite.bind(self));
	            $("#reserve-stage-modal").find('#stage-num').unbind().on('change', function (event) {
	                console.log("this.value = ", this.value);
	                console.log(this.value ? this.value : 1 - 1);
	                $("#reserve-stage-modal").find('#stage-cost').html(self.stages[(this.value ? this.value : 1) - 1].data.cost);
	            });
	        }

	        // BINDS

	    }, {
	        key: '_onBtnApprove',
	        value: function _onBtnApprove(event) {
	            var _this = this;

	            event.preventDefault();
	            var self = this;
	            if (!this.stages.every(function (el) {
	                return el.is_valid();
	            })) {
	                console.error('Не все поля форм валидны');
	                return;
	            }
	            // При выполнении всех ajax запросов
	            Promise.all(this.stages.map(function (el) {
	                return el.sendAjax_approve();
	            })).then(function () {
	                _this.buttons.btnApprove.hide();
	                _this.buttons.btnChange.show();
	                _this.buttons.btnToArchive.show();
	                _this.$stagesCount.attr('disabled', true);
	                // var currentRecipentId = $(self).data('id');
	                // var secureOrder = true;
	                //
	                var message = message_format;
	                message.data.sender_id = userId;
	                message.data.recipent_id = self.recipentId;
	                message.data.order_id = self.orderId;
	                message.data.msg = '\u0423\u0441\u043B\u043E\u0432\u0438\u044F \u0437\u0430\u043A\u0430\u0437\u0430 "' + self.orderName + '" \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u044B \u043D\u0430 \u0441\u043E\u0433\u043B\u0430\u0441\u043E\u0432\u0430\u043D\u0438\u0435';
	                console.log("Send-WS Отправить на согласование");
	                socket.send_stages_approve(message);
	                //TODO: раскомментировать дурацкое окно
	                // $('#popupOk').modal('show');
	                // })
	            });
	        } // "Отправить на согласование"

	    }, {
	        key: '_onBtnChange',
	        value: function _onBtnChange(event) {
	            var _this2 = this;

	            event.preventDefault();
	            var self = this;
	            Promise.all(this.stages.map(function (el) {
	                return el.sendAjax_change();
	            })).then(function () {
	                _this2.buttons.btnApprove.show();
	                _this2.buttons.btnChange.hide();
	                _this2.buttons.btnToArchive.hide();
	                _this2.$stagesCount.attr('disabled', false);
	                var message = message_format;
	                message.data.sender_id = userId;
	                message.data.recipent_id = self.recipentId;
	                message.data.order_id = self.orderId;
	                message.data.msg = '\u0417\u0430\u043A\u0430\u0437 "' + self.orderName + '" \u043E\u0442\u043E\u0437\u0432\u0430\u043D \u0434\u043B\u044F \u0432\u043D\u0435\u0441\u0435\u043D\u0438\u044F \u0438\u0437\u043C\u0435\u043D\u0435\u043D\u0438\u0439';
	                console.log("Send-WS Внести изменения");
	                socket.send_stages_approve(message);
	            });
	        } // "Внести изменения"

	        // TODO: test it

	    }, {
	        key: '_onBtnAToArchive',
	        value: function _onBtnAToArchive(event) {
	            event.preventDefault();
	            var self = this;
	            $.ajax({
	                // async: false,
	                url: '/api/orders/' + this.orderId + '/',
	                type: 'DELETE',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                dataType: 'json'
	            }).done(function (json) {
	                console.log('delete complete');
	                // window.location.href = window.location.href.replace(getHash(), "");
	                var message = message_format;
	                message.data.sender_id = userId;
	                message.data.recipent_id = self.recipentId;
	                message.data.order_id = self.orderId;
	                message.data.msg = '\u0417\u0430\u043A\u0430\u0437\u0430 ' + self.orderName + ' \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u0435\u043D \u0432 \u0430\u0440\u0445\u0438\u0432';
	                console.log("Send-WS Отправить в архив");
	                socket.send_stages_approve(message);
	                self.reload();
	            }).fail(function (xhr, errorMsg, error) {
	                console.log("delete fail, json -->", xhr);
	            });
	        } // "Отказаться и отправить в архив"

	    }, {
	        key: '_onBtnReserve',
	        value: function _onBtnReserve(event) {
	            var self = this;
	            event.preventDefault();
	            // Set modal-window params
	            var $modal = $("#reserve-stage-modal");
	            var total_cost = this.stages.map(function (el) {
	                return parseInt(el.data.cost);
	            }).reduce(function (a, b) {
	                return a + b;
	            }, 0);
	            // console.log('total cost = ', total_cost);
	            $modal.find('#total-cost').html(total_cost);

	            var $select_stageNum = $modal.find('#stage-num');
	            $select_stageNum.find('option').remove().end();
	            var _iteratorNormalCompletion7 = true;
	            var _didIteratorError7 = false;
	            var _iteratorError7 = undefined;

	            try {
	                for (var _iterator7 = this.stages[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
	                    var stage = _step7.value;

	                    if (stage.data.is_paid) continue;
	                    $select_stageNum.append('<option>' + stage.data.pos + '</option>');
	                }
	                // let $stage_cost = $modal.find('#stage-cost');
	                // $stage_cost.html(self.stages[this.value - 1].data.cost);
	            } catch (err) {
	                _didIteratorError7 = true;
	                _iteratorError7 = err;
	            } finally {
	                try {
	                    if (!_iteratorNormalCompletion7 && _iterator7.return) {
	                        _iterator7.return();
	                    }
	                } finally {
	                    if (_didIteratorError7) {
	                        throw _iteratorError7;
	                    }
	                }
	            }

	            $modal.find('#stage-num').trigger('change');

	            $modal.modal('show');
	        } // "Зарезервировать" --> Открывает модальное окно для резервирования

	    }, {
	        key: '_onBtnPaymentFromSite',
	        value: function _onBtnPaymentFromSite(event) {
	            var self = this;
	            event.preventDefault();
	            var $modal = $("#reserve-stage-modal");
	            var sum = void 0,
	                stages_id = void 0;
	            if ($modal.find('input[name=choice_way]:checked').val() == 'all_stages') {
	                sum = $modal.find('#total-cost').html();
	                stages_id = this.stages.map(function (el) {
	                    return el.data.id;
	                });
	            } else {
	                sum = $modal.find('#stage-cost').html();
	                var num_stage = $modal.find('#stage-num').val() - 1;
	                stages_id = [this.stages[num_stage].data.id];
	            }
	            $.ajax({
	                url: '/wallets/payfromscore/',
	                type: 'POST',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                data: {
	                    sum: sum,
	                    stages_id: stages_id.join(';')
	                },
	                dataType: 'json',
	                success: function success(json) {
	                    // console.log('success pay stage, json ', json);
	                    $("#reserve-stage-modal").modal('toggle');
	                    var message = message_format;
	                    message.data.sender_id = userId;
	                    message.data.recipent_id = self.recipentId;
	                    message.data.order_id = self.orderId;
	                    message.data.msg = '\u0417\u0430\u043A\u0430\u0437\u0447\u0438\u043A \u0437\u0430\u0440\u0435\u0437\u0435\u0440\u0432\u0438\u0440\u043E\u0432\u0430\u043B \u0441\u0443\u043C\u043C\u0443 \u0434\u043B\u044F \u044D\u0442\u0430\u043F\u043E\u0432';
	                    console.log("Send-WS Оплата Этапа/Этапов");
	                    socket.send_stages_approve(message);
	                    self.redraw();
	                },
	                error: function error(xhr) {
	                    console.log('error pay stage, json', xhr.responseJSON);
	                    $.jGrowl(xhr.responseJSON.message_error);
	                }
	            });
	        } // Оплата с сайта(Счет на Proekton)

	    }, {
	        key: '_onBtnClose',
	        value: function _onBtnClose(stage, event) {
	            event.preventDefault();
	            var self = this;
	            stage.sendAjax_close().then(function (json) {
	                console.log("Этап закрыт успешно");
	                var message = message_format;
	                message.data.sender_id = userId;
	                message.data.recipent_id = self.recipentId;
	                message.data.order_id = self.orderId;
	                message.data.msg = '\u0417\u0430\u043A\u0430\u0437\u0447\u0438\u043A \u0437\u0430\u043A\u0440\u044B\u043B \u044D\u0442\u0430\u043F "' + json.name + '"';
	                console.log("Send-WS Оплата Этапа/Этапов");
	                socket.send_stages_approve(message);
	                self.redraw();
	            }).catch(function (xhr) {
	                console.log("При закрытии этапа произошла ошибка -->", xhr);
	            });
	        } // "Закрыть этап"

	    }, {
	        key: '_onBtnReviewOpenModal',
	        value: function _onBtnReviewOpenModal(event) {
	            event.preventDefault();
	            var review_type = $(event.target).data('review-type');
	            $('#review-add').find('input[type=radio][value=' + review_type + ']').prop("checked", true);
	            $('#review-add').modal('show');
	        } // Открыть модальное окно "Оставить отзыв"

	    }, {
	        key: '_onBtnSendReview',
	        value: function _onBtnSendReview(event) {
	            event.preventDefault();
	            var self = this;
	            $('#projectReviewId').val(this.projectId);
	            $('#targetContractorId').val(this.recipentId);
	            var formData = $("#review-adds-form").serialize();
	            $.ajax({
	                url: '/api/reviews/',
	                type: 'POST',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                data: formData,
	                dataType: 'json',
	                success: function success(json) {
	                    $('#review-add').modal('hide');
	                    var message = message_format;
	                    message.data.sender_id = userId;
	                    message.data.recipent_id = self.recipentId;
	                    message.data.order_id = self.orderId;
	                    message.data.msg = '\u041E\u0442\u0437\u044B\u0432 \u043D\u0430 \u0437\u0430\u043A\u0430\u0437 "' + self.orderName + '" \u043E\u0441\u0442\u0430\u0432\u043B\u0435\u043D';
	                    console.log("Send-WS Оставить отзыв");
	                    // TODO: в этом сообщении отослать команду на reload if(json.count_reviews == 2)
	                    socket.send_stages_approve(message);
	                    // console.log('json = ', json);
	                    if (json.count_reviews == 1) {
	                        self.redraw();
	                    } else {
	                        self.reload();
	                    }
	                },
	                error: function error(e) {
	                    console.log('error');
	                    console.log(e);
	                }
	            });
	        } // "Оставить отзыв"

	    }, {
	        key: '_onChangeToProtect',
	        value: function _onChangeToProtect(event) {
	            var self = this;
	            var checked = $(event.target).prop('checked');
	            console.log('checked = ', checked);
	            $.ajax({
	                url: '/api/orders/' + this.orderId + '/',
	                type: 'PATCH',
	                beforeSend: function beforeSend(xhr) {
	                    xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	                },
	                data: { secure: checked },
	                dataType: 'json',
	                success: function success(json) {
	                    console.log("Protect switch success, json = ", json);
	                    var message = message_format;
	                    message.data.sender_id = userId;
	                    message.data.recipent_id = self.recipentId;
	                    message.data.order_id = self.orderId;
	                    // Key words "перевел заказ" - на них завязан анализатор на стороне Исполнителя (костыль)
	                    message.data.msg = '\u0417\u0430\u043A\u0430\u0437\u0447\u0438\u043A \u043F\u0435\u0440\u0435\u0432\u0435\u043B \u0437\u0430\u043A\u0430\u0437 "' + self.orderName + '" \u043D\u0430 ' + (checked ? 'безопасную сделку' : 'прямую оплату') + ' ';
	                    console.log("Send-WS Оставить отзыв");
	                    socket.send_stages_approve(message);
	                }
	            });
	        } // "Перевести на Безопасный заказ"


	    }, {
	        key: '_onBtnArbitration',
	        value: function _onBtnArbitration(event) {
	            event.preventDefault();
	            $("#arbitration-add").modal('show');
	        } // "Обратиться в арбитраж"

	    }, {
	        key: '_onLoadDataError',
	        value: function _onLoadDataError(error) {
	            console.log("Error loading data -->", error);
	        }
	    }]);

	    return StagesController;
	}();

	exports.StagesController = StagesController;

/***/ }
/******/ ]);