/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _popups = __webpack_require__(38);

	function checkHash() {
	    // on load of the page: switch to the currently selected tab
	    var hash = window.location.hash;
	    console.log("hash = ", hash);
	    if (hash == '#need-login-for-view') {
	        (0, _popups.addMessage)('Для просмотра личных данных необходимо зарегистрироваться', 'info');
	    }
	}

	$(function () {
	    (0, _popups.showPopupsInit)();
	    checkHash();
	});

/***/ },

/***/ 38:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	//Message TYPES: see in 'popups.sass'

	function showMessage(container, template, message, type) {
	    var $popup = $(template);
	    $popup.hide();
	    $popup.find('.message').html(message);
	    $popup.addClass(type);
	    container.append($popup);
	    $popup.fadeIn(1000, function () {
	        setTimeout(function (el) {
	            $(el).fadeOut(1000);
	        }, 2000, this);
	    });
	}

	function addMessage(message, type) {
	    var $popups_storage = $('#popups-storage');
	    var li = '<li class="' + type + '">' + message + '</li>';
	    $popups_storage.append(li);
	}

	function showPopupsInit() {
	    var $popups_storage = $('#popups-storage');
	    var $popups_container = $('#popups-container');
	    var $popup = $popups_container.find(".popup");
	    var popup_html = $popup[0].outerHTML;
	    $popup.remove();
	    if (!$popups_storage.length) return;
	    $.each($popups_storage.find("li"), function (key, value) {
	        var message = $(value).html();
	        var type = $(value).attr('class');
	        showMessage($popups_container, popup_html, message, type);
	        $(value).remove();
	    });
	    $popups_storage.bind("DOMNodeInserted", function () {
	        var $li = $(this).find('li');
	        var message = $li.html();
	        var type = $li.attr('class');
	        $li.remove();
	        showMessage($popups_container, popup_html, message, type);
	    });
	}

	exports.showPopupsInit = showPopupsInit;
	exports.addMessage = addMessage;

/***/ }

/******/ });