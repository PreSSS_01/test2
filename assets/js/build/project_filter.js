/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _custom_check = __webpack_require__(31);

	var _extended_field = __webpack_require__(32);

	var _ajax_set_filter = __webpack_require__(33);

	function paginateTo(pageNum) {
	    var $form = $('#filter-form');
	    var postfix = "&page=" + pageNum;
	    var $container = $('#resultsBlock');

	    (0, _ajax_set_filter.sendData)($form, $container, pageNum);
	    (0, _ajax_set_filter.modUrl)($form, postfix);
	    return false;
	}

	$(function () {
	    (0, _custom_check.customCheckInit)();
	    (0, _extended_field.extendedFieldInit)();
	    window.sendFilterData = _ajax_set_filter.sendFilterData;
	    window.paginateTo = paginateTo;
	});

/***/ },

/***/ 31:
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function customCheckInit() {
	    function tuneCheckBoxes($boxes) {
	        var currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';
	        $boxes.find("div").hide();
	        $boxes.find("div." + currentState).show();
	    }

	    var $boxes = $('.custom-check');
	    tuneCheckBoxes($boxes);
	    $boxes.on("click", function (e) {
	        var inside_checkBox = $(e.target).parent().find("input");
	        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
	        tuneCheckBoxes($boxes);
	        e.preventDefault();
	        return false;
	    });
	}

	exports.customCheckInit = customCheckInit;

/***/ },

/***/ 32:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function extendedFieldInit() {

	    var $buttonF1 = $('.resButton');
	    if ($('.slide').hasClass("active")) $buttonF1.css('transform', 'rotate(0deg)');

	    $buttonF1.on("click", function (e) {
	        e.preventDefault();
	        var $slide = $('.slide');
	        if ($slide.hasClass("active")) {
	            $buttonF1.css('transform', 'rotate(180deg)');
	            $slide.slideUp(300);
	        } else {
	            $buttonF1.css('transform', 'rotate(0deg)');
	            $slide.slideDown(300);
	        }
	        $slide.toggleClass("active");
	    });
	}

	exports.extendedFieldInit = extendedFieldInit;

/***/ },

/***/ 33:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function getFormData($form, pageNum) {
	    var unindexed_array = $form.serializeArray();
	    var indexed_array = {};

	    $.map(unindexed_array, function (n, i) {
	        indexed_array[n['name']] = n['value'];
	    });
	    indexed_array["page"] = pageNum || "";
	    return indexed_array;
	}

	// TODO: Восстановление фильтров из URL
	function modUrl($form, postfix) {
	    var curLoc = '?' + $form.serialize() + (postfix || "");
	    try {
	        history.replaceState($form.serialize(), null, curLoc);
	    } catch (e) {
	        console.log("Error!");
	    }
	}

	function sendData($form, $update_container, pageNum) {
	    function updateResults(html) {
	        $update_container.html(html);
	        var title = $('#titleScore').val();
	        $('.search-num').html(title);
	    }

	    console.log("form method = ", $form.attr("method"));
	    $.ajax({
	        url: $form.attr("action"),
	        type: $form.attr("method"),
	        dataType: 'html',
	        data: getFormData($form, pageNum),
	        beforeSend: function beforeSend(xhr) {
	            xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
	        },
	        success: updateResults,
	        error: function error(data) {
	            return console.log("Error", data);
	        }
	    });
	}

	function sendFilterData($form, $update_container) {
	    sendData($form, $update_container);
	    modUrl($form);
	    return false;
	}

	exports.sendFilterData = sendFilterData;
	exports.sendData = sendData;
	exports.modUrl = modUrl;

/***/ }

/******/ });