/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _image_upload = __webpack_require__(42);

	var _scroll_on_required = __webpack_require__(39);

	var _ajax_send_form_data = __webpack_require__(43);

	$(function () {
	    (0, _image_upload.imageUploadInit)();
	    (0, _scroll_on_required.scrollOnRequiredInit)();
	    window.sendFormData = _ajax_send_form_data.sendFormData;
	    window.scrollOnRequiredInit = _scroll_on_required.scrollOnRequiredInit;
	});

/***/ },

/***/ 3:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            if (cookie.substring(0, name.length + 1) == name + '=') {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}

	function humanFileSize(bytes, si) {
	    var thresh = si ? 1000 : 1024;

	    if (Math.abs(bytes) < thresh) return bytes + ' B';

	    var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];

	    var u = -1;

	    do {
	        bytes /= thresh;
	        ++u;
	    } while (Math.abs(bytes) >= thresh && u < units.length - 1);

	    return bytes.toFixed(1) + ' ' + units[u];
	}

	exports.humanFileSize = humanFileSize;
	exports.getCookie = getCookie;

/***/ },

/***/ 39:
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function scrollOnRequiredInit() {
	    var $required = $('.required.error');
	    // console.log($required);
	    if (!$required.length) return;
	    $('html, body').animate({
	        scrollTop: $required.offset().top - 25
	    }, 1000);
	}

	exports.scrollOnRequiredInit = scrollOnRequiredInit;

/***/ },

/***/ 42:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.imageUploadInit = undefined;

	var _utils = __webpack_require__(3);

	// function previewImg() {
	//     let $fileUploadWidgets = $('.file-upload-widget');
	//     $.each($fileUploadWidgets, function (ind, el) {
	//         let $fileImg = $(el).find('img');
	//         let $fileInput = $(el).find('.file-upload-input');
	//         // console.log("$fileInput[0].files.length = ", $fileInput[0].files.length);
	//         // console.log("$fileInput.val() = ", $fileInput.val());
	//         console.log("$fileInput = ", $fileInput);
	//         if ($fileInput[0].files.length) {
	//             console.log("Попытка загрузить файл");
	//             let reader = new FileReader();
	//             reader.onload = function (e) {
	//                 $fileImg.attr('src', e.target.result);
	//             };
	//
	//             reader.readAsDataURL($fileInput[0].files[0]);
	//         }
	//     })
	// }

	function imageUploadInit() {
	    var $fileUploadContainer = $('#fileUploadContainer');

	    $('#fileUploadAddBtn').on('click', function ($evt) {
	        $fileUploadContainer.find('.file-upload-widget').last().find('.file-upload-input').click();
	    });

	    $fileUploadContainer.on('change', '.file-upload-input', function ($evt) {
	        var $fileInput = $(this);
	        var $fileUploadWidget = $fileInput.closest('.file-upload-widget');
	        var $fileImg = $fileUploadWidget.find('.preview');
	        var filePath = $fileInput.val().replace(/\\/g, '/');
	        var fileName = path.basename(filePath);
	        var fileSize = $fileInput.get(0).files && (0, _utils.humanFileSize)($fileInput.get(0).files[0].size);
	        console.log("file path", filePath);
	        console.log("file path2 ", $fileInput[0].files[0]);
	        if (fileName) {
	            $fileUploadWidget.find('.file-upload-label').text(fileName + ' ' + fileSize);

	            var $newFileUploadWidget = $fileUploadWidget.clone();
	            $newFileUploadWidget.find('.file-upload-label').text('');

	            $fileUploadContainer.find('.list-new-new').append($newFileUploadWidget);

	            $fileUploadWidget.css('display', 'block');

	            if ($fileImg.length) {
	                var reader = new FileReader();
	                reader.onload = function (e) {
	                    $fileImg.css('background-image', 'url(' + e.target.result + ')');
	                };

	                reader.readAsDataURL($fileInput[0].files[0]);
	            }
	        }
	    });

	    $fileUploadContainer.on('click', '.file-upload-remove-img-btn', function ($evt) {
	        var $btn = $(this);
	        $btn.closest('.file-upload-widget').remove();
	    });
	} //TODO: оформить Upload'еры в виде классов
	//require: <script src='{% static "my-libs.js" %}'></script>
	exports.imageUploadInit = imageUploadInit;
	// export {imageUploadInit, previewImg}

/***/ },

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.sendFormData = undefined;

	var _utils = __webpack_require__(3);

	function sendFormData(e) {
	    e.preventDefault();
	    var $target = $(e.target);
	    var $form = $target.closest("form");
	    // let formData = $form.serializeArray();
	    var formData = new FormData($form[0]);
	    $.ajax({
	        url: $form.attr("action"),
	        data: formData,
	        async: false,
	        method: $form.attr("method"),
	        beforeSend: function beforeSend(xhr) {
	            xhr.setRequestHeader("X-CSRFToken", (0, _utils.getCookie)('csrftoken'));
	        },
	        success: function success(data) {
	            // let data = xhr.responseJSON;
	            // console.log('success data -->', data);
	            window.location.href = data.redirect_to;
	        },
	        cache: false,
	        contentType: false,
	        processData: false,
	        error: function error(xhr, ajaxOptions, thrownError) {
	            var status = xhr.status;
	            $('.error').removeClass('error');
	            if (status == 400) {
	                var data = xhr.responseJSON;
	                $.each(data, function (key, value) {
	                    var $header = $form.find("[name=" + key + "]").siblings('.required');
	                    if ($header.length > 0) {
	                        $header.addClass("error");
	                    }
	                });
	                window.scrollOnRequiredInit();
	            } else {
	                console.log('xhr = ', xhr);
	            }
	        }
	    });
	    // $.post(url, formData).done(function (data) {
	    //     alert(data);
	    // });
	}

	exports.sendFormData = sendFormData;

/***/ }

/******/ });