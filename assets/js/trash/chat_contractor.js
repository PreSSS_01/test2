/***TEMPLATES***/
var wrapper_btn_tmpl = (btn_tmpl) =>
    `
    <div id='btn_wrapper'>${btn_tmpl}</div>
`;

var btn_tmpl_approve = (args = {}) =>
    `
    <div style="margin-left: -13px; margin-right: -13px; text-align: center">  
       <a href="#" data-sender-id="${args.senderId}"  
           class="chat-button icon-hand" 
           style="padding-left: 70px"
           data-recipent-id="${args.recipentId}" data-order-id="${args.orderId}"  
           id="approve-stages">Согласовать</a>  
    </div>
`;

var btn_tmpl_change = (args = {}) =>
    `
    <div style="margin-left: -13px; margin-right: -13px; text-align: center">  
       <a href="#" data-sender-id="${args.senderId}"  
           class="chat-button icon-change" 
           style="padding: 25px 5px"
           data-recipent-id="${args.recipentId}" data-order-id="${args.orderId}"  
           id="cancel-stages">Отправить на внесение изменений</a>  
    </div>
`;

var btn_tmpl_refuse = (args = {}) =>
    `
    <div style="margin-left: -13px; margin-right: -13px; text-align: center">  
       <a href="#" data-sender-id="${args.senderId}"
           style="padding-left: 40px"
           class="chat-button icon-books" 
           data-recipent-id="${args.recipentId}" data-order-id="${args.orderId}"  
           id="refuse-stages">Отказаться от заказа</a>  
    </div>
`;

function replaceButtons($container, buttons_html = []) {
    console.log("replace Buttons");
    $container.html("");
    for (var button_html of buttons_html) {
        $container.append(button_html);
    }
}


$(function () {

    function dialog(message, yesCallback, notCallback) {
        $("#dialog_delete .modal-title").html(message);
        $("#dialog_delete").modal('show');
        $("#btnYes").click(function (e) {
            e.preventDefault();
            yesCallback();
            $("#dialog_delete").modal('hide');
        });
        $("#btnNot").click(function (e) {
            e.preventDefault();
            notCallback();
            $("#dialog_delete").modal('hide');
        });
    }

    var form = document.getElementById('message_form');

    var url = '/chat/create/';

    // + Загрузка документов Team (complete)
    $("#upload-document-team").bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            sender: $("#team-chat-form #senderTeamId").val(),
            recipent: $("#team-chat-form #recipentTeamId").val(),
            order: $("#team-chat-form #orderTeamId").val(),
            team: $("#team-chat-form #teamId").val(),
        }
        console.log(data.formData);
    });

    //Загрузка документов Team (complete)
    $('#upload-document-team').fileupload({
        url: url,
        crossDomain: false,
        beforeSend: function (xhr, settings) {
            $('#progress .progress-bar').css(
                'width',
                '0%'
            );
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var currentValue = $("#documentSendIds").val();
                currentValue += file.id + ';';
                $("#documentSendIds").val(currentValue);
                var htmlImg = '<div style="float: left"><a href="/chat/download/' + file.name + '" class="send-doc" data-id="' + file.id + '">' + file.name + '</a>' +
                    '<div class="remove-document" data-id="' + file.id + '" style="right:-10px;float:left;position: static;"></div></div>';
                var document_send = $(htmlImg).appendTo("#document-send");
            });
        },
        fail: function (e) {
            console.log(e);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    // "Согласовать" (complete)
    $("#order-stages").on('click', "#approve-stages", function (e) {
        e.preventDefault();
        var orderId = $(this).data('order-id');
        var senderId = $(this).data('sender-id');
        var recipentId = $(this).data('recipent-id');
        var caption = "Вы действительно хотите согласовать этапы?";

        confirm(caption, function () {
            $(".stage-block-approve").each(function () {
                var stageId = $(this).data('id');
                $.ajax({
                    //async: false,
                    url: '/api/stages/' + stageId + '/',
                    type: 'PATCH',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                    },
                    data: "status=in_process",
                    dataType: 'json',
                    done: function (json) {
                        console.log(json);
                    },
                    fail: function (e) {
                        console.log('error');
                        console.log(e);
                    }
                });
            });

            $('.btns-approve-stages').hide();

            socket.send_stages_approve({
                "format_type": "approve_stages",
                "data": {
                    "sender_id": senderId,
                    "recipent_id": recipentId,
                    "order_id": orderId,
                    "msg": "Исполнитель согласовал этапы для заказа " + orderId,
                }
            });

            $('body').append('<div class="modal" id="popupOk" role="dialog" aria-labelledby="myModalLabel">' +
            '<div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button>' +
            '<h4 class="modal-title" id="myModalLabel">Уведомление</h4>' +
            '</div><div class="modal-body">Вы согласовали этапы</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">ОК</button>' +
            '</div></div></div></div>');
            $('#popupOk').modal('show');
        }.bind(orderId, senderId, recipentId), caption);



    });

    // "Отправить на внесение изменений" (complete)
    $("#order-stages").on('click', "#cancel-stages", function (e) {
        console.log("Отправить на внесение изменений");
        e.preventDefault();
        var senderId = $(this).attr('data-sender-id');
        var recipentId = $(this).attr('data-recipent-id');
        var orderId = $(this).attr('data-order-id');
        var caption = "Вы действительно хотите отказаться от этапов?";
        confirm(caption, function () {
            $(".stage-block-approve").each(function () {
                var stageId = $(this).data('id');
                $.ajax({
                    url: '/api/stages/' + stageId + '/',
                    type: 'PATCH',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                    },
                    data: "status=cancel_approve",
                    //data: "status=send_approve",
                    dataType: 'json',
                    done: function (json) {
                        console.log(json);

                    },
                    fail: function (e) {
                        console.log('error');
                        console.log(e);
                    }
                });

            });
            $('.btns-approve-stages').hide();

            socket.send_stages_approve({
                "format_type": "approve_stages",
                "data": {
                    "sender_id": senderId,
                    "recipent_id": recipentId,
                    "order_id": orderId,
                    "msg": "Исполнитель отказался от текущих этапов " + orderId,
                }
            });
        }.bind(), caption);

    });

    $(".team-chat-user").on('click', function (e) {
        e.stopPropagation();
        var recipentId = $(this).attr('data-id');
        $("#team-chat-form #recipentTeamId").val(recipentId);
    });

    $(".team-order-block").on('click', function () {

        $('.team-order-block, .team-block').each(function () {
            $(this).removeClass('orAct');
        });
        $(this).addClass('orAct');

        var teamIds = '';
        $.each($(this).find('.team-chat-user'), function (i, v) {
            teamIds += $(this).attr('data-id') + ";";
        });
        $("#team-chat-form #teamIds").val(teamIds);

        var teamId = $(this).attr('data-team-id');
        var orderId = $(this).attr('data-order-id');
        location.hash = '#teamorder' + orderId;

        var newCount = parseInt($("#count-tab-team").text());
        var currNewCount = parseInt($(".teamorder-count-" + teamId).text());
        var resCount = newCount - currNewCount;
        $("#count-tab-team").text(resCount);
        $(".teamorder-count-" + teamId).text(0)

        $("#team-chat-form #teamId").val(teamId);
        $("#team-chat-form #recipentTeamId").val("");
        $("#team-chat-form #orderTeamId").val(orderId);

        $("#add-form-team-note #teamNote").val(teamId);
        $("#add-form-team-note #orderNote").val(orderId);

        var inbox = document.getElementById('message-chat-team-space');
        inbox.innerHTML = '';
        var docList = document.getElementById('documentTeamSpace');
        docList.innerHTML = '';

        $.ajax({
            url: '/api/message',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'team': teamId, 'order': orderId},
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    var senderName = 'Вы';
                    var className = 'youChat';
                    if (v.sender.id !== userId) {
                        senderName = v.sender.username;
                        className = '';
                    }
                    inbox.innerHTML += '<div class="col-lg-12 insetCommChat ' + className + '"><div class="topCommChat">' +
                        '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
                        '<p class="textCommChat">' + v.text + '</p></div>';
                });
                var height = inbox.scrollHeight;
                inbox.scrollTop = height;
            }
        });

        $.ajax({
            url: '/api/documents',
            type: 'GET',
            data: {
                csrfmiddlewaretoken: csrftoken,
                'order': orderId,
                'team': teamId,
                'is_delete': false,
                'is_send': true,
            },
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'order': orderId, 'team': teamId},
            dataType: 'json',
            success: function (json) {
                console.log(json.results);
                var noteHtmlInbox = '';
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += '<li>' + v.text + '<li>';

                });
                $(".team-notes-block").html(noteHtmlInbox);
            }
        });

    });

    // Отображаем сообщения "Группы"(copy)
    $('.team-block').on('click', function () {

        $('.team-order-block, .team-block').each(function () {
            $(this).removeClass('orAct');
        });
        $(this).addClass('orAct');

        var teamIds = '';
        $.each($(this).find('.team-chat-user'), function (i, v) {
            teamIds += $(this).attr('data-id') + ";";
        });
        $("#team-chat-form #teamIds").val(teamIds);

        var inbox = document.getElementById('message-chat-team-space');
        inbox.innerHTML = '';

        var docList = document.getElementById('documentTeamSpace');
        docList.innerHTML = '';

        var teamId = $(this).attr('data-team-id');
        location.hash = '#myteam' + teamId;

        var newCount = parseInt($("#count-tab-team").text());
        var currNewCount = parseInt($(".team-count-" + teamId).text());
        var resCount = newCount - currNewCount;
        $("#count-tab-team").text(resCount);
        $(".team-count-" + teamId).text(0)

        $("#team-chat-form #teamId").val(teamId);
        $("#add-form-team-note #teamNote").val(teamId);
        $("#team-chat-form #recipentTeamId").val("");
        $("#team-chat-form #orderTeamId").val("");
        $("#add-form-team-note #orderNote").val("");

        $.ajax({
            url: '/api/message',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'team': teamId, 'order__isnull': 'true'},
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    var senderName = 'Вы';
                    var className = 'youChat';
                    if (v.sender.id !== userId) {
                        senderName = v.sender.username;
                        className = '';
                    }
                    inbox.innerHTML += '<div class="col-lg-12 insetCommChat ' + className + '"><div class="topCommChat">' +
                        '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
                        '<p class="textCommChat">' + v.text + '</p></div>';
                });
                var height = inbox.scrollHeight;
                inbox.scrollTop = height;
            }
        });

        $.ajax({
            url: '/api/documents',
            type: 'GET',
            data: {
                csrfmiddlewaretoken: csrftoken,
                'team': teamId,
                'is_delete': false,
                'is_send': true,
            },
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'team': teamId},
            dataType: 'json',
            success: function (json) {
                console.log(json.results);
                var noteHtmlInbox = '';
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += '<li>' + v.text + '<li>';

                });
                $(".team-notes-block").html(noteHtmlInbox);
            }
        });

    });

    // Заполняем страницу Заказа(complete)
    $('.order-block').on('click', function () {
        console.log("Заполняем страницу Заказа");
        $('.order-block').each(function () {
            $(this).removeClass('orAct');
        });
        $(this).addClass('orAct');
        var orderId = $(this).data('id');
        location.hash = '#order' + orderId;
        var senderId = '';
        var recipentId = $(this).data('recipent-id');
        var projectId = $(this).data('project-id');
        var teamCurrentId = parseInt($(this).data('team-id'));

        var newCount = parseInt($("#count-tab-order").text());
        var currNewCount = parseInt($(".order-count-" + orderId).text());
        var resCount = newCount - currNewCount;
        $("#count-tab-order").text(resCount);
        $(".order-count-" + orderId).text(0);

        if (teamCurrentId > 0) {
            $("#fromTeamId").val(teamCurrentId);
            $("#fromContractorId").val("");
        } else {
            $("#fromTeamId").val("");
            $("#fromContractorId").val(userId);
        }

        $("#chat-order-add #orderId").val(orderId);
        $("#add-form-order-note #orderNote").val(orderId);
        $("#orderArbitrationId").val(orderId);
        $("#projectReviewId").val(projectId);

        $("#chat-order-add #recipentId").val(recipentId);
        $("#targetCustomerId").val(recipentId);
        $("#add-form-order-note #recipentNote").val(recipentId);

        var docList = document.getElementById('documentOrderSpace');
        var inbox = document.getElementById('message-chat-order-space');
        inbox.innerHTML = '';
        docList.innerHTML = '';

        $.ajax({
            url: '/api/message',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'order': orderId, 'team__isnull': 'true'},
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    var senderName = 'Вы';
                    var className = 'youChat';
                    if (v.sender.id !== userId) {
                        senderName = v.sender.username;
                        className = '';
                    }
                    inbox.innerHTML += '<div class="col-lg-12 insetCommChat ' + className + '"><div class="topCommChat">' +
                        '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
                        '<p class="textCommChat">' + v.text + '</p></div>';
                });

                var height = inbox.scrollHeight;
                inbox.scrollTop = height;
            }
        });

        $.ajax({
            url: '/api/documents',
            type: 'GET',
            data: {
                csrfmiddlewaretoken: csrftoken,
                'order': orderId,
                'is_delete': false,
                'is_send': true,
            },
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'order': orderId},
            dataType: 'json',
            success: function (json) {
                console.log(json.results);
                var noteHtmlInbox = '';
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += '<li>' + v.text + '<li>';

                });
                $(".order-notes-block").html(noteHtmlInbox);
            }
        });

        $.ajax({
            url: '/api/orders/' + orderId + '/',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken},
            dataType: 'json',
        }).then(function (data) {
            var htmlInbox = "";
            var stagesReservedHtml = "";
            var stagesPaidProcess = [];
            var stagesInWork = [];
            var stagesResults = data.stages;
            var statusNotAgreed = false;
            var statusSendApprove = false;
            var stagesCompleted = [];
            if (stagesResults.length > 0) {
                $.each(stagesResults, function (i, v) {
                    console.log(v.status);
                    if (v.status == "completed") {
                        stagesCompleted.push(v);
                    }
                    if (v.status == "not_agreed" || v.status == "send_approve") {
                        statusNotAgreed = true;
                    }
                    if (!data.secure) {
                        if (v.status == "in_process") {
                            stagesInWork.push(v);

                        }
                    } else if ((v.status == "in_process") && (v.is_paid)) {
                        stagesPaidProcess.push(v);
                        stagesInWork.push(v);
                    }

                    if (data.secure) {
                        if (v.is_paid) {
                            stagesReservedHtml += '<li class="reserved">Сумма за этап ' + v.pos + '.Зарезервирована.</li>';
                        } else {
                            stagesReservedHtml += '<li class="unreserved">Сумма за этап ' + v.pos + '.Не зарезервирована.</li>';
                        }
                    }

                    var statusName = '';
                    switch (v.status) {
                        case 'not_agreed':
                            statusName = 'Не согласован';
                            break;
                        case 'send_approve':
                            statusName = 'На согласовании';
                            break;
                        case 'cancel_approve':
                            statusName = 'Исполнитель отказался';
                            break;
                        case 'in_process':
                            statusName = 'В процессе';
                            break;
                        case 'completed':
                            statusName = 'Завершен';
                            break;
                    }
                    if (v.status == 'completed') {
                        statusName = 'Завершен';
                    }

                    htmlInbox += '<div data-id="' + v.id + '" class="numberStepp box-sizing stage-block-approve"><div class="insetNumStepp">' +
                        '<p class="titleNumStepp"><span>Этап ' + v.pos + '</span>' + v.name + '</p>' +
                        '<p class="textNumStepp">Результаты этапа:' + v.result + '</p><div>' +
                        '<p> Срок до ' + v.term + '</p><span>' + v.cost + ' <i class="fa fa-rub"></i></span>' +
                        '<p>Cрок этапа рассчитывается с момента резервирования средств</p>' +
                        '</div><div><p>' + statusName + '</p></div></div></div>';
                });


                if (statusNotAgreed) {
                    // htmlInbox += wrapper_btn_tmpl(
                    //     btn_tmpl_approve() + btn_tmpl_change() + btn_tmpl_refuse()
                    // );
                    if (stagesResults[0] && stagesResults[0].status == 'send_approve') {
                        htmlInbox +=
                            wrapper_btn_tmpl(btn_tmpl_approve({
                                    senderId: senderId,
                                    recipentId: recipentId,
                                    orderId: orderId
                                })
                                + btn_tmpl_change({
                                    senderId: senderId,
                                    recipentId: recipentId,
                                    orderId: orderId
                                })
                                + btn_tmpl_refuse({
                                    senderId: senderId,
                                    recipentId: recipentId,
                                    orderId: orderId
                                })
                            );
                    } else {
                        htmlInbox += wrapper_btn_tmpl(btn_tmpl_refuse({
                                senderId: senderId,
                                recipentId: recipentId,
                                orderId: orderId
                            })
                        );

                    }
                }
            }

            $("#order-stages").html(htmlInbox);
            $(".stages-paid").html(stagesReservedHtml);
            if (data.secure) {
                $("#reserveSpace").show();

            } else {
                $("#reserveSpace").hide();
            }

            if (stagesInWork.length > 0) {
                $("#completeWork").show();
                var stage = stagesInWork[0];
                var stageWork = '<div class="numberStepp box-sizing">' +
                    '<div class="insetNumStepp"><div><p>В работе ' + stage.name + '</p></div>' +
                    '<div><p>Результат этапа : ' + stage.result + '</p></div>' +
                    '<div><p>Срок сдачи ' + stage.term + ' <span>' + stage.cost + '<i class="fa fa-rub"></i></span></p></div></div></div>';

                if (!stage.close_contractor) {
                    stageWork += '<div class="textAreaBlock2 FFD box-sizing disTab"><a href="#" class="closeStage" data-sender-id="' + userId + '" data-recipent-id="' + recipentId + '"' +
                        ' data-order-id="' + data.id + '" data-stage-id="' + stage.id + '">Завершить этап</a></div>';
                } else {
                    stageWork += '<div><p>Этап ожидает завершения статуса от заказчика</p><div>';
                }
                if (data.secure) {
                    stageWork += '<div class="textAreaBlock2 FFD box-sizing disTab"><a href="#" data-toggle="modal" data-target="#arbitration-add">Обратитьсяв арбитраж</a></div>';
                }
                $("#stagesWork").html(stageWork);
            } else {
                $("#completeWork").hide();
            }
            if (!data.has_user_review) {
                if ((stagesCompleted.length == stagesResults.length) && stagesCompleted.length > 0) {
                    $("#leaveReview").show();
                } else {
                    $("#leaveReview").hide();
                }
            } else {
                $("#leaveReview").hide();
            }
        });


    });

    // Закрыть этап (complete)
    $('#tab2').on('click', '.closeStage', function (e) {
        e.preventDefault();
        var stageId = $(this).attr('data-stage-id');
        var _this = $(this);
        $.ajax({
            url: '/api/stages/' + stageId + '/',
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: "close_contractor=True",
            dataType: 'json',
            success: function (json) {

                socket.send_stages_approve({
                    "format_type": "approve_stages",
                    "data": {
                        "sender_id": _this.attr('data-sender-id'),
                        "recipent_id": _this.attr('data-recipent-id'),
                        "order_id": _this.attr('data-order-id'),
                        "msg": "Исполнитель завершил этап " + json.name,
                    }
                });

                console.log(json);
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    });

    //Добавить сообщение для исполнителей в группе(copy)
    $("#add-team-chat-message").on('click', function (e) {
        e.preventDefault();
        var chatMessage = $("#team-chat-form #chatText").val();
        var recipentId = $("#team-chat-form #recipentTeamId").val();
        var senderId = $("#team-chat-form #senderTeamId").val();
        var teamId = $("#team-chat-form #teamId").val();
        var orderId = $("#team-chat-form #orderTeamId").val();
        var documentSendIds = $("#documentSendIds").val();
        var teamIds = $("#team-chat-form #teamIds").val();
        var sendLinks = $("#document-send a");
        if (chatMessage || sendLinks.length > 0) {
            var sendLinkIds = "";
            var documentLinks = "";
            var documentAttachFiles = "";
            $.each(sendLinks, function (i, v) {
                sendLinkIds += $(this).attr('data-id') + ';';
                documentLinks += 'Приложенный файл. скачать: <br> <a href="' + $(this).attr('href') + '">' + $(this).text() + '</a><br>';
                documentAttachFiles += '<li style="word-break: break-all;">' +
                    '<a class="file-link" href="' + $(this).attr('href') + '">' + $(this).text() + '</a>' +
                    '<div class="remove-document" data-id="' + $(this).attr('data-id') + '" style="right:-10px;"></div></li>';

            });
            socket.send_message({
                "format_type": "add_message_team",
                "data": {
                    "sender_id": senderId,
                    "recipent_id": recipentId,
                    "chat_message": chatMessage,
                    "team_id": teamId,
                    "team_ids": teamIds,
                    "order_id": orderId,
                    "document_send_links": sendLinkIds,
                    "document_data": {
                        "document_links": documentLinks,
                        "document_attach_files": documentAttachFiles,
                    }
                }
            });

            $("#team-chat-form #chatText").val("");
            $("#document-send").html("");
            $("#documentSendIds").val("");
        } else {
            $("#team-chat-form .errorEmptyMessage").show();
        }
    });

});
