$(function () {

    var form = document.getElementById('message_form');

    $('body').on('focus', ".term-picker", function () {
        $(this).datepicker({
            minDate: 0,
        });
    })

    $("#reserve-button").on("click", function (e) {
        e.preventDefault();
        $("#reserve-stage-modal").modal('show');
        var orderId = $(this).attr('data-order-id');
        $.ajax({
            url: '/api/stages/',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'order': orderId},
            dataType: 'json',
            success: function (json) {
                var outputValues = '';
                var totalSum = 0;
                var stagesIds = '';
                var notPaidCount = 0;
                $.each(json.results, function (i, v) {
                    if ((v.status == 'in_process') && (!v.is_paid)) {
                        totalSum += parseInt(v.cost);
                        notPaidCount += 1;
                        outputValues += '<option data-stage-sum="' + v.cost + '" value="' + v.id + '">' + v.name + '</option>';
                        stagesIds += v.id + ';'
                    }
                });

                $("#stagesSelect").html(outputValues);
                $(".totalSum").text(totalSum);

                if (json.results.length > notPaidCount && notPaidCount > 0) {
                    $("#choiceWayOrder").hide();
                }

                $("#choiceWayOrder").val(totalSum);
                $("#ordermodalId").val(orderId);

                $("#stages-pay-form #stageSumPay").val(totalSum);
                $("#stages-pay-form #stagesIds").val(stagesIds);

                $("#choiceWayOrder").attr('data-stages-ids', stagesIds);

                $("#stagesSelect").selectpicker({
                    style: 'btn-info',
                    size: 4
                });


            }
        });

    });

    $("#tab2").on("change", "input[name=choice_way]:radio", function (e) {
        var sumStage = $("#stagesSelect").find('option:selected').attr('data-stage-sum');
        var currIdStage = $("#stagesSelect").find('option:selected').val();
        $("#choiceWayStage").val(sumStage);
        var currValue = $(this).val();
        $("#stages-pay-form #stageSumPay").val(currValue);
        var selectId = ($(this).attr('id'));
        if (selectId == 'choiceWayOrder') {
            $("#stages-pay-form #stagesIds").val($(this).attr('data-stages-ids'));
            $("#stagesSelect").prop('disabled', 'disabled');
        } else {
            $("#stages-pay-form #stagesIds").val(currIdStage);
            $("#stagesSelect").prop('disabled', false);
            $(".stageSum").text(sumStage);
        }
    });

    $("#tab2").on("change", "#stagesSelect", function (e) {
        var sumStage = $(this).find('option:selected').attr("data-stage-sum");
        $("#choiceWayStage").val(sumStage);
        $("#stages-pay-form #stageSumPay").val(sumStage);
        $(".stageSum").text(sumStage);
        $("#stages-pay-form #stagesIds").val($(this).val());
    });

    //Получить заказы
    function getStages(orderId, senderId, recipentId, secureOrder) {
        $.ajax({
            url: '/api/orders/' + orderId + '/',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken},
            dataType: 'json',
        }).then(function (data) {
            var isReviewLeave = data.has_user_review;
            var stagesResults = data.stages;
            var stageCount = stagesResults.length;
            if (stageCount == 0) {
                $("#reserveSpace").hide();
                stageCountVal = 1;
            } else {
                stageCountVal = stageCount;
            }
            var htmlInbox = "";
            var htmlInboxStage = '<p class="textStepss">Какое кол-во этапов подразумевает работа? ' +
                '<input type="text" id="countStage" value="' + stageCountVal + '"size="3"/></p>';

            if (stageCount == 0) {
                htmlInboxStage += '<div class="numberStepp box-sizing" id="stage1">' +
                    '<p>Этап <span class="stage-span-id">1</span></p><form class="new-stages-form" id="stage-form">' +
                    '<label>Название</label><input class="form-control" name="name" type="text"><p class="error error-name"></p>' +
                    '<label>Цена</label><input class="form-control" name="cost" type="text"><p class="error error-cost"></p>' +
                    '<input class="form-control orderStagesInput" name="order" type="hidden" value="' + orderId + '">' +
                    '<input class="form-control" type="hidden" name="status" value="send_approve">' +
                    '<label>Срок</label><input class="term-picker form-control datepicker" name="term" type="text"><p class="error error-term"></p>' +
                    '<label>Результат</label><input class="form-control" name="result" type="text"><p class="error error-result"></p>' +
                    '<input class="form-control" name="pos" value="1" type="hidden">' +
                    '</form></div>';
            }
            var statusNotAgreed = true;
            var stagesInWork = [];
            var stagesPaidProcess = [];
            var stagesCompleted = [];
            var stagePaidCount = 0;
            var stagesReservedHtml = "";

            $.each(stagesResults, function (i, v) {
                if (v.status == "completed") {
                    stagesCompleted.push(v);
                }
                if (!data.secure) {
                    if (v.status == "in_process") {
                        stagesInWork.push(v);
                    }
                } else if ((v.status == "in_process") && (v.is_paid)) {
                    stagesInWork.push(v);
                    stagesPaidProcess.push(v);
                }

                if (v.is_paid) {
                    stagePaidCount += 1;
                }

                if (v.status == "not_agreed" || v.status == 'cancel_approve' || v.status == 'send_approve') {
                    htmlInbox += '<div class="numberStepp box-sizing">' +
                        '<p>Этап</p><form class="update-stages-form" data-stage-id="' + v.id + '" id="stage-form-' + v.pos + '">' +
                        '<label>Название</label><input class="form-control" type="text" name="name" value="' + v.name + '"><p class="error error-name"></p>' +
                        '<label>Цена</label><input class="form-control" type="text" name="cost" value="' + v.cost + '" ><p class="error error-cost"></p>' +
                        '<input class="form-control" name="pos" type="hidden" value="' + v.pos + '" ><p class="error error-pos"></p>' +
                        '<input class="form-control orderStagesInput" type="hidden" name="order" value="' + v.order + '">' +
                        '<input class="form-control" type="hidden" name="status" value="send_approve">' +
                        '<label>Срок</label><input class="term-picker form-control datepicker" type="text" name="term" value="' + v.term + '" ><p class="error error-term"></p>' +
                        '<label>Результат</label><input class="form-control" type="text" name="result" value="' + v.result + '" ><p class="error error-result"></p>' +
                        '</form></div>';
                } else {
                    statusNotAgreed = false;
                    htmlInboxStage = "";
                    var statusName = "";
                    switch (v.status) {
                        case 'completed':
                            statusName = 'Завершен';
                            break;
                        case 'in_process':
                            statusName = 'Согласовано';
                            break;
                        default:
                            statusName = '';
                            break;
                    }
                    if (v.status == 'completed') {
                        statusName = 'Завершен';
                    }
                    htmlInbox += '<div class="numberStepp box-sizing"><div class="insetNumStepp">' +
                        '<p class="titleNumStepp"><span>Этап ' + v.pos + '</span>' + v.name + '</p>' +
                        '<p class="textNumStepp">Результаты этапа:' + v.result + '</p><div>' +
                        '<p>Срок до ' + v.term + '</p><span>' + v.cost + '<i class="fa fa-rub"></i></span>' +
                        '<p>Cрок заказа рассчитывается с момента резервирования средств</p>' +
                        '</div><div><p style="font-color:red;">' + statusName + '</p></div></div></div>';

                }
                if (data.secure) {
                    if (v.is_paid) {
                        stagesReservedHtml += '<li class="reserved">Сумма за этап ' + v.pos + '.Зарезервирована.</li>';
                    } else {
                        stagesReservedHtml += '<li class="unreserved">Сумма за этап ' + v.pos + '.Не зарезервирована.</li>';
                    }
                }
            });

            if (stagesResults.length == stagePaidCount && data.secure) {
                $("#reserve-button").parent().hide();
            } else {
                $("#reserve-button").parent().show();
            }

            if (statusNotAgreed) {
                if (!data.secure) {
                    htmlInbox += '<div class="box-sizing disTab">' +
                        '<div class="checkbox"><label><input id="id_cro" name="cro" type="checkbox"><span></span></label>' +
                        'Перейти в режим безопасной сделки</div></div>';
                }

                htmlInbox += '<div class="textAreaBlock2 box-sizing disTab">' +
                    '<a href="#" data-sender-id="' + senderId + '" ' +
                    'data-recipent-id="' + recipentId + '" data-order-id="' + orderId + '" ' +
                    'id="addStagesForm">отправить на согласование</a> </div>';
            } else if ((stagesResults.length > 0) && (data.secure)) {
                $("#reserveSpace").show();
            }

            htmlInbox = htmlInboxStage + htmlInbox;
            $("#order-stages").html(htmlInbox);
            $("#completeWork").hide();
            $("#completeWork").hide();

            if (stagesInWork.length > 0) {
                $("#completeWork").show();
                var stage = stagesInWork[0];
                var stageWork = '<div class="numberStepp box-sizing">' +
                    '<div class="insetNumStepp"><div><p>В работе ' + stage.name + '</p></div> ' +
                    '<div><p>Результат этапа : ' + stage.result + '</p></div>' +
                    '<div><p>Срок сдачи ' + stage.term + '</p><span>' + stage.cost + '<i class="fa fa-rub"></i></span></div>' +
                    '</div></div>';

                if (stage.close_contractor) {
                    stageWork += '<div class="textAreaBlock2 FFD box-sizing disTab"><a href="#" class="closeStage" data-order-id="' + orderId + '" data-sender-id="'+ userId +'"' +
                        ' data-recipent-id="' + recipentId + '" data-stage-id="' + stage.id + '">Закрыть этап ' + stage.pos + '</a></div>';
                }

                if (data.secure) {
                    stageWork += '<div class="textAreaBlock2 FFD box-sizing disTab"><a href="#" data-toggle="modal" data-target="#arbitration-add">Обратитьсяв арбитраж</a></div>';
                }
                $("#stagesWork").html(stageWork);
            }

            if (!data.secure) {
                $("#reserveSpace").hide();
            }

            if (!data.has_user_review) {
                if ((stagesCompleted.length == stagesResults.length) && (stagesCompleted.length > 0) && (!isReviewLeave)) {
                    $("#leaveReview").show();
                } else {
                    $("#leaveReview").hide();
                }
            } else {
                $("#leaveReview").hide();
            }
            $(".stages-paid").html(stagesReservedHtml);

        });
    }

    //Закрыть этап
    $('#tab2').on('click', '.closeStage', function (e) {
        e.preventDefault();
        var stageId = $(this).attr('data-stage-id');
        var _this = $(this);
        $.ajax({
            url: '/api/stages/' + stageId + '/',
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {close_customer: true, status: 'completed'},
            dataType: 'json',
            success: function (json) {
                socket.send_stages_approve({
                    "format_type": "approve_stages",
                    "data": {
                        "sender_id": _this.attr('data-sender-id'),
                        "recipent_id": _this.attr('data-recipent-id'),
                        "order_id": _this.attr('data-order-id'),
                        "msg": "Заказчик закрыл этап " + json.name,
                    }
                });
                console.log(json);
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    });

    // Добавление этапов(complete)
    $("#order-stages").on('click', "#addStagesForm", function (e) {
        e.preventDefault();
        var currentOrderId = $(this).attr('data-order-id');
        var secureOrderEl = $("#secureOrder");
        if (secureOrderEl.length > 0) {
            var secOrderVal = false;
            if (secureOrderEl.prop('checked')) {
                secOrderVal = true;
            }
            $.ajax({
                url: '/api/orders/' + currentOrderId + '/',
                type: 'PATCH',

                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                },
                data: {secure: secOrderVal},
                dataType: 'json',
                success: function (json) {
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        var callbacks = [];

        $(".new-stages-form").each(function (i, v) {
            var _this = $(this);
            console.log($(this).serialize());
            $.ajax({
                    async: false,
                    url: '/api/stages/',
                    type: 'POST',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                    },
                    data: $(this).serialize(),
                    dataType: 'json'
                })
                .done(function (json) {
                    _this.removeClass('new-stages-form').addClass('update-stages-form');
                    _this.find('.error').html("");
                    console.log(json);
                })
                .fail(function (xhr, errorMsg, error) {
                    console.log(xhr);
                    $.each(xhr.responseJSON, function (i, v) {
                        _this.find('.error-' + i).html(v).css('color', 'red');
                        console.log(v);
                        console.log(i);
                    });
                });

        });


        $(".update-stages-form").each(function (i, v) {
            var _this = $(this);
            var currentStageId = parseInt($(this).attr('data-stage-id'));
            console.log($(this).serialize());
            $.ajax({
                    async: false,
                    url: '/api/stages/' + currentStageId + '/',
                    type: 'PUT',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                    },
                    data: $(this).serialize(),
                    dataType: 'json'
                })
                .done(function (json) {
                    _this.find('.error').html("");
                })
                .fail(function (xhr, errorMsg, error) {
                    $.each(xhr.responseJSON, function (i, v) {
                        _this.find('.error-' + i).html(v).css('color', 'red');
                        console.log(v);
                        console.log(i);
                    });
                });
        });

        var currentRecipentId = $(this).attr('data-recipent-id');
        var secureOrder = true

        socket.send_stages_approve({
            "format_type": "approve_stages",
            "data": {
                "sender_id": userId,
                "recipent_id": currentRecipentId,
                "order_id": currentOrderId,
                "msg": "Этапы для заказа " + currentOrderId + " изменены",
            }
        });
        $('body').append('<div class="modal" id="popupOk" role="dialog" aria-labelledby="myModalLabel">' +
            '<div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button>' +
            '<h4 class="modal-title" id="myModalLabel">Уведомление</h4>' +
            '</div><div class="modal-body">Этапы отправлены исполнителю для согласования</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">ОК</button>' +
            '</div></div></div></div>');
        $('#popupOk').modal('show');
        //$.jGrowl("Этапы для заказа изменены и отправлены исполнителю", { life: 15000});

    });

    //Изменение счетчика(complete)
    $('#order-stages-tab').on('change', '#countStage', function () {
        var countStage = parseInt($(this).val());
        var currentCountStage = $("#order-stages .numberStepp").length;
        if ((countStage < 1) || isNaN(countStage)) {
            countStage = 1;
            $('#order-stages-tab #countStage').val(currentCountStage);
        } else {
            if (countStage > currentCountStage) {
                for (var jj = currentCountStage; jj < countStage; jj++) {
                    var pos = jj + 1;
                    var lastFormStage = $("#order-stages .numberStepp").last();
                    var orderId = lastFormStage.find('.orderStagesInput').val();
                    var addFormTemplate = '<div class="numberStepp box-sizing" id="stage1">' +
                        '<p>Этап <span class="stage-span-id">' + pos + '</span></p><form class="new-stages-form" id="stage-form">' +
                        '<label>Название</label><input class="form-control" name="name" type="text"><p class="error error-name"></p>' +
                        '<label>Цена</label><input class="form-control" name="cost" type="text"><p class="error error-cost"></p>' +
                        '<input class="form-control orderStagesInput" name="order" type="hidden" value="' + orderId + '">' +
                        '<input class="form-control" type="hidden" name="status" value="send_approve">' +
                        '<label>Срок</label><input class="term-picker form-control datepicker" name="term" type="text"><p class="error error-term"></p>' +
                        '<label>Результат</label><input class="form-control" name="result" type="text"><p class="error error-result"></p>' +
                        '<input class="form-control" name="pos" value="' + pos + '" type="hidden"></form></div>';
                    lastFormStage.after(addFormTemplate);
                }

            } else if (countStage < currentCountStage) {
                var ii = currentCountStage;
                $($("#order-stages .numberStepp").get().reverse()).each(function () {
                    var currenFormName = ($(this).find('form').attr('class'));
                    if (ii > countStage) {
                        $(this).remove();
                    }
                    ii--;
                });

            }
        }
    });


    // Для заказов все вытащить(complete)
    $('.order-block').on('click', function () {
        var newCount = parseInt($("#count-tab-order").text());
        $("#chat-order-add").css("display", "block");
        $("#add-form-order-note").css("display", "block");
        $("#formsetStage").css("display", "block");

        $('.order-block').each(function () {
            $(this).removeClass('orAct');
        });

        $(this).addClass('orAct');
        var orderId = $(this).attr('data-id');
        location.hash = '#order' + orderId;
        var projectId = $(this).attr('data-project-id');
        var recipentId = $(this).attr('data-recipent-id');
        var secureOrder = $(this).attr('data-secure-deal');
        secureOrder = Boolean(secureOrder);

        var currNewCount = parseInt($(".order-count-" + orderId).text());
        var resCount = newCount - currNewCount;
        $("#count-tab-order").text(resCount);
        $(".order-count-" + orderId).text(0);

        var teamCurrentId = parseInt($(this).attr('data-team-id'));

        if (teamCurrentId>0){
            $("#targetTeamId").val(teamCurrentId);
            $("#targetContractorId").val("");
        }else {
            $("#targetTeamId").val("")
            $("#targetContractorId").val(recipentId);
        }
        $("#chat-order-add #orderId").val(orderId);
        $("#add-form-order-note #orderNote").val(orderId);
        $("#orderArbitrationId").val(orderId);
        $("#projectReviewId").val(projectId);
        $("#reserve-button").attr('data-order-id', orderId);

        $("#chat-order-add #recipentId").val(recipentId);
        $("#add-form-order-note #recipentNote").val(recipentId);
        $(".orderStagesInput").val(orderId);
        var inbox = document.getElementById('message-chat-order-space');
        var docList = document.getElementById('documentOrderSpace');
        inbox.innerHTML = '';
        docList.innerHTML = '';

        $.ajax({
            url: '/api/message',
            type: 'GET',
            data: {csrfmiddlewaretoken: csrftoken, 'order': orderId, 'team__isnull': 'true'},
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    var senderName = 'Вы';
                    var className = 'youChat';

                    if (v.sender.id !== userId) {
                        senderName = v.sender.username;
                        className = '';
                    }

                    inbox.innerHTML += '<div class="col-lg-12 insetCommChat ' + className + '"><div class="topCommChat">' +
                        '<p class="nameCommChat">' + senderName + '</p><span>' + v.created + '</span></div>' +
                        '<p class="textCommChat">' + v.text + '</p></div>';

                });
                var height = inbox.scrollHeight;
                inbox.scrollTop = height;
            }
        });

        // Нечто непонятное
        $.ajax({
            url: '/api/users/' + userId + '/',
            type: 'GET',
            data: {
                csrfmiddlewaretoken: csrftoken,
            },
            dataType: 'json',
            success: function (json) {
                var score = json.score;
                $("#order-stages-tab").attr('data-score', score);
            },
            error: function (e, jqxhr) {
                console.log(jqxhr);
            }

        })

        $.ajax({
            url: '/api/documents',
            type: 'GET',
            data: {
                csrfmiddlewaretoken: csrftoken,
                'order': orderId,
                'is_delete': false,
                'is_send': true,
            },
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            data: {
                csrfmiddlewaretoken: csrftoken,
                'order': orderId,
            },
            dataType: 'json',
            success: function (json) {
                var noteHtmlInbox = '';
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += '<li>' + v.text + '<li>';
                });
                $(".order-notes-block").html(noteHtmlInbox);
            }
        });
        getStages(orderId, userId, recipentId, secureOrder);
    });

})
;
