$(document).ready(function(){

	$('.messd:first').addClass('mesAct');
	$('.orderBlock:first').addClass('orAct');
    
    if ($.cookie('slideResVisible')) {
        $('.slideRes').slideDown(300);
        $('.resButtonF1').css('transform','rotate(0deg)');
        $('#extraFields').val('on')
    } else {
        $('.slideRes').slideUp(300);
        $('.resButtonF1').css('transform','rotate(180deg)');
        $('#extraFields').val('')
    }
    
	$('.resButtonF1').click(function(e){
		e.preventDefault();
		$('.slideRes').toggleClass('activeSlide');
		if($('.slideRes').hasClass('activeSlide')) {
			$('.slideRes').slideUp(300);
			$(this).css('transform','rotate(180deg)');
            $.cookie('slideResVisible', '', {expires: new Date(new Date().getTime() + 300000)}) // 5 minutes
            $('#extraFields').val('')
		} else {
			$('.slideRes').slideDown(300);
			$(this).css('transform','rotate(0deg)');
            $.cookie('slideResVisible', 'on', {expires: new Date(new Date().getTime() + 300000)})
            $('#extraFields').val('on')
		}
	});

	$('.showPress').click(function(){
		var ind = $('.showPress').index(this);
		$('.showSpec').eq(ind).slideDown(300);
	});


	$('.selectpicker').selectpicker({
  		style: 'btn-info',
  		size: 10,
  		width: '237px',
		liveSearch: true,
		liveSearchNormalize: true,
		liveSearchPlaceholder: 'Поиск',
		noneSelectedText: 'Выберите значение'
	});

	$('.selectpicker6').selectpicker({
  		style: 'btn-info',
  		size: 10,
  		width: '237px',
		noneSelectedText: 'Выберите значение'
	});

	$('.selectpicker2').selectpicker({
  		style: 'btn-info',
  		size: 4,
  		width: '90px',
		noneSelectedText: 'Выберите значение'
	});

	$('.selectpicker3').selectpicker({
  		style: 'btn-info',
  		size: 4,
  		width: '360px',
		noneSelectedText: 'Выберите значение'
	});

	$('.selectpicker4').selectpicker({
  		style: 'btn-info',
  		size: 4,
  		width: '254px',
		noneSelectedText: 'Выберите значение'
	});

	$('.selectpicker5').selectpicker({
  		style: 'btn-info',
  		size: 4,
  		width: '117px',
		noneSelectedText: 'Выберите значение'
	});

});
