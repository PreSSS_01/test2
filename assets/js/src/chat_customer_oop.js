import {ChatPageController} from './chat/ChatCustomerPageController'
import {
    bindOrders,
    bindOrderInfo,
    bindArbitrationSend,
    bindOnTabs,
    bindUserContacts,
    bindGetUserMessages,
    bindDeleteContact,
    bindCtrlEnterSendMessage,
    restoreTabFromHash
} from './chat/BINDS'


import {chatContactsInit, chatOrdersInit} from './chat/chats'
import {uploadDocumentsOrderInit, uploadDocumentsContactInit} from './chat/documents'
import {connect} from './chat/wsChatConnect'

import {bindContractorNotes, bindOrderNotes} from './chat/notes'

import {bindArchiveProjects} from './chat/archiveProjects'

window.connect = connect;
window.socket = undefined;

$(function () {
    $('body').on('focus', ".term-picker", function () {
        $(this).datepicker({
            minDate: 0,
        });
    });

    bindArbitrationSend();
    window.onhashchange = function (e) {
        // console.log("Change Hash!!! ", 'a[data-toggle="tab"][href="#' + location.hash.slice(1) + '"]');
        $('a[data-toggle="tab"][href="#' + location.hash.slice(1) + '"]').trigger("click");
    };

    window.chatController = new ChatPageController();
    bindOrders();
    bindOrderInfo();
    bindOnTabs();
    restoreTabFromHash();
    bindUserContacts();
    bindGetUserMessages();
    bindArchiveProjects();
    bindDeleteContact();
    bindCtrlEnterSendMessage();

    //Chats
    chatContactsInit();
    chatOrdersInit();

    //Documents
    uploadDocumentsContactInit();
    uploadDocumentsOrderInit();

    //Notes
    bindContractorNotes();
    bindOrderNotes();

});