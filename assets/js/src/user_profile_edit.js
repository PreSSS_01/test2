import {avatarUploadInit} from './seeds/avatar_upload'
import {tabsHashInit, restoreTab} from './seeds/bootstrap_tabs'
import {checkBoxBindingInit} from './seeds/user_check_statuses'
import {customSelectInit} from './seeds/custom_select'

$(function () {
    avatarUploadInit();
    tabsHashInit();
    checkBoxBindingInit();
    customSelectInit();
});