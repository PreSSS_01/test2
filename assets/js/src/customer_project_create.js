import {fileUploadInit} from './seeds/file_upload'
import {extendedFieldInit} from './seeds/extended_field'
import {customCheckInit} from './seeds/custom_check'
import {readMoreInit} from './seeds/read_more'
import {onlyOneCheckboxInit} from './seeds/only_one_checkbox'
import {showPopupsInit, addMessage} from  './seeds/popups'
import {scrollOnRequiredInit} from './seeds/scroll_on_required'
import {ajaxRegistrationInit} from './seeds/ajax_registration'
import {print} from './seeds/test_seeds'

function showHideRealry() {
    let check = $('#checkbox-sb-realty');
    check.on("click", function (e) {
        let $realty = $('#sb-realty');
        if ($(e.target).prop("checked")) {
            $realty.show();
        } else {
            $realty.hide();
        }
    })
}

function changeButtonText() {
    //Если перешли со страницы профиля по кнопке "Добавить заказ"
    let id = window.location.hash.replace("#", "");
    if (id) $('.js-change-text').val("ДОБАВИТЬ ЗАКАЗ");
}

$(function () {
    fileUploadInit();
    extendedFieldInit();
    customCheckInit();
    showHideRealry();
    readMoreInit();
    onlyOneCheckboxInit('#safe', '#by_agreement');
    scrollOnRequiredInit();
    showPopupsInit();
    ajaxRegistrationInit('customer');
    changeButtonText();
    window.addMessage = addMessage;
});