import {imageUploadInit} from './seeds/image_upload'
import {scrollOnRequiredInit} from './seeds/scroll_on_required'
import {sendFormData} from './seeds/ajax_send_form_data'


$(function () {
    imageUploadInit();
    scrollOnRequiredInit();
    window.sendFormData = sendFormData;
    window.scrollOnRequiredInit = scrollOnRequiredInit;
});