import {getCookie} from '../utils'
import {onClickCardWithCount} from './messageCounters'
import {loadTemplate} from './loaders'
import {bindRemoveNotes} from './notes'

function dialog(message, yesCallback, notCallback) {
    $("#dialog_delete .modal-title").html(message);
    $("#dialog_delete").modal('show');
    $("#btnYes").click(function (e) {
        e.preventDefault();
        yesCallback();
        $("#dialog_delete").modal('hide');
    });
    $("#btnNot").click(function (e) {
        e.preventDefault();
        notCallback();
        $("#dialog_delete").modal('hide');
    });
}

function isNullOrders() {
    let buttons = {
        btnApprove: $('#btnApprove'),       // "Согласовать"
        btnChange: $('#btnChange'),        // "Отправить на внесение изменений"
        btnsToArchive: $('.js-btnToArchive'),     // "Отказаться от заказа"
        btnToArchive: $('#btnToArchive'),     // "Отказаться и отправить в Архив"
        btnReserve: $('#btnReserve'),     // "Зарезервировать"
        btnsArbitration: $('.js-btnArbitration'),// "Обратиться в арбитраж"
        btnSendReview: $('#order-review-add')  // "Оставить отзыв"
    };
    let stages_elements = {
        $approve: $('#conditions-approve'), //1. Согласование условия
        $reserve: $('#reserveSpace'),       //2. Резервирование (Отобразить)
        $works: $('#completeWork')        //3. Выполненная работа
    };

    let $orderStagesContainer = $('#order-stages');

    // hide all buttons
    for (let key in buttons) {
        if (buttons[key].length)buttons[key].hide();
    }

    for (let key in stages_elements) {
        stages_elements[key].show();
    }
    if ($orderStagesContainer.length) $orderStagesContainer.parent().hide();


}

function restoreTabFromHash() {
    var currentHash = URI(location.href).hash();
    if (currentHash.indexOf("#order") == 0) {
        $("a[href='#tab2']").trigger('click');
        // console.log("click on ", "#orderBlock" + currentHash.replace("#order", ""));
        let obj_id = currentHash.replace("#order", "");
        // console.log("obj_id =  ", obj_id);
        if (obj_id) {
            $("#orderBlock" + currentHash.replace("#order", "")).trigger('click');
        } else {
            let $order_block = $(".order-block");
            if ($order_block.length) {
                $order_block.first().trigger('click');
            } else {
                window.location.hash = '#order';
                isNullOrders();
            }
        }

    } else if (currentHash.indexOf("#user") == 0) {
        $("a[href='#tab1']").trigger('click');
    } else if (currentHash.indexOf("#teamorder") == 0 || currentHash.indexOf("#myteam") == 0) {
        $("a[href='#tab3']").trigger('click');
    } else {
        $("a[href='#tab1']").trigger('click');
    }
}

function bindOrders() {
    $('.order-block').on('click', function (event) {
        event.preventDefault();
        let $this = $(this);
        $("#chat-order-add").show();
        onClickCardWithCount($this);
        $('.order-block').each(function (i, v) {
            $(v).removeClass('orAct');
        });
        $this.addClass('orAct');
        let orderId = $this.data('id');
        let projectId = $this.data('project-id');
        let recipentId = $this.data('recipent-id');
        let orderName = $this.data('order-name');
        var secureOrder = $(this).data('secure-deal');
        secureOrder = Boolean(secureOrder);
        window.location.hash = `order${orderId}`;

        $("#chat-order-add #orderId").val(orderId);
        $("#add-form-order-note #orderNote").val(orderId);
        $("#orderArbitrationId").val(orderId);
        $("#projectReviewId").val(projectId);
        $("#reserve-button").attr('data-order-id', orderId);

        $("#chat-order-add #recipentId").val(recipentId);
        window.chatController.create(orderId, projectId, recipentId, orderName, secureOrder);

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                'order': orderId,
            },
            dataType: 'json',
            success: function (json) {
                var noteHtmlInbox = '';
                var note_tmpl = loadTemplate('note_tmpl');
                // console.log("note json -->", json);
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += note_tmpl({text: v.text, note_id: v.id});
                });
                $(".order-notes-block").html(noteHtmlInbox);
                bindRemoveNotes();
            }
        });
    });
    $('.order-block .dimovChat').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        // .toggle();
        let $arrow = $(event.target);
        let $info = $arrow.siblings('.hideOBB');
        if ($info.hasClass("open")) {
            $arrow.css('transform', 'rotate(0deg)');
            $info.hide();
        } else {
            $arrow.css('transform', 'rotate(90deg)');
            $info.show();
        }
        $info.toggleClass("open");
        // console.log('click on tr');
    });
}

function bindOrderInfo() {
    $(".messageBlock").on('click', '.full-order-info', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var orderId = $(this).closest('.orderBlock').data('id');
        if (!orderId) {
            orderId = $(this).closest('.trashedOrderBlock').data('id');
        }
        $.ajax({
            url: '/api/orders/' + orderId + '/',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            dataType: 'json',
            success: function (data) {
                let outTable_tmpl = loadTemplate('order_info_tmpl');
                var outTable = outTable_tmpl({order: data});

                $("#order-info table").html(outTable);
                $("#order-info").modal('show');
            },
            error: function (e, jqxhr) {
                console.log(e);
            }
        });
    });
}

function bindTeams() {
    let message_tmpl = loadTemplate('message_tmpl');
    $('.team-block').on('click', function () {
        onClickCardWithCount($(this));
        $('.team-order-block, .team-block').each(function () {
            $(this).removeClass('orAct');
        });
        $(this).addClass('orAct');

        var teamIds = '';
        $.each($(this).find('.team-chat-user'), function (i, v) {
            teamIds += $(this).attr('data-id') + ";";
        });
        $("#team-chat-form #teamIds").val(teamIds);

        var inbox = document.getElementById('message-chat-team-space');
        inbox.innerHTML = '';

        var docList = document.getElementById('documentTeamSpace');
        docList.innerHTML = '';

        var teamId = $(this).attr('data-team-id');
        location.hash = '#myteam' + teamId;

        // var newCount = parseInt($("#count-tab-team").text());
        // var currNewCount = parseInt($(".team-count-" + teamId).text());
        // var resCount = newCount - currNewCount;
        // $("#count-tab-team").text(resCount);
        $(".team-count-" + teamId).text(0)

        $("#team-chat-form #teamId").val(teamId);
        $("#add-form-team-note #teamNote").val(teamId);
        $("#team-chat-form #recipentTeamId").val("");
        $("#team-chat-form #orderTeamId").val("");
        $("#add-form-team-note #orderNote").val("");

        $.ajax({
            url: '/api/message',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {'team': teamId, 'order__isnull': 'true'},
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    var senderName = 'Вы';
                    var className = 'youChat';
                    if (v.sender.id !== userId) {
                        senderName = v.sender.username;
                        className = '';
                    }
                    inbox.innerHTML += message_tmpl({className: className, senderName: senderName, message: v});
                    // inbox.innerHTML += '<div class="col-lg-12 insetCommChat ' + className + '"><div class="topCommChat">' +
                    //     '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
                    //     '<p class="textCommChat">' + v.text + '</p></div>';
                });
                var height = inbox.scrollHeight;
                inbox.scrollTop = height;
            }
        });

        $.ajax({
            url: '/api/documents',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                'team': teamId,
                'is_delete': false,
                'is_send': true,
            },
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {'team': teamId},
            dataType: 'json',
            success: function (json) {
                // console.log(json.results);
                var noteHtmlInbox = '';
                var note_tmpl = loadTemplate('note_tmpl');
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += note_tmpl({text: v.text, note_id: v.id});

                });
                $(".team-notes-block").html(noteHtmlInbox);
                bindRemoveNotes();
            }
        });

    });
}

function bindArbitrationSend() {
    // TODO: Test it
    $('#order-arbitration-add').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var formData = $("#arbitration-add-form").serialize();
        $.ajax({
            url: '/projects/arbitration/create/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: formData,
            dataType: 'json',
            success: function (json) {
                // console.log(json);
                $("#arbitration-add").modal('hide');
                $.jGrowl("Обращение в арбитраж добавлено", {
                    life: 4000
                });
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    });
}

function bindOnTabs() {
    /**
     * Биндит обработчики на Закладки
     */
    $('a[data-toggle="tab"]').unbind().on('show.bs.tab', function (e) {
        // console.log("TAB!");
        var activeTab = $(this).attr('href').substring(1);
        var liveHash = URI(location.href).hash();

        switch (activeTab) {
            case 'tab1':
                setTimeout(function () {
                    if (liveHash.indexOf("#user") != -1) {
                        var userHashId = liveHash.replace("#user", "");
                        $("#userBlock" + userHashId).trigger('click');
                    } else {
                        $(".user-block").first().trigger('click');
                    }
                }, 100);
                break;

            case 'tab2':
                // console.log("tab2");
                setTimeout(function () {
                    if (liveHash.indexOf("#order") != -1) {
                        var ordHashId = liveHash.replace("#order", "");
                        $("#orderBlock" + ordHashId).trigger('click');
                    } else {
                        let $order_block = $(".order-block");
                        if ($order_block.length) {
                            $order_block.first().trigger('click');
                        } else {
                            window.location.hash = '#order';
                            isNullOrders();
                        }
                    }
                }, 100);
                break;

            case 'tab3':
                setTimeout(function () {
                    // console.log("on active TAB team");
                    if (liveHash.indexOf("#teamorder") != -1) {
                        var teamHashId = liveHash.replace("#teamorder", "");
                        $("#teamOrderBlock" + teamHashId).trigger('click');
                    } else if (liveHash.indexOf("#myteam") != -1) {
                        var teamHashId = liveHash.replace("#myteam", "");
                        $("#teamMyBlock" + teamHashId).trigger('click');
                    } else {
                        var firstTeamBlock = $(".team-block").first();
                        var firstTeamOrder = $(".team-order-block").first();
                        if (firstTeamOrder.length == 1) {
                            firstTeamOrder.trigger('click');
                        } else if (firstTeamBlock.length == 1) {
                            firstTeamBlock.trigger('click');
                        }
                    }
                }, 100);

        }

    });
}

function bindUserContacts() {
    $(".conMess").click('on', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var userId = $(this).attr('data-id');
        $.ajax({
            url: '/api/users/' + userId + '/',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            dataType: 'json',
            success: function (data) {
                var outTable = '';
                if (data.username) {
                    outTable += '<tr><td>Ник</td><td>' + data.username + '</td>';
                }

                if (data.fio) {
                    outTable += '<tr><td>Ф.И.О</td><td>' + data.fio + '</td>';
                }
                if (data.skype) {
                    outTable += '<tr><td>Skype</td><td>' + data.skype + '</td>';
                }

                if (data.website) {
                    outTable += '<tr><td>Сайт</td><td>' + data.website + '</td>';
                }

                if (data.phone) {
                    outTable += '<tr><td>Телефон</td><td>' + data.phone + '</td>';
                }

                $("#contact-info table").html(outTable);
                $("#contact-info").modal('show');
                // console.log(data);
            },
            error: function (e, jqxhr) {
                console.log(e);
            }
        });
    });
}

function bindGetUserMessages() {
    let message_tmpl = loadTemplate('message_tmpl');
    $('.user-block').on('click', function () {
        onClickCardWithCount($(this));
        // var newCount = parseInt($("#count-tab-contact").text());
        var contactId = $(this).attr('data-id');
        location.hash = '#user' + contactId;
        $("#contact-chat-form #recipentContactId").val(contactId);
        $("#add-form-contractor-note #recipentNoteContractor").val(contactId);

        $('.user-block').each(function () {
            $(this).removeClass('mesAct');
        });

        $(this).addClass('mesAct');
        var inbox = document.getElementById('message-chat-space');
        var sumSenderRecipent = parseInt(userId) + parseInt(contactId);

        $("#message-chat-space").removeClass().addClass("contact-space" + sumSenderRecipent);
        $(".contact-count-" + sumSenderRecipent).text(0);
        var docList = document.getElementById('documentSpace');
        inbox.innerHTML = '';
        docList.innerHTML = '';

        $.ajax({
            url: '/api/message',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                'operand': 'in',
                'sender_id': userId,
                'recipent_id': contactId
            },
            dataType: 'json',
            success: function (json) {
                $.each(json.results, function (i, v) {
                    var senderName = 'Вы';
                    var className = 'youChat';
                    if (v.sender.id == contactId) {
                        senderName = v.sender.username;
                        className = '';
                    }
                    inbox.innerHTML += message_tmpl({className: className, senderName: senderName, message: v});
                    // '<div class="col-lg-12 insetCommChat  ' + className + '"><div class="topCommChat">' +
                    //     '<p class="nameCommChat">' + senderName + '</p> <span>' + v.created + '</span></div>' +
                    //     '<p class="textCommChat">' + v.text + '</p></div>'
                });
                var height = inbox.scrollHeight;
                inbox.scrollTop = height;
            }
        });

        $.ajax({
            url: '/api/documents',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                'operand': 'in',
                'sender_id': userId,
                'recipent_id': contactId,
                'is_delete': false,
                'is_send': true,
            },
            dataType: 'json',

            success: function (json) {
                // console.log(json);

                $.each(json.results, function (i, v) {
                    docList.innerHTML += '<li style="word-break: break-all;"><a class="file-link" href="/chat/download/' + v.file + '">' + v.file + '</a><div class="remove-document" data-id="' + v.id + '" style="right:-10px;"></div></li>';
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        $.ajax({
            url: '/api/note/',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                'operand': 'in',
                'sender_id': userId,
                'recipent_id': contactId
            },
            dataType: 'json',
            success: function (json) {
                // console.log(json.results);
                var noteHtmlInbox = '';
                var note_tmpl = loadTemplate('note_tmpl');
                $.each(json.results, function (i, v) {
                    noteHtmlInbox += note_tmpl({text: v.text, note_id: v.id});
                });
                $(".contractor-notes-block").html(noteHtmlInbox);
                bindRemoveNotes();
            }
        });

    });
}

function bindDeleteContact() {
    $('.deleteMess').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var senderId = userId;
        var recipentId = $(this).attr('data-recipent-id');
        var _this = $(this);

        dialog("Вы действительно хотите удалить сообщения этого пользователя?",
            function () {
                $.ajax({
                    url: '/chat/messages_delete/',
                    type: 'POST',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                    },
                    data: {'sender_id': senderId, 'recipent_id': recipentId},
                    dataType: 'json',
                    success: function (json) {

                        if (json.status == 'ok') {
                            _this.parent().remove();
                            $("#message-chat-space").html("");
                        }

                    },
                    error: function (e) {
                        console.log('error');
                        console.log(e);
                    }
                });
            }.bind(null, senderId, recipentId, _this),
            function () {
            });


    });
}

function bindCtrlEnterSendMessage() {
    $('textarea.js-chat').keydown(function (e) {
        let $target = $(e.target);
        if (e.ctrlKey && e.keyCode == 13) {
            // console.log("Send button -->", $target.parent().find('.btn-send'))
            let $btn_send = $target.parent().find('.btn-send');
            $btn_send.trigger('click');
        }
    })
}

export {
    bindOrders,
    bindOrderInfo,
    bindArbitrationSend,
    bindOnTabs,
    bindUserContacts,
    bindGetUserMessages,
    bindTeams,
    bindDeleteContact,
    bindCtrlEnterSendMessage,
    restoreTabFromHash,
}