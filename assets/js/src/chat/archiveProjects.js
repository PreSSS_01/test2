import {getCookie} from '../utils'


function bindArchiveProjects() {
    // Нажимаем на кнопку архивные сообщения
    $("#trashed-button").on('click', function (e) {
        e.preventDefault();
        var state = $(this).attr('data-show');
        var trashedOrderHtml = "";

        if (state == 'true') {
            $(this).attr('data-show', 'false');
            $(this).text("Скрыть архивные заказы");

            $("#archive-space").show();
            $("#show-archive-label").show();

        } else {
            $(this).attr('data-show', 'true');
            $(this).text("Показать архивные заказы");
            $("#archive-space").hide();
            $("#show-archive-label").hide();
        }


    });

    // Нажимаем на заказ в архивных заказах
    $(".messageBlock").on('click', '.trashedOrderBlock', function () {
        let $this = $(this);
        $("#chat-order-add").hide();
        $('.order-block, .trashedOrderBlock').each(function () {
            $(this).removeClass('orAct');
        });
        $this.addClass('orAct');
        let orderId = $this.data('id');
        location.hash = '#order' + orderId;
        window.chatController.create(orderId);
    });
}

export {bindArchiveProjects}