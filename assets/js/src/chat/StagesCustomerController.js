import {getCookie} from '../utils'
import {loadTemplate} from  './loaders'
import {StageForm, StageReserved, StageInWork} from './Stages'

let message_format = {
    "format_type": "approve_stages",
    "data": {
        "sender_id": "",
        "recipent_id": "",
        "order_id": "",
        "msg": "",
        "is_system": true
    }
};

const STATUSES = {
    'not_agreed': 'не согласован',
    'send_approve': '...на согласовании',
    'agreed': '...согласовано',
    'cancel_approve': '...исполнитель отказался',
    'in_process': '...в процессе',
    'completed': '...завершен',
    'closed': '...закрыт',

};

//Customer
//TODO: Вынесли общую логику в родительский класс
class StagesController {
    constructor(orderId, projectId, recipentId, orderName, secureOrder, kwargs = {}) {
        const self = this;
        this.orderId = orderId;
        this.orderName = orderName;
        this.projectId = projectId;
        this.recipentId = recipentId;
        this.secureOrder = secureOrder;
        // this.is_archive_project
        this.data = {}; //JSON
        this.stages = [];
        this.stages_reserved = [];
        this.stages_work = [];
        this.STAGE_STATUSES = {
            'not_agreed': this.buildNotAgreedStage.bind(self),
            'send_approve': this.buildSendApproveStage.bind(self),
            'agreed': this.buildAgreedStage.bind(self),
            'in_process': this.buildProcessStage.bind(self),
            'completed': this.buildProcessStage.bind(self),
            'closed': this.buildProcessStage.bind(self),
        };
        this.btnCompleteTmpl = loadTemplate('bntCompleteStage_tmpl');
        this.btnSendReviewTmpl = loadTemplate('btnSendReview_tmpl');
        this.switch_to_protected_tmpl = loadTemplate('switch_to_protected_tmpl');

        this.$orderStagesContainer = $('#order-stages');
        this.$orderStagesContainer.html('');
        this.$stagesCount = $('#countStage');
        this.$stagesCount.unbind().on("change", this._changeNumStages.bind(self));
        this.$stagesCount.parent().show();
        this.buttons = {
            btnApprove: $('#btnApprove'),
            btnChange: $('#btnChange'),
            btnToArchive: $('#btnToArchive'),
            btnReserve: $('#btnReserve'),
            btnsArbitration: $('.js-btnArbitration'),
            btnSendReview: $('#order-review-add')
        };
        this.stages_elements = {
            $approve: $('#conditions-approve'), //1. Согласование условия
            $reserve: $('#reserveSpace'),       //2. Резервирование
            $works: $('#completeWork')          //3. Выполненная работа
        };
        this.temp = {
            approve_stage_header_text: this.stages_elements.$approve.find('.js-stage-header').html(),
        };
        this.init();
    }

    init() {
        const self = this;

        this.dataPromise = this.getOrderData({orderId: this.orderId});
        this.dataPromise
            .then(
                self._onLoadData.bind(self)
            )
            .catch(
                self._onLoadDataError.bind(self)
            );
    }

    getOrderData({orderId}) {
        const self = this;
        return Promise.resolve($.ajax({
            url: `/api/orders/${orderId}/`,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
        }))
    }

    redraw() {
        /**
         * Полностью перерисовываем страницу Заказа
         */
        console.log("Redraw customer stages");
        // $("#orderBlock" + this.orderId).trigger('click');
        this.init();
    }

    reload() {
        /**
         * Перезагружаем страницу Чата(при удалении заказа)
         */
        window.location = '/chat/#order';
        location.reload();
    }

    _buildPage() {
        console.log("Build Page!");
        // Restore html to default
        this.stages_elements.$approve.find('.js-stage-header').html(this.temp.approve_stage_header_text);
        console.log("restore this -->", this.stages_elements.$approve.find('.js-select'));
        this.stages_elements.$approve.find('.js-select').addClass("select");
        this.stages_elements.$reserve.find('.js-select').addClass("select");
        this.stages_elements.$works.find('.js-select').addClass("select");
        let $swith_to_protected = this.$orderStagesContainer.siblings('.switch');
        if ($swith_to_protected.length) $swith_to_protected.remove();
        this.stages_elements.$reserve.css({'border-top': 'none'});
        this.stages_elements.$works.css({'border-top': 'none'});
        $('#stagesWork').find('.js-select').addClass('select');
        this.stages_elements.$reserve.find('.js-select').addClass('select');
        if ($('#send-review').length)$('#send-review').remove();

        this.stages_elements.$reserve.hide();
        this.stages_elements.$works.hide();
        if (this.data.stages.length == 0) {
            this.buildStartStage()
        } else {
            let stageStatus = this.data.stages[0].status;
            // console.log('stageStatus = ', stageStatus);
            this.STAGE_STATUSES[stageStatus]();
        }
        this._bindEvents();
    }

    buildStartStage() {
        /**
         * Стадия: "Проект Предложен"(нет этапов)
         */
        // Выделить цифру 1. красным
        // $('#conditions-approve').find('.select')
        this.$orderStagesContainer.show();
        this.buttons.btnApprove.show();
        this.buttons.btnChange.hide();
        this.buttons.btnToArchive.hide();
        this.$stagesCount.removeAttr('disabled');
        this.$stagesCount.val(1);
        this.$stagesCount.trigger('change');
        this.stages_elements.$approve.find('.js-select').addClass('select');
        // this.stages_elements.$approve.find('.js-help-text').show();
        // this.stages_elements.$reserve.find('.js-help-text').show();
        // this.stages_elements.$reserve.find('.stages-paid').hide();
        // this.stages_elements.$works.find('.js-help-text').show();
        // this.stages_elements.$works.find('#stagesWork').show();
        if (!this.secureOrder) {
            // console.log("add switch to protect");
            this.$orderStagesContainer.after(this.switch_to_protected_tmpl());
            $('#switch-to-protected').unbind().on('change', this._onChangeToProtect.bind(this));
            this.buttons.btnsArbitration.hide();
        }

    } // Нет Этапов

    buildNotAgreedStage() {
        console.log("Stage: not_agreed");
        this._renderStage('stage_tmpl');
        this.buttons.btnApprove.show();
        this.buttons.btnChange.hide();
        this.buttons.btnToArchive.hide();
        if (!this.secureOrder) {
            // console.log("add switch to protect");
            this.$orderStagesContainer.after(this.switch_to_protected_tmpl());
            $('#switch-to-protected').unbind().on('change', this._onChangeToProtect.bind(this));
            this.buttons.btnsArbitration.hide();
        }
    } // Статус "Не согласован"

    buildSendApproveStage() {
        console.log("Stage: send_approve");
        this._renderStage('stage_tmpl', true);
        this.$stagesCount.attr('disabled', true);
        this.stages_elements.$reserve.hide();
        this.stages_elements.$works.hide();
        this.buttons.btnApprove.hide();
        this.buttons.btnChange.show();
        this.buttons.btnToArchive.show();
    } // Статус "На согласовании"

    buildAgreedStage() {
        console.log('Stage: agreed');
        this.buttons.btnApprove.hide();
        this.buttons.btnChange.hide();
        this.buttons.btnToArchive.hide();
        this.$stagesCount.parent().hide();
        this._renderStage('stage_approved_tmpl', true);
        this.stages_elements.$approve.find('.js-help-text').hide();
        this.stages_elements.$approve.find('.js-select').removeClass('select');
        this.stages_elements.$reserve.find('.js-help-text').show();
        this.stages_elements.$reserve.show();
        this.buttons.btnReserve.show();
        this.buttons.btnsArbitration.show();
        this._renderStageReserved('reserved_tmpl')

    } // Статус "Согласован"

    buildProcessStage() {
        console.log('Stage: in_process');
        this.buildAgreedStage();
        // Block-Stage-1
        this.stages_elements.$approve.find('.js-select').removeClass('select');
        // Block-Stage-2
        this.stages_elements.$reserve.find('.js-select').removeClass('select');
        this.stages_elements.$reserve.find('.js-btnArbitration').hide();
        this.stages_elements.$reserve.css({'border-top': '1px solid black'});
        // Block-Stage-3
        this.stages_elements.$works.show();
        this.stages_elements.$works.css({'border-top': '1px solid black'});
        this._renderStageInWork('work_in_process_tmpl');
        if (this.secureOrder) {

        } else {
            this.stages_elements.$reserve.hide();
            this.buttons.btnsArbitration.hide();
        }
    } // Статус "В процессе"/"Завершен"/"Закрыт"


    _renderStage(template_name, disable = false) {
        let i = 0;
        this.$orderStagesContainer.html("");
        for (let stage_data of this.data.stages) {
            i++;
            let stage = new StageForm(this.$orderStagesContainer,
                {
                    orderId: this.orderId, stage_num: i, stage_status: STATUSES[stage_data.status],
                    type: 'update', template_name: template_name, data: stage_data
                });
            if (disable) stage.disable();
            this.stages.push(stage);

        }
        this.$stagesCount.val(i);
    }

    _renderStageReserved(template_name) {
        let $container = this.stages_elements.$reserve.find('.stages-paid');
        $container.html("");
        this.stages_reserved = [];
        // Нет незарезервированных Этапов
        let has_not_reserved_stages = false;
        for (let stage_data of this.data.stages) {
            if (stage_data.status == 'agreed') has_not_reserved_stages = true;
            let stage = new StageReserved($container,
                {
                    template_name, data: stage_data
                });
            this.stages_reserved.push(stage);
        }
        if (!has_not_reserved_stages) {
            this.buttons.btnReserve.hide();
            this.stages_elements.$reserve.find('.js-help-text').hide();
        }
    }

    _renderStageInWork(template_name) {
        /**
         * Отрисовываем блок "Выволнение работы", включая "Выполненныа работа" и "Оставить отзыв"
         */
        let $container = this.stages_elements.$works.find('#stagesWork');
        $container.html("");
        let all_closed = this.data.stages.every((el)=>el.status == 'closed');
        if (all_closed) {
            this.stages_elements.$works.find('.titleStepss').html('3. Выполненная работа');
            this.stages_elements.$works.find('.js-btnArbitration').hide();
            this.stages_elements.$works.find('.js-help-text').hide();
            for (let stage_data of this.data.stages) {
                let stage = new StageInWork($container,
                    {
                        template_name,
                        data: stage_data,
                    });
                this.stages_work.push(stage);
            }
            if (!this.data.has_user_review) {
                let btnReviewOpenModel = $(this.btnSendReviewTmpl());
                btnReviewOpenModel.unbind().on('click', this._onBtnReviewOpenModal.bind(this));
                // Если кнопка "Оставить отзыв" еще не добавлена - добавляем
                if (!this.stages_elements.$works.find('#send-review').length) this.stages_elements.$works.append(btnReviewOpenModel);
            } else {
                if (this.stages_elements.$works.find('#send-review').length) this.stages_elements.$works.find('#send-review').remove()
            }
            $('#stagesWork').find('.js-select').removeClass('select');
            this.stages_elements.$reserve.find('.js-select').removeClass('select');

        } else {
            for (let stage_data of this.data.stages) {
                if (stage_data.status == 'closed') continue;
                let stage = new StageInWork($container,
                    {
                        template_name, data: stage_data, note_text: 'Закройте этап или подробно опишите замечания в чате'
                    });
                if (stage_data.status == 'completed') {
                    let $btn = $(this.btnCompleteTmpl({stage: stage_data, text: 'Закрыть этап'}));
                    $container.html();
                    $container.append($btn);
                    $btn.on('click', this._onBtnClose.bind(this, stage))
                }
                this.stages_work.push(stage);
            }
        }

    }

    _onLoadData(json) {
        this.data = json;
        this._buildPage();
        if (this.data.project.state == 'deleted') {
            for (let key in this.buttons) {
                this.buttons[key].hide();
            }
        }
    }

    _changeNumStages(event) {
        /**
         * Изменяет кол-во этапов(stages)
         */
        let newNumStages = parseInt($(event.target).val());
        if (newNumStages < 1 || isNaN(newNumStages)) {
            newNumStages = 1;
            this.$stagesCount.val(newNumStages)
        }
        let currentNumStages = $('.js-stage-form:not(.remove-stages-form)').length;
        if (currentNumStages == newNumStages) return;
        if (currentNumStages > newNumStages) {
            for (let stage of this.stages.slice().reverse()) {
                if (stage.remove()) {
                    let index = this.stages.indexOf(stage);
                    if (index >= 0) {
                        this.stages.splice(index, 1);
                    }
                }
                currentNumStages--;
                if (currentNumStages == newNumStages) break;
            }
        } else {
            for (let stage of this.stages.slice()) {
                if (stage.type == 'remove') {
                    stage.restore();
                } else continue;
                currentNumStages++;
                if (currentNumStages == newNumStages) break;
            }
            while (currentNumStages < newNumStages) {
                currentNumStages++;
                let stage = new StageForm(this.$orderStagesContainer,
                    {orderId: this.orderId, stage_num: currentNumStages, type: 'new', template_name: 'stage_tmpl'});
                this.stages.push(stage);
            }
        }
    } //При изменении кол-ва этапов

    _bindEvents() {
        const self = this;
        this.buttons.btnApprove.unbind().on("click", this._onBtnApprove.bind(self));
        this.buttons.btnChange.unbind().on("click", this._onBtnChange.bind(self));
        this.buttons.btnToArchive.unbind().on("click", this._onBtnAToArchive.bind(self));
        this.buttons.btnReserve.unbind().on("click", this._onBtnReserve.bind(self));
        this.buttons.btnsArbitration.unbind().on("click", this._onBtnArbitration.bind(self));
        this.buttons.btnSendReview.unbind().on("click", this._onBtnSendReview.bind(self));
        $('#paymentfromSite').unbind().on('click', this._onBtnPaymentFromSite.bind(self));
        $("#reserve-stage-modal").find('#stage-num').unbind().on('change', function (event) {
            console.log("this.value = ", this.value);
            console.log(this.value?this.value:1 - 1);
            $("#reserve-stage-modal").find('#stage-cost').html(self.stages[(this.value?this.value:1) - 1].data.cost);
        });
    }

    // BINDS

    _onBtnApprove(event) {
        event.preventDefault();
        const self = this;
        if (!this.stages.every((el)=>el.is_valid())) {
            console.error('Не все поля форм валидны');
            return
        }
        // При выполнении всех ajax запросов
        Promise.all(this.stages.map((el)=>el.sendAjax_approve())).then(()=> {
            this.buttons.btnApprove.hide();
            this.buttons.btnChange.show();
            this.buttons.btnToArchive.show();
            this.$stagesCount.attr('disabled', true);
            // var currentRecipentId = $(self).data('id');
            // var secureOrder = true;
            //
            let message = message_format;
            message.data.sender_id = userId;
            message.data.recipent_id = self.recipentId;
            message.data.order_id = self.orderId;
            message.data.msg = `Условия заказа "${self.orderName}" отправлены на согласование`;
            console.log("Send-WS Отправить на согласование");
            socket.send_stages_approve(message);
            //TODO: раскомментировать дурацкое окно
            // $('#popupOk').modal('show');
            // })
        });
    } // "Отправить на согласование"

    _onBtnChange(event) {
        event.preventDefault();
        const self = this;
        Promise.all(this.stages.map((el)=>el.sendAjax_change())).then(()=> {
            this.buttons.btnApprove.show();
            this.buttons.btnChange.hide();
            this.buttons.btnToArchive.hide();
            this.$stagesCount.attr('disabled', false);
            let message = message_format;
            message.data.sender_id = userId;
            message.data.recipent_id = self.recipentId;
            message.data.order_id = self.orderId;
            message.data.msg = `Заказ "${self.orderName}" отозван для внесения изменений`;
            console.log("Send-WS Внести изменения");
            socket.send_stages_approve(message);
        });

    } // "Внести изменения"

    // TODO: test it
    _onBtnAToArchive(event) {
        event.preventDefault();
        const self = this;
        $.ajax({
            // async: false,
            url: `/api/orders/${this.orderId}/`,
            type: 'DELETE',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            dataType: 'json',
        })
            .done(function (json) {
                console.log('delete complete');
                // window.location.href = window.location.href.replace(getHash(), "");
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                message.data.msg = `Заказа ${self.orderName} отправлен в архив`;
                console.log("Send-WS Отправить в архив");
                socket.send_stages_approve(message);
                self.reload();
            })
            .fail(function (xhr, errorMsg, error) {
                console.log("delete fail, json -->", xhr);
            });

    } // "Отказаться и отправить в архив"

    _onBtnReserve(event) {
        const self = this;
        event.preventDefault();
        // Set modal-window params
        let $modal = $("#reserve-stage-modal");
        let total_cost = this.stages.map((el)=>parseInt(el.data.cost)).reduce((a, b) => a + b, 0);
        // console.log('total cost = ', total_cost);
        $modal.find('#total-cost').html(total_cost);

        let $select_stageNum = $modal.find('#stage-num');
        $select_stageNum
            .find('option')
            .remove()
            .end();
        for (let stage of this.stages) {
            if (stage.data.is_paid) continue;
            $select_stageNum.append(`<option>${stage.data.pos}</option>`);
        }
        // let $stage_cost = $modal.find('#stage-cost');
        // $stage_cost.html(self.stages[this.value - 1].data.cost);
        $modal.find('#stage-num').trigger('change');

        $modal.modal('show');
    } // "Зарезервировать" --> Открывает модальное окно для резервирования

    _onBtnPaymentFromSite(event) {
        const self = this;
        event.preventDefault();
        let $modal = $("#reserve-stage-modal");
        let sum, stages_id;
        if ($modal.find('input[name=choice_way]:checked').val() == 'all_stages') {
            sum = $modal.find('#total-cost').html();
            stages_id = this.stages.map((el)=>el.data.id);
        } else {
            sum = $modal.find('#stage-cost').html();
            let num_stage = $modal.find('#stage-num').val() - 1;
            stages_id = [this.stages[num_stage].data.id];
        }
        $.ajax({
            url: '/wallets/payfromscore/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                sum: sum,
                stages_id: stages_id.join(';'),
            },
            dataType: 'json',
            success: function (json) {
                // console.log('success pay stage, json ', json);
                $("#reserve-stage-modal").modal('toggle');
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                message.data.msg = `Заказчик зарезервировал сумму для этапов`;
                console.log("Send-WS Оплата Этапа/Этапов");
                socket.send_stages_approve(message);
                self.redraw();
            },
            error: function (xhr) {
                console.log('error pay stage, json', xhr.responseJSON);
                $.jGrowl(xhr.responseJSON.message_error);
            }
        });
    }  // Оплата с сайта(Счет на Proekton)

    _onBtnClose(stage, event) {
        event.preventDefault();
        const self = this;
        stage.sendAjax_close()
            .then(function (json) {
                console.log("Этап закрыт успешно");
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                message.data.msg = `Заказчик закрыл этап "${json.name}"`;
                console.log("Send-WS Оплата Этапа/Этапов");
                socket.send_stages_approve(message);
                self.redraw();
            })
            .catch(function (xhr) {
                console.log("При закрытии этапа произошла ошибка -->", xhr);
            })

    } // "Закрыть этап"

    _onBtnReviewOpenModal(event) {
        event.preventDefault();
        let review_type = $(event.target).data('review-type');
        $('#review-add').find(`input[type=radio][value=${review_type}]`).prop("checked", true);
        $('#review-add').modal('show');
    } // Открыть модальное окно "Оставить отзыв"

    _onBtnSendReview(event) {
        event.preventDefault();
        const self = this;
        $('#projectReviewId').val(this.projectId);
        $('#targetContractorId').val(this.recipentId);
        var formData = $("#review-adds-form").serialize();
        $.ajax({
            url: '/api/reviews/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: formData,
            dataType: 'json',
            success: function (json) {
                $('#review-add').modal('hide');
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                message.data.msg = `Отзыв на заказ "${self.orderName}" оставлен`;
                console.log("Send-WS Оставить отзыв");
                // TODO: в этом сообщении отослать команду на reload if(json.count_reviews == 2)
                socket.send_stages_approve(message);
                // console.log('json = ', json);
                if(json.count_reviews == 1){
                    self.redraw();
                }else {
                    self.reload();
                }
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    } // "Оставить отзыв"

    _onChangeToProtect(event) {
        const self = this;
        let checked = $(event.target).prop('checked');
        console.log('checked = ', checked);
        $.ajax({
            url: '/api/orders/' + this.orderId + '/',
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {secure: checked},
            dataType: 'json',
            success: function (json) {
                console.log("Protect switch success, json = ", json);
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                // Key words "перевел заказ" - на них завязан анализатор на стороне Исполнителя (костыль)
                message.data.msg = `Заказчик перевел заказ "${self.orderName}" на ${checked ? 'безопасную сделку': 'прямую оплату'} `;
                console.log("Send-WS Оставить отзыв");
                socket.send_stages_approve(message);
            }
        })
    } // "Перевести на Безопасный заказ"


    _onBtnArbitration(event) {
        event.preventDefault();
        $("#arbitration-add").modal('show');
    } // "Обратиться в арбитраж"

    _onLoadDataError(error) {
        console.log("Error loading data -->", error);
    }
}

export {StagesController}