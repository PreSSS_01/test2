import {loadTemplate} from  './loaders'
// new-stages-form
// update-stages-form
// remove-stages-form

class StageForm {
    constructor($container, {orderId, stage_num, stage_status, type = 'new', formNamePostfix = '-stages-form', template_name, data = {}},
                kwargs = {
                    stage_num: stage_num,
                    stage_status: stage_status,
                    form_name: (type + formNamePostfix),
                    orderId: orderId,
                    stage: data
                },)
    // kwargs - auto generate from name_attributes
    {
        // console.log('Stage form template_name = ', template_name);
        this.orderId = orderId;
        this._type = type;
        this.$container = $container;
        this.self_tmpl = loadTemplate(template_name);
        this.data = data;
        this.$form = undefined;
        this.stageId = (type != 'new') ? data.id : undefined;
        this.create(kwargs);
    }

    create(kwargs) {
        /**
         * Добавление шаблона-формы Этапа на страницу
         */
        let el = $(this.self_tmpl(kwargs));
        this.$container.append(el);
        this.$form = el.find('form');
        // console.log("form --> ", this.$form);
        if (this.$form.length) this.$form.find('input[name=cost]').mask('000000000');
    }

    remove() {
        /**
         * Удаление, при уменьшении кол-ва этапов
         * return true - удаляем из [] stages
         */
        if (this.type == 'new') {
            this.$form.parent().remove();
            return true;
        }
        this.type = 'remove';
        // this.$form.removeClass('update-stages-form').addClass('remove-stages-form');
        return false
    }

    restore() {
        /**
         * Восстановление, при увеличении кол-ва этапов
         */
        if (this.type == 'new') throw new Error("Попытка восстановить элемент с type='new'");
        this.type = 'update';
        // this.$form.removeClass('remove-stages-form').addClass('update-stages-form');
    }

    set type(newType) {
        this.$form.removeClass(`${this._type}-stages-form`).addClass(`${newType}-stages-form`);
        if (newType == 'remove') this.hide();
        if (newType == 'update') this.show();
        this._type = newType
    }

    get type() {
        return this._type
    }

    disable() {
        this.$form.find('input').attr('readonly', true);
    }

    enable() {
        this.$form.find('input').attr('readonly', false);
    }

    hide() {
        this.$form.parent().hide();
    }

    show() {
        this.$form.parent().show()
    }

    is_valid() {
        let self = this;
        let mesage = 'Это поле обязательно';
        let valid = true;
        //Очищаем старые ошибки
        this.$form.find('.error').html("");
        // Отображаем новые
        this.$form.find(":input:not([type=hidden])").each(function (i, v) {
            if (!$(v).val()) {
                self.$form.find(`.error-${$(v).attr("name")}`).html(mesage).css('color', 'red');
                valid = false
            }
        });
        return valid
    }

    sendAjax_approve() {
        /**
         * Отправка Этапа "на согласование"
         */
        let self = this;
        // console.log("Send AJAX Approve");
        if (this.type == 'new') {
            // console.log('new stages approve');
            return Promise.resolve($.ajax({
                // async: false,
                url: '/api/stages/',
                type: 'POST',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                },
                data: this.$form.serialize(),
                dataType: 'json',
            })
                .done(function (json) {
                    self.type = 'update';
                    self.disable();
                    self.$form.find('.error').html("");
                    // console.log("json -->", json);
                    self.stageId = json.id;
                    // console.log(json);
                })
                .fail(function (xhr, errorMsg, error) {
                    console.log("ERROR, xhr", xhr);
                    $.each(xhr.responseJSON, function (i, v) {
                        self.$form.find('.error-' + i).html(v).css('color', 'red');
                        // console.log(self.$form);
                        // console.log(v);
                        // console.log(i);
                    });
                }));
        } else if (this.type == 'update') {
            this.$form.find('input[name=status]').val('send_approve');
            return Promise.resolve($.ajax({
                url: `/api/stages/${this.stageId}/`,
                type: 'PUT',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                },
                data: this.$form.serialize(),
                dataType: 'json',
            })
                .done(function (json) {
                    self.$form.find('.error').html("");
                    self.disable();
                })
                .fail(function (xhr, errorMsg, error) {
                    $.each(xhr.responseJSON, function (i, v) {
                        self.$form.find('.error-' + i).html(v).css('color', 'red');
                        console.log(v);
                        console.log(i);
                    });
                }));
        } else if (this.type == 'remove') {
            return Promise.resolve($.ajax({
                url: `/api/stages/${this.stageId}/`,
                type: 'DELETE',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                },
                dataType: 'json',
            })
                .done(function (json) {
                })
                .fail(function (xhr, errorMsg, error) {
                    console.log("delete fail, json -->", xhr);
                }));
        }
    }

    sendAjax_accept(secureOrder) {
        /**
         * "Согласовать" Этапы (Исполнителем)
         */
        // console.log("secureOrder = ", secureOrder);
        // console.log("set new status =", secureOrder ? 'agreed': 'in_process');
        return Promise.resolve($.ajax({
            url: '/api/stages/' + this.stageId + '/',
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {
                status: secureOrder ? 'agreed': 'in_process',
            },
            dataType: 'json',
        }))
    }

    sendAjax_change() {
        /**
         * Отправка Этапа "Внести изменения"
         */
        let self = this;
        // this.$form.find('input[name=status]').val('not_agreed');
        // console.log("ajax Change form -->", this.$form);
        return Promise.resolve($.ajax({
            url: '/api/stages/' + this.stageId + '/',
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {status: 'not_agreed'},
            dataType: 'json',
        })
            .done(function (json) {
                // enableStageFields(json.id);
                // $form.find('.error').html("");
                self.enable();
            })
            .fail(function (xhr) {
                console.log("Ошибка, которой не должно быть json -->", xhr.responseJSON);
            }));
    }

}

class StageReserved {
    constructor($container, {template_name = 'reserved_tmpl', data},
                kwargs = {
                    reserved_cls: '',
                    reserved_name: '',
                    stage: data
                },) {
        // Вывод текста резервирования в зависимости от статуса этапа
        let reserved_names = {
            agreed: 'Не зарезервирована',
            in_process: 'Зарезервирована',
            completed: 'Зарезервирована',
            closed: 'Переведена исполнителю',
        };
        // Вывод текста резервирования в зависимости от статуса этапа
        let reserved_classes = {
            agreed: 'unreserved',
            in_process: 'reserved',
            completed: 'reserved',
            closed: 'closed',
        };
        kwargs.reserved_cls = reserved_classes[data.status];
        kwargs.reserved_name = reserved_names[data.status];
        this.data = data;
        this.self_tmpl = loadTemplate(template_name);
        this.$container = $container;
        this.create(kwargs);
    }

    create(kwargs) {
        /**
         * Добавление шаблона "Резервирование" Этапа на страницу
         */
        this.$self = $(this.self_tmpl(kwargs));
        this.$container.append(this.$self);
        // console.log("form --> ", this.$form);
        // this.$form.find('input[name=cost]').mask('000000000');
    }
}

class StageInWork {
    constructor($container, {template_name = 'work_in_process_tmpl', note_text = '', data},
                kwargs = {stage: data, note_text: note_text}) {
        this.stageId = data.id;
        this.data = data;
        this.self_tmpl = loadTemplate(template_name);
        this.$container = $container;
        this.create(kwargs);
    }

    create(kwargs) {
        /**
         * Добавление шаблона "Выполнение работы" Этапа на страницу
         */
        this.$self = $(this.self_tmpl(kwargs));
        this.$container.append(this.$self);
    }

    hide() {
        this.$self.hide();
    }

    sendAjax_complete() {
        /**
         * Отправка Этапа "Закрыть этап"
         */
        let self = this;
        return Promise.resolve($.ajax({
            url: `/api/stages/${this.stageId}/`,
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {status: 'completed'},
            dataType: 'json',
        })
            .done(function (json) {
                // enableStageFields(json.id);
                // $form.find('.error').html("");
                // self.enable();
            })
            .fail(function (xhr) {
                console.log("Ошибка, которой не должно быть json -->", xhr.responseJSON);
            }));
    }

    sendAjax_close() {
        /**
         * Отправка Этапа "Закрыть этап"
         */
        let self = this;
        return Promise.resolve($.ajax({
            url: `/api/stages/${this.stageId}/`,
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {status: 'closed'},
            dataType: 'json',
        })
            .done(function (json) {
                // enableStageFields(json.id);
                // $form.find('.error').html("");
                // self.enable();
            })
            .fail(function (xhr) {
                console.log("Ошибка, которой не должно быть json -->", xhr.responseJSON);
            }));
    }
}

export {StageForm, StageReserved, StageInWork}