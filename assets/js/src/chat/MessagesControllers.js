import {getCookie} from '../utils'
import {loadTemplate} from './loaders'

class MessagesController {
    constructor(orderId) {
        console.log('Create MessagesController');
        const self = this;
        this.orderId = orderId;
        this.$inbox = $('#message-chat-order-space');
        this.$inbox.html("");
        this.messageTemplate = loadTemplate('message_tmpl');
        this.dataPromise = this.getMessagesData();
        this.dataPromise.then(
            self._onLoadData.bind(self)
        )
    }

    getMessagesData() {
        const self = this;
        return Promise.resolve($.ajax({
            url: '/api/message',
            type: 'GET',
            data: {'order': self.orderId, 'team__isnull': 'true'},
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            dataType: 'json',
            success: function (json) {
                console.log('Success Messages')
            }
        }));
    }

    _onLoadData(json) {
        const self = this;
        self.$inbox.html("");
        $.each(json.results, function (i, v) {
            var senderName = 'Вы';
            var className = 'youChat';

            if (v.sender.id !== userId) {
                senderName = v.sender.username;
                className = '';
            }
            if (v.is_system) {
                senderName = (senderName == 'Вы') ? 'Системное от Вас': `Системное от ${senderName}`;
                className = 'systemChat'
            }
            let message = $(self.messageTemplate({className: className, senderName: senderName, message: v}));
            self.$inbox.append(message);

        });
        // console.log("messages render complete");
        self.$inbox.scrollTop(self.$inbox.prop("scrollHeight"));
    }
}

export {MessagesController}