import {loadTemplate} from './loaders'
var note_tmpl = loadTemplate('note_tmpl');

function bindContractorNotes() {
    $('#add-note-contractor').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/note/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: $("#add-form-contractor-note").serialize(),
            dataType: 'json',
            success: function (json) {
                // console.log(json);
                $("#add-form-contractor-note #chat2").val("");
                let li = note_tmpl({text: json.text, note_id: json.id});
                $(li).appendTo(".contractor-notes-block");
                bindRemoveNotes();
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    });
}

function bindOrderNotes() {
    /**
     * Create new Note
     */
    $('#add-note-button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/note/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: $("#add-form-order-note").serialize(),
            dataType: 'json',
            success: function (json) {
                // $("<li>" + json.text + "</li>").appendTo(".order-notes-block");
                // console.log('note json = ', json);
                let li = note_tmpl({text: json.text, note_id: json.id});
                $(li).appendTo(".order-notes-block");
                $("#add-form-order-note #chat2").val("");
                bindRemoveNotes();
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    });
}

function bindTeamNotes() {
    $('#add-team-note-button').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/note/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: $("#add-form-team-note").serialize(),
            dataType: 'json',
            success: function (json) {
                let li = note_tmpl({text: json.text, note_id: json.id});
                $(li).appendTo(".team-notes-block");
                $("#add-form-team-note #chat2").val("");
                bindRemoveNotes();
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    });
}

function bindRemoveNotes() {
    // console.log('num notes = ', $('.remove-note').length);
    $('.remove-note').unbind().on('click', function (e) {
        let $note = $(e.target).parent();
        let noteId = $(e.target).data('id');
        // console.log('click remove note');
        $.ajax({
            url: `/api/note/${noteId}`,
            type: 'DELETE',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            dataType: 'json',
            success: function (json) {
                $note.remove();
                // console.log('Note deleted!');

                // let li = note_tmpl({text: json.text, note_id: json.id});
                // $(li).appendTo(".team-notes-block");
                // $("#add-form-team-note #chat2").val("");
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    })
}

export {bindContractorNotes, bindOrderNotes, bindTeamNotes, bindRemoveNotes}