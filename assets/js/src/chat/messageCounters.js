function recalculateTabsCounter() {
    // let tabs = [$('#count-tab-contact'), $('#count-tab-order'), $('#count-tab-team')]
    let tabs = [$('#tab1'), $('#tab2'), $('#tab3')];
    let total_messages_count = 0;
    for (let tab of tabs){
        let count_sum = Array.from((tab.find('.js-count').map((i, el)=>parseInt($(el).html())))).reduce((a, b) => a + b, 0);
        let $tab_counter = $(`a[href="#${tab.attr('id')}"]`).find('.count-tab');
        $tab_counter.html(count_sum);
        total_messages_count += count_sum;
        // console.log($tab_counter, 'new value -->', count_sum);
    }
    let $header_counter = $('.js-all-messages');
    $header_counter.html(total_messages_count);
}

function countPlus(message, place) {
    /**
     * Увеличиваем счетчик соответствующий сообщению(message)
     */
    // console.log("MESSAGE = ", message);
    let $container;
    if (message.answer_type == "add_message_contact"){
        $container = $(`.contact-count-${message.sender_id}`);
    } else if (message.answer_type == "add_message_order" || message.answer_type == "approve_stages"){
        $container = $(`#count-order-${message.order_id}`);
    } else if (message.answer_type == "add_message_team") {
        $container = $(`#count-team-${message.team_id}`);
    }
    // console.log("container = ", $container);
    $container.html(parseInt($container.html()) + 1);
    recalculateTabsCounter();
}

function onClickCardWithCount($card){
    /**
     * При нажатии на карточку со счетчиком новых сообщений
     */
    // console.log('Обнулем счетчик ', $card);
    $card.find('.js-count').html(0);
    recalculateTabsCounter();
}

export {countPlus, onClickCardWithCount}