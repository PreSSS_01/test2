function ws_print(...messages) {
    let message = messages.join(' ');
    console.log(`%c WS: ${message}`, 'background: white; color: blue');
}

export {ws_print}