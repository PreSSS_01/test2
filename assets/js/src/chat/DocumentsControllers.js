import {getCookie} from '../utils'
import {loadTemplate} from './loaders'

class DocumentsController {
    constructor(orderId) {
        console.log('Create MessagesController');
        const self = this;
        this.orderId = orderId;
        this.$container = $('#documentOrderSpace');
        this.$container.html("");

        this.messageTemplate = loadTemplate('document_attach_file_tmpl');
        this.dataPromise = this.getMessagesData();
        this.dataPromise.then(
            self._onLoadData.bind(self)
        )
    }

    getMessagesData() {
        const self = this;
        return Promise.resolve(
            $.ajax({
                url: '/api/documents',
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
                },
                data: {
                    'order': self.orderId,
                    'is_delete': false,
                    'is_send': true,
                },
                dataType: 'json',
                // success: function (json) {
                //
                // },
                error: function (e) {
                    console.log(e);
                }
            }));
    }

    addDocument(json) {

    }

    _onLoadData(json) {
        const self = this;
        // console.log('mesages json = ', json);
        // console.log('$inbox = ', this.$inbox);
        // console.log("messages render start");
        this.$container.html("");
        $.each(json.results, function (i, v) {
            let document = $(self.messageTemplate(
                {
                    href: `/chat/download/' + ${v.file}`,
                    text: v.file,
                    document_id: v.id
                }));
            self.$container.append(document);
        });
        // console.log("messages render complete");
        // self.$inbox.scrollTop(self.$inbox.prop("scrollHeight"));
    }
}

export {DocumentsController}