import {loadTemplate} from './loaders'

function chatContactsInit() {
    /**
     * Bind на кнопку "Отправить" в Закладке "Личные контакты"
     */
    $('#contact-chat-add-message').on('click', function (e) {
        e.preventDefault();
        var chatMessage = $("#chat").val();
        var recipentId = $("#recipentContactId").val();
        var senderId = $("#senderContactId").val();
        var sendLinks = $("#document-send-contact a");

        if (chatMessage || sendLinks.length > 0) {
            $("#contact-chat-form .errorEmptyMessage").hide();

            var sendLinkIds = "";
            var documentLinks = "";
            var documentAttachFiles = "";
            let document_attach_file_tmpl = loadTemplate('document_attach_file_tmpl');
            let document_link_tmpl = loadTemplate('document_link_tmpl');
            $.each(sendLinks, function (i, v) {
                sendLinkIds += $(this).attr('data-id') + ';';
                documentLinks += document_link_tmpl({href: $(this).attr('href'), text: $(this).text()});
                documentAttachFiles += document_attach_file_tmpl({href: $(this).attr('href'), document_id: $(this).attr('data-id'), text: $(this).text()});
            });
            socket.send_message({
                "format_type": "add_message_contact",
                "data": {
                    "sender_id": senderId,
                    "recipent_id": recipentId,
                    "chat_message": chatMessage,
                    "document_send_links": sendLinkIds,
                    "document_data": {
                        "document_links": documentLinks,
                        "document_attach_files": documentAttachFiles,
                    }
                }
            });
            $("#chat").val("");
            $("#document-send-contact").html("");
        } else {
            $("#contact-chat-form .errorEmptyMessage").show();
        }

    });
}

function chatOrdersInit() {
    /**
     * Bind на кнопку "Отправить" в Закладке "Исполнители/Заказчики"
     */
    $('#order-chat-add-message').on('click', function (e) {
        e.preventDefault();
        var chatMessage = $("#chat-order-add #chat").val();
        var recipentId = $("#chat-order-add #recipentId").val();
        var senderId = $("#chat-order-add #senderId").val();
        var orderId = $("#chat-order-add #orderId").val();
        var sendLinks = $("#document-send-order a");
        if (chatMessage || sendLinks.length > 0) {
            var sendLinkIds = "";
            var documentLinks = "";
            var documentAttachFiles = "";
            let document_attach_file_tmpl = loadTemplate('document_attach_file_tmpl');
            let document_link_tmpl = loadTemplate('document_link_tmpl');
            $.each(sendLinks, function (i, v) {
                sendLinkIds += $(this).attr('data-id') + ';';
                documentLinks += document_link_tmpl({href: $(this).attr('href'), text: $(this).text()});
                documentAttachFiles += document_attach_file_tmpl({href: $(this).attr('href'), document_id: $(this).attr('data-id'), text: $(this).text()});
            });
            socket.send_message({
                "format_type": "add_message_order",
                "data": {
                    "sender_id": senderId,
                    "recipent_id": recipentId,
                    "chat_message": chatMessage,
                    "order_id": orderId,
                    "document_send_links": sendLinkIds,
                    "document_data": {
                        "document_links": documentLinks,
                        "document_attach_files": documentAttachFiles,
                    }
                }

            });
            $("#chat-order-add #chat").val("");
            $("#document-send-order").html("");
        } else {
            $("#chat-order-add .errorEmptyMessage").show();
        }

    });
}

function chatTeamsInit() {
    $("#add-team-chat-message").on('click', function (e) {
        e.preventDefault();
        var chatMessage = $("#team-chat-form #chatText").val();
        // var recipentId = $("#team-chat-form #recipentTeamId").val();
        var senderId = $("#team-chat-form #senderTeamId").val();
        var teamId = $("#team-chat-form #teamId").val();
        // var orderId = $("#team-chat-form #orderTeamId").val();
        var documentSendIds = $("#documentSendIds").val();
        var teamIds = $("#team-chat-form #teamIds").val();
        var sendLinks = $("#document-send a");
        let document_attach_file_tmpl = loadTemplate('document_attach_file_tmpl');
        if (chatMessage || sendLinks.length > 0) {
            var sendLinkIds = "";
            var documentLinks = "";
            var documentAttachFiles = "";
            let document_attach_file_tmpl = loadTemplate('document_attach_file_tmpl');
            let document_link_tmpl = loadTemplate('document_link_tmpl');
            $.each(sendLinks, function (i, v) {
                sendLinkIds += $(this).attr('data-id') + ';';
                documentLinks += document_link_tmpl({href: $(this).attr('href'), text: $(this).text()});
                documentAttachFiles += document_attach_file_tmpl({href: $(this).attr('href'), document_id: $(this).attr('data-id'), text: $(this).text()});
            });
            socket.send_message({
                "format_type": "add_message_team",
                "data": {
                    "sender_id": senderId,
                    // "recipent_id": recipentId,
                    "chat_message": chatMessage,
                    "team_id": teamId,
                    "team_ids": teamIds,
                    // "order_id": orderId,
                    "document_send_links": sendLinkIds,
                    "document_data": {
                        "document_links": documentLinks,
                        "document_attach_files": documentAttachFiles,
                    }
                }
            });

            $("#team-chat-form #chatText").val("");
            $("#document-send").html("");
            $("#documentSendIds").val("");
        } else {
            $("#team-chat-form .errorEmptyMessage").show();
        }
    });
}


export {chatContactsInit, chatOrdersInit, chatTeamsInit}