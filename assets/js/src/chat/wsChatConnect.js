import {loadTemplate} from './loaders'
import {countPlus} from './messageCounters'

function getUserPlace() {
    /**
     * Определяем в какой закладке Чата пользователь
     */
    let hash = location.hash;
    let tab, id;

    for (let str of ["user", "order", "myteam"]) {
        if (hash.indexOf(`#${str}`) != -1) {
            tab = str;
            id = hash.replace(`#${str}`, '');
        }
    }
    return {tab, id}
}

function checkMessageInPlace(message, place) {
    /**
     * Проверяем, направлено ли входящее сообщение на текущую вкладку пользователя
     */
        // message.answer_type=place.tab
    let eq = ['add_message_contact=user', 'add_message_order=order', 'add_message_team=myteam', 'approve_stages=order'];
    // console.log([message.answer_type, place.tab].join('='));
    // console.log(message.order_id, '==', place.id, message.order_id == place.id);
    if ((eq.indexOf([message.answer_type, place.tab].join('=')) != -1) &&
        ((message.order_id == place.id) || (message.recipent_id == place.id) || (message.sender_id == place.id) || (message.team_id == place.id))) {

        return true
    }
    return false
}

function connect() {
    wsConnect.then(function (_socket) {
        socket = _socket;
        // Onmessage in Chat page
        socket.addEventListener("message", function (event) {
            var data = JSON.parse(event.data);
            print.ws_print("new message on Chat page");
            console.log(", message =", data);

            let user_place = getUserPlace();
            // console.log("User place ", place.tab, place.id);

            if (checkMessageInPlace(data, user_place)) {
                console.log("Сообщение принято открытым чатом");
                let chat_container_selectors = {
                    "user": "#message-chat-space",
                    "order": "#message-chat-order-space",
                    "myteam": "#message-chat-team-space",
                };
                let documents_container_seletors = {
                    "user": "#documentSpace",
                    "order": "#documentOrderSpace",
                    "myteam": "#documentTeamSpace",
                };
                let $chat_container = $(chat_container_selectors[user_place.tab]);
                let $documents_container = $(documents_container_seletors[user_place.tab]);
                var classMessage = 'youChat';
                var senderName = 'Вы';
                if (data.sender_id != userId) {
                    senderName = data.sender_name;
                    classMessage = '';
                }
                if (data.is_system){
                    senderName = (senderName == 'Вы') ? 'Системное от Вас': `Системное от ${senderName}`;
                    classMessage = 'systemChat'
                }

                let chat_message = loadTemplate('message_tmpl')({
                    className: classMessage,
                    senderName: senderName,
                    message: {created: data.msg_time, text: data.msg}
                });
                $chat_container.append(chat_message);
                $chat_container.scrollTop($chat_container.prop("scrollHeight"));

                $documents_container.append(data.docs_attach);

                if (data.answer_type == 'approve_stages' && data.sender_id != userId) {
                    if (data.msg.indexOf('перевел заказ') != -1){
                        let secure = false;
                        if (data.msg.indexOf('безопасную сделку') != -1) secure = true;
                        window.chatController.statesController._changeOrderProtect(secure);
                    }
                    window.chatController.statesController.redraw();
                }

            } else {
                console.log("Сообщение учтено счетчиком");
                countPlus(data, user_place)
            }

        });

        socket.addEventListener("close", function () {
            console.error("Connection Lost");
            connect();
        });

        socket.send_message = function (messageData) {
            console.log('send message -->', messageData);
            socket.send(JSON.stringify(messageData));
        };

        socket.send_stages_approve = function (messageData) {
            socket.send(JSON.stringify(messageData));
        };
    });
    wsConnect.catch(function (reason) {
        console.error("Server is not available", reason)
    })
}

export {connect}