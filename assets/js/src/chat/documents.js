import {getCookie} from '../utils'
import {loadTemplate} from './loaders'

let document_before_upload_tmpl = loadTemplate('document_before_upload_tmpl');

function uploadDocumentsContactInit() {
    $("#upload-document-contact").bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            sender: $("#contact-chat-form #senderContactId").val(),
            recipent: $("#contact-chat-form #recipentContactId").val(),
        }

    });

    $('#upload-document-contact').fileupload({
        url: '/chat/create/',
        crossDomain: false,
        beforeSend: function (xhr, settings) {
            // console.log("Upload form data -->", this.formData);
            $('#progress .progress-bar').css(
                'width',
                '0%'
            );
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
        },
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                let htmlImg = document_before_upload_tmpl({file: file});
                $(htmlImg).appendTo("#document-send-contact");
            });
        },
        fail: function (e) {
            console.log(e);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function uploadDocumentsOrderInit() {
    $("#upload-document-order").bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            sender: $("#chat-order-add #senderId").val(),
            recipent: $("#chat-order-add #recipentId").val(),
            order: $("#chat-order-add #orderId").val(),
        }
    });

    $('#upload-document-order').fileupload({
        url: '/chat/create/',
        crossDomain: false,
        beforeSend: function (xhr, settings) {
            $('#progress .progress-bar').css(
                'width',
                '0%'
            );
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
        },
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                let htmlImg = document_before_upload_tmpl({file: file});
                $(htmlImg).appendTo("#document-send-order");
            });
        },
        fail: function (e) {
            console.log(e);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function uploadDocumentsTeamInit() {
    $("#upload-document-team").bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            sender: $("#team-chat-form #senderTeamId").val(),
            recipent: $("#team-chat-form #recipentTeamId").val(),
            order: $("#team-chat-form #orderTeamId").val(),
            team: $("#team-chat-form #teamId").val(),
        };
        // console.log(data.formData);
    });

    $('#upload-document-team').fileupload({
        url: '/chat/create/',
        crossDomain: false,
        beforeSend: function (xhr, settings) {
            $('#progress .progress-bar').css(
                'width',
                '0%'
            );
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        },
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                var currentValue = $("#documentSendIds").val();
                currentValue += file.id + ';';
                $("#documentSendIds").val(currentValue);
                let htmlImg = document_before_upload_tmpl({file: file});
                $(htmlImg).appendTo("#document-send");
            });
        },
        fail: function (e) {
            console.log(e);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function bindRemoveDocuments() {
    $('.tab-content').on('click', '.remove-document', function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var _this = $(this);
        $.ajax({
            url: '/api/documents/' + dataId + '/',
            type: 'PATCH',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: {is_delete: true},
            dataType: 'json',
            success: function (json) {
                _this.parent().remove();
                // console.log(json);
            },
            error: function (e, jqxhr) {
                console.log(jqxhr);
            }
        });
    });
}

export {uploadDocumentsContactInit, uploadDocumentsOrderInit, uploadDocumentsTeamInit, bindRemoveDocuments}