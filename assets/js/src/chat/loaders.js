import stage_tmpl from './templates/stage_tmpl.html'
import stage_approved_tmpl from './templates/stage_approved_tmpl.html'
import reserved_tmpl from './templates/reserved_tmpl.html'
import message_tmpl from './templates/message_tmpl.html'
import work_in_process_tmpl from './templates/work_in_process_tmpl.html'
import bntCompleteStage_tmpl from './templates/buttons/bntCompleteStage_tmpl.html'
import btnSendReview_tmpl from './templates/buttons/btnSendReview_tmpl.html'
import document_attach_file_tmpl from './templates/links/document_attach_file_tmpl.html'
import document_link_tmpl from './templates/links/document_link_tmpl.html'
import note_tmpl from './templates/note_tmpl.html'
import order_info_tmpl from './templates/order_info_tmpl.html'
import stage_contractor_approve_tmpl from './templates/stage_contractor_approve_tmpl.html'
import document_before_upload_tmpl from './templates/links/document_before_upload_tmpl.html'
import switch_to_protected_tmpl from './templates/switch_to_protected_tmpl.html'

function loadTemplate(template_name) {
    let templates = {
        stage_tmpl: stage_tmpl,
        stage_approved_tmpl: stage_approved_tmpl,
        reserved_tmpl: reserved_tmpl,
        message_tmpl: message_tmpl,
        work_in_process_tmpl: work_in_process_tmpl,
        bntCompleteStage_tmpl: bntCompleteStage_tmpl,
        btnSendReview_tmpl: btnSendReview_tmpl,
        document_attach_file_tmpl: document_attach_file_tmpl,
        document_link_tmpl: document_link_tmpl,
        note_tmpl: note_tmpl,
        order_info_tmpl: order_info_tmpl,
        stage_contractor_approve_tmpl: stage_contractor_approve_tmpl,
        document_before_upload_tmpl: document_before_upload_tmpl,
        switch_to_protected_tmpl: switch_to_protected_tmpl,
    };

    if (!templates[template_name]) throw new Error(`Template ${template_name} does not exist`);
    return templates[template_name]
}

export {loadTemplate}