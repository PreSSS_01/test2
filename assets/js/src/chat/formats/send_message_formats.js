//1-approve_stages
var approve_stages = {
    "format_type": "approve_stages",
    "data": {
        "sender_id": "",
        "recipent_id": "",
        "order_id": "",
        "msg": "Заказчик зарезервировал сумму для этапов  " + json.stages,
    }
};

//2-add_message_order
var add_message_order = {
    "format_type": "add_message_order",
    "data": {
        "sender_id": "",
        "recipent_id": "",
        "chat_message": "",
        "order_id": "",
        "document_send_links": "id;id;id",
        "document_data": {
            "document_links": 'links/document_link_tmpl.html(...copy)',
            "document_attach_files": 'links/document_attach_file_tmpl.html(...copy)',
        }
    }

};

//3-add_message_contact
var add_message_contact = add_message_order;

//4-add_message_team
var add_message_team = add_message_order;
add_message_team["team_id"] = "";
add_message_team["team_ids"] = "id;id;id";

