import {getCookie} from '../utils'
import {loadTemplate} from  './loaders'
import {StageForm, StageReserved, StageInWork} from './Stages'

let message_format = {
    "format_type": "approve_stages",
    "data": {
        "sender_id": "",
        "recipent_id": "",
        "order_id": "",
        "msg": "",
        "is_system": true
    }
};

const STATUSES = {
    'not_agreed': 'не согласован',
    'send_approve': '...на согласовании',
    'agreed': '...согласовано',
    'cancel_approve': '...исполнитель отказался',
    'in_process': '...в процессе',
    'completed': '...завершен',
    'closed': '...закрыт',

};

//Contractor
//TODO: Вынесли общую логику в родительский класс
class StagesController {
    constructor(orderId, projectId, recipentId, orderName, secureOrder) {
        const self = this;
        this.orderId = orderId;
        this.projectId = projectId;
        this.recipentId = recipentId;
        this.orderName = orderName;
        this.secureOrder = secureOrder;
        // console.log("ids -->", orderId, projectId, recipentId);
        this.data = {}; //JSON
        this.stages = [];
        this.stages_reserved = [];
        this.stages_work = [];
        this.STAGE_STATUSES = {
            'not_agreed': this.buildStartStage.bind(self),
            'send_approve': this.buildSendApproveStage.bind(self),
            'agreed': this.buildAgreedStage.bind(self),
            'in_process': this.buildProcessStage.bind(self),
            'completed': this.buildProcessStage.bind(self),
            'closed': this.buildProcessStage.bind(self),
        };
        this.btnCompleteTmpl = loadTemplate('bntCompleteStage_tmpl');
        this.btnSendReviewTmpl = loadTemplate('btnSendReview_tmpl');
        this.$orderStagesContainer = $('#order-stages');
        this.$orderStagesContainer.html('');
        this.buttons = {
            btnApprove: $('#btnApprove'),       // "Согласовать"
            btnChange: $('#btnChange'),        // "Отправить на внесение изменений"
            btnsToArchive: $('.js-btnToArchive'),     // "Отказаться от заказа"
            btnsArbitration: $('.js-btnArbitration'),// "Обратиться в арбитраж"
            btnSendReview: $('#order-review-add')  // "Оставить отзыв"
        };
        this.stages_elements = {
            $approve: $('#conditions-approve'), //1. Согласование условия
            $reserve: $('#reserveSpace'),       //2. Резервирование (Отобразить)
            $works: $('#completeWork')        //3. Выполненная работа
        };
        this.temp = {
            approve_stage_header_text: this.stages_elements.$approve.find('.js-stage-header').html(),
            reserve_help_text: this.stages_elements.$approve.find('.js-help-text').html(),
        };
        this.init();
    }

    init() {
        const self = this;

        this.dataPromise = this.getOrderData({orderId: this.orderId});
        this.dataPromise
            .then(
                self._onLoadData.bind(self)
            )
            .catch(
                self._onLoadDataError.bind(self)
            );
    }

    getOrderData({orderId}) {
        const self = this;
        return Promise.resolve($.ajax({
            url: `/api/orders/${orderId}/`,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
        }))
    }

    redraw() {
        /**
         * Полностью перерисовываем страницу Заказа
         */
        console.log("Redraw contractor stages");
        // $("#orderBlock" + this.orderId).trigger('click');
        this.init();
    }

    reload() {
        /**
         * Перезагружаем страницу Чата(при удалении заказа)
         */
        window.location = '/chat/#order';
        location.reload();
    }

    _buildPage() {
        // console.log("Build PAge");
        // Restore html to default
        this.stages_elements.$approve.find('.js-stage-header').html(this.temp.approve_stage_header_text);
        this.stages_elements.$reserve.find('.js-help-text').html(this.temp.reserve_help_text);
        this.stages_elements.$approve.find('.js-select').addClass("select");
        this.stages_elements.$reserve.find('.js-select').addClass("select");
        this.stages_elements.$works.find('.js-select').addClass("select");
        this.stages_elements.$reserve.css({'border-top': 'none'});
        this.stages_elements.$works.css({'border-top': 'none'});
        $('#stagesWork').find('.js-select').addClass('select');
        this.stages_elements.$reserve.find('.js-select').addClass('select');
        if ($('#send-review').length)$('#send-review').remove();

        this.stages_elements.$reserve.hide();
        this.stages_elements.$works.hide();
        if (this.data.stages.length == 0) {
            this.buildStartStage()
        } else {
            let stageStatus = this.data.stages[0].status;
            // console.log('stageStatus = ', stageStatus);
            this.STAGE_STATUSES[stageStatus]();
        }
        this._bindEvents();
    }

    buildStartStage() {
        /**
         * Стадия: "Проект Предложен"(нет этапов)
         */
        this.stages_elements.$approve.show();
        this.stages_elements.$reserve.show();
        this.stages_elements.$works.show();
        this.$orderStagesContainer.parent().hide();
        this.buttons.btnsToArchive.first().hide();
        this.$orderStagesContainer.show();
        this.buttons.btnApprove.hide();
        this.buttons.btnChange.hide();
        this.buttons.btnsArbitration.hide();
        this.stages_elements.$approve.find('.js-help-text').show();
        this.stages_elements.$reserve.find('.stages-paid').html("");
        this.stages_elements.$works.find('.js-help-text').show();
        this.stages_elements.$works.find('#stagesWork').html("");
        if (this.secureOrder){
            this.stages_elements.$reserve.find('.js-help-text').show();
        } else {
            this.buttons.btnsArbitration.hide();
            this.stages_elements.$reserve.find('.js-help-text').show();
            this.stages_elements.$reserve.find('.js-select').removeClass('select');
            this.stages_elements.$reserve.find('.js-help-text').html(
                '<span class="select">Резервирование не предусмотрено, безопасная сделака не активна</span>'
            );
        }

    } // Нет Этапов / "Не согласован"

    // buildNotAgreedStage() {
    //     console.log("Stage: not_agreed");
    //     // this._renderStage('stage_tmpl');
    //     this.buttons.btnApprove.hide();
    //     this.buttons.btnChange.hide();
    //     this.buttons.btnToArchive.hide();
    //     this.buttons.btnArbitration.hide();
    // } // Статус "Не согласован"

    buildSendApproveStage() {
        console.log("Stage: send_approve");
        this._renderStage('stage_contractor_approve_tmpl', true);
        this.$orderStagesContainer.parent().show();
        this.buttons.btnApprove.show();
        this.buttons.btnChange.show();
        this.buttons.btnsToArchive.first().show();
        this.stages_elements.$reserve.hide();
        this.stages_elements.$works.hide();
    } // Статус "На согласовании"

    buildAgreedStage() {
        console.log('Stage: agreed');
        this.$orderStagesContainer.parent().show();
        this.buttons.btnApprove.hide();
        this.buttons.btnChange.hide();
        this.buttons.btnsToArchive.hide();
        this.buttons.btnsArbitration.first().hide();
        this.buttons.btnsArbitration.last().show();
        this._renderStage('stage_approved_tmpl', true);
        this.stages_elements.$approve.find('.js-help-text').hide();
        this.stages_elements.$approve.find('.js-select').removeClass('select');
        this.stages_elements.$reserve.show();
        if (this.secureOrder){
            this._renderStageReserved('reserved_tmpl');
            this.stages_elements.$reserve.find('.js-help-text').show();
        }else{
            this.buttons.btnsArbitration.hide();
            this.stages_elements.$reserve.find('.stages-paid').html("");
            this.stages_elements.$reserve.find('.js-help-text').hide();
        }
    } // Статус "Согласован"

    buildProcessStage() {
        console.log('Stage: in_process');
        // Block-Stage-1
        this.stages_elements.$approve.find('.js-stage-header').html('Согласованные условия');
        this.stages_elements.$approve.find('.js-select').removeClass('select');
        this.buildAgreedStage();
        // Block-Stage-2
        this.stages_elements.$reserve.find('.js-help-text').hide();
        this.stages_elements.$reserve.css({'border-top': '1px solid black'});
        // Block-Stage-3
        this.stages_elements.$works.show();
        this.stages_elements.$works.css({'border-top': '1px solid black'});

        this._renderStageInWork('work_in_process_tmpl');
        if (this.secureOrder) {
            // this._renderStageInWork('work_in_process_tmpl');
        } else {
            this.buttons.btnsArbitration.hide();
            this.stages_elements.$reserve.hide();
        }
    } // Статус "В процессе"/"Завершен"/"Закрыт"


    _renderStage(template_name, disable = false) {
        let i = 0;
        // console.log("this.data.stages = ", this.data.stages);
        this.$orderStagesContainer.html("");
        for (let stage_data of this.data.stages) {
            i++;
            // console.log('stage_data = ', stage_data);
            let stage = new StageForm(this.$orderStagesContainer,
                {
                    orderId: this.orderId, stage_num: i, stage_status: STATUSES[stage_data.status],
                    type: 'update', template_name: template_name, data: stage_data
                });
            if (disable) stage.disable();
            this.stages.push(stage);

        }
    }

    _renderStageReserved(template_name) {
        /**
         * Отрисовываем блок "Резервирование"
         */
            // console.log("_renderStageReserved");
        let $container = this.stages_elements.$reserve.find('.stages-paid');
        $container.html("");
        this.stages_reserved = [];
        // console.log("this.data.stages = ", this.data.stages);
        // Нет незарезервированных Этапов
        // let has_not_reserved_stages = false;
        for (let stage_data of this.data.stages) {
            // if (stage_data.status == 'agreed') has_not_reserved_stages = true;
            let stage = new StageReserved($container,
                {
                    template_name, data: stage_data
                });
            this.stages_reserved.push(stage);
        }
        // if (!has_not_reserved_stages) {
        //     this.buttons.btnReserve.hide();
        //     this.stages_elements.$reserve.find('.js-help-text').hide();
        // }
    }

    _renderStageInWork(template_name) {
        /**
         * Отрисовываем блок "Выволнение работы", включая "Выполненныа работа" и "Оставить отзыв"
         */
        let $container = this.stages_elements.$works.find('#stagesWork');
        $container.html("");
        let all_closed = this.data.stages.every((el)=>el.status == 'closed');
        if (all_closed) {
            this.stages_elements.$works.find('.titleStepss').html('3. Выполненная работа');
            this.stages_elements.$works.find('.js-btnArbitration').hide();
            this.stages_elements.$works.find('.js-help-text').hide();
            for (let stage_data of this.data.stages) {
                let stage = new StageInWork($container,
                    {
                        template_name, data: stage_data
                    });
                this.stages_work.push(stage);
            }
            // console.log("has_user_review = ", this.data.has_user_review);
            if (!this.data.has_user_review) {
                let btnReviewOpenModel = $(this.btnSendReviewTmpl());
                btnReviewOpenModel.unbind().on('click', this._onBtnReviewOpenModal.bind(this));
                // Если кнопка "Оставить отзыв" еще не добавлена - добавляем
                if (!this.stages_elements.$works.find('#send-review').length) this.stages_elements.$works.append(btnReviewOpenModel);
            } else {
                if (this.stages_elements.$works.find('#send-review').length) this.stages_elements.$works.find('#send-review').remove()
            }
            $('#stagesWork').find('.js-select').removeClass('select');
            this.stages_elements.$reserve.find('.js-select').removeClass('select');

        } else {
            for (let stage_data of this.data.stages) {
                if (stage_data.status == 'closed' || stage_data.status == 'agreed') continue;
                let note_text = (stage_data.status == 'completed') ? '...НА УТВЕРЖДЕНИИ У ЗАКАЗЧИКА' : '';
                let stage = new StageInWork($container,
                    {
                        template_name, data: stage_data, note_text: note_text
                    });
                if (stage_data.status == 'in_process') {
                    let $btn = $(this.btnCompleteTmpl({stage: stage_data, text: 'Завершить этап'}));
                    $container.html();
                    $container.append($btn);
                    $btn.on('click', this._onBtnComplete.bind(this, stage))
                }
                this.stages_work.push(stage);
            }
        }

    }

    _onLoadData(json) {
        this.data = json;
        this._buildPage();
        if (this.data.project.state == 'deleted') {
            for (let key in this.buttons){
                this.buttons[key].hide();
            }
        }
    }

    _bindEvents() {
        const self = this;
        this.buttons.btnApprove.unbind().on("click", this._onBtnAccept.bind(self));
        this.buttons.btnChange.unbind().on("click", this._onBtnChange.bind(self));
        this.buttons.btnsToArchive.unbind().on("click", this._onBtnAToArchive.bind(self));
        this.buttons.btnsArbitration.unbind().on("click", this._onBtnArbitration.bind(self));
        this.buttons.btnSendReview.unbind().on("click", this._onBtnSendReview.bind(self));
        $("#reserve-stage-modal").find('#stage-num').unbind().on('change', function (event) {
            // console.log("select stage cost = ", self.stages[this.value - 1].data.cost);
            $("#reserve-stage-modal").find('#stage-cost').html(self.stages[this.value - 1].data.cost);
        });
    }

    // BINDS

    _onBtnAccept(event) {
        event.preventDefault();
        const self = this;
        Promise.all(this.stages.map((el)=>el.sendAjax_accept(self.secureOrder))).then(()=> {
            console.log('Этапы согласованы');
            self.redraw();

            let message = message_format;
            message.data.sender_id = userId;
            message.data.recipent_id = self.recipentId;
            message.data.order_id = self.orderId;
            message.data.msg = `Условия заказа "${self.orderName}" приняты`;
            console.log("Send-WS Условия приняты");
            socket.send_stages_approve(message);
            //TODO: раскомментировать дурацкое окно
            // $('#popupOk').modal('show');
            // })
        });
    } // "Согласовать"

    _onBtnChange(event) {
        event.preventDefault();
        const self = this;
        Promise.all(this.stages.map((el)=>el.sendAjax_change())).then(()=> {
            self.redraw();
            let message = message_format;
            message.data.sender_id = userId;
            message.data.recipent_id = self.recipentId;
            message.data.order_id = self.orderId;
            message.data.msg = `Заказ "${self.orderName}" отправлен для внесения изменений`;
            console.log("Send-WS Внести изменения");
            socket.send_stages_approve(message);
        });

    } // "Отправить на внесение изменений"

    // TODO: test it
    _onBtnAToArchive(event) {
        event.preventDefault();
        console.error("Отказаться от заказа. Не протестировано!!");
        // TODO: Не только удалять заказ, но и делать его копию со статусом "Открыто"
        $.ajax({
            // async: false,
            url: `/api/orders/${this.orderId}/`,
            type: 'DELETE',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            dataType: 'json',
        })
            .done(function (json) {
                console.log('delete complete');
                window.location.href = window.location.href.replace(getHash(), "");
            })
            .fail(function (xhr, errorMsg, error) {
                console.log("delete fail, json -->", xhr);
            });

    } // "Отказаться от заказа"

    _onBtnComplete(stage, event) {
        event.preventDefault();
        const self = this;
        // let stageId = $(event.target).data('stage-id');
        // console.log('Complete stage id = ', stage.data.id);
        stage.sendAjax_complete()
            .then(function (json) {
                self.redraw();
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                message.data.msg = `Этап "${json.name}" закрыт`;
                console.log("Send-WS Закрытие этапа");
                socket.send_stages_approve(message);
            })
            .catch(function (xhr) {
                console.log("При закрытии этапа произошла ошибка -->", xhr);
            })

    } // "Закрыть этап"

    _onBtnReviewOpenModal(event) {
        event.preventDefault();

        let review_type = $(event.target).data('review-type');
        $('#review-add').find(`input[type=radio][value=${review_type}]`).prop("checked", true);
        $('#review-add').modal('show');
        console.log('Modal show review_type = ', review_type);
    } // Открыть модальное окно "Оставить отзыв"

    _onBtnSendReview(event) {
        event.preventDefault();
        const self = this;
        $('#projectReviewId').val(this.projectId);
        $('#targetCustomerId').val(this.recipentId);
        // $('#fromContractorId').val('....current user');
        var formData = $("#review-adds-form").serialize();
        // console.log('Оставить отзыв formdata -->', formData);
        $.ajax({
            url: '/api/reviews/',
            type: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            },
            data: formData,
            dataType: 'json',
            success: function (json) {
                // console.log('Отзыв успешно отправлен, json -->', json);
                // $('#review-add').modal('hide');
                // self.stages_elements.$works.find('.js-btnSendReview').hide();
                $('#review-add').modal('hide');
                let message = message_format;
                message.data.sender_id = userId;
                message.data.recipent_id = self.recipentId;
                message.data.order_id = self.orderId;
                message.data.msg = `Отзыв на заказ "${self.orderName}" оставлен`;
                console.log("Send-WS Оставить отзыв");
                // TODO: в этом сообщении отослать команду на reload if(json.count_reviews == 2)
                socket.send_stages_approve(message);
                // $("a[href='#tab2']").trigger('click');
                if(json.count_reviews == 1){
                    self.redraw();
                }else {
                    self.reload();
                }
                // window.location = '/chat/#order';
                // location.reload();
            },
            error: function (e) {
                console.log('error');
                console.log(e);
            }
        });
    } // "Оставить отзыв"

    _changeOrderProtect(secure) {
        this.secureOrder = secure;
        console.log('secure = ', this.secureOrder);
        this.redraw();
    }// Если заказчик изменил secure


    _onBtnArbitration(event) {
        event.preventDefault();
        $("#arbitration-add").modal('show');
    } // "Обратиться в арбитраж"

    _onLoadDataError(error) {
        console.log("Error loading data -->", error);
    }
}

export {StagesController}