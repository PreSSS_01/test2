import {toggler} from './seeds/show_hide'
import {tabsHashInit, restoreTab} from './seeds/bootstrap_tabs'
import {sortRealtyBy} from './seeds/sort_by'
import {customCheckInit} from './seeds/custom_check'

$(function () {
    restoreTab();
    tabsHashInit();
    customCheckInit();
    window.toggler = toggler;
    window.sortRealtyBy = sortRealtyBy;
    // on load of the page: switch to the currently selected tab

});