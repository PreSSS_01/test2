import {ChatPageController} from './chat/ChatContractorPageController'
import {
    bindOrders,
    bindOrderInfo,
    bindArbitrationSend,
    bindOnTabs,
    bindUserContacts,
    bindGetUserMessages,
    bindTeams,
    bindDeleteContact,
    bindCtrlEnterSendMessage,
    restoreTabFromHash,
} from './chat/BINDS'

import {
    uploadDocumentsContactInit,
    uploadDocumentsOrderInit,
    uploadDocumentsTeamInit,
    bindRemoveDocuments
} from './chat/documents'

import {bindContractorNotes, bindOrderNotes, bindTeamNotes} from './chat/notes'


import {bindArchiveProjects} from './chat/archiveProjects'

import {chatContactsInit, chatOrdersInit, chatTeamsInit} from './chat/chats'
import {connect} from './chat/wsChatConnect'

window.connect = connect;
window.socket = undefined;

$(function () {
    bindArbitrationSend();
    window.onhashchange = function (e) {
        $('a[data-toggle="tab"][href="#' + location.hash.slice(1) + '"]').trigger("click");
    };
    window.chatController = new ChatPageController();
    bindOrders();
    bindOrderInfo();
    bindTeams();
    bindOnTabs();
    bindUserContacts();
    bindGetUserMessages();
    bindArchiveProjects();
    bindDeleteContact();
    bindCtrlEnterSendMessage();

    // Chats
    chatContactsInit();
    chatOrdersInit();
    chatTeamsInit();

    //Documents
    uploadDocumentsContactInit();
    uploadDocumentsOrderInit();
    uploadDocumentsTeamInit();
    bindRemoveDocuments();

    //Notes
    bindContractorNotes();
    bindOrderNotes();
    bindTeamNotes();

    //restore
    restoreTabFromHash();
});