import {customSelectInit} from './seeds/custom_select'
import {customCheckInit} from './seeds/custom_check'

$(function () {
    customSelectInit();
    customCheckInit();
});