import {imageUploadInit} from './seeds/image_upload'
import {scrollOnRequiredInit} from './seeds/scroll_on_required'
import {showPopupsInit, addMessage} from  './seeds/popups'
import {ajaxRegistrationInit} from './seeds/ajax_registration'
import {sendFormData} from './seeds/ajax_send_form_data'

$(function () {
    imageUploadInit();
    scrollOnRequiredInit();
    showPopupsInit();
    ajaxRegistrationInit('contractor');
    window.addMessage = addMessage;
    window.sendFormData = sendFormData;
    window.scrollOnRequiredInit = scrollOnRequiredInit;
});