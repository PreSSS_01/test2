import {customCheckInit} from './seeds/custom_check'
import {extendedFieldInit} from './seeds/extended_field'
import {sendFilterData, sendData, modUrl} from './seeds/ajax_set_filter'

function paginateTo(pageNum) {
    const $form = $('#filter-form');
    const postfix = "&page=" + pageNum;
    const $container = $('#resultsBlock');

    sendData($form, $container, pageNum);
    modUrl($form, postfix);
    return false;
}

$(function () {
    customCheckInit();
    extendedFieldInit();
    window.sendFilterData = sendFilterData;
    window.paginateTo = paginateTo;
});