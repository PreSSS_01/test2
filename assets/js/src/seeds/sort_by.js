import {getCookie} from '../utils'

function sortRealtyBy(data, container_id) {
    console.log("sort_by = ", data);
    console.log("container_id = ", container_id);
    let url = '/objects/sort/';
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
        },
        success: function (data) {
            $(container_id).html(data);
            // console.log("data = ", data);
        }
    })
}

export {sortRealtyBy}