function extendedFieldInit() {

    let $buttonF1 = $('.resButton');
    if ($('.slide').hasClass("active")) $buttonF1.css('transform', 'rotate(0deg)');

    $buttonF1.on("click", function (e) {
        e.preventDefault();
        let $slide = $('.slide');
        if ($slide.hasClass("active")) {
            $buttonF1.css('transform', 'rotate(180deg)');
            $slide.slideUp(300);
        } else {
            $buttonF1.css('transform', 'rotate(0deg)');
            $slide.slideDown(300);
        }
        $slide.toggleClass("active");
    });
}

export {extendedFieldInit}