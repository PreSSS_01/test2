function getFormData($form, pageNum) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    indexed_array["page"] = pageNum || "";
    return indexed_array;
}

// TODO: Восстановление фильтров из URL
function modUrl($form, postfix) {
    let curLoc = '?' + $form.serialize() + (postfix || "");
    try {
        history.replaceState($form.serialize(), null, curLoc);
    } catch (e) {
        console.log("Error!");
    }
}

function sendData($form, $update_container, pageNum) {
    function updateResults(html) {
        $update_container.html(html);
        let title = $('#titleScore').val();
        $('.search-num').html(title);
    }

    console.log("form method = ", $form.attr("method"));
    $.ajax({
        url: $form.attr("action"),
        type: $form.attr("method"),
        dataType: 'html',
        data: getFormData($form, pageNum),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
        },
        success: updateResults,
        error: (data) => console.log("Error", data)
    });
}

function sendFilterData($form, $update_container) {
    sendData($form, $update_container);
    modUrl($form);
    return false;
}

export {sendFilterData, sendData, modUrl}