import {humanFileSize} from '../utils'

function fileUploadInit() {
    var $fileUploadContainer = $('#fileUploadContainer');

    $('#fileUploadAddBtn').on('click', function ($evt) {
        $fileUploadContainer.find('.file-upload-widget').last().find('.file-upload-input').click()
    });

    $fileUploadContainer.on('change', '.file-upload-input', function ($evt) {
        var $fileInput = $(this);
        var $fileUploadWidget = $fileInput.closest('.file-upload-widget');
        var filePath = $fileInput.val().replace(/\\/g, '/');
        var fileName = path.basename(filePath);
        //var fileExt = path.extname(filePath)
        var fileSize = $fileInput.get(0).files && humanFileSize($fileInput.get(0).files[0].size);

        if (fileName) {
            $fileUploadWidget.find('.file-upload-label').text(fileName + ' ' + fileSize);

            var $newFileUploadWidget = $fileUploadWidget.clone();
            $newFileUploadWidget.find('.file-upload-label').text('');

            $fileUploadContainer.find('ul').first().append($newFileUploadWidget);

            $fileUploadWidget.css('display', 'block')
        }
    });

    $fileUploadContainer.on('click', '.file-upload-remove-btn', function ($evt) {
        var $btn = $(this);
        $btn.closest('.file-upload-widget').remove()
    });

}

export {fileUploadInit}