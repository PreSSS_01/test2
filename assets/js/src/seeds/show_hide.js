function toggler(from, divId) {
    $(from).toggleClass("clicked");
    $("#" + divId).toggle();
}

export {toggler}