function sendData(form_data) {
    console.log("form_data = ", form_data);
    $.ajax({
        url: '/users/register/',
        type: 'post',
        data: form_data,
        success: function (data) {
            console.log("success data -->", data)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("status = ", xhr.status);
            console.log("data = ", xhr.responseText);
            console.log(ajaxOptions);
            console.log(thrownError);
        }
    })
}