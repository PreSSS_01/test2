function filterToggleInit() {
    let $group = $('.btn-group.toggle');
    let $buttons = $group.find('a.btn').add('.link-sort a.btn');
    // let $input = $('input[name="party_types"]');

    $buttons.on("click", function (e) {
        e.preventDefault();
        let $target = $(e.target);
        let $parent = $target.parent();
        // console.log('parent class = ', $parent.attr("class"));
        $parent.children().removeClass('active');
        $target.addClass('active');
        // console.log('type = ', $target.data("type"));
        $parent.find('input').val($target.data("type"));
        // console.log('click');
        sendFilterData($('#filter-form'), $('#resultsBlock'));
    })
}

export {filterToggleInit}