// Live image avatar (single) upload ---------------------------------
var LIVE_IMAGE_UPLOAD_URL = '/common/live-image-upload/create/';

function avatarUploadInit() {
    var $container = $('.-live-image-avatar-upload-container').first();

    var $avatarImage = $container.find('.-avatar-image').first();
    var $liveImageUpload = $container.find('.-live-image-upload').first();
    var $liveImageId = $container.find('.-live-image-id').first();
    var $liveImageDelete = $container.find('.-live-image-delete').first();

    $avatarImage.attr('orig-src', $avatarImage.attr('src'));

    $liveImageUpload.fileupload({
        url: LIVE_IMAGE_UPLOAD_URL,
        dataType: 'json',

        done: function($evt, data) {
            var image = data.result;

            $avatarImage.attr('src', image.thumbnailUrl);
            $liveImageId.val(image.id);

            $liveImageDelete.data('url', image.deleteUrl);
            $liveImageDelete.css('display', 'block')
        }
    });

    $liveImageDelete.on('click', function($evt) {
        var $that = $(this);

        $.post($that.data('url')).then(function(res) {
            if (res.status == 'success') {
                $avatarImage.attr('src', $avatarImage.attr('orig-src') || STUB_IMAGE_URL);
                $liveImageId.val('');
                $that.css('display', 'none')
            }
        })
    })
}

export {avatarUploadInit}