function customCheckInit() {
    function tuneCheckBoxes($boxes) {
        let currentState = $boxes.find("input").prop("checked") ? 'checked' : 'not-checked';
        $boxes.find("div").hide();
        $boxes.find("div." + currentState).show();
    }

    let $boxes = $('.custom-check');
    tuneCheckBoxes($boxes);
    $boxes.on("click", function (e) {
        let inside_checkBox = $(e.target).parent().find("input");
        inside_checkBox.prop("checked", !inside_checkBox.prop("checked"));
        tuneCheckBoxes($boxes);
        e.preventDefault();
        return false;
    });
}

export {customCheckInit}