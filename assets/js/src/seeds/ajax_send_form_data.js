import {getCookie} from '../utils'

function sendFormData(e) {
    e.preventDefault();
    let $target = $(e.target);
    let $form = $target.closest("form");
    // let formData = $form.serializeArray();
    let formData = new FormData($form[0]);
    $.ajax({
        url: $form.attr("action"),
        data: formData,
        async: false,
        method: $form.attr("method"),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
        },
        success: function (data) {
            // let data = xhr.responseJSON;
            // console.log('success data -->', data);
            window.location.href = data.redirect_to;

        },
        cache: false,
        contentType: false,
        processData: false,
        error: function (xhr, ajaxOptions, thrownError) {
            let status = xhr.status;
            $('.error').removeClass('error');
            if (status == 400) {
                let data = xhr.responseJSON;
                $.each(data, function (key, value) {
                    let $header = $form.find(`[name=${key}]`).siblings('.required');
                    if ($header.length > 0) {
                        $header.addClass("error");
                    }

                });
                window.scrollOnRequiredInit();
            }else{
                console.log('xhr = ', xhr);
            }
        }
    });
    // $.post(url, formData).done(function (data) {
    //     alert(data);
    // });
}

export {sendFormData}