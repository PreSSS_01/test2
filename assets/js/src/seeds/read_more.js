function readMoreInit() {

    $('.description .more').on("click", function (e) {
        let $target = $(e.target);
        $target.siblings(".complete").toggle();
        $target.toggleClass("less");
    });
}

export {readMoreInit}