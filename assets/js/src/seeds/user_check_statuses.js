function checkBoxBindingInit() {
    /*
     Скрываем/Показываем формы/части форм, взависимости от выбранных checkBox'ов
     */
    let $residency_checkBoxes = $('input[name=fin_info-residency]');
    let $legal_status_checkBoxes = $('input[name=fin_info-legal_status]');
    let $legal_status_boxes = $legal_status_checkBoxes.closest('div');
    const $fin_infos = $('.js-fin-info');
    let shows = {
        'russian_resident': ['individual', 'entity', 'employed'],
        'non_russian_resident': ['individual', 'entity'],
        'russian_stay_permit': ['individual'],
    };
    $fin_infos.hide();
    $('.-russian_stay_permit-individual').hide();

    $residency_checkBoxes.on("click", function (e) {
        $legal_status_checkBoxes.first().trigger("click");
    });
    $residency_checkBoxes.on("change", function (e) {
        $legal_status_boxes.hide();
        let value = $(e.target).val();
        for (let legal_status of shows[value]) {
            $(`input[value=${legal_status}]`).closest('div').show();
        }

        let fin_info_id = '';
        fin_info_id += '-' + $residency_checkBoxes.filter(':checked').val();
        fin_info_id += '-' + $legal_status_checkBoxes.filter(':checked').val();
        if (fin_info_id == '-non_russian_resident-individual') {
            $('.-non_russian_resident-individual').hide();
            $('.-non_russian_resident-individual').children('input').val('');
        } else {
            $('.-non_russian_resident-individual').show()
        }
        if (fin_info_id == '-russian_stay_permit-individual') {
            $('.-russian_stay_permit-individual').show();
        } else {
            $('.-russian_stay_permit-individual').hide();
            $('.-russian_stay_permit-individual').children('input').val('');
        }
    });

    $legal_status_checkBoxes.on("click", function (e) {
        $fin_infos.each(function (ind, el) {
            $(el).find('input').val('');
        });
    });
    $legal_status_checkBoxes.on("change", function (e) {
        $fin_infos.hide();

        let fin_info_id = '';
        fin_info_id += '-' + $residency_checkBoxes.filter(':checked').val();
        fin_info_id += '-' + $legal_status_checkBoxes.filter(':checked').val();

        if (fin_info_id != '-non_russian_resident-entity') {
            fin_info_id = '-russian_resident';
            fin_info_id += '-' + $legal_status_checkBoxes.filter(':checked').val();
        }
        $(`#${fin_info_id}`).show();
    });
    $residency_checkBoxes.first().trigger("change");
    $legal_status_checkBoxes.first().trigger("change");
}

export {checkBoxBindingInit}