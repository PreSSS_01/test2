//TODO: оформить Upload'еры в виде классов
//require: <script src='{% static "my-libs.js" %}'></script>
import {humanFileSize} from '../utils'

// function previewImg() {
//     let $fileUploadWidgets = $('.file-upload-widget');
//     $.each($fileUploadWidgets, function (ind, el) {
//         let $fileImg = $(el).find('img');
//         let $fileInput = $(el).find('.file-upload-input');
//         // console.log("$fileInput[0].files.length = ", $fileInput[0].files.length);
//         // console.log("$fileInput.val() = ", $fileInput.val());
//         console.log("$fileInput = ", $fileInput);
//         if ($fileInput[0].files.length) {
//             console.log("Попытка загрузить файл");
//             let reader = new FileReader();
//             reader.onload = function (e) {
//                 $fileImg.attr('src', e.target.result);
//             };
//
//             reader.readAsDataURL($fileInput[0].files[0]);
//         }
//     })
// }

function imageUploadInit() {
    var $fileUploadContainer = $('#fileUploadContainer');

    $('#fileUploadAddBtn').on('click', function ($evt) {
        $fileUploadContainer.find('.file-upload-widget').last().find('.file-upload-input').click()
    });

    $fileUploadContainer.on('change', '.file-upload-input', function ($evt) {
        var $fileInput = $(this);
        var $fileUploadWidget = $fileInput.closest('.file-upload-widget');
        let $fileImg = $fileUploadWidget.find('.preview');
        var filePath = $fileInput.val().replace(/\\/g, '/');
        var fileName = path.basename(filePath);
        var fileSize = $fileInput.get(0).files && humanFileSize($fileInput.get(0).files[0].size);
        console.log("file path", filePath);
        console.log("file path2 ", $fileInput[0].files[0]);
        if (fileName) {
            $fileUploadWidget.find('.file-upload-label').text(fileName + ' ' + fileSize);

            var $newFileUploadWidget = $fileUploadWidget.clone();
            $newFileUploadWidget.find('.file-upload-label').text('');

            $fileUploadContainer.find('.list-new-new').append($newFileUploadWidget);

            $fileUploadWidget.css('display', 'block');

            if ($fileImg.length) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    $fileImg.css('background-image', 'url('+e.target.result +')');
                };

                reader.readAsDataURL($fileInput[0].files[0]);
            }
        }
    });

    $fileUploadContainer.on('click', '.file-upload-remove-img-btn', function ($evt) {
        var $btn = $(this);
        $btn.closest('.file-upload-widget').remove()
    });

}

export {imageUploadInit}
// export {imageUploadInit, previewImg}