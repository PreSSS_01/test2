//Message TYPES: see in 'popups.sass'

function showMessage(container, template, message, type) {
    let $popup = $(template);
    $popup.hide();
    $popup.find('.message').html(message);
    $popup.addClass(type);
    container.append($popup);
    $popup.fadeIn(1000, function () {
        setTimeout(function (el) {
            $(el).fadeOut(1000);
        }, 2000, this)

    });
}

function addMessage(message, type) {
    const $popups_storage = $('#popups-storage');
    let li = `<li class="${type}">${message}</li>`;
    $popups_storage.append(li);
}

function showPopupsInit() {
    const $popups_storage = $('#popups-storage');
    const $popups_container = $('#popups-container');
    let $popup = $popups_container.find(".popup");
    let popup_html = $popup[0].outerHTML;
    $popup.remove();
    if (!$popups_storage.length) return;
    $.each($popups_storage.find("li"), function (key, value) {
        let message = $(value).html();
        let type = $(value).attr('class');
        showMessage($popups_container, popup_html, message, type);
        $(value).remove();
    });
    $popups_storage.bind("DOMNodeInserted", function () {
        let $li = $(this).find('li');
        let message = $li.html();
        let type = $li.attr('class');
        $li.remove();
        showMessage($popups_container, popup_html, message, type);
    });
}

export {showPopupsInit, addMessage}