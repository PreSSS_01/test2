function onlyOneCheckboxInit(selector_first, selector_second) {
    //TODO: need refactoring...
    let $first = $(selector_first);
    let $second = $(selector_second);
    $first.change(()=> {
        if ($first.prop("checked")){
            $second.prop("checked", false)
        }else {
            $second.attr("checked", true)
        }
    });
    $second.change(()=> {
        if ($second.prop("checked")){
            $first.attr("checked", false)
        }else {
            $first.attr("checked", true)
        }
    })
}

export {onlyOneCheckboxInit}