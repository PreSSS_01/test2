function tabsHashInit() {
    // store the currently selected tab in the hash value
    $("a[data-toggle=tab]").on("shown.bs.tab", function (e) {
        let target = $(e.target);
        if(target.hasClass("tab-inserted")){
            target.siblings(".active").removeClass("active");
            target.addClass("active");
            return
        }
        let id = target.attr("href").substr(1);
        let scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = id;
        $('html,body').scrollTop(scrollmem);
    });
}

function restoreTab() {
    // on load of the page: switch to the currently selected tab
    let hash = window.location.hash;
    let a = $(`a[data-toggle="tab"][href="${hash}"]`);
    hash && a.tab('show');
}

export {tabsHashInit, restoreTab}