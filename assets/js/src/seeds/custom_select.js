function customSelectInit() {
    // CUSTOM-SELECT
    let $select_container = $('.custom-select');
    let $sc_headers = $select_container.find('.simple-select');
    let $sc_options = $select_container.find('.select-box-options');

    $sc_options.hide();

    $sc_headers.on("click", function (e) {
        $(e.target).siblings('.select-box-options').show();
    });

    let $options = $sc_options.find('li');
    $options.on("click", function (e) {
        const target = $(e.target);
        let header = target.closest('.select-box-options').siblings('.simple-select');
        let data = target.closest('.select-box-options').siblings('input[type=hidden]');
        header.val(target.html());
        data.val(target.data("id"));
        // $sc_data.val($(e.target).data("id"));
        $sc_options.hide();
        e.preventDefault()
    });
    $(document).click(function (event) {
        //FIXME: запомнить на ком был клик, и не закрывать именно его
        if ($(event.target).closest($select_container).length) {
            return;
        }
        $sc_options.hide();
    });
}

export {customSelectInit}