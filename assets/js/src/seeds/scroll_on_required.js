function scrollOnRequiredInit() {
    let $required = $('.required.error');
    // console.log($required);
    if (!$required.length) return;
    $('html, body').animate({
        scrollTop: $required.offset().top - 25
    }, 1000);
}

export {scrollOnRequiredInit}