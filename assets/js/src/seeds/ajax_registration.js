const $form = $('#form-registration');

function clearErrors() {
    let $error_tags = $form.find('.errorlist');
    if ($error_tags.length > 0) $error_tags.remove();
}

function sendData(form_data) {
    // console.log("form_data = ", form_data);
    $.ajax({
        url: '/users/register/',
        type: 'post',
        data: form_data,
        success: function (data) {
            // console.log("success data -->", data);
            $('#registrationFormModal').modal('toggle');
            $('input[name=not_auth_user_id]').val(data.pk);
            $('#not_customer').hide();
            $('#is_customer').show();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            let status = xhr.status;
            if (status == 400) {
                let data = JSON.parse(xhr.responseText);
                $.each(data, function (key, value) {
                    let ul = $("<ul class='errorlist'></ul>");
                    for (let error of value) {
                        ul.append(`<li>${error}</li>`)
                    }
                    let $field;
                    if (key == 'captcha_html') {
                        // console.log("captcha html = ", key, '/', value);
                        $form.find('.captcha').html(value);
                        return;
                    } else if (key == 'captcha') {
                        $field = $form.find('.g-recaptcha');
                    } else if (key == 'tos') {
                        $field = $form.find(`input[name=${key}]`).parent();
                    } else {
                        $field = $form.find(`input[name=${key}]`);
                        if ($field.length == 0) console.log(`!!!field ${key} not found`);
                    }
                    if ($field.length > 0) $field.parent().append(ul);

                });
                // console.log('captcha error = ', data.captcha);
                // console.log('data type = ', typeof data);
            }
            // console.log("data = ", xhr.responseText);
            // console.log(ajaxOptions);
            // console.log(thrownError);
        }
    })
}

function ajaxRegistrationInit(user_type) {
    // const $form = $('#form-registration');
    $form.on("submit", function (e) {
        e.preventDefault();
        // get form data and add user_type
        clearErrors();
        let form_data = $form.serialize() + "&user_type=" + encodeURIComponent(user_type);
        sendData(form_data);
    })
}

export {ajaxRegistrationInit}