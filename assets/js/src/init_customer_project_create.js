import SelectedContainer from 'components/SelectedContainer';
import SelectedContainerCreate from 'components/SelectedContainerCreate';
import NoTreeSelect from 'components/NoTreeSelect';
import TreeSelect from 'components/TreeSelect';
import SingleTreeSelect from 'components/SingleTreeSelect'
import SelectOrCreate from 'components/SelectOrCreate'

$(function () {
    function createSpecs(url) {
        // SPECIALIZATIONS  
        let sb_main = new TreeSelect($('#select-box-1'), {url, visible: true, required: true});
        // sb_main.setHeader("Специальность");
        let select_container = new SelectedContainer($('#selected-spec'),
            {
                obj: sb_main,
                onlyOne: true
            });
        sb_main.connectSelectedContainer(select_container);
        let sb_1 = new TreeSelect($('#select-box-2'), {obj: sb_main});
        let sb_2 = new TreeSelect($('#select-box-3'), {obj: sb_main});
        let sb_3 = new TreeSelect($('#select-box-4'), {obj: sb_main});
        let sb_4 = new TreeSelect($('#select-box-5'), {obj: sb_main});

        select_container.on("add", () => {
            let $container = $('#spec-value');
            $container.html($('#selected-spec').find(".selected-element").find(".name").html());
        });

        sb_main.setNearbySelectBox(sb_1);
        sb_1.setNearbySelectBox(sb_2, sb_main);
        sb_2.setNearbySelectBox(sb_3, sb_1);
        sb_3.setNearbySelectBox(sb_4, sb_2);
        sb_4.setNearbySelectBox("", sb_3);

    }

    function createBuildingClass(url) {
        // BUILDING-CLASSIFICATION  
        sb_build_main = new TreeSelect($('#sb-building-classification'), {url, visible: true});
        sb_build_main.setHeader("Классификация здания");

        let sb_build_1 = new TreeSelect($('#sb-building-sub-classification'), {obj: sb_build_main});

        let select_build_container = new SelectedContainer($('#selected-building-classification'),
            {
                obj: sb_build_main,
                onlyOne: true
            });
        sb_build_main.connectSelectedContainer(select_build_container);

        sb_build_main.setNearbySelectBox(sb_build_1);
        sb_build_1.setNearbySelectBox("", sb_build_main);
    }

    function createConstructionType(url) {
        sb_constr_main = new NoTreeSelect($('#sb-construction-type'), {url, visible: true});
        sb_constr_main.setHeader("Вид строительства");
        let select_constr_type = new SelectedContainer($('#selected-construction-type'), {
            obj: sb_constr_main,
            noTree: true,
            onlyOne: true
        });
        sb_constr_main.connectSelectedContainer(select_constr_type);
    }

    function createLocations(url) {
        sb_loc_main = new TreeSelect($('#sb-location-1'), {url, visible: true});
        sb_loc_main.setHeader("Местоположение");
        let select_loc = new SelectedContainer($('#selected-location'),
            {
                obj: sb_loc_main,
                onlyOne: true
            });
        sb_loc_main.connectSelectedContainer(select_loc);
        let sb_loc_1 = new TreeSelect($('#sb-location-2'), {obj: sb_loc_main});
        let sb_loc_2 = new TreeSelect($('#sb-location-3'), {obj: sb_loc_main});

        sb_loc_main.setNearbySelectBox(sb_loc_1);
        sb_loc_1.setNearbySelectBox(sb_loc_2, sb_loc_main);
        sb_loc_2.setNearbySelectBox("", sb_loc_1);
    }

    function createRealty(url) {
        let sb_realty = new SelectOrCreate($('#sb-realty'), {url, visible: true});
        sb_realty.setHeader(" ");
        let select_realty = new SelectedContainerCreate($('#selected-realty'),
            {
                obj: sb_realty,
                noTree: true,
                onlyOne: true,
                noHeader: true
            });
        sb_realty.connectSelectedContainer(select_realty);
        sb_realty.setLinkBoxes([sb_loc_main, sb_constr_main, sb_build_main]);
        select_realty.on("add", () => {
            $('#checkbox-sb-realty').attr("disabled", true)
        });
        select_realty.on("remove", () => {
            $('#checkbox-sb-realty').attr("disabled", false)
        });
        sb_realty.dataPromise.then(function () {
                let $realty = $('#sb-realty');
                let check = $('#checkbox-sb-realty');
                if (!check.prop("checked")) {
                    $realty.hide();
                }
            }
        );
        let sb_realty_top = new NoTreeSelect($('#sb-realty-top'), {url, visible: true});
        sb_realty_top.setHeader("Объект");
        sb_realty_top.connectSelectedContainer(select_realty);
        sb_realty_top.dataPromise.then(function () {
            if (!sb_realty_top.dataTree.data.length) {
                sb_realty_top.hide()
            }else{
                //Если перешли со страницы профиля по кнопке "Добавить заказ"
                let id = window.location.hash.replace("#", "");
                if (id) {
                    sb_realty_top.setElementById(id);
                    select_realty.add(id);
                }
            }
        });
        select_realty.on("add", (args)=> {
            //TODO: Костыли!!!
            $('#checkbox-sb-realty').prop("checked", true);
            sb_realty.show();
            let id = args[0];
            if (id.text) return;
            let el = sb_realty.dataTree.getElementById(id);
            sb_realty_top.dataPromise.then(function () {
                sb_realty_top.$searchInput.val(el.name);
                sb_realty_top.selectedEl.id = id;
                sb_realty_top.selectedEl.value = el.name;
            });

            sb_realty.selectedEl.id = id;
            sb_realty._fillBoxes();
        })
    }

    let sb_loc_main, sb_constr_main, sb_build_main;

    createSpecs('/api/specializations_flat');

    createBuildingClass('/api/building_classifications');
    createConstructionType('/api/construction_type');
    createLocations('/api/locations_flat');

    createRealty('/api/realties/current_user')

});