import {showPopupsInit, addMessage} from './seeds/popups'

function checkHash() {
    // on load of the page: switch to the currently selected tab
    let hash = window.location.hash;
    console.log("hash = ", hash);
    if (hash == '#need-login-for-view'){
        addMessage('Для просмотра личных данных необходимо зарегистрироваться', 'info');
    }
}

$(function(){
    showPopupsInit();
    checkHash();
});