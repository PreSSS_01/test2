from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from django.db import models
from django import forms

from .models import Specialization


class SpecializationAdmin(MPTTModelAdmin):
    readonly_fields = ('pk', 'lft', 'rght', 'tree_id', 'level')
    list_display = ('pk', 'name', 'order', 'level', 'parent')
    list_filter = ('level', )
    formfield_overrides = {
        models.CharField: {'widget': forms.TextInput(attrs={'size': '114'})},
    }

    def get_queryset(self, request):
        qs = super(SpecializationAdmin, self).get_queryset(request)

        qs.order_by('order')
        return qs


admin.site.register(Specialization, SpecializationAdmin)
