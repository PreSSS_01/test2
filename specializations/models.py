from django.db import models
from mptt.managers import TreeManager
from mptt.models import MPTTModel, TreeForeignKey


class Specialization(MPTTModel):
    name = models.CharField(max_length=500)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    order = models.PositiveIntegerField()
    selectable = models.BooleanField(default=True)

    objects = TreeManager()

    def __str__(self):
        return self.name

    def full_path(self):
        part = self
        path = part.name
        while True:
            if part.parent.name != '_root':
                # print(part.name)
                part = part.parent
                path = '{} / {}'.format(part.name, path)
            else:
                return path

    class Meta:
        get_latest_by = 'order'
        ordering = ['order']
        verbose_name = 'Специализация'
        verbose_name_plural = 'Специализации'

    # class MPTTMeta:
    #     order_insertion_by = ['order', 'name']
