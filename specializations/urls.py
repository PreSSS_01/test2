from django.conf import urls

from .views import SpecListView, SpecChildrenDetailView, test_page, test_spec

urlpatterns = [
    urls.url(r'^$', SpecListView.as_view(), name='spec-list'),
    urls.url(r'^test/$', test_page),
    urls.url(r'^spectest/$', test_spec),
    urls.url(r'^children/(?P<pk>\d+)$', SpecChildrenDetailView.as_view(), name='spec-children'),

]
