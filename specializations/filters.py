from rest_framework_filters import FilterSet, RelatedFilter, AllLookupsFilter

from .models import Specialization


class SpecializationFilterSet(FilterSet):
    id = AllLookupsFilter()
    level = AllLookupsFilter()
    lft = AllLookupsFilter()
    name = AllLookupsFilter()
    rght = AllLookupsFilter()
    tree_id = AllLookupsFilter()

    children = RelatedFilter('specializations.filters.SpecializationFilterSet')
    parent = RelatedFilter('specializations.filters.SpecializationFilterSet')
    projects = RelatedFilter('projects.filters.ProjectFilterSet')

    class Meta:
        model = Specialization
