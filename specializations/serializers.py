from rest_framework.serializers import ModelSerializer

from .models import Specialization


class NestedSpecializationSerializer(ModelSerializer):
    class Meta:
        model = Specialization

        fields = (
            'children',
            'id',
            'level',
            'lft',
            'name',
            'parent',
            'projects',
            'rght',
            'tree_id',
        )


class SpecializationSerializer(ModelSerializer):
    children = NestedSpecializationSerializer(many=True)
    parent = NestedSpecializationSerializer()

    class Meta:
        model = Specialization

        fields = (
            'children',
            'parent',
            # 'projects',

            'id',
            'level',
            'lft',
            'name',
            'rght',
            'tree_id',
        )


class NestedSpecializationSerializerOnlyId(ModelSerializer):
    class Meta:
        model = Specialization

        fields = (
            'id',
        )


class SpecializationSerializerFlat(ModelSerializer):
    children = NestedSpecializationSerializerOnlyId(many=True)
    parent = NestedSpecializationSerializerOnlyId()

    class Meta:
        model = Specialization

        fields = (
            'id',
            'name',
            'children',
            'parent')
