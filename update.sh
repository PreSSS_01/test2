#!/usr/bin/env bash

source ../env/bin/activate &&
pip install -r requirements/base.txt &&
git reset --hard &&
git pull &&
./manage.py migrate --noinput &&
./manage.py collectstatic --noinput &&
supervisorctl restart arch &&
supervisorctl restart arch_chat &&
./manage.py recalculation_spec &&
chown -R www-data:www-data ./ && chmod -R 775 ./ &&
chmod +x update.sh