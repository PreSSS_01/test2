from django.contrib import admin

from .models import Message, Notes, Documents, NewMessage


class MessageAdmin(admin.ModelAdmin):
    list_display = ('text', 'sender', 'recipent', 'is_new',)


class NotesAdmin(admin.ModelAdmin):
    list_display = ('text', 'sender', 'recipent', 'order',)


class DocumentsAdmin(admin.ModelAdmin):
    list_display = ('sender', 'recipent', 'order', 'team')


class NewMessageAdmin(admin.ModelAdmin):
    list_display = ('user', 'message_pk', 'message_sender', 'message_recipent', 'message_order', 'message_team')

    def message_pk(self, obj):
        return obj.message.pk

    def message_sender(self,obj):
        return obj.message.sender

    def message_recipent(self, obj):
        return obj.message.recipent

    def message_order(self, obj):
        return obj.message.order

    def message_team(self, obj):
        return obj.message.team


admin.site.register(Message, MessageAdmin)
admin.site.register(Notes, NotesAdmin)
admin.site.register(Documents, DocumentsAdmin)
admin.site.register(NewMessage, NewMessageAdmin)
