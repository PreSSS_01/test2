from rest_framework_filters import FilterSet, RelatedFilter, AllLookupsFilter

from .models import Message, Notes, Documents


#
# class DocumentsFilterSet(FilterSet):
#     file = AllLookupsFilter()
#     id = AllLookupsFilter()
#     # sender = RelatedFilter('users.filters.UserFilterSet')
#     # recipent = RelatedFilter('users.filters.UserFilterSet')
#     # order = RelatedFilter('projects.filters.OrderFilterSet')
#     # order = AllLookupsFilter()
#
#     class Meta:
#         model = Documents


class MessageFilterSet(FilterSet):
    created = AllLookupsFilter()
    id = AllLookupsFilter()
    private_type = AllLookupsFilter()
    text = AllLookupsFilter()

    order = RelatedFilter('projects.filters.OrderFilterSet')
    recipent = RelatedFilter('users.filters.UserFilterSet')
    sender = RelatedFilter('users.filters.UserFilterSet')
    team = RelatedFilter('users.filters.TeamFilterSet')

    class Meta:
        model = Message


class DocumentFilterSet(FilterSet):
    id = AllLookupsFilter()

    # order = RelatedFilter('projects.filters.OrderFilterSet')

    class Meta:
        model = Documents


class NoteFilterSet(FilterSet):
    created = AllLookupsFilter()
    id = AllLookupsFilter()
    text = AllLookupsFilter()

    recipent = RelatedFilter('users.filters.UserFilterSet')
    sender = RelatedFilter('users.filters.UserFilterSet')

    class Meta:
        model = Notes
