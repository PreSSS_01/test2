from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from users.serializers import UserSerializer
from .models import Message, Notes, Documents


class DocumentsSerializer(ModelSerializer):
    file = serializers.SerializerMethodField()
    file_size = serializers.SerializerMethodField()
    file_url = serializers.SerializerMethodField()

    class Meta:
        model = Documents

        fields = (
            'id',
            'file',
            'file_size',
            'file_url',
            'sender',
            'recipent',
            'team',
            'order',
            'is_send',
            'is_delete',
            'message',
        )

    def get_file(self, obj):
        return obj.file.name

    def get_file_size(self, obj):
        return obj.file.size

    def get_file_url(self, obj):
        return obj.file.url


class MessageSerializer(ModelSerializer):
    sender = UserSerializer()
    recipent = UserSerializer()
    created = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    documents = DocumentsSerializer(read_only=True, many=True)
    text = serializers.SerializerMethodField()

    class Meta:
        model = Message

        fields = (
            'text',
            'created',
            'order',
            'sender',
            'recipent',
            'private_type',
            'team',
            'documents',
            'is_system'
        )

    def get_text(self, obj):
        out = obj.text
        documents = obj.documents.all()
        if len(documents) > 0:
            documents_str = '<br>'.join([
                                            'Приложенный файл. скачать: <br><a target="_blank" href="/chat/download/' + doc.file.name + '">' + doc.file.name + '</a>'
                                            for doc in documents])
            out += documents_str
        return out


class NoteSerializer(ModelSerializer):
    class Meta:
        model = Notes

        fields = (
            'id',
            'text',
            'created',
            'order',
            'sender',
            'recipent',
            'team',
        )
