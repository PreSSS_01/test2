from django.conf import urls

from .views import (
    ChatUserView,
    DocumentCreateView,
    messages_delete,
    project_delete,
    download_file,
)

app_name = 'chat'

urlpatterns = [
    urls.url(r'^$', ChatUserView.as_view(), name='chat-user'),
    urls.url(r'^messages_delete/$', messages_delete, name='chat-messages_delete'),
    urls.url(r'^project/trashed/$', project_delete),
    urls.url(r'^create/$', DocumentCreateView.as_view()),
    urls.url(r'^download/(?P<file_name>.+)', download_file),

]
