import json
from wsgiref.util import FileWrapper

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.views.generic import View, CreateView

from projects.models import Order, Project
from users.models import User, Team
from wallets.models import Transaction
from .models import Message, Documents
from .response import JSONResponse, response_mimetype
from .utils import serialize


class DocumentCreateView(CreateView):
    model = Documents
    fields = '__all__'

    def form_valid(self, form):
        self.object = form.save()
        files = [serialize(self.object)]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        # import code; code.interact(local=dict(globals(), **locals()))
        data = json.dumps(form.errors)
        return HttpResponse(content=data, status=400, content_type='application/json')


class ChatUserView(LoginRequiredMixin, View):
    template_name = ''

    def get(self, request, *args, **kwargs):
        user_id = request.GET.get('user_id', None)
        if request.user.is_customer():
            customer_contacts = Message.objects.values_list('sender_id', 'recipent_id'). \
                filter(Q(recipent_id=request.user.pk) | Q(sender_id=request.user.pk)). \
                filter(Q(team_id=None)). \
                filter(is_delete=False).distinct()

            users_ids = []
            for msg in customer_contacts:
                a, b = msg
                if a != request.user.pk:
                    users_ids.append(a)
                if b != request.user.pk:
                    users_ids.append(b)
            if user_id:
                users_ids.append(int(user_id))

            contacts_users = User.objects.filter(pk__in=users_ids)
            archive_projects = request.user.customer_projects.select_related('order').exclude(state='active')
            print("archive_projects = ", archive_projects)
            # chat_messages = Message.objects.filter(Q(sender=request.user.pk) | Q(recipent=request.user.pk))
            # projects
            projects = request.user.customer_projects.select_related('order').filter(state='active').exclude(
                order__contractor__isnull=True, order__team__isnull=True)
            print("projects = ", projects)
            orders = [project.order for project in projects]
            print("orders = ", orders)
            order_ids = [order.pk for order in orders]
            transaction = Transaction.objects.get_or_create(customer=request.user, type='reservation', complete=False)

            contacts_users_count = request.user.new_messages.filter(message__sender__in=users_ids,
                                                                    message__order__isnull=True,
                                                                    message__team__isnull=True
                                                                    ).count()

            orders_ms_count = request.user.new_messages.filter(message__order__in=order_ids,
                                                               message__team__isnull=True).count()
            self.template_name = 'chat_customer.html'
            return render(request, self.template_name, {'contacts_users': contacts_users,
                                                        # 'chat_messages': chat_messages,
                                                        'contacts_users_count': contacts_users_count,
                                                        'orders_ms_count': orders_ms_count,
                                                        'archive_projects': archive_projects,
                                                        'orders': orders,
                                                        'transaction': transaction[0],
                                                        'YANDEX_MONEY': settings.YANDEX_MONEY,
                                                        })
        else:
            # contractor
            team_ids = []
            if request.user.is_owner_team():
                team_ids.append(request.user.team.pk)
            # team_orders = request.user.team.orders.all()
            #     # teams = Team.objects.filter(contractors__id=request.user.pk).all()
            # else:

            teams = Team.objects.filter(Q(contractors__id=request.user.pk) | Q(owner=request.user.pk)).all()
            team_orders = Order.objects.filter(team_id__in=[team.pk for team in teams]).all()

            orders = Order.objects.filter(Q(contractor=request.user) | Q(team_id__in=team_ids)).filter(
                project__state='active')
            archive_orders = Order.objects.filter(Q(contractor=request.user) | Q(team_id__in=team_ids)).exclude(
                project__state='active')
            contractor_contacts = Message.objects.values_list('sender_id', 'recipent_id').filter(
                Q(recipent_id=request.user.pk) | Q(sender_id=request.user.pk)).filter(Q(team_id=None)). \
                filter(is_delete=False).distinct()
            users_ids = []
            for msg in contractor_contacts:
                a, b = msg
                if a != request.user.pk:
                    users_ids.append(a)
                if b != request.user.pk:
                    users_ids.append(b)
            if user_id:
                users_ids.append(int(user_id))

            contacts_users = User.objects.filter(pk__in=users_ids)
            contacts_users_count = request.user.new_messages.filter(message__sender__in=users_ids,
                                                                    message__order__isnull=True,
                                                                    message__team__isnull=True
                                                                    ).count()
            chat_messages = Message.objects.filter(Q(sender=request.user.pk) | Q(recipent=request.user.pk)).order_by(
                'created')

            your_teams = Team.objects.filter(Q(contractors__id=request.user.pk) | Q(owner=request.user))

            orders_ms_count = request.user.new_messages.filter(message__order__in=orders,
                                                               message__team__isnull=True).count()
            teams_ms_count = request.user.new_messages.filter(message__team__in=your_teams).count()

            self.template_name = 'chat_contractor.html'
            return render(request, self.template_name, {'orders': orders,
                                                        'contacts_users': contacts_users,
                                                        'contacts_users_count': contacts_users_count,
                                                        'orders_ms_count': orders_ms_count,
                                                        'teams_ms_count': teams_ms_count,
                                                        'chat_messages': chat_messages,
                                                        'team_orders': team_orders,
                                                        'your_teams': your_teams,
                                                        'archive_orders': archive_orders,
                                                        })


def messages_delete(request):
    if request.is_ajax():
        sender = request.POST.get('sender_id')
        recipent = request.POST.get('recipent_id')
        queryset = Message.objects.all()
        queryset = queryset.filter(Q(sender__in=[sender, recipent]), Q(recipent__in=[sender, recipent]))
        queryset.update(is_delete=True)
        data = {'status': 'ok'}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404


def project_delete(request):
    if request.is_ajax():
        project_id = request.POST.get('project_id')
        try:
            project = Project.objects.get(pk=project_id, customer=request.user)
        except Documents.DoesNotExist:
            project = None
        if project:
            project.state = 'trashed'
            project.save()
            data = {'status': 'ok'}
        else:
            data = {'status': 'error'}
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        raise Http404


def download_file(request, file_name):
    try:
        document = Documents.objects.get(file=file_name)
    except Documents.DoesNotExist:
        document = None

    file_name = document.file
    response = HttpResponse(FileWrapper(file_name), content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=' + file_name.name
    return response
