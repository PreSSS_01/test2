from django.db import models
from django.utils import timezone

from projects.models import Order
from users.models import User, Team


class Message(models.Model):
    text = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    order = models.ForeignKey(Order, related_name='messages', null=True, blank=True)
    sender = models.ForeignKey(User, related_name='sender_messages')
    # TODO: recipent --> recipient
    recipent = models.ForeignKey(User, related_name='recipent_messages')
    private_type = models.BooleanField(default=False)
    team = models.ForeignKey(Team, related_name='messages', null=True, blank=True)
    is_delete = models.BooleanField(default=False)
    is_new = models.BooleanField(default=True)
    # Системное
    is_system = models.BooleanField(default=False)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class NewMessage(models.Model):
    user = models.ForeignKey(User, related_name='new_messages')
    message = models.ForeignKey(Message, related_name='new_messages')

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Новые сообщения'
        verbose_name_plural = 'Новые сообщения'


class Notes(models.Model):
    text = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    sender = models.ForeignKey(User, related_name='sender_notes')
    recipent = models.ForeignKey(User, related_name='recipent_notes', null=True, blank=True)
    order = models.ForeignKey(Order, related_name='notes', null=True, blank=True)
    team = models.ForeignKey(Team, related_name='notes', null=True, blank=True)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Заметка'
        verbose_name_plural = 'Заметки'


class Documents(models.Model):
    file = models.FileField(upload_to='chat/documents/')
    order = models.ForeignKey(Order, related_name='documents', null=True, blank=True)
    team = models.ForeignKey(Team, related_name='documents', null=True, blank=True)
    sender = models.ForeignKey(User, related_name='sender_documents')
    recipent = models.ForeignKey(User, related_name='recipent_documents', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    is_send = models.BooleanField(default=False)
    is_delete = models.BooleanField(default=False)
    message = models.ForeignKey(Message, related_name='documents', null=True, blank=True)

    def __str__(self):
        return self.file.url

    class Meta:
        verbose_name = 'Входящие Документы'
        verbose_name_plural = 'Входящие Документы'
