import mimetypes


def serialize(instance, file_attr='file'):
    obj = getattr(instance, file_attr)
    return {
        'id': instance.id,
        'url': obj.url,
        'name': obj.name,
        'type': mimetypes.guess_type(obj.path)[0] or 'image/png',
        'size': obj.size,
        'deleteUrl': '/delete',
        'deleteType': 'DELETE',
    }
