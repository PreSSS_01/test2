const NODE_ENV = process.env.NODE_ENV || 'dev';
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    // context: __dirname + "/assets/js/src",
    context: __dirname,
    // context: path.join(__dirname, "static", "js"),

    entry: {
        //DEBUG
        debugUtilsInit: "./assets/js/src/debugUtilsInit.js",
        //Common
        custom_components: "./assets/js/src/custom_components.js",
        // projects
        create_project: "./assets/js/src/customer_project_create.js",
        init_customer_project_create: './assets/js/src/init_customer_project_create.js',
        //worksell
        create_worksell: "./assets/js/src/create_worksell.js",
        init_create_worksell:  './assets/js/src/init_create_worksell.js',
        //home
        home_page: "./assets/js/src/home_page.js",
        //profile
        customer_profile: "./assets/js/src/customer_profile.js",
        user_profile_edit: "./assets/js/src/user_profile_edit.js",
        //filters/search
        init_contractor_filter: "./assets/js/src/init_contractor_filter.js",
        contractor_filter: "./assets/js/src/contractor_filter.js",
        project_filter: "./assets/js/src/project_filter.js",
        init_worksell_filter: "./assets/js/src/init_worksell_filter.js",
        worksell_filter: "./assets/js/src/worksell_filter.js",
        //portfolio
        init_portfolio_create_edit: "./assets/js/src/init_portfolio_create_edit.js",
        portfolio_create_edit: "./assets/js/src/portfolio_create_edit.js",
        //modal Realty edit
        init_modal_realty_edit: "./assets/js/src/init_modal_realty_edit.js",
        //registration
        registration: "./assets/js/src/registration.js",
        //Chat
        chat_customer_oop: "./assets/js/src/chat_customer_oop.js",
        chat_contractor_oop: "./assets/js/src/chat_contractor_oop.js",
    },

    output: {
        path: path.resolve(__dirname, "assets/js/build"),
        filename: "[name].js",
        // ,library: "[name]" // import to global variables
    },

    watch: NODE_ENV == 'dev',

    devtools: "eval",

    plugins: [
        new webpack.DefinePlugin(
            {NODE_ENV: JSON.stringify(NODE_ENV)}),

        new ExtractTextPlugin('styles.css', {
            allChunks: true
        })
        // new webpack.EnvironmentPlugin('NODE_ENV')
    ],

    resolve: {
        alias: {
            components: path.resolve(__dirname, "assets/lib/proekton-components/js/src")
        }
        ,
        // moduleDirectories: [__dirname + '/assets/js/src'],
        extensions: ['', '.js']
    },

    resolveLoader: {
        moduleDirectories: ['node_modules'],
        // moduleTemplate: ['*-loader', '*'],
        // extentions: ['', '.js']
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    cacheDirectory: true,
                    plugins: ['transform-decorators-legacy'],
                    presets: ['es2015']
                }
            },
            {
                test: /\.html$/,
                loader: "babel!es6-template-string?presets[]=es2015",
                exclude: /node_modules/,
            },
            {
                test: /\.sass/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader!resolve-url!sass-loader?sourceMap')
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /\.woff2?$|\.ttf$|\.eot$|\.otf|\.svg$|\.png|\.jpe?g|\.gif$/,
                loader: 'file-loader'
            }
        ]

    }
};