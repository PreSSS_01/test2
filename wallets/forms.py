from django import forms
from django.conf import settings

from .models import WithDraw, PayFromScore


class WithDrawForm(forms.ModelForm):
    class Meta:
        model = WithDraw

        fields = (
            'passport',
            'phone',
            'sum',
            'user',
            'yandex_card',
        )


class PayFromScoreForm(forms.ModelForm):
    class Meta:
        model = PayFromScore

        fields = (
            'sum',
            'stages_id',
        )


class TmpCheckOrderForm(forms.Form):
    action = forms.CharField()  # Has to be "checkOrder"
    md5 = forms.CharField()
    shopId = forms.IntegerField()
    invoiceId = forms.IntegerField()

    def clean_action(self):
        action = self.cleaned_data.get('action')

        if action != 'checkOrder':
            raise forms.ValidationError('Wrong action')

        return action

    def clean_shopId(self):
        shopId = self.cleaned_data.get('shopId')

        if shopId != settings.YANDEX_MONEY['shop_id']:
            raise forms.ValidationError('Wrong shop ID')

        return shopId


class TmpPaymentAvisoForm(forms.Form):
    action = forms.CharField()  # Has to be "paymentAviso"
    md5 = forms.CharField()
    shopId = forms.IntegerField()
    invoiceId = forms.IntegerField()
    orderSumAmount = forms.DecimalField()

    def clean_action(self):
        action = self.cleaned_data.get('action')

        if action != 'paymentAviso':
            raise forms.ValidationError('Wrong action')

        return action

    def clean_shopId(self):
        shopId = self.cleaned_data.get('shopId')

        if shopId != settings.YANDEX_MONEY['shop_id']:
            raise forms.ValidationError('Wrong shop ID')

        return shopId
