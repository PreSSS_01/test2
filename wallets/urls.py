from django.conf import urls, settings
from django.views.generic import TemplateView

from .views import PayFromScore, WithDrawCreate, ScoreView

app_name = 'wallets'

urlpatterns = [
    # urls.url(r'^score/(?P<pk>\d+)/$', ScoreDetailView.as_view(), name='score-detail'),
    urls.url(r'^score/(?P<pk>\d+)/$', ScoreView.as_view(), name='score-detail'),
    urls.url(r'^withdraw/create/$', WithDrawCreate.as_view(), name='withdraw-create'),
    urls.url(r'^payfromscore/$', PayFromScore.as_view(), name='payfromscore'),

    urls.url(
        r'^tmp-yamoney-req/$',
        TemplateView.as_view(template_name='tmp_yandex_money_request_example.html'),
        {'YANDEX_MONEY': settings.YANDEX_MONEY},
        name='tmp-yamoney-req',
    ),

    urls.url(
        r'^tmp-yamoney-sim/$',
        TemplateView.as_view(template_name='tmp_yandex_money_responses_sumulation.html'),
        name='tmp-yamoney-sim',
    ),
]
