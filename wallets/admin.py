from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from .models import InvoiceHistory, WithDraw, Transaction, Wallet, PayFromScore


class InvoiceHistoryAdmin(admin.ModelAdmin):
    list_display = ('comment', 'sum', 'user', 'balance',)
    readonly_fields = list(map(lambda obj: obj.name, InvoiceHistory._meta.get_fields()))


class WithDrawAdmin(ImportExportModelAdmin):
    list_display = ('sum', 'created', 'yandex_card', 'user', 'complete',)
    readonly_fields = ('sum', 'created', 'yandex_card', 'user', 'passport', 'phone')


class WalletAdmin(admin.ModelAdmin):
    list_display = ('customer', 'balance', )
    # readonly_fields = Wallet._meta.get_all_field_names()
    readonly_fields = list(map(lambda obj: obj.name, Wallet._meta.get_fields()))


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('customer', 'complete',)
    readonly_fields = list(map(lambda obj: obj.name, Transaction._meta.get_fields()))


class PayFromScoreAdmin(admin.ModelAdmin):
    list_display = ('customer', 'sum', 'created_at')
    readonly_fields = list(map(lambda obj: obj.name, PayFromScore._meta.get_fields()))


admin.site.register(InvoiceHistory, InvoiceHistoryAdmin)
admin.site.register(WithDraw, WithDrawAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Wallet, WalletAdmin)
admin.site.register(PayFromScore, PayFromScoreAdmin)
