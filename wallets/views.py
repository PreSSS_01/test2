import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, CreateView
from django.views.generic.base import View

from projects.models import Stage
from users.models import User
from .forms import WithDrawForm, TmpCheckOrderForm, TmpPaymentAvisoForm, PayFromScoreForm
from .models import InvoiceHistory, WithDraw, Transaction, PayFromScore
from .signals import *


class PayFromScore(CreateView):
    model = PayFromScore
    form_class = PayFromScoreForm

    def form_valid(self, form):
        if self.request.is_ajax():
            # print("self.request.user.get_score()", self.request.user.get_score(), ">= form.cleaned_data.get('sum')", form.cleaned_data.get('sum'))
            if self.request.user.get_score() >= form.cleaned_data.get('sum'):
                # print("stages_id = ", form.cleaned_data.get('stages_id'), type(form.cleaned_data.get('stages_id')))
                stages_pk = [st for st in form.cleaned_data.get('stages_id').split(';') if st]
                # print("stages_pk = ", stages_pk)
                stages = Stage.objects.filter(pk__in=stages_pk)
                st_names = ','.join([st.order.project.name for st in stages]).rstrip(',')
                stage = stages.first()
                # stage.is_paid = True
                # stage.save()
                order = stage.order
                self.object = form.save(commit=False)
                self.object.customer = self.request.user
                self.object.save()
                data = {
                    'pk': self.object.pk,
                    'status': 'ok',
                    'order': order.pk,
                    'sender': self.request.user.pk,
                    'recipent': order.get_contractor_owner(),
                    'stages': st_names,
                }
                return JsonResponse(data)
            else:
                data = {
                    'status': 'error',
                    'message_error': 'У вас недостаточно средств',
                }
                return JsonResponse(data, status=400)
        return super().form_valid(form)

    def form_invalid(self, form):
        if self.request.is_ajax():
            data = {
                'errors': form.errors,
                'status': 'error',
            }
            return JsonResponse(data)
        return super().form_invalid(form)


class ScoreDetailView(DetailView):
    model = User
    template_name = 'score-detail.html'
    context_object_name = 'user_score'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_score_balance = InvoiceHistory.objects.filter(user=self.get_object()).aggregate(Sum('sum'))
        context['user_score_balance'] = user_score_balance['sum__sum'] or 0
        context['form'] = WithDrawForm()
        return context


class ScoreView(LoginRequiredMixin, View):
    template_name = 'score-detail.html'

    def get(self, request, *args, **kwargs):
        transaction, created = Transaction.objects.get_or_create(customer=request.user, type='add', complete=False)
        # transaction = Transaction.objects.filter(customer=request.user, type='add', complete=False)
        user_score = get_object_or_404(User.objects, pk=kwargs.get('pk'))
        current_sum_info = InvoiceHistory.objects.filter(user=user_score, type="score").aggregate(Sum('sum'))
        user_score_balance = current_sum_info['sum__sum'] or 0
        form = WithDrawForm

        return render(request, self.template_name, {
            'transaction': transaction,
            'YANDEX_MONEY': settings.YANDEX_MONEY,
            'user_score': user_score,
            'user_score_balance': user_score_balance,
            'form': form,
        })


class AjaxableResponseMixin(object):
    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # import code; code.interact(local=dict(globals(), **locals()))
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            messages.info(self.request, 'Ваша заявка на вывод средств принята!')
            data = {
                'pk': self.object.pk,
                'status': 'ok',
            }
            return JsonResponse(data)
        else:
            return response


class WithDrawCreate(CreateView):
    model = WithDraw
    form_class = WithDrawForm

    def form_valid(self, form):
        if self.request.is_ajax():
            self.object = form.save()
            messages.info(self.request, 'Ваша заявка на вывод средств принята!')
            data = {
                'pk': self.object.pk,
                'status': 'ok',
            }
            return JsonResponse(data)
        return super().form_valid(form)

    def form_invalid(self, form):
        if self.request.is_ajax():
            data = {
                'errors': form.errors,
                'status': 'error',
            }
            return JsonResponse(data)
        return super().form_invalid(form)


# Yandex Money ------------------------------------------------



class TmpCheckOrderView(View):
    form_class = TmpCheckOrderForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logging.debug('###############################################')
        logging.debug('TmpCheckOrderView')
        logging.debug('###############################################')

        form = self.form_class(request.POST)

        # trans = form.cleaned_data.get('transaction')

        if form.is_valid():
            res = """<?xml version="1.0" encoding="utf-8"?>
                <checkOrderResponse
                    performedDatetime="{date}"
                    code="0"
                    invoiceId="{invoice_id}"
                    shopId="{shop_id}"/>
            """.format(
                date=timezone.now().isoformat(),
                invoice_id=form.cleaned_data.get('invoiceId'),
                shop_id=form.cleaned_data.get('shopId'),
            )

            return HttpResponse(res, content_type='application/xml')
        else:
            res = """<?xml version="1.0" encoding="utf-8"?>
                <checkOrderResponse
                    performedDatetime="{date}"
                    code="1"
                    invoiceId="{invoice_id}"
                    shopId="{shop_id}"
                    message="Check order, validation error"
                    techMessage="Check order, validation error"/>
            """.format(
                date=timezone.now().isoformat(),
                invoice_id=form.cleaned_data.get('invoiceId'),
                shop_id=form.cleaned_data.get('shopId'),
            )

            return HttpResponse(res, content_type='application/xml')
            # return HttpResponse('<pre>{msg}</pre>'.format(msg=pformat(form.errors))) # Debug


class TmpPaymentAvisoView(View):
    form_class = TmpPaymentAvisoForm

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        logging.debug(form.data)
        if form.is_valid():
            transaction_id = form.data.get('transactionId')
            transaction = Transaction.objects.get(pk=int(transaction_id))
            logging.debug(form.cleaned_data)
            transaction.complete = True
            transaction.sum = form.cleaned_data.get('orderSumAmount')
            transaction.stages_id = form.data.get('stagesId')
            transaction.save()

            res = """<?xml version="1.0" encoding="utf-8"?>
                <paymentAvisoResponse
                    performedDatetime="{date}"
                    code="0"
                    invoiceId="{invoice_id}"
                    shopId="{shop_id}"/>
            """.format(
                date=timezone.now().isoformat(),
                invoice_id=form.cleaned_data.get('invoiceId'),
                shop_id=form.cleaned_data.get('shopId'),
            )

            return HttpResponse(res, content_type='application/xml')
        else:
            res = """<?xml version="1.0" encoding="utf-8"?>
                <paymentAvisoResponse
                    performedDatetime="{date}"
                    code="1"
                    message="Payment aviso, validation error"
                    techMessage="Payment aviso, validation error"/>
            """.format(date=timezone.now().isoformat())

            return HttpResponse(res, content_type='application/xml')
            # return HttpResponse('<pre>{msg}</pre>'.format(msg=pformat(form.errors))) # Debug
