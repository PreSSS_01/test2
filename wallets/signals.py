from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import get_template, render_to_string
from django.utils import timezone

from projects.models import Stage
from .models import WithDraw, InvoiceHistory, Transaction, PayFromScore


@receiver(post_save, sender=WithDraw)
def send_for_accountant(sender, instance, created, **kwargs):
    if created:
        ctx_dict = {
            'username': instance.user.username,
        }
        subject, from_email, to = 'Заявка на распечатку', 'mukhtar@mukhtar', 'muhtarzubanchi05@gmail.com'
        text_content = render_to_string('send_for_accountant.txt', ctx_dict)
        html_content = get_template('send_for_accountant.html').render(ctx_dict)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.send()


@receiver(post_save, sender=Transaction)
def add_invoice_history(sender, instance, created, **kwargs):
    if 'add' in instance.type and instance.complete:
        inv_history = InvoiceHistory()
        inv_history.comment = 'Пополнение счета'
        inv_history.sum = instance.sum
        inv_history.user = instance.customer
        inv_history.type = "score"
        inv_history.save()


@receiver(post_save, sender=Stage)
def add_contractor_inv_from_stage(sender, instance, created, **kwargs):
    if instance.close_contractor and instance.close_customer and instance.is_paid:
        inv = InvoiceHistory()
        inv.comment = 'Сумма за завершение этапа ' + instance.name + ' заказа ' + str(instance.order)
        inv.sum = instance.cost
        inv.type = "score"
        if instance.order.contractor:
            inv.user = instance.order.contractor
        else:
            inv.user = instance.order.team.owner
        inv.save()


@receiver(post_save, sender=Transaction)
def reserve_stages(sender, instance, created, **kwargs):
    if 'reservation' in instance.type and instance.complete:
        order = None
        stages_names = []
        stages_ids_raw = instance.stages_id
        if stages_ids_raw:
            stages_ids = [s for s in stages_ids_raw.split(';') if s]
            for pk in stages_ids:
                stage = Stage.objects.get(pk=pk)
                stages_names.append(stage.name)
                stage.is_paid = True
                delta = stage.term - stage.approve_time.date()
                stage.term = timezone.now().date() + delta
                order = stage.order
                stage.save()

        inv_history = InvoiceHistory()
        inv_history.comment = 'Резервирование средств за этап(ы) ' + ' , '.join(stages_names) + ' заказа ' + str(order)
        inv_history.sum = instance.sum
        inv_history.user = instance.customer
        inv_history.type = "history"
        inv_history.save()


@receiver(post_save, sender=PayFromScore)
def reserve_stages_from_score(sender, instance, created, **kwargs):
    print("PayFromScope post save!")
    order = None
    stages_names = []
    stages_ids_raw = instance.stages_id
    if stages_ids_raw:
        stages_ids = [s for s in stages_ids_raw.split(';') if s]
        for pk in stages_ids:
            stage = Stage.objects.get(pk=pk)
            stages_names.append(stage.name)
            stage.is_paid = True
            stage.status = 'in_process'
            delta = stage.term - stage.approve_time.date()
            stage.term = timezone.now().date() + delta
            order = stage.order
            stage.save()

    inv_history = InvoiceHistory()
    inv_history.comment = 'Резервирование средств за этап(ы) ' + ' , '.join(stages_names) + ' заказа ' + str(order)
    inv_history.sum = -instance.sum
    inv_history.user = instance.customer
    inv_history.type = "score"
    inv_history.save()
