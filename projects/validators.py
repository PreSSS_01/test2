from django.core.exceptions import ValidationError


def validate_term(date):
    if date.weekday() != 0:
        raise ValidationError("Это не ваш день")
