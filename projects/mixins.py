from django.utils import timezone


class LastAccessMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            request.user.last_time_visit = timezone.now()
            request.user.save(update_fields=['last_time_visit'])
        return super().dispatch(request, *args, **kwargs)
