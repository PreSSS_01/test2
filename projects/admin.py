from django import forms
from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import (
    Answer,
    Arbitration,
    BuildingClassfication,
    Candidate,
    ConstructionType,
    Order,
    Portfolio,
    PortfolioPhoto,
    Project,
    ProjectFile,
    ProjectWorkTypeSuggestion,
    Realty,
    Stage,
)


class BuildingClassficationAdmin(MPTTModelAdmin):
    readonly_fields = ('pk', 'lft', 'rght', 'tree_id', 'level')
    list_display = ('pk', 'name', 'order', 'parent')


class ConstructionTypeAdmin(admin.ModelAdmin):
    # readonly_fields = ('pk', 'lft', 'rght', 'tree_id', 'level')
    list_display = ('pk', 'name', 'order')


class ProjectAdminForm(forms.ModelForm):
    files = forms.ModelMultipleChoiceField(queryset=ProjectFile.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance.pk:
            self.fields['files'].queryset = self.instance.files


class ProjectAdmin(admin.ModelAdmin):
    readonly_fields = ('pk',)
    list_display = ('name', 'pk', 'customer', 'state')
    form = ProjectAdminForm


class OrderAdmin(admin.ModelAdmin):
    list_display = ('project', 'status', 'secure',)


class StageAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'pos', 'order', 'is_paid',)


admin.site.register(Answer)
admin.site.register(Arbitration)
admin.site.register(BuildingClassfication, BuildingClassficationAdmin)
admin.site.register(Candidate)
admin.site.register(ConstructionType, ConstructionTypeAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Portfolio)
admin.site.register(PortfolioPhoto)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectFile)
admin.site.register(ProjectWorkTypeSuggestion)
admin.site.register(Realty)
admin.site.register(Stage, StageAdmin)
