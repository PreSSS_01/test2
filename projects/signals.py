from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from .models import Stage, Order


@receiver(post_save, sender=Stage)
def set_approve_time(sender, instance, created, **kwargs):
    if instance.status == 'agreed' and not instance.approve_time:
        instance.approve_time = timezone.now()
        instance.save()
