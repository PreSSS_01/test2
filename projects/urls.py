from django.conf import urls

from .views import (
    # ContractorOfferOrder,
    # TeamOfferOrder,
    add_candidate,
    ArbitrationCreateView,
    CandidateDeleteView,
    # contractor_portfolio_create,
    ContractorAnswerArchiveView,
    ContractorPortfolioCreateView,
    ContractorPortfolioTrashView,
    ContractorPortfolioEditView,
    CustomerOfferOrderView,
    CustomerProjectCreateView,
    CustomerProjectDeleteView,
    CustomerProjectEditView,
    CustomerProjectRestoreView,
    CustomerProjectTrashView,
    PortfolioDetail,
    ProjectAnswerCreateMessageView,
    ProjectComparisonView,
    ProjectDetailWithAnswerView,
    ProjectFilterView,
    ProjectWorkTypeSuggestionView,
    RejectProjectAnswerView,
    RestoreProjectAnswerView,
    sort_candidates,
)

app_name = 'projects'

urlpatterns = [
    urls.url(r'^$', ProjectFilterView.as_view(), name='project-filter'),

    # urls.url(r'^create/$', CustomerProjectCreateView.as_view(), name='customer-project-create'),
    urls.url(r'^create$', CustomerProjectCreateView.as_view(), name='customer-project-create'),
    urls.url(r'^(?P<pk>\d+)/$', ProjectDetailWithAnswerView.as_view(), name='detail'),
    urls.url(r'^(?P<pk>\d+)/edit/$', CustomerProjectEditView.as_view(), name='customer-project-edit'),
    urls.url(r'^(?P<pk>\d+)/trash/$', CustomerProjectTrashView.as_view(), name='customer-project-trash'),
    urls.url(r'^(?P<pk>\d+)/restore/$', CustomerProjectRestoreView.as_view(), name='customer-project-restore'),
    urls.url(r'^(?P<pk>\d+)/delete/$', CustomerProjectDeleteView.as_view(), name='customer-project-delete'),

    urls.url(r'^create-answer-message/(?P<pk>\d+)/$', ProjectAnswerCreateMessageView.as_view(),
             name='create-answer-message'),
    urls.url(r'^reject-project-answer/(?P<pk>\d+)/$', RejectProjectAnswerView.as_view(), name='reject-project-answer'),
    urls.url(r'^restore-project-answer/(?P<pk>\d+)/$', RestoreProjectAnswerView.as_view(),
             name='restore-project-answer'),

    urls.url(r'^arbitration/create/$', ArbitrationCreateView.as_view(), name='arbitration-create'),
    urls.url(r'^answer/move/archive/$', ContractorAnswerArchiveView.as_view(), name='contractor-answer-archive'),

    urls.url(r'^portfolio/create/$', ContractorPortfolioCreateView.as_view(), name='contractor-portfolio-create'),
    urls.url(r'^portfolio/(?P<pk>\d+)/$', PortfolioDetail.as_view(), name='contractor-portfolio-detail'),
    urls.url(r'^portfolio/(?P<pk>\d+)/edit/$', ContractorPortfolioEditView.as_view(),
             name='contractor-portfolio-edit'),
    urls.url(r'^portfolio/(?P<pk>\d+)/trash/$', ContractorPortfolioTrashView.as_view(),
             name='contractor-portfolio-trash'),

    urls.url(r'^candidate/add/(?P<answer_id>(\d+))/(?P<project_id>(\d+))/$', add_candidate, name='add-candidate'),
    urls.url(r'^candidate/delete/(?P<pk>(\d+))/$', CandidateDeleteView.as_view(), name='delete-candidate'),
    urls.url(r'^candidate/comparison/sort/$', sort_candidates, name='comparison-sort'),
    urls.url(r'^candidate/comparison/(?P<pk>\d+)/$', ProjectComparisonView.as_view(), name='comparison'),

    urls.url(r'^customer-offer-order/(?P<answer_id>(\d+))/(?P<project_id>(\d+))/$', CustomerOfferOrderView.as_view(),
             name='customer-offer-order'),

    urls.url(r'^suggest-work-type/$', ProjectWorkTypeSuggestionView.as_view(), name='suggest-work-type'),
]
