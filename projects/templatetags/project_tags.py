import pydash as _;
from django import template
from django.utils.safestring import mark_safe

_.map = _.map_;
_.filter = _.filter_

register = template.Library()


@register.filter
def get_candidates(project):
    return tuple(c.answer.author for c in project.candidates.all())


@register.filter
def get_new_answers(project):
    return set(project.answers.filter(rejected=False)) - set(
        c.answer for c in project.candidates.filter(answer__rejected=False))


@register.filter
def get_candidate_answers(project):
    return tuple(c.answer for c in project.candidates.filter(answer__rejected=False))


@register.filter
def get_rejected_answers(project):
    return project.answers.filter(rejected=True)


@register.filter
def get_answer(project, contractor):
    answer = _.find(project.answers.all(), lambda a: a.author == contractor)

    if not answer:
        answer = _.find(project.answers.all(), lambda a: a.author == contractor.team)

    return answer

# import code; code.interact(local=dict(globals(), **locals()))


@register.filter
def thousand_separator(number):
    return mark_safe('{0:,}'.format(number).replace(',', '&thinsp;'))