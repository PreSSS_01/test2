from rest_framework_filters import FilterSet, RelatedFilter, AllLookupsFilter

from .models import (
    Answer,
    BuildingClassfication,
    ConstructionType,
    Order,
    Portfolio,
    PortfolioPhoto,
    Project,
    Realty,
    Stage,
)


class BuildingClassficationFilterSet(FilterSet):
    id = AllLookupsFilter()
    name = AllLookupsFilter()

    class Meta:
        model = BuildingClassfication


class ConstructionTypeFilterSet(FilterSet):
    id = AllLookupsFilter()
    name = AllLookupsFilter()

    class Meta:
        model = ConstructionType


class ProjectFilterSet(FilterSet):
    budget = AllLookupsFilter()
    budget_by_agreement = AllLookupsFilter()
    created = AllLookupsFilter()
    cro = AllLookupsFilter()
    currency = AllLookupsFilter()
    deal_type = AllLookupsFilter()
    id = AllLookupsFilter()
    name = AllLookupsFilter()
    price_and_term_required = AllLookupsFilter()
    state = AllLookupsFilter()
    term = AllLookupsFilter()
    term_type = AllLookupsFilter()
    text = AllLookupsFilter()
    work_type = AllLookupsFilter()

    answers = RelatedFilter('projects.filters.AnswerFilterSet')
    customer = RelatedFilter('users.filters.UserFilterSet')
    order = RelatedFilter('projects.filters.OrderFilterSet')
    realty = RelatedFilter('projects.filters.RealtyFilterSet')
    specialization = RelatedFilter('specializations.filters.SpecializationFilterSet')

    class Meta:
        model = Project


class AnswerFilterSet(FilterSet):
    budget = AllLookupsFilter()
    created = AllLookupsFilter()
    currency = AllLookupsFilter()
    id = AllLookupsFilter()
    is_archive = AllLookupsFilter()
    object_id = AllLookupsFilter()
    rejected = AllLookupsFilter()
    secure_deal_only = AllLookupsFilter()
    term = AllLookupsFilter()
    term_type = AllLookupsFilter()

    # author = ... # ???

    # messages = RelatedFilter('...')
    content_type = RelatedFilter('common.filters.ContentTypeFilterSet')
    contractors = RelatedFilter('users.filters.UserFilterSet')
    portfolios = RelatedFilter('projects.filters.PortfolioFilterSet')
    project = RelatedFilter('projects.filters.ProjectFilterSet')
    teams = RelatedFilter('users.filters.TeamFilterSet')

    class Meta:
        model = Answer


class OrderFilterSet(FilterSet):
    created = AllLookupsFilter()
    id = AllLookupsFilter()
    secure = AllLookupsFilter()
    status = AllLookupsFilter()

    contractor = RelatedFilter('users.filters.UserFilterSet')
    project = RelatedFilter('projects.filters.ProjectFilterSet')
    team = RelatedFilter('users.filters.TeamFilterSet')

    class Meta:
        model = Order


class StageFilterSet(FilterSet):
    cost = AllLookupsFilter()
    cost_type = AllLookupsFilter()
    id = AllLookupsFilter()
    is_paid = AllLookupsFilter()
    name = AllLookupsFilter()
    pos = AllLookupsFilter()
    result = AllLookupsFilter()
    status = AllLookupsFilter()
    term = AllLookupsFilter()
    term_type = AllLookupsFilter()

    order = RelatedFilter('projects.filters.OrderFilterSet')

    class Meta:
        model = Stage


class RealtyFilterSet(FilterSet):
    id = AllLookupsFilter()
    name = AllLookupsFilter()

    building_classification = RelatedFilter('projects.filters.BuildingClassficationFilterSet')
    construction_type = RelatedFilter('projects.filters.ConstructionTypeFilterSet')
    location = RelatedFilter('common.filters.LocationFilterSet')
    user = RelatedFilter('users.filters.UserFilterSet')

    class Meta:
        model = Realty


class PortfolioPhotoFilterSet(FilterSet):
    id = AllLookupsFilter()

    portfolio = RelatedFilter('projects.filters.PortfolioFilterSet')

    # img = ???

    class Meta:
        model = PortfolioPhoto


class PortfolioFilterSet(FilterSet):
    budget = AllLookupsFilter()
    currency = AllLookupsFilter()
    id = AllLookupsFilter()
    name = AllLookupsFilter()
    term = AllLookupsFilter()
    term_type = AllLookupsFilter()

    photos = RelatedFilter('projects.filters.PortfolioPhotoFilterSet')
    user = RelatedFilter('users.filters.UserFilterSet')

    class Meta:
        model = Portfolio
