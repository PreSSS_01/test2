from django import template

from projects.models import Order
from ratings.models import SpecializationRating
from reviews.models import Review
from users.models import User, Team

register = template.Library()


@register.inclusion_tag('templatetags/specializations_widget.html', takes_context=True)
def specialization_widget(context, user_id, class_name=None):
    user_id = int(user_id)
    specializations = SpecializationRating.objects.select_related('specialization').filter(user_id=user_id)
    return {
        'specializations': specializations,
        'user_id': user_id,
    }


@register.inclusion_tag('templatetags/specializations_widget.html', takes_context=True)
def specialization_team_widget(context, team_id):
    team_id = int(team_id)
    specializations = SpecializationRating.objects.select_related('specialization').filter(team_id=team_id)
    return {
        'specializations': specializations,
    }


@register.inclusion_tag("templatetags/ratings_widget.html", takes_context=True)
def ratings_widget(context, user_id, class_name=None):
    ratings = User.objects.get(pk=user_id).rating
    return {
        'ratings': ratings,
        'class_name': class_name,
        'deals': Order.objects.filter(secure=True, contractor_id=user_id, status=1).count(),
        'reviews_n': Review.objects.filter(target_contractor_id=user_id, type='neutral').count(),
        'reviews_m': Review.objects.filter(target_contractor_id=user_id, type='negative').count(),
        'reviews_p': Review.objects.filter(target_contractor_id=user_id, type='positive').count(),
    }


@register.inclusion_tag("templatetags/ratings_widget.html", takes_context=True)
def ratings_team_widget(context, team_id, class_name=None):
    ratings = Team.objects.get(pk=team_id).rating
    return {
        'ratings': ratings,
        'class_name': class_name,
        'deals': Order.objects.filter(secure=True, team_id=team_id, status=1).count(),
        'reviews_n': Review.objects.filter(target_team_id=team_id, type='neutral').count(),
        'reviews_m': Review.objects.filter(target_team_id=team_id, type='negative').count(),
        'reviews_p': Review.objects.filter(target_team_id=team_id, type='positive').count(),
    }
