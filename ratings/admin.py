from django.contrib import admin

from .models import SpecializationRating, HistoryRating

admin.site.register(SpecializationRating)
admin.site.register(HistoryRating)
