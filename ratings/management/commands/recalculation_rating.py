from django.core.management import BaseCommand
from django.db.models import Sum
from specializations.models import Specialization
from ratings.models import HistoryRating
from users.models import User,Team


class Command(BaseCommand):

    def handle(self, *args, **options):
        users = User.objects.filter(is_superuser=False)
        for user in users:
            current_rating_info = HistoryRating.objects.filter(user_id=user.pk).aggregate(Sum('rating'))
            current_rating = current_rating_info['rating__sum'] or 0
            user.rating = current_rating
            user.save()

        teams = Team.objects.all()
        for team in teams:
            current_rating_info = HistoryRating.objects.filter(team_id=team.pk).aggregate(Sum('rating'))
            current_rating = current_rating_info['rating__sum'] or 0
            team.rating = current_rating
            team.save()
