from django.core.management import BaseCommand
from specializations.models import Specialization
from ratings.models import SpecializationRating
from users.models import User, Team


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = User.objects.values('pk', 'rating').filter(is_superuser=False).order_by('-rating')
        teams = Team.objects.values('pk', 'rating').order_by('-rating')
        result_list = []

        for user in users:
            result_list.append([user['rating'], 'user', user['pk']])

        for team in teams:
            result_list.append([team['rating'], 'team', team['pk'] ])

        result_list = list(reversed(sorted(result_list)))
        SpecializationRating.objects.all().delete()
        specializations = Specialization.objects.all()
        for spec in specializations:
            i = 0
            for res in result_list:
                if 'user' in res[1]:
                    user = User.objects.get(pk=res[2])
                    team = None
                    specializations_current = user.contractor_specializations.all()
                else:
                    team = Team.objects.get(pk=res[2])
                    user = None
                    specializations_current = team.specializations.all()

                if spec in specializations_current:
                    i += 1
                    spec_rating = SpecializationRating()
                    spec_rating.position = i
                    spec_rating.user = user
                    spec_rating.team = team
                    spec_rating.specialization = spec
                    spec_rating.save()
        print('The end')
