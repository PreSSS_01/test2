from django.db import models
from django.utils import timezone

from specializations.models import Specialization
from users.models import User, Team


class HistoryRating(models.Model):
    TYPES_HISTORY_RATING = (
        ('OCCUPANCY_PROFILE', 'occupancy_profile'),
        ('SECURE_DEAL', 'secure_deal'),
        ('REVIEW', 'review'),
        ('MONEY_SPENT', 'money_spent'),
        ('PUBLICATION_PROJECT', 'publication_project'),
        ('VISIT_SITE', 'visit_site'),
        ('CHOICE_CONTRACTOR', 'choice_contractor'),
    )
    user = models.ForeignKey(User, related_name='history_ratings', null=True, blank=True)
    team = models.ForeignKey(Team, related_name='history_ratings', null=True, blank=True)
    rating = models.IntegerField(default=0)
    created = models.DateTimeField(default=timezone.now)
    description = models.TextField(blank=True)
    type = models.CharField(max_length=50, choices=TYPES_HISTORY_RATING, default='review')

    def __str__(self):
        return '{0}'.format(self.rating)

    class Meta:
        verbose_name = 'История рейтинга'
        verbose_name_plural = 'Истории рейтинга'


class SpecializationRating(models.Model):
    user = models.ForeignKey(User, related_name='specialization_rating', null=True, blank=True)
    team = models.ForeignKey(Team, related_name='specialization_rating', null=True, blank=True)
    specialization = models.ForeignKey(Specialization,
                                       related_name='specialization_rating')  # TODO: Pluralize related name
    position = models.PositiveIntegerField(default=0)

    def __str__(self):
        return '{0}'.format(self.pk)

    class Meta:
        verbose_name = 'Рейтинг специализаций'
        verbose_name_plural = 'Рейтинги специализаций'
