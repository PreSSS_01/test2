import pydash as _;
from rest_framework_filters import FilterSet, RelatedFilter, AllLookupsFilter, MethodFilter

_.map = _.map_;
_.filter = _.filter_

from .models import User, Team, ContractorResumeFiles, ContractorResume


class UserFilterSet(FilterSet):
    email = AllLookupsFilter()
    first_name = AllLookupsFilter()
    id = AllLookupsFilter()
    is_active = AllLookupsFilter()
    is_admin = AllLookupsFilter()
    is_contractor = MethodFilter()
    is_customer = MethodFilter()
    last_name = AllLookupsFilter()
    username = AllLookupsFilter()

    projects = RelatedFilter('projects.filters.ProjectFilterSet')

    class Meta:
        model = User

    def filter_is_contractor(self, name, qs, value):
        if value == 'true':
            # return _.filter(qs, lambda user: user.is_contractor())
            return qs.filter(groups__name='Исполнители')

        return qs

    def filter_is_customer(self, name, qs, value):
        if value == 'true':
            # return _.filter(qs, lambda user: user.is_customer())
            return qs.filter(groups__name='Заказчики')

        return qs


class TeamFilterSet(FilterSet):
    id = AllLookupsFilter()
    name = AllLookupsFilter()

    owner = RelatedFilter('users.filters.UserFilterSet')

    class Meta:
        model = Team


class ContractorResumeFilterSet(FilterSet):
    id = AllLookupsFilter()
    resume_file = AllLookupsFilter()
    text = AllLookupsFilter()

    class Meta:
        model = ContractorResume


class ContractorResumeFilesFilterSet(FilterSet):
    id = AllLookupsFilter()
    img = AllLookupsFilter()
    title = AllLookupsFilter()

    resume = RelatedFilter('users.filters.ContractorResumeFilterSet')

    class Meta:
        model = ContractorResumeFiles
