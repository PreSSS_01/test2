import itertools
from archilance.settings import dev as settings

import pydash as _;
from django import forms
from mptt.forms import TreeNodeChoiceField

_.map = _.map_;
_.filter = _.filter_

from .models import User, UserFinancialInfo, Team, ContractorResume, ContractorResumeFiles, GENDERS
from common.models import Location, LiveImageUpload
from projects.models import Project, BuildingClassfication, ConstructionType
from specializations.models import Specialization


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team

        fields = (
            'name',
            'owner',
        )


class ContractorResumeForm(forms.ModelForm):
    class Meta:
        model = ContractorResume

        fields = (
            'text',
        )


class ContractorResumeFilesForm(forms.ModelForm):
    class Meta:
        model = ContractorResumeFiles

        fields = (
            'type',
            'resume',
        )


class UserProfileEditFullForm(forms.ModelForm):
    gender = forms.ChoiceField(
        choices=GENDERS,
        widget=forms.RadioSelect,
        required=False,
    )
    contractor_status = forms.ChoiceField(
        choices=User.STATUSES,
        widget=forms.RadioSelect,
        required=False,
    )

    live_image = forms.ModelChoiceField(
        queryset=LiveImageUpload.objects.all(),
        required=False,
    )

    class Meta:
        model = User

        fields = (
            'id',
            'avatar',
            'first_name',
            'last_name',
            'patronym',
            'location',
            'organization_name',
            'contractor_specializations',  # Специализации
            'contractor_building_classifications',  # Классификация зданий
            'contractor_construction_types',  # Вид строительства
            'cro',
            # Общая информация
            'date_of_birth',
            'gender',
            'contractor_status',
            'skype',
            'website',
            'phone',
            'phone2',
            # Финансовая информация
            # ...
        )

        widgets = {
            # TODO: Use common format with jQueryUI Datepicker:
            'date_of_birth': forms.DateInput(attrs={'class': 'datepicker box-sizing simple-input'}),
            'contractor_status': forms.Select(attrs={'class': 'simple-select'})
        }

    def __init__(self, *args, **kwargs):
        if kwargs.get("request"): self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

    # def clean_location(self):
    #     data = self.cleaned_data['location']
    #     return data

    def clean_contractor_specializations(self):
        data = self.cleaned_data['contractor_specializations']
        return data

    def get_gender_display(self):
        for gender in GENDERS:
            if gender[0] == self.initial['gender']:
                return gender[1]

    def get_status_display(self):
        for status in User.STATUSES:
            if status[0] == self.initial['contractor_status']:
                return status[1]


class UserProfileEditForm(forms.ModelForm):
    gender = forms.ChoiceField(
        choices=GENDERS,
        widget=forms.RadioSelect,
        required=False,
    )

    live_image = forms.ModelChoiceField(
        queryset=LiveImageUpload.objects.all(),
        required=False,
    )

    class Meta:
        model = User

        fields = (
            'contractor_specializations',
            'contractor_status',
            'cro',
            'date_of_birth',
            'first_name',
            'gender',
            'last_name',
            'location',
            'patronym',
            'phone',
            'phone2',
            'skype',
            'website',
        )

        widgets = {
            # TODO: Use common format with jQueryUI Datepicker:
            'date_of_birth': forms.TextInput(attrs={'class': 'datepicker box-sizing simple-input'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

        attrs = self.fields['contractor_status'].widget.attrs
        attrs['class'] = _.join(_.compact((attrs.get('class'), 'box-sizing surr surr2')), ' ')


class UserProfileBasicInfoEditForm(forms.ModelForm):
    live_image = forms.ModelChoiceField(
        queryset=LiveImageUpload.objects.all(),
        required=False,
    )

    class Meta:
        model = User

        fields = (
            'contractor_specializations',
            'first_name',
            'last_name',
            'location',
            'patronym',
        )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)


class UserProfileExperienceEditForm(UserProfileBasicInfoEditForm):
    class Meta:
        model = User

        fields = (
            'contractor_specializations',
            'first_name',
            'last_name',
            'location',
            'patronym',
            'contractor_building_classifications',
            'contractor_construction_types',
        )


class UserFinancialInfoEditForm(forms.ModelForm):
    class Meta:
        model = UserFinancialInfo

        fields = (
            'residency',
            'legal_status',

            'fio',
            'date_of_birth',
            'phone',

            'postal_address',
            'registration_address',

            'credit_card_number',
            'inn',

            'passport_issue_date',
            'passport_issued_by',
            'passport_number',
            'passport_series',
            'subdivision_code',

            'yandex_money',
            'web_money',

            # Юридические данные
            'correspondent_account',
            'bank_bic',
            'bank_inn',
            'checking_account',
            'bank_name',
            'authorized_bank_name',
            'ppc',
            'psrn',

            'organization_name',
            'organization_registered_address',
            'organization_postal_address',
            'organization_reg_number'

        )

        widgets = {
            'date_of_birth': forms.DateInput(attrs={'class': 'datepicker box-sizing simple-input'}),
            'passport_issue_date': forms.DateInput(attrs={'class': 'datepicker box-sizing simple-input'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

        # self.fields['residency'].choices = self.fields['residency'].choices[1:]
        # self.fields['legal_status'].choices = self.fields['legal_status'].choices[1:]

        # self.fields['residency'].empty_label = None
        # self.fields['residency'].widget.choices = self.fields['residency'].choices


class ContractorFilterForm(forms.Form):
    CONTRACTOR_ORDER_CHOICES = (
        ('', 'Сортировать по...'),
        ('name', 'Названию'),
        ('created', 'Дате размещения'),
        ('views', 'Просмотрам'),
        ('reviews', 'Отзывам'),
        ('rating', 'Рейтингу'),
        # ('project_number', 'Кол-ву выполн. проектов'),
    )

    PARTY_TYPES = (
        ('all', 'Все'),
        ('teams', 'Группы'),
        ('contractors', 'Исполнители'),
    )

    order_by = forms.ChoiceField(required=False, choices=CONTRACTOR_ORDER_CHOICES)
    last_order_by = forms.ChoiceField(required=False, choices=CONTRACTOR_ORDER_CHOICES)
    reverse_order = forms.BooleanField(required=False)

    try:  # TODO: dirty
        qs = Specialization.objects.root_nodes()[0].get_descendants()
    except:
        qs = Specialization.objects

    specialization = forms.ModelChoiceField(
        queryset=qs,
        required=False,
    )

    try:  # TODO: dirty
        qs = Location.objects.root_nodes()[0].get_descendants()
    except:
        qs = Location.objects

    location = forms.ModelChoiceField(
        queryset=qs,
        required=False,
    )

    # building_classification = forms.ModelChoiceField(
    #     queryset=BuildingClassfication.objects,
    #     widget=forms.Select(attrs={'class': 'selectpicker'}),
    #     empty_label='',
    # )

    building_classification = TreeNodeChoiceField(
        BuildingClassfication.objects.exclude(name='_root'),
        empty_label='',
        widget=forms.Select(attrs={
            'class': 'selectpicker',
        }),
        required=False,
        level_indicator='  ',
    )

    work_type = forms.ChoiceField(
        choices=tuple(itertools.chain((('', ''),), Project.WORK_TYPES)),
        widget=forms.Select(attrs={'class': 'selectpicker -project-work-type-select-field'}),
        required=False,
    )

    construction_type = forms.ModelChoiceField(
        queryset=ConstructionType.objects,
        widget=forms.Select(attrs={'class': 'selectpicker'}),
        required=False,
        empty_label='',
    )

    cro = forms.BooleanField(required=False)

    party_types = forms.ChoiceField(
        choices=PARTY_TYPES,
        required=False,
    )

    last_party_types = forms.ChoiceField(
        choices=PARTY_TYPES,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)


class CustomerProfileForm(forms.ModelForm):
    class Meta:
        model = User

        fields = (
            'username',
            'first_name',
            'created',
            'avatar',
            'rating'
        )


class CustomerProfileProjectRealtyForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.customer = kwargs.pop('customer')

        super().__init__(*args, **kwargs)

        realties = _.uniq(tuple(p.realty for p in self.customer.customer_projects.all()))

        self.fields['realty'] = forms.ChoiceField(
            widget=forms.Select(attrs={
                'class': 'selectpicker',
                'onchange': "$(this).closest('form').submit()",
            }),

            choices=(('', 'Все объекты'),) + tuple((r.pk, r.name) for r in realties),
            required=False,
        )

# import code; code.interact(local=dict(globals(), **locals()))
