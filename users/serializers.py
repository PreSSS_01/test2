from rest_framework.serializers import ModelSerializer, SerializerMethodField, ReadOnlyField

from specializations.serializers import SpecializationSerializer
from .models import User, Team, ContractorResumeFiles, ContractorResume


class ContractorResumeSerializer(ModelSerializer):
    class Meta:
        model = ContractorResume

        fields = (
            'id',
            'resume_file',
            'text',
        )


class ContractorResumeFilesSerializer(ModelSerializer):
    class Meta:
        model = ContractorResumeFiles

        fields = (
            'id',
            'description',
            'img',
            'resume',
            'title',
            'type',
            'resume',
        )


class UserSerializer(ModelSerializer):
    _type = SerializerMethodField()  # Distinguish when used with generic serializers
    id = ReadOnlyField()
    fio = SerializerMethodField()
    score = SerializerMethodField()

    class Meta:
        model = User

        fields = (
            '_type',
            'avatar',
            'contractor_specializations',
            'contractor_status',
            'created',
            'cro',
            'date_of_birth',
            'email',
            'financial_info',
            'fio',
            'first_name',
            'gender',
            'get_full_name',
            'has_team',
            'id',
            'is_active',
            'is_contractor',
            'is_customer',
            'last_name',
            'last_time_visit',
            'location',
            'patronym',
            'phone',
            'score',
            'skype',
            'username',
            'website',
        )

    def get__type(self, obj):
        return 'User'

    def get_fio(self, obj):
        return obj.get_full_name()

    def get_score(self, obj):
        return obj.get_score()

        # def create(self, validated_data):
        #     return User.objects.create(**validated_data)

        # def update(self, inst, validated_data):
        #     inst.email = validated_data.get('email', inst.email)
        #     inst.first_name = validated_data.get('first_name', inst.first_name)
        #     inst.is_active = validated_data.get('is_active', inst.is_active)
        #     inst.last_name = validated_data.get('last_name', inst.last_name)
        #     # inst.projects = validated_data.get('projects', inst.projects)
        #
        #     inst.save()
        #
        #     return inst


class TeamSerializer(ModelSerializer):
    _type = SerializerMethodField()  # Distinguish when used with generic serializers
    # answers = AnswerSerializer(many=True)
    contractors = UserSerializer(many=True)
    owner = UserSerializer()
    specializations = SpecializationSerializer(many=True)

    class Meta:
        model = Team

        fields = (
            '_type',
            # 'answers',
            'contractors',
            'created',
            'id',
            'name',
            'owner',
            'specializations',
        )

    def get__type(self, obj):
        return 'Team'

# import code; code.interact(local=dict(globals(), **locals()))
