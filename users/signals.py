import math

from django.db.models.signals import post_save
from django.dispatch import receiver
from registration.signals import user_activated

from ratings.models import HistoryRating
from users.models import User

from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.conf import settings
from django.template.loader import render_to_string


@receiver(post_save, sender=User)
def add_rating_for_user_fields(sender, instance, created, **kwargs):
    hs_rating = HistoryRating.objects.get_or_create(user=instance, type='occupancy_profile')
    fields = ['avatar', 'cro', 'first_name', 'gender',
              'last_name', 'patronym', 'phone',
              'skype', 'website', 'location']

    indicator_sum = 0
    for f in fields:
        if hasattr(instance, f) and getattr(instance, f):
            indicator_sum += 1

    rating = math.ceil(math.ceil(100 / len(fields)) * indicator_sum)
    hs_rating[0].rating = rating
    hs_rating[0].description = 'Заполненные поля в профиле. Баллы за рейтинг'
    hs_rating[0].save()


@receiver(user_activated)
def login_on_activation(sender, user, request, **kwargs):
    # Send email
    from_email = getattr(settings, 'REGISTRATION_DEFAULT_FROM_EMAIL',
                         settings.DEFAULT_FROM_EMAIL)
    message_html = render_to_string('after_activation_email.html',
                                   {"login": user.username, "password": user.raw_password})
    message_txt = render_to_string('after_activation_email.txt',
                                   {"login": user.username, "password": user.raw_password})

    email_message = EmailMultiAlternatives("Activation complete", message_txt, from_email, [user.email])
    email_message.attach_alternative(message_html, 'text/html')
    email_message.send()

# def user_registered_callback(sender, user, request, **kwargs):
#     # import code; code.interact(local=dict(globals(), **locals()))
#     group_name = request.POST['group_id'] or 'Исполнители'
#     g = Group.objects.get(name=group_name)
#     g.user_set.add(user)
#     if group_name == 'Исполнители':
#         resume = ContractorResume.objects.create(text='')
#         user.contractor_resume = resume
#         user.save()
