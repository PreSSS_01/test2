from django.core.exceptions import PermissionDenied


class CheckForUserMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            pk = kwargs.get('pk')
            if pk:
                if request.user.pk != int(pk):
                    raise PermissionDenied
        else:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class OwnershipMixin(object):
    def dispatch(self, request, *args, **kwargs):
        pass
