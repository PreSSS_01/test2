from django.conf import urls

from .views import (
    AcceptTeamInvitation,
    RefuseTeamInvitation,
    contractor_resumefile_create,
    ContractorChatProjectsView,
    ContractorFilterView,
    ContractorOfficeProjectsView,
    ContractorOfficeView,
    ContractorProfileView,
    ContractorResumeUpdateView,
    CreateTeamInvitation,
    CustomerProfileView,
    CustomerProfileCurrentProjectsView,
    CustomerProfileReviewsView,
    CustomerProfileTrashedProjectsView,
    TeamCreateView,
    TeamProfileView,
    UserProfileEditViewFull,
    dashboard_redirect
)

app_name = 'users'

urlpatterns = [
    urls.url(r'^(?P<pk>\d+)/edit/$', UserProfileEditViewFull.as_view(), name='user-profile-edit'),
    urls.url(r'^dashboard_redirect/$', dashboard_redirect, name='dashboard-redirect'),
    urls.url(r'^customers/(?P<pk>\d+)/$', CustomerProfileView.as_view(),
             name='customer-profile'),
    urls.url(r'^customers/(?P<pk>\d+)/trashed-projects/$', CustomerProfileTrashedProjectsView.as_view(),
             name='customer-profile-trashed-projects'),
    urls.url(r'^customers/(?P<pk>\d+)/current-projects/$', CustomerProfileCurrentProjectsView.as_view(),
             name='customer-profile-current-projects'),
    urls.url(r'^customers/(?P<pk>\d+)/reviews/$', CustomerProfileReviewsView.as_view(),
             name='customer-profile-reviews'),

    urls.url(r'^contractors/$', ContractorFilterView.as_view(), name='contractor-filter'),
    urls.url(r'^contractors/resume/(?P<pk>\d+)/edit/$', ContractorResumeUpdateView.as_view(),
             name='contractor-resume-update'),
    urls.url(r'^contractors/team/create/$', TeamCreateView.as_view(), name='team-create'),
    urls.url(r'^contractorsresumefiles/create/$', contractor_resumefile_create, name='contractor-resume-file-create'),
    urls.url(r'^contractors/(?P<pk>\d+)/$', ContractorProfileView.as_view(), name='contractor-profile'),
    urls.url(r'^contractor-office/$', ContractorOfficeView.as_view(), name='contractor-office'),
    urls.url(r'^contractor-office/open-projects/$', ContractorOfficeProjectsView.as_view(),
             name='contractor-office-open-projects'),
    urls.url(r'^contractor-office/work-projects/$', ContractorChatProjectsView.as_view(),
             name='contractor-office-chat-projects'),

    urls.url(r'^teams/(?P<pk>\d+)/$', TeamProfileView.as_view(), name='team-profile'),

    urls.url(r'^create-team-invitation/(?P<contractor_id>\d+)/$', CreateTeamInvitation.as_view(),
             name='create-team-invitation'),
    urls.url(r'^accept-team-invitation/(?P<owner_id>\d+)/$', AcceptTeamInvitation.as_view(),
             name='accept-team-invitation'),
    urls.url(r'^refuse-team-invitation/(?P<owner_id>\d+)/$', RefuseTeamInvitation.as_view(),
             name='refuse-team-invitation'),
]
