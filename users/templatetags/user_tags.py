from chat.models import NewMessage
from django import template
from django.core.paginator import Page
from django.db.models import Q, Count
import itertools
import math
import logging

from archilance import util
from projects.models import Order
from reviews.models import Review
from users.helpers import get_projects_grouped

logger = logging.getLogger(__name__)

register = template.Library()


@register.inclusion_tag('templatetags/contractor_indicator.html', takes_context=True)
def contractor_indicator(context, contractor):
    fields = ['avatar', 'first_name', 'gender',
              'last_name', 'patronym', 'phone',
              'skype', 'website', 'location',
              'contractor_specializations', 'contractor_building_classifications', 'contractor_construction_types',
              'contractor_resume__text']

    indicator_sum = 0
    for f in fields:
        if hasattr(contractor, f) and getattr(contractor, f):
            try:
                if not getattr(contractor, f).all():
                    continue
            except AttributeError:
                pass
            indicator_sum += 1

    # Если в резюме > 100 символов - считаем его заполненным
    try:
        if len(getattr(contractor, 'contractor_resume').text) > 100:
            indicator_sum += 1
    except Exception as e:
        logger.error(e.__str__())

    current_indicator = math.ceil(math.ceil(100 / len(fields) * indicator_sum))
    current_indicator_px = math.ceil(current_indicator / 2)
    return {
        'current_indicator': current_indicator,
        'current_indicator_px': current_indicator_px,
    }


@register.filter('has_group')
def has_group(user, group_name):
    groups = user.groups.all().values_list('name', flat=True)
    return True if group_name in groups else False


@register.inclusion_tag('templatetags/user_new_count.html', takes_context=True)
def count_new_message(context, user):
    from chat.models import Message, NewMessage
    new_count = NewMessage.objects.filter(user=user).count()
    return {
        'new_count': new_count,
    }


@register.inclusion_tag('templatetags/user_new_count_orders.html', takes_context=True)
def count_new_message_orders(context, user):
    """

    :param context:
    :param user:
    :return:
    """
    new_count = NewMessage.objects.filter(user=user, message__order__in=user.orders.filter(status='created'),
                                          message__team__isnull=True).count()
    try:
        team = user.team
    except:
        team = None

    if team:
        new_count_team = NewMessage.objects.filter(user=user,
                                                   message__order__in=user.team.orders.filter(status='created'),
                                                   message__team__isnull=True).count()
        new_count += new_count_team

    if new_count > 99:
        new_count = '99+'
    if new_count == 0:
        new_count = ''
    return {
        'new_count': new_count,
    }


@register.simple_tag
def get_new_count_message(team_pk, user=None):
    count = NewMessage.objects.filter(user=user, message__team=team_pk, message__order__isnull=True).count()
    return count


@register.simple_tag
def get_new_count_for_contact(contact, current_user):
    count = current_user.new_messages.filter(message__sender=contact, message__order__isnull=True,
                                             message__team__isnull=True).count()
    if count > 99:
        count = '99+'
    return count


@register.simple_tag
def get_new_count_for_order(current_user, order_id):
    count = current_user.new_messages.filter(message__order=order_id, message__team__isnull=True).count()
    return count


@register.simple_tag
def get_new_count_for_team(current_user, team_id, order_id=None):
    if order_id:
        count = current_user.new_messages.filter(message__order=order_id, message__team=team_id).count()
    else:
        count = current_user.new_messages.filter(message__order__isnull=True, message__team=team_id).count()
    return count


@register.simple_tag
def get_reviews_count(user):
    if user.is_customer():
        count = Review.objects.filter(target_customer=user).count()
    else:
        count = Review.objects.filter(target_contractor=user).count()
    return count


@register.simple_tag
def get_customer_chat_open_projects(user):
    team_ids = []
    if user.is_owner_team():
        team_ids.append(user.team.pk)
    count = Order.objects.filter(Q(contractor=user) | Q(team_id__in=team_ids)).count()
    return count


@register.filter
def get_project_message_count(user):
    if user.is_customer():
        projects = user.customer_projects.filter(state='active').exclude(order__contractor__isnull=True,
                                                                         order__team__isnull=True)
        return projects.aggregate(c=Count('answers__messages'))['c']
    elif user.is_contractor():
        message_count = \
        user.contractor_answers.filter(project__state='active', rejected=False).aggregate(c=Count('messages'))['c']

        if util.has_related(user, 'team'):
            message_count += \
            user.team.answers.filter(project__state='active', rejected=False).aggregate(c=Count('messages'))['c']

        return message_count


@register.simple_tag
def get_open_projects_grouped(contractor):
    private_open_projects = tuple(
        a.project for a in contractor.contractor_answers.filter(project__state='active', rejected=False))

    try:
        team_open_projects = tuple(
            a.project for a in contractor.team.answers.filter(project__state='active', rejected=False))
    except:
        team_open_projects = ()

    open_project_projects = len(private_open_projects) + len(team_open_projects)

    return open_project_projects
