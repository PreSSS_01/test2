from django.contrib import admin

from .models import (
    ContractorResume,
    ContractorResumeFiles,
    Team,
    TeamInvitation,
    User,
    UserFinancialInfo,
)


class UserAdmin(admin.ModelAdmin):
    readonly_fields = ('pk', 'is_staff')
    list_display = ('username', 'email', 'get_groups', 'cro', 'is_active', 'rating', 'last_time_visit')
    ordering = ('-rating',)

    def get_groups(self, obj):
        return ', '.join(g.name for g in obj.groups.all())


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'rating', 'owner',)
    ordering = ('-rating',)


admin.site.register(ContractorResume)
admin.site.register(ContractorResumeFiles)
admin.site.register(Team, TeamAdmin)
admin.site.register(TeamInvitation)
admin.site.register(User, UserAdmin)
admin.site.register(UserFinancialInfo)
