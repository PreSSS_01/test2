from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import redirect, render_to_response
from django.template.loader import get_template, render_to_string
from social.pipeline.partial import partial

from users.models import ContractorResume


def send_user_mail(user):
    ctx_dict = {
        'user': user,
    }
    subject, from_email, to = 'Регистрация черз социальные сети', 'mukhtar@mukhtar', user.email
    text_content = render_to_string('register_social_mail.txt', ctx_dict)
    html_content = get_template('register_social_mail.html').render(ctx_dict)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


@partial
def success_social_register(backend, details, response, user, is_new=False, *args, **kwargs):
    if is_new:
        group_name = backend.strategy.session_get('user_type')
        # group_name = 'Исполнители'
        g = Group.objects.get(name=group_name)
        g.user_set.add(user)
        if group_name == 'Исполнители':
            resume = ContractorResume.objects.create(text='Здесь должна быть описание вашего резюме')
            user.contractor_resume = resume
            user.save()
        # Отправка письма на почту
        send_user_mail(user)


@partial
def add_email_for_user(backend, details, response, is_new=False, *args, **kwargs):
    data = backend.strategy.request_data()
    if is_new:
        if not details.get('email'):
            if 'email' in data:
                return {'email': data.get('email')}
            else:
                return render_to_response('add_email_form.html')


@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if kwargs.get('ajax') or user and user.email:
        return
    elif is_new and not details.get('email'):
        email = strategy.request_data().get('email')
        if email:
            details['email'] = email
        else:
            return redirect('require_email')
