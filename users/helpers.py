def get_projects_grouped(contractor):
    private_open_projects = tuple(
        a.project for a in contractor.contractor_answers.filter(project__state='active', rejected=False))

    try:
        team_open_projects = tuple(
            a.project for a in contractor.team.answers.filter(project__state='active', rejected=False))
    except:
        team_open_projects = ()

    private_archived_projects = tuple(
        a.project for a in contractor.contractor_answers.filter(project__state='active', rejected=True))

    try:
        team_archived_projects = tuple(
            a.project for a in contractor.team.answers.filter(project__state='active', rejected=True))
    except:
        team_archived_projects = ()

    return {
        'private_open_projects': private_open_projects,
        'team_open_projects': team_open_projects,
        'private_archived_projects': private_archived_projects,
        'team_archived_projects': team_archived_projects,
    }
