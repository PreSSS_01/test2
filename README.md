### Установка

Создаем виртуальное окружение virtualenv:

```
cd path/to/venv/folder && \
virtualenv  --no-site-packages -p python3.5 proekton && \
source bin/activate && \
Переходим в корневую папку проекта и выполняем
pip install --requirement requirements/requirements.txt
```

```
python manage.py migrate
```

Включаем  solr-thumbnail:

```
python manage.py makemigrations thumbnail
python manage.py migrate
```

Команда для пересчета рейтинга:

```
python manage.py recalculation_spec
```

```
python manage.py runserver
```

```
python manage.py shell_plus --use-pythonrc
```

----------------------------------------

Генерируем данные на сайте если необходимо (Опционально):

```
python manage.py generate_* commands
```


Data generation order:

1. Superuser
2. Specializations
3. Locations
4. Perm. groups
5. Build. classif-s
6. Constr. types
7. Users
8. Teams
9. Realties
10. Projects
11. Portfolios
12. Reviews

or here is right commands in one str:

```
python manage.py generate_superuser && \
python manage.py generate_specializations && \
python manage.py generate_locations && \
python manage.py generate_perm_groups && \
python manage.py generate_build_classifs && \
python manage.py generate_constr_types && \
python manage.py generate_users && \
python manage.py generate_teams && \
python manage.py generate_realties && \
python manage.py generate_projects && \
python manage.py generate_portfolios && \
python manage.py generate_work_sells && \
python manage.py generate_reviews
```

----------------------------------------

# Tooltip examples

```
<div data-tooltip title="{% tooltip pk=123 %}"></div>
```

```
<div
    data-tooltip
    data-placement="bottom"
    title="{% tooltip name='Безопасная сделка' %}">
</div>
```

```
<div
    data-tooltip
    data-placement="{% tooltip_placement pk=123 %}"
    title="{% tooltip pk=123 %}">
</div>
```

----------------------------------------

## Local smtp debug server
```
sudo python3 -m smtpd -n -c DebuggingServer localhost:25
```

## Local char server
```
python3 -m tornado.autoreload chat/chat.py
```
