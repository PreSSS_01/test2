from django.conf import urls

from .views import (
    PrintServiceView,
    ApiListPaperTypes,
    ApiProducts,
    ApiExtraServices,
    UploadFile,
    RemoveUploadedFile,
    ApiTypesDetailed,
    SaveOrder,
    OrderHistory,
    RemoveOrder,
)


urlpatterns = [
    urls.url(r'^$', PrintServiceView.as_view(), name='print-service-form'),
    urls.url(r'^types$', ApiListPaperTypes.as_view({'get': 'list'})),
    urls.url(r'^products$', ApiProducts.as_view({'get': 'list'})),
    urls.url(r'^services$', ApiExtraServices.as_view({'get': 'list'})),
    urls.url(r'^upload/$', UploadFile.as_view()),
    urls.url(r'^remove/$', RemoveUploadedFile.as_view()),
    urls.url(r'^types-all/$', ApiTypesDetailed.as_view()),
    urls.url(r'^save/$', SaveOrder.as_view()),
    urls.url(r'^history/$', OrderHistory.as_view()),
    urls.url(r'^delete/$', RemoveOrder.as_view()),
]
