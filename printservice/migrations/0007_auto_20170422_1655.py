# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-04-22 13:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('printservice', '0006_auto_20170422_1654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderfile',
            name='order_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='printservice.Order'),
        ),
    ]
