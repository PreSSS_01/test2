from django.apps import AppConfig


class PrintserviceConfig(AppConfig):
    name = 'printservice'
