from rest_framework.serializers import ModelSerializer
from .models import PaperType, Products, ExtraService


class PaperTypeSerializer(ModelSerializer):
    class Meta:
        model = PaperType
        fields = (
            'id',
            'name'
        )


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Products
        fields = (
            'id',
            'format',
            'black_white_1',
            'black_white_2',
            'color_1',
            'color_2',
        )


class ExtraserviceSerializer(ModelSerializer):
    class Meta:
        model = ExtraService
        fields = (
            'id',
            'name',
            'price',
        )
