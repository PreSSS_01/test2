import uuid

from django.db import models


DELIVERY_TYPES = (
    ('PICKUP', 'Самовывоз'),
    ('DELIVERY', 'Доставка'),
)


class Order(models.Model):
    status_id = models.ForeignKey('printservice.OrderStatus', verbose_name='Статус заказа', null=True,
                                  on_delete=models.SET_NULL)
    user_id = models.ForeignKey('users.User', verbose_name='Пользователь', blank=True, null=True, default=None)
    paper_type = models.ForeignKey('printservice.PaperType', verbose_name='Тип бумаги')
    comment = models.CharField(max_length=500, blank=True)
    delivery_type = models.CharField('Способ доставки', max_length=8, choices=DELIVERY_TYPES)
    client_name = models.CharField('Получатель', max_length=50)
    phone = models.CharField(max_length=30)
    email = models.EmailField(max_length=35)
    address = models.CharField(max_length=90, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return self.client_name if self.client_name else '#{}'.format(self.pk)


class PaperType(models.Model):
    name = models.CharField('Название бумаги', max_length=100)
    sort = models.PositiveIntegerField('Порядок сортировки')
    is_active = models.BooleanField('Опубликовано', default=True)

    class Meta:
        verbose_name = 'Тип бумаги'
        verbose_name_plural = 'Типы бумаги'

    def __str__(self):
        return self.name if self.name else '#{}'.format(self.pk)


class Products(models.Model):
    paper_type = models.ForeignKey('printservice.PaperType', verbose_name='Формат бумаги', null=True,
                                   on_delete=models.CASCADE)
    format = models.CharField('Формат', max_length=2)
    black_white_1 = models.PositiveIntegerField('Черно-белая печать, с одной стороны',
                                                blank=True, null=True, default=None)
    black_white_2 = models.PositiveIntegerField('Черно-белая печать, с двух сторон',
                                                blank=True, null=True, default=None)
    color_1 = models.PositiveIntegerField('Цветная печать, с одной стороны',
                                          blank=True, null=True, default=None)
    color_2 = models.PositiveIntegerField('Цветная печать, с двух сторон',
                                          blank=True, null=True, default=None)
    is_active = models.BooleanField('Опубликовано', default=True)
    sort = models.PositiveIntegerField('Порядок сортировки', default=0)

    class Meta:
        unique_together = ('paper_type', 'format')
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.format if self.format else '#{}'.format(self.pk)


class OrderedItem(models.Model):
    order_id = models.ForeignKey('printservice.Order', null=True, on_delete=models.CASCADE)
    paper_id = models.CharField('Тип бумаги', max_length=100)
    format = models.CharField('Формат', max_length=2, blank=True, null=True)
    black_white_1_price = models.PositiveIntegerField('Черно-белая печать, с одной стороны, цена',
                                                blank=True, null=True, default=None)
    black_white_1_count = models.PositiveIntegerField('Черно-белая печать, с одной стороны, к-во',
                                                      blank=True, null=True, default=None)
    black_white_2_price = models.PositiveIntegerField('Черно-белая печать, с двух сторон, цена',
                                                      blank=True, null=True, default=None)
    black_white_2_count = models.PositiveIntegerField('Черно-белая печать, с двух сторон, к-во',
                                                      blank=True, null=True, default=None)
    color_1_price = models.PositiveIntegerField('Цветная печать, с одной стороны, цена',
                                                blank=True, null=True, default=None)
    color_1_count = models.PositiveIntegerField('Цветная печать, с одной стороны, к-во',
                                                blank=True, null=True, default=None)
    color_2_price = models.PositiveIntegerField('Цветная печать, с двух сторон, цена',
                                                blank=True, null=True, default=None)
    color_2_count = models.PositiveIntegerField('Цветная печать, с двух сторон, к-во',
                                                blank=True, null=True, default=None)

    class Meta:
        verbose_name = 'Позиция заказа'
        verbose_name_plural = 'Позиции заказа'

    def __str__(self):
        return self.format if self.format else '#{}'.format(self.pk)


class OrderService(models.Model):
    order_id = models.ForeignKey('printservice.Order', null=True, on_delete=models.CASCADE)
    name = models.CharField('Дополнительная услуга в заказе', max_length=100)
    price = models.PositiveIntegerField('Цена')


class ExtraService(models.Model):
    name = models.CharField('Дополнительная услуга', max_length=100)
    price = models.PositiveIntegerField('Цена')
    is_active = models.BooleanField('Активно', default=True)

    class Meta:
        verbose_name = 'Дополнительная услуга'
        verbose_name_plural = 'Дополнительные услуги'

    def __str__(self):
        return self.name if self.name else '#{}'.format(self.pk)


class OrderStatus(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Статус заказа'
        verbose_name_plural = 'Статусы заказов'

    def __str__(self):
        return self.name if self.name else '#{}'.format(self.pk)


def upload_to(instance, filename):
    parts = filename.split('.')
    file = '{}.{}'.format(uuid.uuid4(), parts[-1].lower()) if len(parts) > 1 else uuid.uuid4()
    return 'print-service/{}'.format(file)


class OrderFile(models.Model):
    file_code = models.CharField(max_length=36, unique=True)
    file_name = models.CharField(max_length=200, blank=True)
    order_id = models.ForeignKey('printservice.Order', blank=True, null=True, on_delete=models.CASCADE)
    file = models.FileField(upload_to=upload_to)
    ext = models.CharField('Расширение', max_length=8)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.file_code = uuid.uuid4()
        super(OrderFile, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'

    def __str__(self):
        return self.file_name if self.file_name else '#{}'.format(self.pk)
