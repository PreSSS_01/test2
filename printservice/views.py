import uuid

import pydash as _

from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.viewsets import ModelViewSet

from archilance.mixins import BaseMixin

from .models import PaperType, Products, ExtraService, OrderFile, Order, OrderedItem, OrderService, OrderStatus
from .forms import OrderFileForm, OrderForm
from .serializers import PaperTypeSerializer, ProductSerializer, ExtraserviceSerializer


_.map = _.map_
_.filter = _.filter_


class PrintServiceView(BaseMixin, View):

    template_name = 'print_service_form.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**_.merge({}, request.GET, kwargs))

        uploaded_files = []
        session_files = request.session.get('print-files')

        if session_files:
            uploaded_files = OrderFile.objects.filter(file_code__in=session_files)

        context.update({
            'uploaded_files': uploaded_files,
            'services': ExtraService.objects.filter(is_active=True)
        })

        return render(request, self.template_name, context)


class ApiListPaperTypes(ModelViewSet):
    queryset = PaperType.objects.filter(is_active=True)
    serializer_class = PaperTypeSerializer


class ApiProducts(ModelViewSet):
    queryset = Products.objects.filter(is_active=True)
    serializer_class = ProductSerializer

    def get_queryset(self):
        queryset = Products.objects.filter(is_active=True).order_by('sort').all()
        try:
            paper = int(self.request.query_params.get('paper'))
        except ValueError:
            paper = None
        if paper:
            queryset = queryset.filter(paper_type=paper)
        return queryset


class ApiExtraServices(ModelViewSet):
    queryset = ExtraService.objects.filter(is_active=True).all()
    serializer_class = ExtraserviceSerializer


class UploadFile(BaseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UploadFile, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = OrderFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES.get('file')
            ext = None
            if file:
                ext = file.name.split('.')[-1].lower()
            order_file = OrderFile(
                file_code=uuid.uuid4(),
                file_name=file.name,
                file=request.FILES.get('file'),
                ext=ext,
            )
            order_file.save()

            session_files = request.session.get('print-files', [])
            session_files.append(str(order_file.file_code))
            request.session['print-files'] = session_files

            return JsonResponse({
                'file': str(order_file.file_code),
                'name': order_file.file_name[:7],
                'ext': order_file.ext,
            })
        else:
            return JsonResponse({
                'file': '',
                'errors': {key: value[0] for key, value in form.errors.items()},
            })


class RemoveUploadedFile(BaseMixin, View):

    def get(self, request, *args, **kwargs):
        id = request.GET.get('id')
        if id:
            try:
                file = OrderFile.objects.get(file_code=id)
                file.delete()
            except:
                pass
        return JsonResponse({
            'ok': True
        })


class ApiTypesDetailed(BaseMixin, View):

    def get(self, request):
        response = []
        paper_types = PaperType.objects.filter(is_active=True).order_by('sort')
        for type in paper_types:
            products = Products.objects.filter(is_active=True).filter(paper_type=type).order_by('sort')
            products_elements = []
            for p in products:
                products_elements.append({
                    'id': p.id,
                    'format': p.format,
                    'black_white_1': p.black_white_1,
                    'black_white_2': p.black_white_2,
                    'color_1': p.color_1,
                    'color_2': p.color_2,
                })
            response.append({
                'id': type.id,
                'name': type.name,
                'products': products_elements,
            })
        return JsonResponse(response, safe=False)


class SaveOrder(BaseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SaveOrder, self).dispatch(request, *args, **kwargs)

    def post(self, request):

        form = OrderForm(request.POST)

        if not form.is_valid():
            return JsonResponse({
                'error': [dict([('field', key), ('message', value[0])]) for key, value in form.errors.items()]
            })

        """
        сохранение заказа
        """

        order = Order(
            user_id=request.user if request.user.is_authenticated() else None,
            paper_type=form.cleaned_data['paper_type'],
            comment=request.POST.get('comment', ''),
            delivery_type=form.cleaned_data['delivery_type'],
            client_name=form.cleaned_data['client_name'],
            phone=form.cleaned_data['phone'],
            email=form.cleaned_data['email'],
            address=request.POST.get('address', ''),
        )

        status = OrderStatus.objects.order_by('id').first()

        if status:
            order.status_id = status

        order.save()

        """
        сохранить отправленный заказ в сессии,
        чтобы незарегистрированные пользователи
        могли видеть историю своих заказов
        """

        history_ids = request.session.get('orders_history_ids', [])
        history_ids.append(order.id)
        request.session['orders_history_ids'] = history_ids

        """
        сохранение позиций
        """

        paper_type = form.cleaned_data['paper_type']

        products = Products.objects.filter(is_active=True, paper_type_id=form.cleaned_data['paper_type'])

        for product in products:
            try:
                count_white_1 = int(request.POST.get('white_1[{}]'.format(product.id), 0))
            except ValueError:
                count_white_1 = 0

            try:
                count_white_2 = int(request.POST.get('white_2[{}]'.format(product.id), 0))
            except ValueError:
                count_white_2 = 0

            try:
                count_color_1 = int(request.POST.get('color_1[{}]'.format(product.id), 0))
            except ValueError:
                count_color_1 = 0

            try:
                count_color_2 = int(request.POST.get('color_2[{}]'.format(product.id), 0))
            except ValueError:
                count_color_2 = 0

            order_product = OrderedItem(
                order_id=order,
                paper_id=paper_type.name,
                format=product.format,
                black_white_1_price=product.black_white_1,
                black_white_1_count=count_white_1,
                black_white_2_price=product.black_white_2,
                black_white_2_count=count_white_2,
                color_1_price=product.color_1,
                color_1_count=count_color_1,
                color_2_price=product.color_2,
                color_2_count=count_color_2,
            )
            order_product.save()

        """
        сохранение дополнительных услуг
        """

        for service_id in request.POST.get('services[]', []):
            try:
                service = ExtraService.objects.get(pk=service_id)
            except:
                continue
            order_service = OrderService(
                order_id=order,
                name=service.name,
                price=service.price,
            )
            order_service.save()

        """
        прикрепить файлы к сессии
        """

        session_files = request.session.get('print-files')

        if session_files:
            uploaded_files = OrderFile.objects.filter(file_code__in=session_files)

            for file in uploaded_files:
                file.order_id = order
                file.save()

            request.session['print-files'] = []

        return JsonResponse({
            'error': None,
        })


class OrderHistory(BaseMixin, View):

    def get(self, request):
        response = []

        history_ids = request.session.get('orders_history_ids', [])

        if not history_ids and not request.user.is_authenticated():
            return JsonResponse(response, safe=False)

        queryset = Order.objects

        if request.user.is_authenticated():
            queryset = queryset.filter(user_id=request.user)
        else:
            if history_ids:
                queryset = queryset.filter(id__in=history_ids)

        orders = queryset.order_by('-pk')
        for order in orders:
            files = OrderFile.objects.filter(order_id=order)
            response.append({
                'id': order.id,
                'date': order.created_at.strftime("%d.%m.%Y %H:%M") if order.created_at else '',
                'client_name': order.client_name,
                'phone': order.phone,
                'email': order.email,
                'address': order.address,
                'status': order.status_id.name if order.status_id else '',
                'files': [{
                    'ext': file.ext,
                    'name': file.file_name[:8] if file.file_name else '',
                } for file in files]
            })
        return JsonResponse(response, safe=False)


class RemoveOrder(BaseMixin, View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RemoveOrder, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        try:
            id = int(request.POST.get('id'))
        except ValueError:
            id = None
        history_ids = request.session.get('orders_history_ids', [])

        if id and id in history_ids:
            try:
                order = Order.objects.filter(pk=id).get()
                order.delete()
            except Exception as e:
                return JsonResponse({'status': True, 'e': e.__str__()})

        if not request.user.is_authenticated() or not id:
            return JsonResponse({'status': True})

        try:
            order = Order.objects.filter(pk=id).filter(user_id=request.user).get()
            order.delete()
        except Exception as e:
            return JsonResponse({'status': True, 'e': e.__str__()})
        return JsonResponse({'status': True})
