from django import forms
from django.core.exceptions import ValidationError
from .models import OrderFile, Order


class OrderFileForm(forms.ModelForm):

    VALID_EXTENSIONS = [
        'doc', 'docx', 'psd', 'dwg', 'gif', 'pln', 'png', 'tiff', 'txt', 'xls', 'xlsx', 'bmp',
        'pdf', 'jpg'
    ]

    class Meta:
        model = OrderFile
        fields = ('file',)

    def clean_file(self):
        file = self.cleaned_data.get('file')
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in self.VALID_EXTENSIONS:
                raise ValidationError('Вы пытаетесь загрузить запрещенный тип файла')
            else:
                return file


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('paper_type', 'delivery_type', 'client_name', 'phone', 'email', 'address')

    def clean_address(self):
        if self.cleaned_data.get('delivery_type') == 'DELIVERY' and not self.cleaned_data.get('address'):
            raise ValidationError('Адрес доставки обязателен для заполнения, когда выбрана доставка')
        else:
            return self.cleaned_data.get('address')
