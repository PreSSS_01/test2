from django.contrib import admin
from .models import Order, PaperType, Products, OrderedItem, OrderService, ExtraService, OrderStatus, OrderFile


class ProductAdmin(admin.ModelAdmin):
    list_display = ('format', 'paper_type', 'is_active')


class ExtraServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'is_active')


class OrderItemInline(admin.TabularInline):
    model = OrderedItem
    extra = 0


class ExtraServiceInline(admin.TabularInline):
    model = OrderService
    extra = 0


class OrderFileInline(admin.StackedInline):
    model = OrderFile
    extra = 0


class OrderAdmin(admin.ModelAdmin):
    inlines = (OrderItemInline, ExtraServiceInline, OrderFileInline)


admin.site.register(PaperType)
admin.site.register(Products, ProductAdmin)
admin.site.register(ExtraService, ExtraServiceAdmin)
admin.site.register(OrderStatus)
admin.site.register(Order, OrderAdmin)
